# Add "AS build" for later use
FROM node:18-alpine AS build

# This is exactly what you had before
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build:prod

# Now build the actual image, starting over.
FROM nginx:alpine
COPY --from=build /app/dist/out-tsc /usr/share/nginx/html
COPY /nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80