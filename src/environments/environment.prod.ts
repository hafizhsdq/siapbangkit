/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  baseUrl: "http://siapbangkit2.sidoarjokab.go.id/api",
  storageUrl: "http://siapbangkit2.sidoarjokab.go.id/api",
  simpegUrl: "http://simpeg2.sidoarjokab.go.id/index.php?r=apiPublic/pegawai",
  simpegAppToken: "zpGAXLqSvK6mSFhOOtOxzXttyXQ0AOYY"
};
