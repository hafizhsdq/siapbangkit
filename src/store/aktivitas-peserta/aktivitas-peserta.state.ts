import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { AktivitasPesertaService } from '../../app/shared/services/aktivitas-peserta.service';
import { ManajemenUserService } from '../../app/shared/services/manajemen-user.service';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { AktivitasPesertas } from './aktivitas-peserta';
import { CreateUpdateData, DeleteData, GetData } from './aktivitas-peserta.action';


@State<AktivitasPesertas.State>({
    name: 'aktivitaspeserta',
    defaults: { response: {} } as AktivitasPesertas.State
})
@Injectable()
export class AktivitasPesertaState {
    @Selector()
    static getAktivitasPesertas(state: AktivitasPesertas.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAAktivitasPesertas(state: AktivitasPesertas.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: AktivitasPesertas.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: AktivitasPesertas.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private aktivitaspesertaservice: AktivitasPesertaService) {}
    
    @Action(GetData)
    get(ctx: StateContext<AktivitasPesertas.State>, action: GetData) {        
        if (action.id) {
            return this.aktivitaspesertaservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.aktivitaspesertaservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<AktivitasPesertas.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.aktivitaspesertaservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.aktivitaspesertaservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<AktivitasPesertas.State>, action: DeleteData) {
        return this.aktivitaspesertaservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}