import { GetPage } from "../base-model";
import { AktivitasPesertas } from "./aktivitas-peserta";

export class CreateUpdateData {
    static readonly type = '[AktivitasPeserta] create update';
    constructor(public payload: AktivitasPesertas.CreateUpdateAktivitasPesertaInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[AktivitasPeserta] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[AktivitasPeserta] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[AktivitasPeserta] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }