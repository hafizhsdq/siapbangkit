import { BaseModel, BaseResponse } from "../base-model";

export namespace AktivitasPesertas {
  export interface State {
    response: BaseResponse<AktivitasPeserta>;
  }

    export interface CreateUpdateAktivitasPesertaInput {
      aktivitas: string;
      deskripsi: string;
      bukti: string;
      tglMulai: number;
      tglSelesai: number;
  }
  
    export interface AktivitasPeserta extends BaseModel {
      aktivitas: string;
      deskripsi: string;
      tglMulai: number;
      tglSelesai: number;
    }
  
    // export enum AktivitasPesertaType {
      
    // }
}