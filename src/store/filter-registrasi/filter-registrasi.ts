import { BaseModel } from "../base-model";

export namespace FilterRegistrasis {
    export interface State {
      filterRegistrasi: Response;
    }
  
    export interface Response {
      items: FilterRegistrasi[];
      totalCount: number;
    }

    export interface CreateUpdateFilterRegistrasiInput {
      kelompokCek: boolean;
      kelompok: string;
      entitasCek: boolean;
      entitas: string;
      tahunCek: boolean;
      tahun: number;
    }
  
    export interface FilterRegistrasi extends BaseModel {
        kelompok: string;
        entitas: string;
        tahun: number;
    }
  
    export enum FilterRegistrasiType {
      kelompok,
      entitas,
      tahun
    }
}