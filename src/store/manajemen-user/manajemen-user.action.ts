import { GetPage } from "../base-model";
import { ManajemenUsers } from "./manajemen-user";

export class CreateUpdateData {
    static readonly type = '[ManajemenUser] create update';
    constructor(public payload: ManajemenUsers.CreateUpdateManajemenUserInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[ManajemenUser] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[ManajemenUser] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[ManajemenUser] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }