import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { ManajemenUserService } from '../../app/shared/services/manajemen-user.service';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { ManajemenUsers } from './manajemen-user';
import { CreateUpdateData, DeleteData, GetData } from './manajemen-user.action';


@State<ManajemenUsers.State>({
    name: 'manajemenuser',
    defaults: { response: {} } as ManajemenUsers.State
})
@Injectable()
export class ManajemenUserState {
    @Selector()
    static getManajemenUsers(state: ManajemenUsers.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAManajemenUsers(state: ManajemenUsers.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: ManajemenUsers.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: ManajemenUsers.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private manajemenuserservice: ManajemenUserService) {}
    
    @Action(GetData)
    get(ctx: StateContext<ManajemenUsers.State>, action: GetData) {        
        if (action.id) {
            return this.manajemenuserservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.manajemenuserservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<ManajemenUsers.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.manajemenuserservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.manajemenuserservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<ManajemenUsers.State>, action: DeleteData) {
        return this.manajemenuserservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}