import { BaseModel, BaseResponse } from "../base-model";

export namespace ManajemenUsers {
  export interface State {
    response: BaseResponse<ManajemenUser>;
  }

    export interface CreateUpdateManajemenUserInput {
        username: string;
        email: string;
        role: string;
        status: string;

        password: string;
        konfirmasiPass: string;
    }
  
    export interface ManajemenUser extends BaseModel {
        username: string;
        nip: number;
        role: string;
        status: string;
    }
  
    // export enum ManajemenUserType {
    //     undefined,
    //     internal,
    //     eksternal,
    //     bkd
    // }
}