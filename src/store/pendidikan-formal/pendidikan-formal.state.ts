import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { DiklatService } from '../../app/shared/services/diklat.service';
import { KalenderDiklatService } from '../../app/shared/services/kalender-diklat.service';
import { PendidikanFormalService } from '../../app/shared/services/pendidikan-formal.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { PendidikanFormals } from './pendidikan-formal';
import { CreateUpdateData, DeleteData, GetData } from './pendidikan-formal.action';


@State<PendidikanFormals.State>({
    name: 'formal',
    defaults: { response: {} } as PendidikanFormals.State
})
@Injectable()
export class PendidikanFormalState {
    @Selector()
    static getPendidikanFormals(state: PendidikanFormals.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAPendidikanFormals(state: PendidikanFormals.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: PendidikanFormals.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: PendidikanFormals.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private pendidikanformalservice: PendidikanFormalService) {}
    
    @Action(GetData)
    get(ctx: StateContext<PendidikanFormals.State>, action: GetData) {        
        if (action.id) {
            return this.pendidikanformalservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.pendidikanformalservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<PendidikanFormals.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.pendidikanformalservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.pendidikanformalservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<PendidikanFormals.State>, action: DeleteData) {
        return this.pendidikanformalservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}