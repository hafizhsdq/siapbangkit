import { BaseModel, BaseResponse } from "../base-model";

export namespace PendidikanFormals {
  export interface State {
    response: BaseResponse<PendidikanFormal>;
  }

  export interface CreateUpdatePendidikanFormalInput {
    nip: number;
    grade: string;
    school_name: string;
    department: string;
    location: string;
    graduate_at: number;
    other_info: string;
    izinBelajar: string;
    permit_study_no: number;
    permit_study_at: number;
    permit_study_letter: string;
    ijazah: string;
    transcript: string;
    izin_belajar: string;

  }
  
  export interface PendidikanFormal extends BaseModel {
    nip: number;
    grade: string;
    school_name: string;
    department: string;
    location: string;
    graduate_at: number;
    other_info: string;
  }

  // export enum PendidikanFormalType {
  //     online,
  //     offline
  //   }
}