import { GetPage } from "../base-model";
import { PendidikanFormals } from "./pendidikan-formal";

export class CreateUpdateData {
    static readonly type = '[PendidikanFormal] create update';
    constructor(public payload: PendidikanFormals.CreateUpdatePendidikanFormalInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[PendidikanFormal] delete';
    constructor(public id: string, public payload?: GetPage) {}
}

export class GetData {
    static readonly type = '[PendidikanFormal] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[PendidikanFormal] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }