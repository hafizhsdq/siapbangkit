import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionnaireService } from '../../app/shared/services/questionnaire.service';
import { ModelHelper } from '../base-model';
import { Questionnaires } from './questionnaire';
import { GetData, CreateUpdateData, DeleteData } from './questionnaire.action';


@State<Questionnaires.State>({
    name: 'questionnaire',
    defaults: { response: {} } as Questionnaires.State
})
@Injectable()
export class QuestionnaireState {
    @Selector()
    static getQuestionnaires(state: Questionnaires.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAQuestionnaire(state: Questionnaires.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    constructor(private questionnaireservice: QuestionnaireService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Questionnaires.State>, action: GetData) {        
        if (action.id) {
            return this.questionnaireservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.questionnaireservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Questionnaires.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.questionnaireservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData({include: 'training'}))
                })
            )
        }
        return this.questionnaireservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData({include: 'training'}))
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Questionnaires.State>, action: DeleteData) {
        return this.questionnaireservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData(action.payload))
            })
        )
    }
}