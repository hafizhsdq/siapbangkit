import { BaseModel, BaseResponse } from "../base-model";

export namespace Questionnaires {
  export interface State {
    response: BaseResponse<Questionnaire>;
  }

  export interface CreateUpdateQuestionnaireInput {
    training_periode_id: string;
    url: string;
    email: string;
    platform: string;
  }
  
  export interface Questionnaire extends BaseModel {
    training_periode_id: string;
    url: string;
    email: string;
    platform:string;
  }

  export enum QuestionnaireType {
      organizer,
      participant
    }
}