import { GetPage } from "../base-model";
import { Questionnaires } from "./questionnaire";

export class CreateUpdateData {
    static readonly type = '[Questionnaire] create update';
    constructor(public payload: Questionnaires.CreateUpdateQuestionnaireInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Questionnaire] delete';
    constructor(public id: string, public payload: GetPage) {}
}

export class GetData {
    static readonly type = '[Questionnaire] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Questionnaire] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }