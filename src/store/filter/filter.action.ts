import { GetPage } from "../base-model";
import { Filters } from "./filter";

export class CreateUpdateData {
    static readonly type = '[Filter] create update';
    constructor(public payload: Filters.CreateUpdateFilterInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Filter] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Filter] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Filter] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }