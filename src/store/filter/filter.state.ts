import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { FilterService } from '../../app/shared/services/filter.service';
import { ManajemenUserService } from '../../app/shared/services/manajemen-user.service';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { Filters } from './filter';
import { CreateUpdateData, DeleteData, GetData } from './filter.action';


@State<Filters.State>({
    name: 'filter',
    defaults: { response: {} } as Filters.State
})
@Injectable()
export class FilterState {
    @Selector()
    static getFilters(state: Filters.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAFilters(state: Filters.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: Filters.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: Filters.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private filterservice: FilterService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Filters.State>, action: GetData) {        
        if (action.id) {
            return this.filterservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.filterservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Filters.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.filterservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.filterservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Filters.State>, action: DeleteData) {
        return this.filterservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}