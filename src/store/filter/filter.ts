import { BaseModel, BaseResponse } from "../base-model";

export namespace Filters {
  export interface State {
    response: BaseResponse<Filter>;
  }

    export interface CreateUpdateFilterInput {
        cari: string;
        opd: string;
        status: string;
    }
  
    export interface Filter extends BaseModel {
        cari: string;
        opd: string;
        status: string;
    }
  
    // export enum FilterType {
    // }
}