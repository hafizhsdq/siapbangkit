import { BaseModel, BaseResponse } from "../base-model";

export namespace KaryaTuliss {
  export interface State {
    response: BaseResponse<KaryaTulis>;
    loading: boolean
  }

  export interface CreateUpdateKaryaTulisInput {
    nip: number;
    title: string;
    publish_at: string;
    year: number;
    other_info: string;
  }
  
  export interface KaryaTulis extends BaseModel {
    nip: number;
    title: string;
    publish_at: string;
    year: number;
    other_info: string;
  }

  // export enum KaryaTulisType {
  //     online,
  //     offline
  //   }
}