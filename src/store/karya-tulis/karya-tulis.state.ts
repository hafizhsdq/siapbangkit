import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { DiklatService } from '../../app/shared/services/diklat.service';
import { KalenderDiklatService } from '../../app/shared/services/kalender-diklat.service';
import { KaryaTulisService } from '../../app/shared/services/karya-tulis.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { KaryaTuliss } from './karya-tulis';
import { CreateUpdateData, DeleteData, GetData } from './karya-tulis.action';


@State<KaryaTuliss.State>({
    name: 'papers',
    defaults: { response: {}, loading: true } as KaryaTuliss.State
})
@Injectable()
export class KaryaTulisState {
    @Selector()
    static getKaryaTuliss(state: KaryaTuliss.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getAKaryaTuliss(state: KaryaTuliss.State) {
    //     const data = ModelHelper.nestInclude(state.response)
    //     return {ModelHelper.nestInclude(state.response)}
    // }

    // @Selector()
    // static getKalenderPesertaDiklats(state: KaryaTuliss.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: KaryaTuliss.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private karyatulisservice: KaryaTulisService) {}
    
    @Action(GetData)
    get(ctx: StateContext<KaryaTuliss.State>, action: GetData) {       
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.karyatulisservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.karyatulisservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<KaryaTuliss.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.karyatulisservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.karyatulisservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<KaryaTuliss.State>, action: DeleteData) {
        return this.karyatulisservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData(action.payload))
            })
        )
    }
}