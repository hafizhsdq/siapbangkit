import { GetPage } from "../base-model";
import { KaryaTuliss } from "./karya-tulis";

export class CreateUpdateData {
    static readonly type = '[KaryaTulis] create update';
    constructor(public payload: KaryaTuliss.CreateUpdateKaryaTulisInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[KaryaTulis] delete';
    constructor(public id: string, public payload?: GetPage) {}
}

export class GetData {
    static readonly type = '[KaryaTulis] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[KaryaTulis] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }