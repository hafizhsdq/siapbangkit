import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { MENU_ITEMS } from '../../app/pages/pages-menu';
import { BiodataService } from '../../app/shared/services/biodata.service';
import { BaseResponse } from '../base-model';

export class GetMenu {
    static readonly type = '[Menu] Get menu';
    constructor(public payload: any) {}
}

export class MenuData {
    response: any
}

@State<MenuData>({
    name: 'menu',
    defaults: { response: {} } as MenuData
})
@Injectable()
export class MenuState {

    @Selector()
    static getRoles(state: MenuData) {        
        const roles = state.response.included.filter(i => i.type == 'roles')
        return roles
    }

    @Selector()
    static getMenu(state: MenuData) {
        const roles = state.response.roles

        // false = show; true = hidden
        const checkRoles = (roles, data) => {
            if (!data) return false
            return roles[0].attributes.permission.find(p => {                
                const permission = data.roles[p.name]                
                if (permission) {
                    return permission.map(perm => 
                        // negation the permission so it become false hidden 
                        // if the value true
                        !p.permission[perm]).reduce((p,c) => p |= c, false)
                }
            
                return false
            }) != null
        }

        return MENU_ITEMS.map(m => {
            const hidden = checkRoles(roles, m.data)
  
            if (m.children && m.children.length > 0) {
              m.children = m.children.map(mchild => {
                const hidden = checkRoles(roles, mchild.data)
                return {...mchild, hidden}
              })
            }
  
            return {...m, hidden}
        })
    }

    constructor(private biodataService: BiodataService) {}

    @Action(GetMenu)
    get(ctx: StateContext<MenuData>, action: GetMenu) {
        return this.biodataService.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }
}