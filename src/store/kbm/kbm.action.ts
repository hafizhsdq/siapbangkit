import { GetPage } from "../base-model";
import { Kbms } from "./kbm";

export class CreateUpdateData {
    static readonly type = '[KBM] create update';
    constructor(public payload: Kbms.CreateUpdateKbmInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[KBM] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[KBM] get';
    constructor(public payload?: GetPage, public id?: string) {}
}