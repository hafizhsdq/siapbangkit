import { BaseModel, BaseResponse } from "../base-model";

export namespace Kbms {
    export interface State {
      response: BaseResponse<Kbm>;
    }

    export interface CreateUpdateKbmInput {
      name: string,
      facilitator_activity: string,
      participant_activity: string,
      metode: string,
      tools: string,
      time_allocation: number,
      training_subject_id: number
  }
  
    export interface Kbm extends BaseModel {
      name: string,
      facilitator_activity: string,
      participant_activity: string,
      metode: string,
      tools: string,
      time_allocation: number;
      training_subject: any;
    }
  
    // export enum KbmType {
    //   undefined,
    //   internal,
    //   eksternal,
    //   bkd
    // }
}