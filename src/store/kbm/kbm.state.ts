import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { KbmService } from '../../app/shared/services/kbm.service';
import { ModelHelper } from '../base-model';
import { GetPhase } from '../mata-diklat/mata-diklat.action';
import { Kbms } from './kbm';
import { CreateUpdateData, DeleteData, GetData } from './kbm.action';

@State<Kbms.State>({
  name: 'kbm',
  defaults: { response: {} } as Kbms.State
})
@Injectable()
export class KbmState {

    @Selector()
    static getKbm(state: Kbms.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAKbm(state: Kbms.State) {
        return ModelHelper.nestInclude(state.response)[0] || {}
    }

    constructor(public kbmservice: KbmService) {}

    @Action(GetData)
    get(ctx: StateContext<Kbms.State>, action: GetData) {        
        if (action.id) {
            return this.kbmservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.kbmservice.get(action.payload)
            .pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Kbms.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.kbmservice.update(action.payload, action.id)
        }

        return this.kbmservice.create(action.payload)
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Kbms.State>, action: DeleteData) {
        return this.kbmservice.delete(action.id).pipe(
            switchMap(() => ctx.dispatch(new GetData()))
        )
    }
}