import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { ManajemenUserService } from '../../app/shared/services/manajemen-user.service';
import { PasswordService } from '../../app/shared/services/password.service';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { Passwords } from './password';
import { CreateUpdateData, DeleteData, GetData } from './password.action';


@State<Passwords.State>({
    name: 'password',
    defaults: { response: {} } as Passwords.State
})
@Injectable()
export class PasswordState {
    @Selector()
    static getPasswords(state: Passwords.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAPasswords(state: Passwords.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: Passwords.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: Passwords.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private passwordservice: PasswordService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Passwords.State>, action: GetData) {        
        if (action.id) {
            return this.passwordservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.passwordservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Passwords.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.passwordservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.passwordservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Passwords.State>, action: DeleteData) {
        return this.passwordservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}