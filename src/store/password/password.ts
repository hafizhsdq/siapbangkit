import { BaseModel, BaseResponse } from "../base-model";

export namespace Passwords {
  export interface State {
    response: BaseResponse<Password>;
  }

    export interface CreateUpdatePasswordInput {
      lama: string;
      baru: string;
      konfirmasi: string;
    }
  
    export interface Password extends BaseModel {
        lama: string;
        baru: string;
        konfirmasi: string;
    }
  
    // export enum PasswordType {
      
    // }
}