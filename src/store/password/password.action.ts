import { GetPage } from "../base-model";
import { Passwords } from "./password";

export class CreateUpdateData {
    static readonly type = '[Password] create update';
    constructor(public payload: Passwords.CreateUpdatePasswordInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Password] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Password] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Password] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }