import { GetPage } from "../base-model";
import { Users } from "./user";

export class CreateUpdateData {
    static readonly type = '[User] create update';
    constructor(public payload: Users.CreateUpdateUserInput, public id?: string | number) {}
}

export class DeleteData {
    static readonly type = '[User] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[User] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}