import { BaseModel, BaseResponse } from "../base-model";

export namespace Users {
    export interface State {
      response: BaseResponse<User>;
      loading: boolean
    }

    export interface CreateUpdateUserInput {
        nip: string;
        firstname: string;
        lastname: string;
        role_ids: number[];
        password: string;
        status: string;
        unit_organisasi: string;
        sub_unit_organisasi: string
    }
  
    export interface User extends BaseModel {
        nip: string;
        firstname: string;
        lastname: string;
        role: number[];
        password: string;
        status: string;
        unit_organisasi: string;
        sub_unit_organisasi: string
    }
}