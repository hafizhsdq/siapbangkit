import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { UserService } from '../../app/shared/services/user.service';
import { ModelHelper } from '../base-model';
import { Users } from './user';
import { CreateUpdateData, DeleteData, GetData } from './user.action';


@State<Users.State>({
    name: 'users',
    defaults: { response: {}, loading: true } as Users.State
})
@Injectable()
export class UserState {
    @Selector()
    static getUsers(state: Users.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getAUser(state: Users.State) {
    //     return {...state, response: ModelHelper.nestInclude(state.response)}
    // }

    constructor(private userservice: UserService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Users.State>, action: GetData) {        
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.userservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {                    
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.userservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Users.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.userservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.userservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Users.State>, action: DeleteData) {
        return this.userservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}