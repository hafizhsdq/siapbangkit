import { BaseModel } from "../base-model";

export namespace FilterDiklats {
    export interface State {
      filterDiklat: Response;
    }
  
    export interface Response {
      items: FilterDiklat[];
      totalCount: number;
    }

    export interface CreateUpdateFilterDiklatInput {
      kelompokCek: boolean;
      kelompok: string;
      diklatCek: boolean;
      diklat: string;
      tahunCek: boolean;
      tahun: number;
    }
  
    export interface FilterDiklat extends BaseModel {
        kelompok: string;
        diklat: string;
        tahun: number;
    }
  
    export enum FilterDiklatType {
      kelompok,
      diklat,
      tahun
    }
}