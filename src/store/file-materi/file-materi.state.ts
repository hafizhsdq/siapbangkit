import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { FileMateriService } from '../../app/shared/services/file-materi.service';
import { ModelHelper } from '../base-model';
import { GetFileMaterial, GetPhase } from '../mata-diklat/mata-diklat.action';
import { FileMateris } from './file-materi';
import { CreateUpdateData, DeleteData, GetData } from './file-materi.action';

@State<FileMateris.State>({
  name: 'filemateri',
  defaults: { response: {} } as FileMateris.State
})
@Injectable()
export class FileMateristate {

    @Selector()
    static getFiles(state: FileMateris.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    constructor(public filemateriservice: FileMateriService) {}

    @Action(GetData)
    get(ctx: StateContext<FileMateris.State>, action: GetData) {        
        if (action.id) {
            return this.filemateriservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.filemateriservice.get(action.payload)
            .pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<FileMateris.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.filemateriservice.update(action.payload, action.id)
                .pipe(
                    switchMap(() => ctx.dispatch(new GetData({params: `training_subject_id=${action.payload.training_subject_id}`})))
                )
        }

        return this.filemateriservice.create(action.payload).pipe(
            switchMap(() => ctx.dispatch(new GetData({params: `training_subject_id=${action.payload.training_subject_id}`})))
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<FileMateris.State>, action: DeleteData) {
        return this.filemateriservice.delete(action.id).pipe(
            switchMap(() => ctx.dispatch(new GetData(action.payload)))
        )
    }
}