import { BaseModel, BaseResponse } from "../base-model";

export namespace FileMateris {
    export interface State {
      response: BaseResponse<FileMateri>;
    }

    export interface CreateUpdateFileMateriInput {
      info: string;
      file: File;
      training_subject_id: string;
  }
  
    export interface FileMateri extends BaseModel {
      name: string;
      path: string;
      size: number;
      type: string;
      info: string;
    }
  
    // export enum FileMateriType {
      
    // }
}