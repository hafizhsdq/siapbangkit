import { GetPage } from "../base-model";
import { FileMateris } from "./file-materi";

export class CreateUpdateData {
    static readonly type = '[File Mater] create update';
    constructor(public payload: FileMateris.CreateUpdateFileMateriInput, public id?: string | number) {}
}

export class DeleteData {
    static readonly type = '[File Mater] delete';
    constructor(public id: string, public payload?: GetPage) {}
}

export class GetData {
    static readonly type = '[File Mater] get';
    constructor(public payload?: GetPage, public id?: string) {}
}