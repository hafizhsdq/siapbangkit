import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { SimpegService } from '../../app/shared/services/simpeg.service';
import { GetDataSimpeg, Simpeg } from './simpeg';

@State<Simpeg.State>({
    name: 'simpeg',
    defaults: { response: {} } as Simpeg.State
})
@Injectable()
export class SimpegState {
    @Selector()
    static getSimpeg(state: Simpeg.State) {        
        if (!state.response?.result) return {}

        const {response: {result: {
            id,
            nip,
            nama,
            agama,
            asal_pelimpahan,
            bahasa,
            domisili_alamat,
            domisili_kecamatan,
            domisili_kelurahan,
            domisili_kodepos,
            domisili_kota,
            domisili_provinsi,
            domisili_rt,
            domisili_rw,
            duk,
            email,
            email_sidoarjokab,
            eselon,
            foto,
            gelar_belakang,
            gelar_depan,
            general_checkup,
            handphone_1,
            handphone_2,
            hasil_general_checkup,
            jabatan,
            jabatan_fungsional,
            jenis_kelamin,
            jenis_pegawai,
            kedudukan_pegawai,
            keluarga,
            keterangan_general_checkup,
            ktp_alamat,
            ktp_kecamatan,
            ktp_kelurahan,
            ktp_kodepos,
            ktp_kota,
            ktp_provinsi,
            ktp_rt,
            ktp_rw,
            nama_panggilan,
            nik,
            nip_lama,
            nomor_askes,
            nomor_bpjs,
            nomor_karis_karsu,
            nomor_karpeg,
            nomor_kk,
            nomor_taspen,
            npwp,
            nuptk,
            pangkat,
            pendidikan,
            sesuai_ktp,
            status_kawin,
            status_pegawai,
            status_pelimpahan,
            tanggal_general_checkup,
            tanggal_kawin,
            tanggal_lahir,
            tanggal_pelimpahan,
            telepon,
            tempat_lahir,
            tugas_tambahan,
            whatsapp
        }}} = state
        
        return {id,
            nip,
            nama,
            agama,
            asal_pelimpahan,
            bahasa,
            domisili_alamat,
            domisili_kecamatan,
            domisili_kelurahan,
            domisili_kodepos,
            domisili_kota,
            domisili_provinsi,
            domisili_rt,
            domisili_rw,
            duk,
            email,
            email_sidoarjokab,
            eselon,
            foto,
            gelar_belakang,
            gelar_depan,
            general_checkup,
            handphone_1,
            handphone_2,
            hasil_general_checkup,
            jabatan,
            jabatan_fungsional,
            jenis_kelamin,
            jenis_pegawai,
            kedudukan_pegawai,
            keluarga,
            keterangan_general_checkup,
            ktp_alamat,
            ktp_kecamatan,
            ktp_kelurahan,
            ktp_kodepos,
            ktp_kota,
            ktp_provinsi,
            ktp_rt,
            ktp_rw,
            nama_panggilan,
            nik,
            nip_lama,
            nomor_askes,
            nomor_bpjs,
            nomor_karis_karsu,
            nomor_karpeg,
            nomor_kk,
            nomor_taspen,
            npwp,
            nuptk,
            pangkat,
            pendidikan,
            sesuai_ktp,
            status_kawin,
            status_pegawai,
            status_pelimpahan,
            tanggal_general_checkup,
            tanggal_kawin,
            tanggal_lahir,
            tanggal_pelimpahan,
            telepon,
            tempat_lahir,
            tugas_tambahan,
            whatsapp}
    }

    constructor(private simpegervice: SimpegService) {}
    
    @Action(GetDataSimpeg)
    get(ctx: StateContext<Simpeg.State>, action: GetDataSimpeg) {
        return this.simpegervice.get(action.nip).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }
}