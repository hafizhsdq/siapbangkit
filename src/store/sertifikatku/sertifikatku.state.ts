import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { Sertifikats } from './sertifikatku';
import { CreateUpdateData, DeleteData, GetData } from './sertifikatku.action';


@State<Sertifikats.State>({
    name: 'sertifikat',
    defaults: { response: {} } as Sertifikats.State
})
@Injectable()
export class SertifikatState {
    @Selector()
    static getSertifikats(state: Sertifikats.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getASertifikats(state: Sertifikats.State) {
    //     return {...state, response: ModelHelper.nestInclude(state.response)}
    // }

    // @Selector()
    // static getKalenderPesertaDiklats(state: Sertifikats.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: Sertifikats.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private sertifikatservice: SertifikatService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Sertifikats.State>, action: GetData) {        
        if (action.id) {
            return this.sertifikatservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.sertifikatservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Sertifikats.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.sertifikatservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.sertifikatservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Sertifikats.State>, action: DeleteData) {
        return this.sertifikatservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}