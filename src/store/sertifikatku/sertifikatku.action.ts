import { GetPage } from "../base-model";
import { Sertifikats } from "./sertifikatku";

export class CreateUpdateData {
    static readonly type = '[Sertifikat] create update';
    constructor(public payload: Sertifikats.CreateUpdateSertifikatInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Sertifikat] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Sertifikat] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Sertifikat] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }