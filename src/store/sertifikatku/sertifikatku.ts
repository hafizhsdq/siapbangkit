import { BaseModel, BaseResponse } from "../base-model";

export namespace Sertifikats {
  export interface State {
    response: BaseResponse<Sertifikat>;
  }

  export interface CreateUpdateSertifikatInput {
    jp: number;
    start_date: number;
    end_date: number;
    certificate_number: number;
    file_sertifikat: string;
  }
  
  export interface Sertifikat extends BaseModel {
    jp: number;
    start_date: number;
    end_date: number;
    certificate_number: number;
  }
//   export enum SertifikatType {
//       organizer,
//       participant
//     }
}