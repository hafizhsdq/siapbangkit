import { BaseModel, BaseResponse } from "../base-model";

export namespace Dokumens {
  export interface State {
    response: BaseResponse<Dokumen>;
  }

  export interface CreateUpdateDokumenInput {
    nip: number;
    cpns_letter: string;
    pns_letter: string;
    letter_of_promoted: string;
    letter_of_promoted2: string;
  }
  
  export interface Dokumen extends BaseModel {
    nip: number;
    cpns_letter: string;
    pns_letter: string;
    letter_of_promoted: string;
    letter_of_promoted2: string;
  }

  // export enum DokumenType {
  //     online,
  //     offline
  //   }
}