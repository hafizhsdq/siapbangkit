import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { DiklatService } from '../../app/shared/services/diklat.service';
import { DokumenService } from '../../app/shared/services/dokumen-persyaratan.service';
import { KalenderDiklatService } from '../../app/shared/services/kalender-diklat.service';
import { KaryaTulisService } from '../../app/shared/services/karya-tulis.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { Dokumens } from './dokumen-persyaratan';
import { CreateUpdateData, DeleteData, GetData } from './dokumen-persyaratan.action';


@State<Dokumens.State>({
    name: 'dokumen',
    defaults: { response: {} } as Dokumens.State
})
@Injectable()
export class DokumenState {
    @Selector()
    static getDokumens(state: Dokumens.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    // @Selector()
    // static getADokumens(state: Dokumens.State) {
    //     const data = ModelHelper.nestInclude(state.response)
    //     return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    // }

    // @Selector()
    // static getKalenderPesertaDiklats(state: Dokumens.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: Dokumens.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private dokumenservice: DokumenService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Dokumens.State>, action: GetData) {        
        if (action.id) {
            return this.dokumenservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.dokumenservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Dokumens.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.dokumenservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.dokumenservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Dokumens.State>, action: DeleteData) {
        return this.dokumenservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}