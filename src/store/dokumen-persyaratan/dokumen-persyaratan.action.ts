import { GetPage } from "../base-model";
import { Dokumens } from "./dokumen-persyaratan";

export class CreateUpdateData {
    static readonly type = '[Dokumen] create update';
    constructor(public payload: Dokumens.CreateUpdateDokumenInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Dokumen] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Dokumen] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Dokumen] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }