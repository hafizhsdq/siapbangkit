import { BaseModel, BaseResponse } from "../base-model";

export namespace Participants {
  export interface State {
    response: BaseResponse<Participant>;
    loading: boolean
  }

  export interface Participant extends BaseModel {
    register_at: string;
    status: string;
    employee: Employee;
  }

  export interface RegisterParticipant {
    nip: string[]; 
    skip: boolean;
    training_periode_id: string | number;
  }

  export interface ChangeStatusInput {
    status: string;
    info: string;
    file?: File;
  }
  
  export interface Employee extends BaseModel {
    name: string;
    nip: string;
    front_title: string;
    back_title: string;
    department: string;
    tmt_grade: string;
    tmt_echelon: string;
    tmt_pns: string;
    tmt_cpns: string;
    religion: string;
    last_education: string;
    email: string;
    birth_date: string;
    birth_place: string;
    ktp: string;
    npwp: string;
    bank_account: string;
    bank_account_name: string;
    bank_account_number: string;
    work_address: string;
    work_address_ward: string;
    work_address_district: string;
    work_address_city: string;
    work_address_province: string;
    work_phone: string;
    work_fax: string;
    home_address: string;
    home_address_ward: string;
    home_address_district: string;
    home_address_city: string;
    home_address_province: string;
    home_phone: string;
    home_mobile: string;
    home_zip_code: string;
    is_domicile_ktp: string;
    domicile_address: string;
    domicile_address_ward: string;
    domicile_address_district: string;
    domicile_address_city: string;
    domicile_address_province: string;
    domicile_phone: string;
    domicile_mobile: string;
    domicile_zip_code: string;
    gender: string;
    grade: string;
    position: string;
    position_type: string;
    user_id: string;
  }
}