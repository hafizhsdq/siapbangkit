import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { ModelHelper } from '../base-model';
import { DeleteData, GetData, Register } from './participant.action';
import { Participants } from './participant';
import { ParticipantService } from '../../app/shared/services/participant.service';


@State<Participants.State>({
    name: 'peserta',
    defaults: { response: {}, loading: true } as Participants.State
})
@Injectable()
export class ParticipantState {
    @Selector()
    static getParticipant(state: Participants.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    constructor(private participantService: ParticipantService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Participants.State>, action: GetData) {
        ctx.setState(state => ({...state, loading: true}))
        return this.participantService.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Participants.State>, action: DeleteData) {
        return this.participantService.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData(action.payload))
            })
        )
    }

    @Action(Register)
    add(ctx: StateContext<Participants.State>, action: Register) {
        return this.participantService.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData({include: 'employee,training_schedule', params: `training_periode_id=${action.payload.training_periode_id}`}))
            })
        )
    }
}