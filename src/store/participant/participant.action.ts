import { GetPage } from "../base-model";
import { Participants } from "./participant";

export class Register {
    static readonly type = '[Participant] register';
    constructor(public payload: Participants.RegisterParticipant) {}
}

export class DeleteData {
    static readonly type = '[Participant] delete';
    constructor(public id: string, public payload?: GetPage) {}
}

export class GetData {
    static readonly type = '[Participant] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}