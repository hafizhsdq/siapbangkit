import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { id } from '@swimlane/ngx-charts';
import { switchMap, tap } from 'rxjs/operators';
import { DiklatService } from '../../app/shared/services/diklat.service';
import { ModelHelper } from '../base-model';
import { Diklats } from './diklat';
import { CreateUpdateData, DeleteData, GetData, GetMataDiklatByDiklatId } from './diklat.action';

@State<Diklats.State>({
  name: 'diklat',
  defaults: { response: {}, loading: true } as Diklats.State
})
@Injectable()
export class DiklatState {

    @Selector()
    static getDiklat(state: Diklats.State) {
        return state
    }

    @Selector()
    static getADiklat(state: Diklats.State) {
        const data = ModelHelper.nestInclude(state.response).data[0]
 
        let latestPeriode = data.relationships?.training_schedule?.attributes?.periode
        // const {data} = state.response
        // return data.length > 0 ? data[0] : []
        
        return {...data, latestPeriode}
    }

    @Selector()
    static getPeriodes(state: Diklats.State) {
        const {response: {included}, loading} = state
        const periodes = included.filter(item => item.type == 'training_periode')
        return periodes.map(p => ({id: p.id, ...p.attributes})) || []
    }

    constructor(public diklatService: DiklatService) {}

    @Action(GetMataDiklatByDiklatId)
    getByDiklatId(ctx: StateContext<Diklats.State>, action: GetMataDiklatByDiklatId) {
        ctx.setState(state => ({...state, loading: true}))
        return this.diklatService.getById(action.id, {include: 'subject'}).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(GetData)
    get(ctx: StateContext<Diklats.State>, action: GetData) {  
        ctx.setState(state => ({...state, loading: true}))      
        if (action.id) {// action id = training/diklat id
            return this.diklatService.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.diklatService.get(action.payload)
            .pipe(
                tap((response: any) => {                    
                    ctx.patchState({response, loading: false})
                })
            )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Diklats.State>, action: CreateUpdateData) {
        // const state = ctx.getState();
        if (action.id) {
            return this.diklatService.update(action.payload, action.id)
                .pipe(
                    switchMap(() => ctx.dispatch(new GetData()))
                )
        }

        return this.diklatService.create(action.payload).pipe(
            switchMap(() => ctx.dispatch(new GetData()))
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Diklats.State>, action: DeleteData) {
        return this.diklatService.delete(action.id).pipe(
            switchMap(() => ctx.dispatch(new GetData()))
        )
    }
}