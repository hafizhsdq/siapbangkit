import { BaseModel, BaseResponse } from "../base-model";

export namespace Diklats {
    export interface State {
      response: BaseResponse<Diklat>;
      loading: boolean
    }

    export interface CreateUpdateDiklatInput {
      name: string;
      group_name: string;
      explanation: string;
      // angkatan: number;
      // jumlah: number;
      // durasi: number;
      // waktu: number;
      // lokasi: string;
      // tujuan: string;
      // persyaratan: string;
      // mulaiReg: number;
      // selesaiReg: number;
      // statusReg: string;
      // keterangan: string;
      
  }
  
    export interface Diklat extends BaseModel {
      name: string;
      group_name: string;
      explanation: string;
    }
  
    export enum DiklatType {
      undefined,
      internal,
      eksternal,
      bkd
    }

    export enum Rumpun {
      TEKNIK = 'Teknik',
      PERTANIAN = 'Pertanian',
      ADMINISTRASI = 'Administrasi',
      PEMERINTAHAN = 'Pemerintahan',
      KEMASYARAKATAN = 'Kemasyarakatan',
      HUKUM = 'Hukum',
      EKONOMI = 'Ekonomi'
    }
}