import { GetPage } from "../base-model";
import { Diklats } from "./diklat";

export class CreateUpdateData {
    static readonly type = '[Diklat] create update';
    constructor(public payload: Diklats.CreateUpdateDiklatInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Diklat] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Diklat] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

export class GetMataDiklatByDiklatId {
    static readonly type = '[Diklat] get mata diklat by diklat id';
    constructor(public payload?: GetPage, public id?: string | number) {}
}