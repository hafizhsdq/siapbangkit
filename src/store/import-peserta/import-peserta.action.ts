import { GetPage } from "../base-model";
import { ImportPesertas } from "./import-peserta";

export class CreateUpdateData {
    static readonly type = '[ImportPeserta] create update';
    constructor(public payload: ImportPesertas.ImportPeserta, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[ImportPeserta] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[ImportPeserta] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[ImportPeserta] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }