import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { ImportPesertaService } from '../../app/shared/services/import-peserta.service';
import { ManajemenUserService } from '../../app/shared/services/manajemen-user.service';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { ImportPesertas } from './import-peserta';
import { CreateUpdateData, DeleteData, GetData } from './import-peserta.action';


@State<ImportPesertas.State>({
    name: 'importpeserta',
    defaults: { response: {} } as ImportPesertas.State
})
@Injectable()
export class ImportPesertaState {
    @Selector()
    static getImportPesertas(state: ImportPesertas.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAImportPesertas(state: ImportPesertas.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: ImportPesertas.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: ImportPesertas.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private importpesertaservice: ImportPesertaService) {}
    
    @Action(GetData)
    get(ctx: StateContext<ImportPesertas.State>, action: GetData) {        
        if (action.id) {
            return this.importpesertaservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.importpesertaservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<ImportPesertas.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.importpesertaservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.importpesertaservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<ImportPesertas.State>, action: DeleteData) {
        return this.importpesertaservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}