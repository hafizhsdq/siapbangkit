import { BaseModel, BaseResponse } from "../base-model";

export namespace ImportPesertas {
  export interface State {
    response: BaseResponse<ImportPeserta>;
  }

    export interface CreateUpdateImportPesertaInput {
      nama: string;
      nip: number;
      nrk: number;
      jabatan: string;
      entitasAkun: string;
  }
  
    export interface ImportPeserta extends BaseModel {
      nama: string;
      nip: number;
      nrk: number;
      jabatan: string;
      entitasAkun: string;
    }
  
    // export enum ImportPesertaType {
      
    // }
}