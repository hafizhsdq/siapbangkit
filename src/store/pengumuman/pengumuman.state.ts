import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { Pengumumans } from './pengumuman';
import { CreateUpdateData, DeleteData, GetData } from './pengumuman.action';


@State<Pengumumans.State>({
    name: 'pengumuman',
    defaults: { response: {} } as Pengumumans.State
})
@Injectable()
export class PengumumanState {
    @Selector()
    static getPengumumans(state: Pengumumans.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAPengumumans(state: Pengumumans.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: Pengumumans.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: Pengumumans.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private pengumumanservice: PengumumanService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Pengumumans.State>, action: GetData) {        
        if (action.id) {
            return this.pengumumanservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.pengumumanservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Pengumumans.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.pengumumanservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.pengumumanservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Pengumumans.State>, action: DeleteData) {
        return this.pengumumanservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}