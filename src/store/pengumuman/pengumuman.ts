import { BaseModel, BaseResponse } from "../base-model";

export namespace Pengumumans {
  export interface State {
    response: BaseResponse<Pengumuman>;
  }

    export interface CreateUpdatePengumumanInput {
        judul: string;
        tanggal: number;
        isi: string;
    }
  
    export interface Pengumuman extends BaseModel {
        judul: string;
        tanggal: number;
        isi: string;
    }
  
    // export enum PengumumanType {
    //     undefined,
    //     internal,
    //     eksternal,
    //     bkd
    // }
}