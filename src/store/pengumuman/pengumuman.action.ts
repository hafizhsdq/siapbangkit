import { GetPage } from "../base-model";
import { Pengumumans } from "./pengumuman";

export class CreateUpdateData {
    static readonly type = '[Pengumuman] create update';
    constructor(public payload: Pengumumans.CreateUpdatePengumumanInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Pengumuman] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Pengumuman] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Pengumuman] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }