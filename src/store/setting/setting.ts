import { BaseResponse } from "../base-model";

export class UpdateSetting {
    static readonly type = '[Setting] update';
    public constructor(public payload: Settings.UpdateSettingInput, public slug: string) {}
}

export class GetSetting {
    static readonly type = '[Setting] get';
    public constructor(public slug: string) {}
}

export namespace Settings {
    export interface State {
        response: BaseResponse<Setting>;
    }

    export interface UpdateSettingInput {
        config: string;
    }
    
    export interface Setting {
        slug: string;
        name: string;
        config: string;
    }
}