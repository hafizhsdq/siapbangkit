import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Settingservice } from '../../app/shared/services/setting.service';
import { GetSetting, Settings, UpdateSetting } from './setting';


@State<Settings.State>({
    name: 'settings',
    defaults: { response: {} } as Settings.State
})
@Injectable()
export class Settingstate {
    @Selector()
    static getSettings(state: Settings.State) {
        return state.response
    }

    constructor(private settingservice: Settingservice) {}
    
    @Action(GetSetting)
    get(ctx: StateContext<Settings.State>, action: GetSetting) {
        return this.settingservice.get(action.slug).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(UpdateSetting)
    update(ctx: StateContext<Settings.State>, action: UpdateSetting) {
        return this.settingservice.update(action.slug, action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }
}