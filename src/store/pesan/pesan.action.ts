import { GetPage } from "../base-model";
import { Pesans } from "./pesan";

export class CreateUpdateData {
    static readonly type = '[Pesan] create update';
    constructor(public payload: Pesans.CreateUpdatePesanInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Pesan] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Pesan] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[Pesan] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }