import { BaseModel, BaseResponse } from "../base-model";

export namespace Pesans {
  export interface State {
    response: BaseResponse<Pesan>;
  }

    export interface CreateUpdatePesanInput {
        to: string;
        subject: string;
        content: string;
        notify: boolean;
    }
  
    export interface Pesan extends BaseModel {
        to: string;
        from: string;
        subject: string;
        created_at: number;
        content: string;
    }
  
    export enum PesanType {
      notify
    }
}