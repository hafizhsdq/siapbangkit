import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { PesanService } from '../../app/shared/services/pesan.service';
import { ModelHelper } from '../base-model';
import { Pesans } from './pesan';
import { GetData, CreateUpdateData, DeleteData } from './pesan.action';


@State<Pesans.State>({
    name: 'pesan',
    defaults: { response: {} } as Pesans.State
})
@Injectable()
export class PesanState {
    @Selector()
    static getPesans(state: Pesans.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAPesans(state: Pesans.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getPesanMasuk(state: Pesans.State) {
    //     const data = ModelHelper.nestInclude(state.response)
    //     console.log(data)
        
    //     const coba = data.forEach((d) => console.log(d) )
    //     return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    // }

    // @Selector()
    // static getPeriode(state: Pesans.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private pesanservice: PesanService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Pesans.State>, action: GetData) {        
        if (action.id) {
            return this.pesanservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.pesanservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Pesans.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.pesanservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.pesanservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Pesans.State>, action: DeleteData) {
        return this.pesanservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}