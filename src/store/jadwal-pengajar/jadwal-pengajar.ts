import { BaseModel, BaseResponse } from "../base-model";

export namespace Pengajars {
    export interface State {
      response: BaseResponse<Pengajar>;
      loading: boolean
    }

    export interface CreateUpdateJadwalPengajarInput {
      type: string;
      start_date: Date;
      start_time: Date;
      end_time: Date;
      teaching_material: string;
      teacher: string;
      assitant: string;
      info: string;
    }
  
    export interface Pengajar extends BaseModel {
      type: string;
      start_date: Date;
      start_time: Date;
      end_time: Date;
      teaching_material: string;
      teacher: string;
      assitant: string;
      info: string;
    }
  
    export enum PengajarType {
      umum,
      khusus,
      emailKonfirmasi
    }
}