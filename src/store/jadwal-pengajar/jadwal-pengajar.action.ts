import { GetPage } from "../base-model";
import { Pengajars } from "./jadwal-pengajar";

export class CreateUpdateData {
    static readonly type = '[Pengajar] create update';
    constructor(public payload: Pengajars.CreateUpdateJadwalPengajarInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[Pengajar] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[Pengajar] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}