import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { PengajarService } from '../../app/shared/services/pengajar.service';
import { ModelHelper } from '../base-model';
import { Pengajars } from './jadwal-pengajar';
import { CreateUpdateData, DeleteData, GetData } from './jadwal-pengajar.action';


@State<Pengajars.State>({
    name: 'pengajar',
    defaults: { response: {}, loading: true } as Pengajars.State
})
@Injectable()
export class PengajaraState {
    // @Selector()
    // static getALecture(state: Pengajars.State) {
    //     return ModelHelper.nestInclude(state.response)[0] || {}
    // }

    @Selector()
    static getLecture(state: Pengajars.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    constructor(private pengajarService: PengajarService) {}
    
    @Action(GetData)
    get(ctx: StateContext<Pengajars.State>, action: GetData) {
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.pengajarService.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.pengajarService.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<Pengajars.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.pengajarService.update(action.payload, action.id)
        }
        return this.pengajarService.create(action.payload)
    }

    @Action(DeleteData)
    delete(ctx: StateContext<Pengajars.State>, action: DeleteData) {
        return this.pengajarService.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}