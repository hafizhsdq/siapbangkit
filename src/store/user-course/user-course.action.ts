import { GetPage } from "../base-model";
import { UserCourses } from "./user-course";

export class CreateUpdateData {
    static readonly type = '[UserCourse] create update';
    constructor(public payload: UserCourses.UserCourseInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[UserCourse] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[UserCourse] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[UserCourse] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }