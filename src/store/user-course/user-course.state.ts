import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { UserCourseService } from '../../app/shared/services/user-course.service';
import { ModelHelper } from '../base-model';
import { UserCourses } from './user-course';
import { CreateUpdateData, DeleteData, GetData } from './user-course.action';


@State<UserCourses.State>({
    name: 'usercourse',
    defaults: { response: {}, loading: true } as UserCourses.State
})
@Injectable()
export class UserCourseState {
    @Selector()
    static getUserCourses(state: UserCourses.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAUserCourses(state: UserCourses.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    constructor(private usercourseservice: UserCourseService) {}
    
    @Action(GetData)
    get(ctx: StateContext<UserCourses.State>, action: GetData) {      
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.usercourseservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response, loading: false})
                })
            )    
        }
        
        return this.usercourseservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<UserCourses.State>, action: CreateUpdateData) {
        // if (action.id) {
        //     return this.usercourseservice.update(action.payload, action.id)
        // }
        return this.usercourseservice.create(action.payload)
    }

    @Action(DeleteData)
    delete(ctx: StateContext<UserCourses.State>, action: DeleteData) {
        return this.usercourseservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}