import { BaseModel, BaseResponse } from "../base-model";

export namespace UserCourses {
    export interface State {
      response: BaseResponse<UserCourse>;
      loading: boolean
    }

    export interface UserCourseInput {
        organized_by: string;
        year: string;
        certificate_date: Date;
        certificate_no: string;
        start_date: Date;
        end_date: Date;
        jp: number;
        other_info: string;
        attachments: UserCourseAttachment[],
    }

    export interface ChangeStatusInput {
        status: string;
        info: string;
        file?: File;
    }

    export interface UserCourse {
        organized_by: string;
        year: string;
        certificate_date: Date;
        certificate_no: string;
        start_date: Date;
        end_date: Date;
        jp: number;
        other_info: string;
        attachments: UserCourseAttachment[],
    }

    export interface UserCourseAttachment {
        name: string;
        file: File;
    }
}