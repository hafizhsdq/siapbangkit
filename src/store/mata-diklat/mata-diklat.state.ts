import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { DiklatService } from '../../app/shared/services/diklat.service';
import { FileMateriService } from '../../app/shared/services/file-materi.service';
import { KbmService } from '../../app/shared/services/kbm.service';
import { MataDiklatService } from '../../app/shared/services/mata-diklat.service';
import { ModelHelper } from '../base-model';
import { Diklats } from '../diklat/diklat';
import { MataDiklats } from './mata-diklat';
import { CreateUpdateData, DeleteData, GetData, GetFileMaterial, GetPhase } from './mata-diklat.action';


@State<MataDiklats.State | Diklats.State>({
    name: 'matadiklat',
    defaults: { response: {} } as MataDiklats.State | Diklats.State
})
@Injectable()
export class MataDiklatState {
    @Selector()
    static getMataDiklats(state: MataDiklats.State) {
        console.log(state);
        
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getAMataDiklats(state: MataDiklats.State) {
    //     return ModelHelper.nestInclude(state.response)[0] || {}
    // }

    @Selector()
    static getTotalTimeAllocation(state: MataDiklats.State) {
        return state.response.data.map(s => s.attributes.time_allocation).reduce((p, c) => p+c, 0)
    }

    // currentStateId(ctx: StateContext<MataDiklats.State>) {
    //     const state = ctx.getState()
    //     const items = state.response.items
    //     const id = items?.length > 0 && items[0].id
    //     return id
    // }

    constructor(private diklatservice: DiklatService, private mataDiklatservice: MataDiklatService, private kbmservice: KbmService, private filemateriservice: FileMateriService) {}
    
    @Action(GetData)
    get(ctx: StateContext<MataDiklats.State>, action: GetData) {

        if (action.id) {// action id = trainin/diklat id
            return this.mataDiklatservice.getById(action.id).pipe(
                tap((response: any) => {                    
                    ctx.patchState({response})
                })
            )    
        }

        return this.mataDiklatservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(GetPhase)
    getPhase(ctx: StateContext<MataDiklats.State>, action: GetPhase) {
        return this.mataDiklatservice.getPhase(action.id, action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(GetFileMaterial)
    getFile(ctx: StateContext<MataDiklats.State>, action: GetFileMaterial) {
        return this.mataDiklatservice.getFile(action.id, action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<MataDiklats.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.mataDiklatservice.update(action.payload, action.id)
        }
        return this.mataDiklatservice.create(action.payload)
    }

    @Action(DeleteData)
    delete(ctx: StateContext<MataDiklats.State>, action: DeleteData) {
        const state = ctx.getState()
        return this.mataDiklatservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData(action.payload))
            })
        )
    }
}