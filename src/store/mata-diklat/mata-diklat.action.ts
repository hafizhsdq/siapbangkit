import { GetPage } from "../base-model";
import { FileMateris } from "../file-materi/file-materi";
import { Kbms } from "../kbm/kbm";
import { MataDiklats } from "./mata-diklat";

export class CreateUpdateData {
    static readonly type = '[MataDiklat] create update';
    constructor(public payload: MataDiklats.CreateUpdateMataDiklatInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[MataDiklat] delete';
    constructor(public id: string, public payload?: GetPage) {}
}

export class GetData {
    static readonly type = '[MataDiklat] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

export class GetPhase {
    static readonly type = '[MataDiklat] get phase';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

export class CreateUpdateDataPhase {
    static readonly type = '[KBM] create update';
    constructor(public payload: Kbms.CreateUpdateKbmInput, public id?: string) {}
}

export class DeleteDataPhase {
    static readonly type = '[KBM] delete';
    constructor(public id: string) {}
}

export class GetFileMaterial {
    static readonly type = '[MataDiklat] get file';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

export class CreateUpdateDataFileMaterial {
    static readonly type = '[FileMaterial] create update';
    constructor(public payload: FileMateris.CreateUpdateFileMateriInput, public id?: string) {}
}

export class DeleteDataFileMaterial {
    static readonly type = '[FileMaterial] delete';
    constructor(public id: string) {}
}