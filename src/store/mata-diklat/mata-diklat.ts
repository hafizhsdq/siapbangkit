import { DiklatCardComponent } from "../../app/shared/components/diklat-card/diklat-card.component";
import { BaseModel, BaseResponse } from "../base-model";
import { Diklats } from "../diklat/diklat";
import { FileMateris } from "../file-materi/file-materi";
import { Kbms } from "../kbm/kbm";

export namespace MataDiklats {
  export interface State {
    response: BaseResponse<MataDiklat>;
  }

  export interface CreateUpdateMataDiklatInput {
    name: string;
    description: string;
    basic_competencies: string;
    success_indicator: string;
    teaching_guidance: string;
    participant_guidance: string;
    reference: string;
    evaluation: string;
    other_info: string;
  }
  
  export interface MataDiklat extends BaseModel {
    id: string;
    name: string;
    description: string;
    basic_competencies: string;
    success_indicator: string;
    teaching_guidance: string;
    participant_guidance: string;
    reference: string;
    evaluation: string;
    other_info: string;
    time_allocation: number;
    phase: Kbms.Kbm[];
    files: FileMateris.FileMateri[];
  }
  
  export enum MataDiklatType {
    
  }
}