import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { KalenderDiklatService } from '../../app/shared/services/kalender-diklat.service';
import { ModelHelper } from '../base-model';
import { KalenderDiklats } from './kalender-diklat';
import { CreateUpdateData, DeleteData, GetData } from './kalender-diklat.action';


@State<KalenderDiklats.State>({
    name: 'kalenderdiklat',
    defaults: { response: {}, loading: true } as KalenderDiklats.State
})
@Injectable()
export class KalenderDiklatState {
    @Selector()
    static getKalenderDiklats(state: KalenderDiklats.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAKalenderDiklats(state: KalenderDiklats.State) {
        const data = ModelHelper.nestInclude(state.response)
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getKalenderPesertaDiklats(state: KalenderDiklats.State) {
        const {included, data} = state.response
        return state.response || {}
    }

    // @Selector()
    // static getPeriode(state: KalenderDiklats.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private kalenderdiklatservice: KalenderDiklatService) {}
    
    @Action(GetData)
    get(ctx: StateContext<KalenderDiklats.State>, action: GetData) {      
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.kalenderdiklatservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.kalenderdiklatservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<KalenderDiklats.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.kalenderdiklatservice.update(action.payload, action.id)
        }
        return this.kalenderdiklatservice.create(action.payload)
    }

    @Action(DeleteData)
    delete(ctx: StateContext<KalenderDiklats.State>, action: DeleteData) {
        return this.kalenderdiklatservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}