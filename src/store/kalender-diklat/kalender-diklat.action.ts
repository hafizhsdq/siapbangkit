import { GetPage } from "../base-model";
import { KalenderDiklats } from "./kalender-diklat";

export class CreateUpdateData {
    static readonly type = '[KalenderDiklat] create update';
    constructor(public payload: KalenderDiklats.CreateUpdateKalenderDiklatInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[KalenderDiklat] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[KalenderDiklat] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

export class GetPatricipant {
    static readonly type = '[KalenderDiklat] get participant';
    constructor(public payload?: GetPage, public id?: string | number) {}
}