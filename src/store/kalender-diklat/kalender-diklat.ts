import { DiklatCardComponent } from "../../app/shared/components/diklat-card/diklat-card.component";
import { BaseModel, BaseResponse } from "../base-model";
import { Diklats } from "../diklat/diklat";
import { FileMateris } from "../file-materi/file-materi";
import { Kbms } from "../kbm/kbm";

export namespace KalenderDiklats {
  export interface State {
    response: BaseResponse<KalenderDiklat>;
    loading: boolean
  }

  export interface ChangeStatusInput {
    status: string;
    info: string;
    file?: File;
  }

  export interface CreateUpdateKalenderDiklatInput {
    quota: number;
    duration: number;
    start_date: Date;
    location: string;
    purpose: string;
    requirement: string;
    start_registration: Date;
    end_registration: Date;
    registration_status: string;
    info: string;
    training_id: string;
  }
  
  export interface KalenderDiklat extends BaseModel {
    quota: number;
    duration: number;
    start_date: Date;
    location: string;
    purpose: string;
    requirement: string;
    start_registration: Date;
    end_registration: Date;
    registration_status: string;
    info: string;
    periode: number;
  }
}