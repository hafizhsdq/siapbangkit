import { BaseModel, BaseResponse } from "../base-model";

export namespace Biodatas {
    export interface State {
      response: BaseResponse<Biodata>;
    }

    export interface AddDocumentInput {
      file: File;
      name: string;
    }

    export interface CreateUpdateBiodataInput {
        avatar: string,
        nama: string,
        nip: string,
        gelar_depan: string,
        gelar_belakang: string,
        jenis_kelamin: string,
        status_kawin: string,
        jenis_pegawai: string,
        status_pegawai: string,
        kedudukan_pegawai: string,
        jabatan: string,
        pangkat: string,
        nip_lama: string,
        tempat_lahir: string,
        tanggal_lahir: string,
        telepon: string,
        handphone_1: string,
        handphone_2: string,
        whatsapp: string,
        email: string,
        email_sidoarjokab: string,
        is_sesuai_ktp: boolean,
        ktp_kelurahan: string,
        ktp_kodepos: string,
        ktp_rw: string,
        ktp_rt: string,
        ktp_alamat: string,
        domisili_kelurahan: string,
        domisili_kodepos: string,
        domisili_rw: string,
        domisili_rt: string,
        domisili_alamat: string,
        nik: string,
        nomor_kk: string,
        npwp: string,
        nuptk: string,
        duk: string,
        nomor_karpeg: string,
        nomor_korpri: string,
        unit_organisasi: string,
        sub_unit_organisasi: string,
        seksi: string
        // nip: number;
        // avatar: string;
        // front_title: string;
        // name: string;
        // back_title: string;
        // department: string;
        // position: string;
        // position_type: string;
        // tmt_grade: number;
        // tmt_echelon: number;
        // tmt_pns: number;
        // tmt_cpns: number;
        // grade: string;
        // religion: string;
        // last_education: string;
        // email: string;
        // gender: string;
        // birth_place: string;
        // birth_date: string;
        // ktp: number;
        // npwp: number;
        // bank_account: string;
        // bank_account_name: string;
        // bank_account_number: number;

        // work_address: string;
        // work_address_ward: string;
        // work_address_district: string;
        // work_address_city: string;
        // work_address_province: string;
        // work_phone: string;
        // work_fax: number;

        // home_address: string;
        // home_address_ward: string;
        // home_address_district: string;
        // home_address_city: string;
        // home_address_province: string;
        // home_phone: string;
        // home_zip_code: number;
        // home_mobile: number;

        // is_domicile_ktp: boolean;
        // domicile_address: string;
        // domicile_address_ward: string;
        // domicile_address_district: string;
        // domicile_address_city: string;
        // domicile_address_province: string;
        // domicile_phone: string;
        // domicile_mobile: number;
        // domicile_zip_code: number;
        // totalJP: number;
    }

    export interface GetBiodataPage {
      page?: number;
      per_page?: number;
      name?: string;
  }
  
    export interface Biodata extends BaseModel {
        avatar: string,
        verified: string,
        nama: string,
        nip: string,
        gelar_depan: string,
        gelar_belakang: string,
        jenis_kelamin: string,
        status_kawin: string,
        jenis_pegawai: string,
        status_pegawai: string,
        kedudukan_pegawai: string,
        jabatan: string,
        pangkat: string,
        nip_lama: string,
        foto: string,
        tempat_lahir: string,
        tanggal_lahir: string,
        telepon: string,
        handphone_1: string,
        handphone_2: string,
        whatsapp: string,
        email: string,
        email_sidoarjokab: string,
        is_sesuai_ktp: boolean,
        ktp_kelurahan: string,
        ktp_kodepos: string,
        ktp_rw: string,
        ktp_rt: string,
        ktp_alamat: string,
        domisili_kelurahan: string,
        domisili_kodepos: string,
        domisili_rw: string,
        domisili_rt: string,
        domisili_alamat: string,
        nik: string,
        nomor_kk: string,
        npwp: string,
        nuptk: string,
        duk: string,
        nomor_karpeg: string,
        nomor_korpri: string,
        unit_organisasi: string,
        sub_unit_organisasi: string,
        seksi: string
      // type: string;
      // avatar: string;
      // // attributes: BiodataAttribute;

      // nip: number;
      // front_title: string;
      // name: string;
      // back_title: string;
      // department: string;
      // position: string;
      // position_type: string;
      // tmt_grade: number;
      // tmt_echelon: number;
      // tmt_pns: number;
      // tmt_cpns: number;
      // grade: string;
      // religion: string;
      // last_education: string;
      // email: string;
      // gender: string;
      // birth_place: string;
      // birth_date: string;
      // ktp: number;
      // npwp: number;
      // bank_account: string;
      // bank_account_name: string;
      // bank_account_number: number;

      // work_address: string;
      // work_address_ward: string;
      // work_address_district: string;
      // work_address_city: string;
      // work_address_province: string;
      // work_phone: string;
      // work_fax: number;

      // home_address: string;
      // home_address_ward: string;
      // home_address_district: string;
      // home_address_city: string;
      // home_address_province: string;
      // home_phone: string;
      // home_zip_code: number;
      // home_mobile: number;

      // is_domicile_ktp: boolean;
      // domicile_address: string;
      // domicile_address_ward: string;
      // domicile_address_district: string;
      // domicile_address_city: string;
      // domicile_address_province: string;
      // domicile_phone: string;
      // domicile_mobile: number;
      // domicile_zip_code: number;
      // totalJP: number;
    }

    // export interface BiodataAttribute {
    //   nip: number;
    //   front_title: string;
    //   name: string;
    //   back_title: string;
    //   department: string;
    //   position: string;
    //   position_type: string;
    //   tmt_grade: number;
    //   tmt_echelon: number;
    //   tmt_pns: number;
    //   tmt_cpns: number;
    //   grade: string;
    //   religion: string;
    //   last_education: string;
    //   email: string;
    //   gender: string;
    //   birth_place: string;
    //   birth_date: string;
    //   ktp: number;
    //   npwp: number;
    //   bank_account: string;
    //   bank_account_name: string;
    //   bank_account_number: number;

    //   work_address: string;
    //   work_address_ward: string;
    //   work_address_district: string;
    //   work_address_city: string;
    //   work_address_province: string;
    //   work_phone: string;
    //   work_fax: number;

    //   home_address: string;
    //   home_address_ward: string;
    //   home_address_district: string;
    //   home_address_city: string;
    //   home_address_province: string;
    //   home_phone: string;
    //   home_zip_code: number;
    //   home_mobile: number;

    //   is_domicile_ktp: boolean;
    //   domicile_address: string;
    //   domicile_address_ward: string;
    //   domicile_address_district: string;
    //   domicile_address_city: string;
    //   domicile_address_province: string;
    //   domicile_phone: string;
    //   domicile_mobile: number;
    //   domicile_zip_code: number;
    // }
  
    export enum BiodataType {
      sesuaiKtp
    }
}