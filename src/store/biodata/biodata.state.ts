import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { MENU_ITEMS } from '../../app/pages/pages-menu';
import { BiodataService } from '../../app/shared/services/biodata.service';
import { ModelHelper } from '../base-model';
import { Biodatas } from './biodata';
import { AddDocument, CreateUpdateBiodata, DeleteDocument, GetBiodatas } from './biodata.action';


@State<Biodatas.State>({
    name: 'biodata',
    defaults: { response: {} } as Biodatas.State
})
@Injectable()
export class BiodataState {

    @Selector()
    static getMenu(state: Biodatas.State) {
        const roles = state.response.included.filter(i => i.type == 'roles')

        // false = show; true = hidden
        const checkRoles = (roles, data) => {
            if (!data) return false
            return roles[0].attributes.permission.find(p => {                
                const permission = data.roles[p.name]                
                if (permission) {
                    return permission.map(perm => 
                        // negation the permission so it become false hidden 
                        // if the value true
                        !p.permission[perm]).reduce((p,c) => p |= c, false)
                }
            
                return false
            }) != null
        }

        return MENU_ITEMS.map(m => {
            const hidden = checkRoles(roles, m.data)
  
            if (m.children && m.children.length > 0) {
              m.children = m.children.map(mchild => {
                const hidden = checkRoles(roles, mchild.data)
                return {...mchild, hidden}
              })
            }
  
            return {...m, hidden}
        })
    }

    @Selector()
    static getUserSchedule(state: Biodatas.State) {   
        const training_schedule = ModelHelper.nestInclude(state.response).data[0].relationships.training_schedule        
        return training_schedule?.length ? training_schedule : training_schedule ? [training_schedule] : []  
    }

    @Selector()
    static getTrainingSuggestion(state: Biodatas.State) {        
        return state.response.included.filter(i => i.type == 'training_schedule').map(q => {
            return ModelHelper.findInclude(state.response.included, 'training_schedule', q.id)
        })
    }

    @Selector()
    static getRoles(state: Biodatas.State) {        
        const roles = state.response.included.filter(i => i.type == 'roles')        
        return roles
    }

    @Selector()
    static getQuestionnaire(state: Biodatas.State) {
        return state.response.included.filter(i => i.type == 'questionnaire').map(q => {
            return ModelHelper.findInclude(state.response.included, 'questionnaire', q.id)
        })
    }

    @Selector()
    static getUserDoc(state: Biodatas.State) {        
        const userDoc = ModelHelper.nestInclude(state.response).data[0].relationships.attachment
        return userDoc?.length ? userDoc : userDoc ? [userDoc] : []
    }

    @Selector()
    static getUserPermit(state: Biodatas.State) {
        const userPermit = ModelHelper.nestInclude(state.response).data[0].relationships.user_permit
        return {data: userPermit?.length ? userPermit : userPermit ? [userPermit] : []}
    }

    @Selector()
    static getUserCourse(state: Biodatas.State) {
        const userCourse = ModelHelper.nestInclude(state.response).data[0].relationships.user_course        
        return userCourse?.length ? userCourse : userCourse ? [userCourse] : []
    }

    @Selector()
    static getUserEducation(state: Biodatas.State) {
        const userEdu = ModelHelper.nestInclude(state.response).data[0].relationships.user_education        
        return userEdu?.length ? userEdu : userEdu ? [userEdu] : []
    }

    @Selector()
    static getDiklat(state: Biodatas.State) {
        const diklats = ModelHelper.nestInclude(state.response).data[0].relationships.training_schedule                
        return diklats?.length ? diklats : diklats ? [diklats] : []
    }

    @Selector()
    static getUserPaper(state: Biodatas.State) {
        const userPaper = ModelHelper.nestInclude(state.response).data[0].relationships.user_paper       
        return userPaper?.length ? userPaper : userPaper ? [userPaper] : []
    }

    @Selector()
    static getBiodatas(state: Biodatas.State) {
        const participants = ModelHelper.getOnly(state.response.included, 'participant')        
        const user_permits = ModelHelper.getOnly(state.response.included, 'user_permit')    

        try {

            let participant_status = null
            participants.forEach(p => {
                const {relationships: {activity_log}} = p

                if (activity_log.length <= 0 && participant_status && participant_status['unverified'])  {
                    participant_status['unverified'] += 1
                } 
                else if (activity_log.length <= 0) {
                    participant_status = []
                    participant_status['unverified'] = 1
                }
                else if (activity_log.length >= 0 && participant_status && participant_status[activity_log[0].attributes.status]) {
                    participant_status[activity_log[0].attributes.status] += 1
                } 
                else if (activity_log.length >= 0) {
                    participant_status = []
                    participant_status[activity_log[0].attributes.status] = 1
                } 
                // else 
            })

            let user_permit_status = null
            user_permits.forEach(p => {
                const {attributes: {category}, relationships: {activity_log}} = p

                if (activity_log.length <= 0) return

                if (user_permit_status && user_permit_status[category]) {

                    if (user_permit_status[category][activity_log[0].attributes.status]) {
                        user_permit_status[category][activity_log[0].attributes.status] += 1
                    } else {
                        user_permit_status[category][activity_log[0].attributes.status] = 1
                    }
                } else {
                    user_permit_status = []
                    user_permit_status[category] = 1
                }
            })
            
            return {...ModelHelper.nestInclude(state.response).data[0], participant_status, user_permit_status}
        } catch(e) {
            console.log(e);
            
        }
    }

    constructor(private biodataService: BiodataService) {}

    @Action(GetBiodatas)
    get(ctx: StateContext<Biodatas.State>, action: GetBiodatas) {
        return this.biodataService.get(action.payload).pipe(
            tap((response: any) => {       
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateBiodata)
    save(ctx: StateContext<Biodatas.State>, action: CreateUpdateBiodata) {
        return this.biodataService.update(action.payload, action.nip).pipe(switchMap(() => ctx.dispatch(new GetBiodatas())))
    }

    @Action(AddDocument)
    addDoc(ctx: StateContext<Biodatas.State>, action: AddDocument) {
        return this.biodataService.addDoc(action.payload)
    }

    @Action(DeleteDocument)
    deleteDoc(ctx: StateContext<Biodatas.State>, action: DeleteDocument) {
        return this.biodataService.deleteDoc(action.id)
    }
}