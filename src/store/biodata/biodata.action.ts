import { GetPage } from "../base-model";
import { Biodatas } from "./biodata"; 

export class CreateUpdateBiodata {
    static readonly type = '[Biodata] create update';
    constructor(public payload: Biodatas.CreateUpdateBiodataInput, public nip: string) {}
}

export class DeleteBiodata {
    static readonly type = '[Biodata] delete';
    constructor(public id: string) {}
}

export class GetBiodatas {
    static readonly type = '[Biodata] get';
    constructor(public payload?: GetPage) {}
}

export class AddDocument {
    static readonly type = '[Biodata] add document';
    constructor(public payload: Biodatas.AddDocumentInput, public id?: string) {}
}

export class DeleteDocument {
    static readonly type = '[Biodata] delete document';
    constructor(public id: string) {}
}


// export class GetPermissions {
//     static readonly type = '[Biodata Permission] get permission';
//     constructor(public payload?: any, public biodataId?: string) {}
// }