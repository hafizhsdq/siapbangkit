import { GetPage } from "../base-model";
import { PengaturanUmums } from "./pengaturan-umum";

export class CreateUpdateData {
    static readonly type = '[PengaturanUmum] create update';
    constructor(public payload: PengaturanUmums.CreateUpdatePengaturanUmumInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[PengaturanUmum] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[PengaturanUmum] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[PengaturanUmum] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }