import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { PengaturanUmums } from './pengaturan-umum';
import { CreateUpdateData, DeleteData, GetData } from './pengaturan-umum.action';


@State<PengaturanUmums.State>({
    name: 'pengaturanumum',
    defaults: { response: {} } as PengaturanUmums.State
})
@Injectable()
export class PengaturanUmumState {
    @Selector()
    static getPengaturanUmums(state: PengaturanUmums.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getAPengaturanUmums(state: PengaturanUmums.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: PengaturanUmums.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: PengaturanUmums.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private pengaturanumumservice: PengaturanUmumService) {}
    
    @Action(GetData)
    get(ctx: StateContext<PengaturanUmums.State>, action: GetData) {        
        if (action.id) {
            return this.pengaturanumumservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.pengaturanumumservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<PengaturanUmums.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.pengaturanumumservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.pengaturanumumservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<PengaturanUmums.State>, action: DeleteData) {
        return this.pengaturanumumservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}