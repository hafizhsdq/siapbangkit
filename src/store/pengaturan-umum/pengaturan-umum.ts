import { BaseModel, BaseResponse } from "../base-model";

export namespace PengaturanUmums {
  export interface State {
    response: BaseResponse<PengaturanUmum>;
  }

    export interface CreateUpdatePengaturanUmumInput {
        banner: string;
        cari: string;
        tampilkan: boolean;
        action: boolean;
    }
  
    export interface PengaturanUmum extends BaseModel {
        jenis: string;
        jadwal: string;
        lokasi: string;
    }
  
    export enum PengaturanUmumType {
        tampilkan,
        action
    }
}