import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { RoleService } from '../../app/shared/services/role.service';
import { ModelHelper } from '../base-model';
import { RolePermissions, Roles } from './role-permission';
import { CreateUpdateRole, DeleteRole, GetPermissions, GetRoles } from './role.action';


@State<Roles.State>({
    name: 'role',
    defaults: { response: {}, loading: true } as Roles.State
})
@Injectable()
export class RoleState {
    @Selector()
    static getRoles(state: Roles.State) {
        return {...state, response: RoleState.inlinePermission(ModelHelper.nestInclude(state.response))}
    }

    @Selector()
    static getARoles(state: Roles.State) {
        return {...state, response: RoleState.inlinePermission(ModelHelper.nestInclude(state.response)).data[0]}
    }

    private static inlinePermission = (response) => {
        return {...response, data: response.data.map(role => {
            return ({...role, permission: role.attributes.permission.map(p => ({[p.name]: p.permission})).reduce((p, c) => ({...p, ...c}))})
        })}
    }

    constructor(private roleService: RoleService) {}

    @Action(GetRoles)
    get(ctx: StateContext<Roles.State>, action: GetRoles) {
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.roleService.getById(action.id, action.payload).pipe(
                tap((response: any) => {              
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.roleService.get(action.payload).pipe(
            tap((response: any) => {              
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateRole)
    save(ctx: StateContext<Roles.State>, action: CreateUpdateRole) {
        if (action.id) {
            return this.roleService.update(action.payload, action.id)
        }
        return this.roleService.create(action.payload)
    }

    @Action(DeleteRole)
    delete(ctx: StateContext<Roles.State>, action: DeleteRole) {
        return this.roleService.delete(action.id).pipe(switchMap(() => ctx.dispatch(new GetRoles())))
    }
}

@State<RolePermissions.State>({
    name: 'permission',
    defaults: { response: {}, loading: false } as RolePermissions.State
})
@Injectable()
export class RolePermissionState {
    @Selector()
    static getPermissions(state: RolePermissions.State) {
        return state.response.length ? state.response : []
    }

    constructor(private roleService: RoleService) {}

    @Action(GetPermissions)
    get(ctx: StateContext<RolePermissions.State>, action: GetPermissions) {
        if (action.roleId) {
            // return forkJoin([this.roleService.getPermissions(), this.roleService.getRolePermissions(action.roleId)]).pipe(
            //     tap((response: any) => {
            //         // const permissions = response[0].map(p => {
            //         //     const found = response[1].find(f => p.name == f.name)
            //         //     p.permission = {create: false, read: false, update: false, delete: false, print: false}
            //         //     p.permission.create = found?.permission.create ?? false
            //         //     p.permission.read = found?.permission.read ?? false
            //         //     p.permission.update = found?.permission.update ?? false
            //         //     p.permission.delete = found?.permission.delete ?? false
            //         //     p.permission.print = found?.permission.print ?? false
            //         //     return p
            //         // })
            //         ctx.patchState({response})
            //     })
            // )
        }
        return this.roleService.getPermissions().pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }
}