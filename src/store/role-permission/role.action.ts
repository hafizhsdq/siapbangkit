import { GetPage } from "../base-model";
import { Roles } from "./role-permission";

export class CreateUpdateRole {
    static readonly type = '[Role] create update';
    constructor(public payload: Roles.CreateUpdateRoleInput, public id?: string) {}
}

export class DeleteRole {
    static readonly type = '[Role] delete';
    constructor(public id: string) {}
}

export class GetRoles {
    static readonly type = '[Role] get';
    constructor(public payload?: GetPage, public id?: string) {}
}

export class GetPermissions {
    static readonly type = '[Role Permission] get permission';
    constructor(public payload?: any, public roleId?: string) {}
}