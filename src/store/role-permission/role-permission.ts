import { BaseResponse } from "../base-model";

export namespace Roles {
    export interface State {
        response: BaseResponse<Role>;
        loading: boolean
    }

    export interface CreateUpdateRoleInput {
        name: string;
        description: string;
        permission: RolePermissions.UpdateRolePermission[]
    }

    export interface GetRolePage {
        page?: number;
        per_page?: number;
        name?: string;
    }

    export interface Role {
        name: string;
        description: string;
        permission: RolePermissions.RolePermission[]
    }
}

export namespace RolePermissions {
    export interface State {
        response: RolePermission[];
        loading: boolean
    }

    export interface Permission {
        name: string;
        description: string;
        permission_key: string[];
        permission: PermissionAttribute;
    }
  
    export interface RolePermission {
        name: string;
        description: string;
        permission: PermissionAttribute;
    }

    export interface UpdateRolePermission {
        name: string;
        permission: PermissionAttribute;
    }

    export interface PermissionAttribute {
        create: boolean;
        read: boolean;
        update: boolean;
        delete: boolean;
        print: boolean;
    }
}