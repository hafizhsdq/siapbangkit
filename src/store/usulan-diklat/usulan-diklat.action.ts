import { GetPage } from "../base-model";
import { UsulanDiklats } from "./usulan-diklat";

export class CreateUpdateData {
    static readonly type = '[UsulanDiklat] create update';
    constructor(public payload: UsulanDiklats.CreateUpdateUsulanDiklatInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[UsulanDiklat] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[UsulanDiklat] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[UsulanDiklat] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }