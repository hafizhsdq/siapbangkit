import { BaseModel, BaseResponse } from "../base-model";

export namespace UsulanDiklats {
  export interface State {
    response: BaseResponse<UsulanDiklat>;
    loading: boolean
  }

  export interface CreateUpdateUsulanDiklatInput {
    nip: number;
    name: string;
    title: string;
    explanation: string;
  }
  
  export interface UsulanDiklat extends BaseModel {
    nip: number;
    name: string;
    title: string;
    explanation: string;
  }

//   export enum UsulanDiklatType {
//       organizer,
//       participant
//     }
}