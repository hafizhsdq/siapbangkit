import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { UsulanDiklatService } from '../../app/shared/services/usulan-diklat.service';
import { ModelHelper } from '../base-model';
import { UsulanDiklats } from './usulan-diklat';
import { CreateUpdateData, DeleteData, GetData } from './usulan-diklat.action';


@State<UsulanDiklats.State>({
    name: 'usulandiklat',
    defaults: { response: {}, loading: true } as UsulanDiklats.State
})
@Injectable()
export class UsulanDiklatState {
    @Selector()
    static getUsulanDiklats(state: UsulanDiklats.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    @Selector()
    static getAUsulanDiklats(state: UsulanDiklats.State) {
        const data = ModelHelper.nestInclude(state.response)
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: UsulanDiklats.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: UsulanDiklats.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private usulandiklatservice: UsulanDiklatService) {}
    
    @Action(GetData)
    get(ctx: StateContext<UsulanDiklats.State>, action: GetData) {
        ctx.setState(state => ({...state, loading: true}))
        if (action.id) {
            return this.usulandiklatservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response, loading: false})
                })
            )    
        }

        return this.usulandiklatservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response, loading: false})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<UsulanDiklats.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.usulandiklatservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.usulandiklatservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<UsulanDiklats.State>, action: DeleteData) {
        return this.usulandiklatservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}