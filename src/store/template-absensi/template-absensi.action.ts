import { GetPage } from "../base-model";
import { TemplateAbsensis } from "./template-absensi";

export class CreateUpdateData {
    static readonly type = '[TemplateAbsensi] create update';
    constructor(public payload: TemplateAbsensis.TemplateAbsensi, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[TemplateAbsensi] delete';
    constructor(public id: string) {}
}

export class GetData {
    static readonly type = '[TemplateAbsensi] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[TemplateAbsensi] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }