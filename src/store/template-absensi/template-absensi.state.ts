import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { ManajemenUserService } from '../../app/shared/services/manajemen-user.service';
import { PengaturanUmumService } from '../../app/shared/services/pengaturan-umum.service';
import { PengumumanService } from '../../app/shared/services/pengumuman.service';
import { SertifikatService } from '../../app/shared/services/sertifikatku.service';
import { TemplateAbsensiService } from '../../app/shared/services/template-absensi.service';
import { BaseResponse, ModelHelper } from '../base-model';
import { TemplateAbsensis } from './template-absensi';
import { CreateUpdateData, DeleteData, GetData } from './template-absensi.action';


@State<TemplateAbsensis.State>({
    name: 'templateabsensi',
    defaults: { response: {} } as TemplateAbsensis.State
})
@Injectable()
export class TemplateAbsensiState {
    @Selector()
    static getTemplateAbsensis(state: TemplateAbsensis.State) {
        return ModelHelper.nestInclude(state.response) || []
    }

    @Selector()
    static getATemplateAbsensis(state: TemplateAbsensis.State) {
        const data = ModelHelper.nestInclude(state.response)
        return data?.length ? ModelHelper.nestInclude(state.response)[0] : {}
    }

    // @Selector()
    // static getKalenderPesertaDiklats(state: TemplateAbsensis.State) {
    //     const {included, data} = state.response
    //     return state.response || {}
    // }

    // @Selector()
    // static getPeriode(state: TemplateAbsensis.State) {
    //     return state.response.included[0] || []
    // }

    constructor(private templateabsensiservice: TemplateAbsensiService) {}
    
    @Action(GetData)
    get(ctx: StateContext<TemplateAbsensis.State>, action: GetData) {        
        if (action.id) {
            return this.templateabsensiservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.templateabsensiservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<TemplateAbsensis.State>, action: CreateUpdateData) {
        if (action.id) {
            return this.templateabsensiservice.update(action.payload, action.id).pipe(
                switchMap(() => {
                    return ctx.dispatch(new GetData())
                })
            )
        }
        return this.templateabsensiservice.create(action.payload).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }

    @Action(DeleteData)
    delete(ctx: StateContext<TemplateAbsensis.State>, action: DeleteData) {
        return this.templateabsensiservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}