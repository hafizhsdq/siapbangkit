import { BaseModel, BaseResponse } from "../base-model";

export namespace TemplateAbsensis {
  export interface State {
    response: BaseResponse<TemplateAbsensi>;
  }

    export interface CreateUpdateTemplateAbsensiInput {
      kegiatan: boolean;
      harian: boolean;
      apel: boolean;
      tandaTerima: boolean;
      lainnya: boolean;

      excel: boolean;
      pdf: boolean;
  }
  
    export interface TemplateAbsensi extends BaseModel {
        kegiatan: boolean;
        harian: boolean;
        apel: boolean;
        tandaTerima: boolean;
        lainnya: boolean;

        excel: boolean;
        pdf: boolean;
    }
  
    export enum TemplateAbsensiType {
        kegiatan,
        harian,
        apel,
        tandaTerima,
        lainnya,

        excel,
        pdf
    }
}