import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { UserPermitService } from '../../app/shared/services/user-permit.service';
import { ModelHelper } from '../base-model';
import { UserPermits } from './user-permit';
import { CreateUpdateData, DeleteData, GetData } from './user-permit.action';


@State<UserPermits.State>({
    name: 'userpermit',
    defaults: { response: {} } as UserPermits.State
})
@Injectable()
export class UserPermitState {
    @Selector()
    static getUserPermits(state: UserPermits.State) {
        return {...state, response: ModelHelper.nestInclude(state.response)}
    }

    // @Selector()
    // static getAUserPermits(state: UserPermits.State) {
    //     return {...state, response: ModelHelper.nestInclude(state.response)}
    // }

    constructor(private userpermitservice: UserPermitService) {}
    
    @Action(GetData)
    get(ctx: StateContext<UserPermits.State>, action: GetData) {        
        if (action.id) {
            return this.userpermitservice.getById(action.id, action.payload).pipe(
                tap((response: any) => {
                    ctx.patchState({response})
                })
            )    
        }

        return this.userpermitservice.get(action.payload).pipe(
            tap((response: any) => {
                ctx.patchState({response})
            })
        )
    }

    @Action(CreateUpdateData)
    save(ctx: StateContext<UserPermits.State>, action: CreateUpdateData) {
        // if (action.id) {
        //     return this.userpermitservice.update(action.payload, action.id)
        // }
        return this.userpermitservice.create(action.payload)
    }

    @Action(DeleteData)
    delete(ctx: StateContext<UserPermits.State>, action: DeleteData) {
        return this.userpermitservice.delete(action.id).pipe(
            switchMap(() => {
                return ctx.dispatch(new GetData())
            })
        )
    }
}