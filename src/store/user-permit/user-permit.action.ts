import { GetPage } from "../base-model";
import { UserPermits } from "./user-permit";

export class CreateUpdateData {
    static readonly type = '[UserPermit] create update';
    constructor(public payload: UserPermits.UserPermitsInput, public id?: string) {}
}

export class DeleteData {
    static readonly type = '[UserPermit] delete';
    constructor(public id: string | number) {}
}

export class GetData {
    static readonly type = '[UserPermit] get';
    constructor(public payload?: GetPage, public id?: string | number) {}
}

// export class GetPatricipant {
//     static readonly type = '[UserPermit] get participant';
//     constructor(public payload?: GetPage, public id?: string | number) {}
// }