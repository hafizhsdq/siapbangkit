import { BaseModel, BaseResponse } from "../base-model";

export namespace UserPermits {
    export interface State {
      response: BaseResponse<UserPermit>;
    }

    export interface UserPermitsInput {
        category: string;
        attachments: UserPermitAttachment[],
        employee_id?: string;
    }

    export interface ChangeStatusInput {
        status: string;
        info: string;
        file?: File;
    }

    export interface UserPermit {
        category: string;
        attachments: UserPermitAttachment[]
    }

    export interface UserPermitAttachment {
        name: string;
        file: File;
        originalName: string;
    }
}