import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NbBadgeModule, NbButtonModule, NbCardModule, NbCheckboxModule, NbContextMenuModule, NbDatepickerModule, NbFormFieldModule, NbIconModule, NbInputModule, NbPopoverModule, NbSelectModule, NbSpinnerModule } from "@nebular/theme";
import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { DnsDatePipe } from "../@theme/pipes/dns-date.pipe";
import { FilterPipe } from "../@theme/pipes/filter.pipe";
import { FindPipe } from "../@theme/pipes/find.pipe";
import { JPPipe } from "../@theme/pipes/jp.pipe";
import { MomentPipe } from "../@theme/pipes/moment.pipe";
import { Object2ValuePipe } from "../@theme/pipes/object-to-value.pipe";
import { RolePipe } from "../@theme/pipes/role.pipe";
import { SetPipe } from "../@theme/pipes/set.pipe";
import { SimpleDataPipe } from "../@theme/pipes/simple-data.pipe";
import { StatusPipe } from "../@theme/pipes/status.pipe";
import { ToArrayPipe } from "../@theme/pipes/toArray.pipe";
import { AddDocumentDialog } from "./components/add-document-dialog.component";
import { AnnouncementCardComponent } from "./components/announcement-card/announcement-card.component";
import { ChangeStatusDialog } from "./components/change-status-dialog.component";
import { DetailDiklatComponent } from "./components/detail-diklat/detail-diklat.component";
import { DiklatCardComponent } from "./components/diklat-card/diklat-card.component";
import { PermitTableComponent } from "./components/permit-table/permit-table.component";
import { SimpleTableComponent } from "./components/simple-table.component";
import { StatusButtonComponent } from "./components/status-button.component";
import { TableFilterComponent, DialogDownloadExcelComponent } from "./components/table-filter.component";
import { UploadBerkasComponent } from "./components/upload-berkas/upload-berkas.component";
import { UploadButtonComponent } from "./components/upload-button.component";
import { UploadFileComponent } from "./components/upload-file.component";
import { UploadImageComponent } from "./components/upload-image.component";

const NbModules = [
    NbIconModule,
    NbButtonModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbFormFieldModule,
    NbCheckboxModule,
    NbContextMenuModule,
    NbDatepickerModule,
    NbBadgeModule,
    NbSpinnerModule,
    NbPopoverModule
]

@NgModule({
   declarations: [
    TableFilterComponent,
    Object2ValuePipe,
    SimpleTableComponent,
    UploadFileComponent,
    UploadImageComponent,
    DiklatCardComponent,
    AnnouncementCardComponent,
    DialogDownloadExcelComponent,
    DetailDiklatComponent,
    UploadBerkasComponent,
    PermitTableComponent,
    UploadButtonComponent,
    StatusButtonComponent,
    AddDocumentDialog,
    ChangeStatusDialog,

    SimpleDataPipe,
    FilterPipe,
    DnsDatePipe,
    SetPipe,
    MomentPipe,
    StatusPipe,
    FindPipe,
    RolePipe,
    JPPipe,
    ToArrayPipe
   ],
   imports: [
    CommonModule,
    RouterModule,
    ...NbModules,
    SweetAlert2Module,
    NgxDatatableModule,
    FormsModule,
   ],
   exports: [
    CommonModule,
    ...NbModules,
    NgxDatatableModule,
    FormsModule,
    SweetAlert2Module,

    TableFilterComponent,
    Object2ValuePipe,
    SimpleTableComponent,
    UploadFileComponent,
    UploadImageComponent,
    DiklatCardComponent,
    AnnouncementCardComponent,
    DetailDiklatComponent,
    UploadBerkasComponent,
    PermitTableComponent,
    UploadButtonComponent,
    StatusButtonComponent,
    AddDocumentDialog,
    ChangeStatusDialog,

    SimpleDataPipe,
    FilterPipe,
    DnsDatePipe,
    SetPipe,
    MomentPipe,
    StatusPipe,
    FindPipe,
    RolePipe,
    JPPipe
   ],
   bootstrap: [],
})
export class SharedModule {
}
 