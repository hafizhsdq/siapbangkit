import { Location } from "@angular/common";
import { HttpErrorResponse } from "@angular/common/http";
import { Component, ComponentRef, Injector, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NbDialogService, NbMenuService } from "@nebular/theme";
import { Select, Store } from "@ngxs/store";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import * as moment from "moment";
import { BehaviorSubject, Observable, Subject, Subscription, of } from "rxjs";
import { map, take, tap } from "rxjs/operators";
import { SweetAlertOptions } from "sweetalert2";
import { BaseData, ModelHelper } from "../../../store/base-model";
import { Biodatas } from "../../../store/biodata/biodata";
import { BiodataState } from "../../../store/biodata/biodata.state";
import { RolePipe } from "../../@theme/pipes/role.pipe";
import { SimpegState } from "../../../store/simpeg/simpeg.state";
import { GetDataSimpeg, Simpeg } from "../../../store/simpeg/simpeg";
import { GetBiodatas } from "../../../store/biodata/biodata.action";
import { UserService } from "../services/user.service";
import { BiodataService } from "../services/biodata.service";
import { SimpegService } from "../services/simpeg.service";

@Component({
    selector: "base",
    template: ``
})
export class PageBaseComponent implements OnInit, OnDestroy {

    dialogService: NbDialogService 
    nbMenuService: NbMenuService
    userService: UserService
    _location: Location
    activateRoute: ActivatedRoute
    router: Router
    store: Store
    rolePipe: RolePipe
    deleteSubscriber: Subscription = new Subscription();
    biodataService: BiodataService
    simpegService: SimpegService

    startMonth = moment().startOf('month').format('YYYY-MM-DD')
    endMonth = moment().endOf('month').format('YYYY-MM-DD')

    tag: string = ""

    // store key
    storeKey: string = ""

    vc: ViewContainerRef;

    swalHandler: SwalPopupHandler
    paramsObject$: Observable<any>

    onBioReady = new Subject<any>()

    @Select(SimpegState.getSimpeg)
    simpegData$: Observable<any>

    biodata$: Observable<any>
    nip: string

    get roles (): BaseData<Biodatas.Biodata>[] { return this.store.selectSnapshot(BiodataState.getRoles) }

    constructor(public injector: Injector) {
        this.dialogService = injector.get(NbDialogService)
        this.nbMenuService = injector.get(NbMenuService)
        this._location = injector.get(Location)
        this.vc = injector.get(ViewContainerRef)
        this.activateRoute = injector.get(ActivatedRoute)
        this.router = injector.get(Router)
        this.store = injector.get(Store)
        this.userService = injector.get(UserService)
        this.biodataService = injector.get(BiodataService)
        this.simpegService = injector.get(SimpegService)

        this.swalHandler = new SwalPopupHandler(this.vc)
        this.rolePipe = new RolePipe(this.biodataService)
    }

    ngOnDestroy(): void {
        this.deleteSubscriber.unsubscribe() 
    }

    ngOnInit(): void {
        //store key        
        this.storeKey = this.activateRoute.snapshot.data.storeKey        
        // console.log(this.storeKey);

        this.paramsObject$ = this.activateRoute.queryParamMap
            .pipe(map((params) => {
                return { ...params.keys, ...params };
            }
        ));

        // get biodata
        this.biodata$ = this.biodataService.get({include: 'users,participant.training.training_subject.teaching_phase,user_course.training_schedule.training.training_subject.teaching_phase'})
                            .pipe(tap((result) => {
                                this.nip = result.attributes.nip
                                localStorage.setItem('nip', this.nip)                                
                            }))
    }

    public OnDelete(id: number | string) {
        this.swalHandler.deletePopup.instance.fire()
    }

    public back() {
        this._location.back()
    }

    public doRequest(request: Observable<any>, response?: BehaviorSubject<any>) {        
        const subscribe = request.pipe(take(1)).subscribe({
            next: (result) => {
                this.OnSuccess(result, response)
                subscribe.unsubscribe()
            },
            error: (error) => {
                this.OnError(error, response)
                subscribe.unsubscribe()
            },
            complete: () => {
                this.OnComplete(response)
                subscribe.unsubscribe()
            }
        })
    }

    private OnSuccess(result: any, response: BehaviorSubject<any>) {        
        this.swalHandler.successPopup.instance.fire()
        
        const subscribe = this.swalHandler.successPopup.instance.didClose.subscribe(() => {
            if (response) response.next(result)
            subscribe.unsubscribe()
            // this.deleteSubscriber.remove(subscribe)
        })
        // this.deleteSubscriber.add(subscribe)
    }

    private OnError(error: HttpErrorResponse, response: BehaviorSubject<any>) {             
        this.swalHandler.errorPopup.instance.swalOptions = {
            title: error?.error?.code ?? error.status,
            text: error?.error?.message ?? error.message
        }
        const subscribe = this.swalHandler.errorPopup.instance.confirm.asObservable()
            .pipe(tap(() => {
                if (response) response.error(error)
            }))
            .subscribe(() => {
                subscribe.unsubscribe()
            })
        // this.deleteSubscriber.add(subscribe)

        this.swalHandler.errorPopup.instance.fire()
        // this.dialogService.open(DialogErrorComponent)
    }

    private OnComplete(response: BehaviorSubject<any>) {
        if (response) response.complete()
    }
}

export class SwalPopupHandler {
    public successPopup: ComponentRef<SwalComponent>
    public errorPopup: ComponentRef<SwalComponent>
    public deletePopup: ComponentRef<SwalComponent>
    public alertPopup: ComponentRef<SwalComponent>

    constructor(vc: ViewContainerRef) {
        this.deletePopup = vc.createComponent(SwalComponent)
        this.deletePopup.instance.swalOptions = {
            title: "Hapus Data",
            text: "Apa anda benar ingin menghapus?",
            icon: "question",
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            showCancelButton: true
        }

        this.successPopup = vc.createComponent(SwalComponent)
        this.successPopup.instance.swalOptions = {
            title: "Berhasil",
            text: "Perubahan berhasil dilakukan",
            icon: "success",
        }

        this.errorPopup = vc.createComponent(SwalComponent)
        this.errorPopup.instance.swalOptions = {
            title: "Error",
            text: "Terjadi kesalahan, coba ulangi kembali",
            icon: "error",
        }

        this.alertPopup = vc.createComponent(SwalComponent)
    }

    public alert(options: SweetAlertOptions, {callback, confirm}: any = {confirm: null, callback: null}) {
        this.alertPopup.instance.fire()
        this.alertPopup.instance.swalOptions = {
            icon: "warning",
            ...options,
        }
        const subscribeClose = this.alertPopup.instance.didClose.asObservable().subscribe(() => {
            callback && callback()
            console.log('close');
            
            subscribeClose.unsubscribe()
            subscribeConfirm.unsubscribe()
        })

        const subscribeConfirm = this.alertPopup.instance.confirm.asObservable().pipe(take(1)).subscribe(() => {            
            confirm && confirm()
            subscribeConfirm.unsubscribe()
            subscribeClose.unsubscribe()
        })
    }
}

export class ResponseHelper {
    public static do(observable: Observable<any>, response: BehaviorSubject<any>) {
        const subscribe = observable.subscribe((result) => {
            // success response
            if (response) response.next(result)
            subscribe.unsubscribe()
        }, error => {
            // error respones
            if (response) response.error(error)
            subscribe.unsubscribe()
        }, () => {
            // complete response
            if (response) response.complete()
            subscribe.unsubscribe()
        })
    }
}