import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { NbAuthService } from "@nebular/auth";
import { Store } from "@ngxs/store";
import { Observable } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
import { GetBiodatas } from "../../../store/biodata/biodata.action";
import { BiodataState } from "../../../store/biodata/biodata.state";
import { GetMenu } from "../../../store/menu/menu.state";
import { Roles } from "../../../store/role-permission/role-permission";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild  {

  constructor(private auth: NbAuthService, private store: Store, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    return this.auth.isAuthenticatedOrRefresh()
    .pipe(
      map(authenticated => {        
        if (!authenticated) {
          this.router.navigate(['auth/login']);
          return false
        }

        return true
      })
    );
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> {
    
    return this.canActivate(childRoute, state).pipe(
      mergeMap(() => this.store.dispatch([new GetBiodatas({include: 'users'})])),
      map(result => {        
        const dataRoles = this.store.selectSnapshot(BiodataState.getRoles)
        if (!dataRoles) return true
        // if no data roles means user eligible to show this menu
        if (!result || !childRoute?.data?.roles) return true

        return dataRoles.find(data => {
          const {attributes} = data as any
          const dataPermission = (attributes as Roles.Role).permission
          // roles from routing
          const {roles} = childRoute.data
          
          const foundPermission = dataPermission.find(p => roles[p.name])
          let hasRole = false
          console.log(foundPermission);
          if (foundPermission) {
            const {name, permission} = foundPermission
            hasRole = roles[name].map(r => permission[r]).reduce((p,c) => p && c)
          }

          if (!hasRole) { alert('Anda tidak memiliki hak akses') }
          return hasRole
        }) != null
        
      })
    )
  }
}