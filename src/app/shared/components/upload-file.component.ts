import { Component, ElementRef, EventEmitter, Injector, Input, Output, ViewChild } from "@angular/core";
import { Observable, Subscriber } from "rxjs";
import { PageBaseComponent } from "../base/page-base.component";

export interface FileUpload {
    file: File;
    fileName: string;
    mimeType: string;
    fileSize: number;
    filePath: string;
    id: string;
}

@Component({
    selector: 'ngx-upload',
    template: `
    <nb-form-field (click)="browse()">
        <input nbInput
            [disabled]="disabled"
            fullWidth
            type="text"
            [id]="id"
            [placeholder]="fileName"
            fieldSize="medium">
        
        <button nbButton [disabled]="disabled" nbSuffix status="primary">
            <nb-icon icon="file-outline"></nb-icon>
        </button>
        <input #fileInput [disabled]="disabled" [accept]="accept" [attr.data-file]="(file$ | async)" [id]="id" [name]="id" nbInput type="file" (change)="onChange($event)" style="visibility: hidden; left: 0" class="position-absolute"/>
    </nb-form-field>`
})
export class UploadFileComponent extends PageBaseComponent {
    @Input() id: string;
    @Input() limit: number = 5;
    @Input() accept: string
    @Input() disabled: boolean = false
    @Output() change: EventEmitter<FileUpload> = new EventEmitter<FileUpload>()

    fileName: string = "Browse File"

    @ViewChild("fileInput") fileInput: ElementRef;

    constructor(injector: Injector) {
        super(injector);
    }

    file$: Observable<FileUpload>

    browse = () => this.fileInput.nativeElement.click();

    onChange(e: any) {
        e.preventDefault()
        e.stopPropagation()
        
        if (!this.checkSize(e.target.files[0])) return

        const file: File = e.target.files[0]        
        const fileUpload: FileUpload = {
            file,
            fileName: file.name,
            filePath: '',
            id: e.target.id ?? e.target.name,
            fileSize: file.size,
            mimeType: file.type
        } 
        this.fileName = file.name
        
        // this.change.emit(fileUpload)
        var reader = new FileReader()
        reader.readAsDataURL(file)
        this.file$ = new Observable((observer: Subscriber<FileUpload>) => {
            reader.onload = (e: any) => {
                fileUpload.filePath = reader.result as string
                this.change.emit(fileUpload)

                observer.next(fileUpload)
                observer.complete()
            }
        })
    }

    public checkSize(f: File) {
        var sizeInMb = f.size/1024;
        var sizeLimit= 1024*this.limit; // if you want limit MB
        if (sizeInMb > sizeLimit) {
            alert('Sorry the file exceeds the maximum size of 5 MB!');
            // reset the input (code for all browser)
            return false;
        }
        
        return true
    }
}