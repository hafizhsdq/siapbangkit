import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { NbDialogRef, NbDialogService } from "@nebular/theme";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import * as moment from "moment";
import { ExcelReaderService } from "../services/excel-reader.service";
import { SimpleTableComponent } from "./simple-table.component";

@Component({
    selector: 'ngx-table-filter',
    template: `
    <div class="row justify-content-end">
      <div class="col-auto">
        <button nbButton ghost class="mr-3 text-muted" style="font-size: .8rem; text-decoration: none;" (click)="filterClicked = !filterClicked" *ngIf="display">
            Filter
            <input id="filter" [(ngModel)]="filterClicked" type="checkbox" style="position: absolute; z-index: -1">
        </button>
        <button nbButton status="success" size="small" (click)="downloadExcel()">Download .xls<nb-icon icon="download-outline"></nb-icon> </button>
      </div>
      <div class="col-12" *ngIf="display" [ngStyle]="{'visibility': filterClicked ? 'visible' : 'hidden', 'height': filterClicked ? 'auto' : 0}">
        <nb-card accent="info">
          <nb-card-body>
            <div class="row">
              <div class="col-12">
                <h6>Filter</h6>
              </div>
            </div>
            <form #f="ngForm">
              <div class="row">
                <div class="col" *ngIf="display['search']">
                  <nb-form-field>
                    <input nbInput
                            fullWidth
                            name="search"
                            type="text"
                            id="search"
                            ngModel #search="ngModel"
                            placeholder="Cari"
                            fieldSize="medium">
                    <nb-icon nbSuffix icon="search-outline"></nb-icon>
                  </nb-form-field>
                </div>
                <div class="col" *ngIf="display['unit']">
                  <nb-select fullWidth placeholder="OPD" name="opd" ngModel #opd="ngModel">
                    <nb-option value="">Semua</nb-option>
                    <nb-option value="Dinas Pariwisata">Dinas Pariwisata</nb-option>
                    <nb-option value="Dinas Kesehatan">Dinas Kesehatan</nb-option>
                    <nb-option value="Dinas Sosial">Dinas Sosial</nb-option>
                  </nb-select> 
                </div>
                <div class="col" *ngIf="display['status']">
                    <ng-container
                      [ngTemplateOutlet]="display['status'].length > 0 ? customStat : defaultStat"></ng-container>
                    
                    <ng-template #customStat>
                      <nb-select fullWidth placeholder="Status" name="status" ngModel #status="ngModel">
                        <nb-option value="">Semua</nb-option>
                        <nb-option *ngFor="let stats of display['status']" [value]="stats">{{stats}}</nb-option>
                      </nb-select>
                    </ng-template>

                    <ng-template #defaultStat>
                      <nb-select fullWidth placeholder="Status" name="status" ngModel #status="ngModel">
                        <nb-option value="">Semua</nb-option>
                        <nb-option value="diproses">Diproses</nb-option>
                        <nb-option value="diterima">Diterima</nb-option>
                        <nb-option value="ditolak">Ditolak</nb-option>
                      </nb-select>
                    </ng-template>
                </div>
                <div class="col" *ngIf="display['date']">
                  <nb-form-field class="p-0">
                    <input fullWidth nbInput id="dateRange" name="dateRange" [nbDatepicker]="formpicker" [ngModel]="f?.value?.dateRange ?? defaultDateRange" #dateRange="ngModel">
                    <nb-rangepicker #formpicker></nb-rangepicker>
                    <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                  </nb-form-field>
                </div>
              </div>
            </form>
          </nb-card-body>
        </nb-card>
      </div>
    </div>`
})
export class TableFilterComponent implements AfterViewInit {

  @Input() columns: string[]
  @Output() output: EventEmitter<any> = new EventEmitter<any>()
  @Input() table: SimpleTableComponent
  @Input() dataTable: DatatableComponent
  @Input() print: string[]
  @Input() display: any = {search: {}, status: {}, date: {}}

  @Input() templateRef: TemplateRef<any>

  @ViewChild('f') f: NgForm

  filterClicked = false
  defaultDateRange = {start: moment().startOf('year').toDate(), end: moment().endOf('year').toDate()}

  constructor(private dialogService: NbDialogService) { 
  }

  downloadExcel() {
    // console.log(this.table?.column);
    // console.log(this.table?.dataTable.headerComponent.columns);
    // console.log(this.table?.dataTable.rows);
    // console.log(this.dataTable?.rows);
    // console.log(this.dataTable?.headerComponent.columns);
    
    // console.log((this.table?.dataTable ?? this.dataTable)?.columns?.map(c => c.name));
    
    this.dialogService.open(DialogDownloadExcelComponent, { 
      hasScroll: true, 
      context: { 
        columns: this.table?.column ?? this.dataTable?.headerComponent.columns.map(c => c.name).filter(c => c !== 'No.' && c !== 'Action'),
        data: this.table?.dataTable?.rows ?? this.dataTable?.rows,
        print: this.print
      }
    })
  }

  ngAfterViewInit(): void {
    this.f?.valueChanges.subscribe(result => {
      this.output.emit({result, display: this.display})
    })
  }
}

@Component ({
  selector: 'ngx-dialog-download-excel',
  template: `
  <form #f="ngForm" (ngSubmit)="submit(f)">
    <nb-card>
      <nb-card-header class="d-flex">
        <p class="p-0 col-8" style="font-size: .9rem; font-weight: bold;">Pilih Kolom untuk Mencetak</p> 
        <nb-checkbox class="col-6" status="primary" name="all" [(ngModel)]="selectAll">Pilih Semua</nb-checkbox>
      </nb-card-header>
      <nb-card-body class="d-inline-block">
        <nb-checkbox *ngFor="let c of columns" class="col-5" status="primary" name="{{c}}" [ngModel]="selectAll" #{{c}}="ngModel">{{ c }}</nb-checkbox>
      </nb-card-body>
      <nb-card-footer class="d-flex justify-content-end">
        <button class="mr-2" nbButton type="button" ghost (click)="cancel()" size="small">Batal</button>
        <button nbButton status="primary" type="submit" size="small">Download .xls</button>
      </nb-card-footer>
    </nb-card>
  `,
  providers: [ExcelReaderService]
})

export class DialogDownloadExcelComponent {

  columns: string[];
  table: DatatableComponent
  data: any
  print: string[]
  selectAll: boolean = false

  @ViewChild('name') name: ElementRef
  constructor(protected ref: NbDialogRef<DialogDownloadExcelComponent>, private excelService: ExcelReaderService) {}

  cancel() {
    this.ref.close();
  }

  submit(f: NgForm) {
    const printAble = this.columns.filter(name => f.value[name])
                                  .map((c, i) => ({name: c, key: this.print[i]}))    

    if (printAble.length > 0) {
      const dataToPrint = this.data.map((d, i) => {
        return printAble.map(p => {          
          const splitKey = p.key.split('|')

          const key = splitKey.find(key => {
            if (key.indexOf(':') > -1) return key
            return this.getDataValue(d, key) != null
          })

          return {[p.name]: key.indexOf(':') > -1 ? key.slice(1) : this.getDataValue(d, key)}
        }).reduce((p,c) => ({...p, ...c}))
      })
      
      this.excelService.exportAsExcelFile(dataToPrint, moment().toISOString())
    }
  }

  getDataValue(i: any, str: string) {
    const keys = str?.split('.') ?? []
    let obj = i
    return keys.reduce((p, c) => {
      // console.log(c);
      // console.log(Object.keys(obj).find(key => key === c));
      
      p = obj && Object.keys(obj).find(key => key === c) ? obj[c] : null
      obj = p
      return obj
    }, obj)
  }

}