import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: 'announce-card',
    templateUrl: './announcement-card.component.html'
})
export class AnnouncementCardComponent implements OnInit {
    @Input() data: any
    constructor() {}

    ngOnInit(): void {
    }
}