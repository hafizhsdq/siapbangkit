import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, mergeMap, switchMap, tap } from 'rxjs/operators';
import { GetBiodatas } from '../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../store/biodata/biodata.state';
import { UserPermits } from '../../../../store/user-permit/user-permit';
import { DeleteData, GetData } from '../../../../store/user-permit/user-permit.action';
import { UserPermitState } from '../../../../store/user-permit/user-permit.state';
import { PageTableStoreComponent } from '../../base/page-table.component'; 
import { UserPermitService } from '../../services/user-permit.service';
import { ChangeStatusDialog } from '../change-status-dialog.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './permit-table.component.html'
})
export class PermitTableComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(UserPermitState.getUserPermits)
  permits$: Observable<UserPermits.UserPermit[]>

  @Select(BiodataState.getUserPermit)
  userPermits$: Observable<UserPermits.UserPermit[]>

  title: string
  isAdmin = false
  constructor(injector: Injector, private userPermitService: UserPermitService) {
    super(injector, GetData, DeleteData)
    this.tag = "pengajuan-menu"    
  }

  ngOnInit(): void {
    this.nbMenuService.onItemClick()
    .pipe(
        filter(({ tag }) => tag === this.tag),
    )
    .subscribe(result => {        
        if (result.item.title == 'Delete') this.OnDelete(result.item.data)
        if (result.item.title == 'Diterima') this.accepting(result.item.data)
        if (result.item.title == 'Ditolak') this.rejecting(result.item.data)
        if (result.item.title == 'Lakukan Verifikasi') this.verifying(result.item.data)
    });

    this.getData()
  }

  public OnDelete(id: string | number): void {
    this.swalHandler.deletePopup.instance.fire()
    this.deleteSubscriber = this.swalHandler.deletePopup.instance.confirm.asObservable()
    .subscribe(() => {
        this.doRequest(this.store.dispatch(new DeleteData(id)).pipe(tap(() => {
          return this.getData()
        })))
        this.deleteSubscriber.unsubscribe()
    })
  }

  public getData(): void {
    this.activateRoute.data.subscribe(({category, title}) => {
      this.title = title
      const roleNames = this.biodataService.roles?.map((role) => role.attributes.name) ?? []      
      if (roleNames.indexOf('system') > -1 || roleNames.indexOf('admin') > -1) {
        this.isAdmin = true
        this.store.dispatch(new GetData({include: 'employee.users', params: `filter[category]=${category}`}))
      } else {
        this.store.dispatch(new GetData({include: 'employee.users', params: `filter[self]=true&filter[category]=${category}`}))
      }

    })
  }

  public accepting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Diterima ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diterima')
        })
  }

  public rejecting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Ditolak ${name} - ${nip}`, data, withFile: false} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'ditolak')
        })
  }

  public verifying(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Proses ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diproses')
        })
  }

  private showConfirmation(data: any, status: string) {
      const {name, nip, info, file, id, jenjang_pendidikan_id, nomor_izin, tanggal_izin, keterangan} = data;
      
      this.swalHandler.alert({
          title: "Ubah Status", 
          text: `Anda merubah ${name} - ${nip}, dengan status ${status}`,
          confirmButtonText: 'Ubah',
          showCancelButton: true,
          icon: 'question'
      }, {
        confirm: () => {
          this.loading = true

          const data = {
            // id: 2675, // Jika ID kosong, maka akan menambahkan sebagai data baru. Jika ID diisi, maka akan mengubah data sesuai ID.
            pegawai_nip: nip, // Required.
            jenjang_pendidikan_id: jenjang_pendidikan_id, // Required.
            nama: name, // Required.
            nomor_izin: nomor_izin, // Required.
            tanggal_izin: tanggal_izin, // Required.
            keterangan: keterangan
          }

          const request$ = status === 'diterima' ? 
          this.simpegService.storeIzinBelajar(data).pipe(
            mergeMap(respons => this.userPermitService.changeStatus(id,  {status, info, file}))
          ) 
          : this.userPermitService.changeStatus(id,  {status, info, file})

          this.doRequest(request$, {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
          } as BehaviorSubject<any>)
        }
      })
  }

  completeSubmit() {this.loading = false}
  successSubmit(result) {
    this.loading = false
    this.getData()
  }
  errorrSubmit(error) {this.loading = false}
}
