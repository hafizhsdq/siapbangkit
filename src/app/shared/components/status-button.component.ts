import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { environment } from "../../../environments/environment";

@Component ({
  selector: 'stat-btn',
  template: `
    <button nbButton [size]="size" [status]="
    resource[0]?.status == 'diproses' ? 'info' : 
    resource[0]?.status == 'diterima' ? 'success' : 
    resource[0]?.status == 'ditolak' ? 'danger' : ''
    " [nbPopover]="templateRef">
        {{resource[0]?.status ?? defaultStat}}
    </button>

    <ng-template #templateRef>
        <nb-card>
            <nb-card-header>
                Riwayat Tindakan
                <!--{{resource | json}}-->
            </nb-card-header>
            <nb-card-body>
                <div class="row justify-content-between">
                    <div class="col">
                        <span class="caption">Info:</span>
                        <p class="text-basic">{{resource[0]?.info ?? 'Belum ada tindakan'}}</p>
                    </div>
                    <div class="col" *ngIf="resource[0]?.filePath">
                        <span class="caption">Lampiran:</span>
                        <p><a target="_blank" [href]="storageUrl+'/'+resource[0]?.filePath" nbButton status="primary" size="tiny"><nb-icon icon="file-outline"></nb-icon></a></p>
                    </div>
                </div>
                <div style="min-width: '350px'" *ngIf="resource[0]">
                    <ngx-datatable
                        class="bootstrap" 
                        [rows]="resource" 
                        [count]="resource.length ?? 0"
                        [scrollbarH]="true"
                        columnMode="force"
                        default>
                        <ngx-datatable-column name="No." width="10">
                            <ng-template let-rowIndex="rowIndex" ngx-datatable-cell-template>
                                {{rowIndex+1}}.
                            </ng-template>
                        </ngx-datatable-column>
                        <ngx-datatable-column name="User" prop="users.attributes.first_name" width="10"></ngx-datatable-column>
                        <!-- <ngx-datatable-column name="Info" prop="info" width="10"></ngx-datatable-column> -->
                        <ngx-datatable-column name="Tgl" prop="created_at" width="10">
                            <ng-template let-row="row" ngx-datatable-cell-template>
                                {{row.created_at | date:'shortDate'}}
                            </ng-template>
                        </ngx-datatable-column>
                        <ngx-datatable-column name="Log" prop="status" width="10"></ngx-datatable-column>
                    </ngx-datatable>
                </div>
            </nb-card-body>
        </nb-card>
    </ng-template>
  `,
})
export class StatusButtonComponent {
    @Input() size: string = 'tiny';
    @Input() data: any[] //activity log
    @Input() defaultStat: string = 'belum diverifikasi'

    storageUrl = environment.storageUrl

    get resource() {        
        if (!this.data) return [null]

        const populateData = (data) => {  
            
            const {attributes, relationships: {users, attachment}} = data
            
            return {
                status: attributes.status, 
                info: attributes.info, 
                users, 
                created_at: attributes.created_at,
                filePath: (attachment && attachment?.attributes?.path)
            }
        }
        
        return this.data.map(d => populateData(d))
    }
}