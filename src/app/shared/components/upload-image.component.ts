import { Component, EventEmitter, Injector, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { Observable, of, Subscriber, throwError } from "rxjs";
import { FileUpload, UploadFileComponent } from "./upload-file.component";

export interface FileImage extends FileUpload {
    image: string | ArrayBuffer;
    width: number;
    height: number;
}

@Component({
    selector: 'ngx-upload-image',
    template: `
    <div (click)="browse()" class="border rounded banner">
        <ng-template #placeholder><nb-icon icon="image-outline"></nb-icon></ng-template>
        <div class="w-100 h-100 p-2" *ngIf="(base64Image$ | async) as imageBase64; else placeholder">
            <img src="{{imageBase64}}" alt="banner"/>
        </div>

        <div class="position-absolute w-100 h-100 p-2">
            <span class="d-flex">
                    <button nbButton status="primary" size="tiny">
                        <nb-icon icon="image-outline"></nb-icon> Upload Banner 325 x 170
                    </button>
            </span>
        </div>
        <input #fileInput accept="image/*" [id]="id" [name]="id" nbInput type="file" (change)="onChange($event)" style="visibility: hidden; left: 0" class="position-absolute"/>
    </div>
    `,
    styles: [
        `.banner {
            min-height: 200px;
            position: relative;
            margin-bottom: .5em;
            display: flex;
            justify-content: center;
            align-items: center;
            position: relative;
        }`
    ]
})
export class UploadImageComponent extends UploadFileComponent implements OnChanges {    

    @Output() change: EventEmitter<FileImage> = new EventEmitter<FileImage>()

    @Input() base64: string | ArrayBuffer

    base64Image$: Observable<string | ArrayBuffer>
    constructor(injector: Injector) {
        super(injector);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['base64'] && changes['base64'].currentValue != changes['base64'].previousValue) {
            this.base64Image$ = of(changes['base64'].currentValue)
        }
    }

    onChange(e: any): void {
        e.preventDefault()
        e.stopPropagation()
        
        if (!this.checkSize(e.target.files[0])) return

        const file: File = e.target.files[0]
        const fileUpload: FileUpload = {
            file: file,
            fileName: file.name,
            filePath: '',
            id: e.target.id ?? e.target.name,
            fileSize: file.size,
            mimeType: file.type
        } 
        this.fileName = file.name
        
        // this.change.emit(fileUpload)

        var reader = new FileReader()
        reader.readAsDataURL(file)
        this.base64Image$ = new Observable((observer: Subscriber<string | ArrayBuffer>) => {
            reader.onload = (ev) => {
                var image = new Image()
                image.src = <string>ev.target.result

                var binaryStr = ev.target.result
                fileUpload.filePath = binaryStr as string

                image.onload = () => {
                    if (image.width != 325 && image.height != 170) {
                        alert('Image resolution is not in the requirement needs!. Please check again, image resolution must be 325 x 170')
                        observer.error('requirement not match')
                    }
                    
                    this.change.emit({
                        image: binaryStr,
                        width: image.width,
                        height: image.height,
                        ...fileUpload
                    })

                    observer.next(binaryStr)
                    observer.complete()
                }
            }

            reader.onerror = (error) => {
                observer.error(error)
            }
        })
    }

    // image$ = (): Observable<> => {

    // }
}