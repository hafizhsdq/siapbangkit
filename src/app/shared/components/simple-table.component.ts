import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from "@angular/core";
import { DatatableComponent, ColumnMode } from "@swimlane/ngx-datatable";
import { basename } from "path";
import { BaseResponse } from "../../../store/base-model";
import { PageTableComponent } from "../base/page-table.component";

@Component({
    selector: 'ngx-simple-table',
    template: `
    <ngx-datatable 
        #dataTable
        class="bootstrap"
        [rows]="response?.data | simply:selectedData"
        [headerHeight]="50"
        [footerHeight]="50"
        rowHeight="auto"
        [externalPaging]="true"
        [count]="response?.meta?.pagination?.total"
        [offset]="response?.meta?.pagination?.current_page-1"
        [limit]="response?.meta?.pagination?.per_page"
        [scrollbarH]="true"
        (page)="setPage($event)"
        [columnMode]="columnMode">
        <ngx-datatable-column name="No." width="10">
            <ng-template let-rowIndex="rowIndex" ngx-datatable-cell-template>
                #{{((response.meta?.pagination?.current_page-1) * response.meta?.pagination?.per_page) + (rowIndex+1)}}
            </ng-template>
        </ngx-datatable-column>
        <ngx-datatable-column *ngFor="let col of column; let i = index" [name]="col">
            <ng-template let-row="row" let-rowIndex="rowIndex" ngx-datatable-cell-template>
                <ng-container 
                    [ngTemplateOutlet]="templateRef ?? defaultData"
                    [ngTemplateOutletContext]="{$implicit: row, col: col, index: i}"></ng-container>
            </ng-template>
        </ngx-datatable-column>
        <ngx-datatable-column *ngIf="!noActionRef" [name]="'Action'">
            <ng-template let-row="row" let-rowIndex="rowIndex" ngx-datatable-cell-template>
                <ng-container 
                    [ngTemplateOutlet]="actionRef ?? defaultAction"
                    [ngTemplateOutletContext]="{$implicit: row}"></ng-container>
            </ng-template>
        </ngx-datatable-column>
    </ngx-datatable>

    <ng-template #defaultAction let-row>
        <button (click)="OnEdit(row.id)" nbButton status="primary" size="small" class="mr-2"><nb-icon icon="edit-outline"></nb-icon></button>
        <button (click)="OnDelete(row.id ?? null)" nbButton status="danger" outline size="small"><nb-icon icon="trash-outline"></nb-icon></button>
    </ng-template>

    <ng-template #defaultData let-row let-col="col" let-i="index">
        {{row | o2value:i}}
    </ng-template>
    `
})
export class SimpleTableComponent {
    @Input() column: any[] = []
    @Input() response: BaseResponse<any>
    @Input() templateRef: TemplateRef<any>
    @Input() actionRef: TemplateRef<any>
    @Input() noActionRef: boolean = false
    @Input() selectedData: string[] = []

    columnMode: ColumnMode = ColumnMode.force

    @ViewChild('dataTable') dataTable: DatatableComponent

    @Output() delete: EventEmitter<any> = new EventEmitter<any>()
    @Output() edit: EventEmitter<any> = new EventEmitter<any>()
    @Output() page: EventEmitter<any> = new EventEmitter<any>()

    OnEdit(id: string) {
        this.edit.emit(id)
    }

    OnDelete(id: string) {        
        this.delete.emit(id)
    }

    setPage({ offset }: { offset: any; }): void {
        this.page.emit(offset)
    }
}