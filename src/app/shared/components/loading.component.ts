import { Component } from "@angular/core";

@Component({
    selector: 'ngx-loading',
    template: `<div class="position-absolute w-100 h-100" style="z-index: 99">
        <nb-card [nbSpinner]="true" nbSpinnerSize="medium" nbSpinnerStatus="primary">
        </nb-card>
    </div>`,
    styles: ['']
})
export class LoadingComponent {}