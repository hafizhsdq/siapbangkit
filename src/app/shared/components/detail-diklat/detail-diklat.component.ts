import { Component, Injector } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetData } from '../../../../store/kalender-diklat/kalender-diklat.action';
import { GetData as GetDataJadwalPengajar } from '../../../../store/jadwal-pengajar/jadwal-pengajar.action';
import { PageBaseComponent } from '../../base/page-base.component';
import { KalenderDiklatState } from '../../../../store/kalender-diklat/kalender-diklat.state';
import { KalenderDiklats } from '../../../../store/kalender-diklat/kalender-diklat';
import { environment } from '../../../../environments/environment';
import { UploadSertifikat } from '../../../pages/administrasi-peserta/semua-diklat/upload-sertifikat.component';
import { PengajaraState } from '../../../../store/jadwal-pengajar/jadwal-pengajar.state';
import { Pengajars } from '../../../../store/jadwal-pengajar/jadwal-pengajar';

@Component({
  selector: 'ngx-detail-diklat',
  templateUrl: './detail-diklat.component.html'
})
export class DetailDiklatComponent extends PageBaseComponent {

    // @Select(AngkatanDiklatState.getAAngkatanDiklat)
    // periode$: Observable<AngkatanDiklats.AngkatanDiklat>

    @Select(KalenderDiklatState.getAKalenderDiklats)
    schedules$: Observable<KalenderDiklats.KalenderDiklat>

    // @Select(FileMateristate.getFiles)
    // files$: Observable<FileMateris.FileMateri[]>;

    @Select(PengajaraState.getLecture)
    tschedule$: Observable<Pengajars.Pengajar>

    isAdmin: boolean = false
    storageUrl = environment.storageUrl
    constructor(injector: Injector, public store: Store) {
        super(injector);

        const id = this.activateRoute.snapshot.paramMap.get('id')
        const data = this.activateRoute.snapshot.data

        this.isAdmin = data.isAdmin ?? false

        let include = 'training,user_course'
        if (!this.isAdmin) {
          include = 'user_course:me(true),training'
        }

        this.store.dispatch([new GetData({include}, id), new GetDataJadwalPengajar({include: 'teaching_material'})])
    }

    uploadFile = (scheduleId: string, startDate: Date, category: string) => {             
      this.dialogService.open(UploadSertifikat, { hasScroll: true, context: { scheduleId, startDate, category } }).onClose.subscribe((result) => {
        if (result == 'success') {
          this.router.navigateByUrl('/pages/administrasi-peserta/sertifikatku')
        }
      }) 
    }
}
