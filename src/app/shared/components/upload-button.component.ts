import { Component, Input } from "@angular/core";
import { UploadFileComponent } from "./upload-file.component";

@Component({
    selector: 'upload-button',
    template: `        
        <button (click)="browse()" nbButton nbSuffix status="{{status}}" size="{{size}}">
            <nb-icon icon="{{icon}}"></nb-icon> {{text}}
        </button>
        <input #fileInput [id]="id" [name]="id" nbInput type="file" (change)="onChange($event)" style="visibility: hidden; left: 0" class="position-absolute"/>
    `
})
export class UploadButtonComponent extends UploadFileComponent {
    @Input() icon: string;
    @Input() text: string;
    @Input() status: string;
    @Input() size: string;
}