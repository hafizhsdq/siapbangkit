import { HttpClient } from "@angular/common/http";
import { Component, Injector, Input } from "@angular/core";
import { Store } from "@ngxs/store";
import { BehaviorSubject, Subscription } from "rxjs";
import { filter, tap } from "rxjs/operators";
import { BaseData } from "../../../../store/base-model";
import { KalenderDiklats } from "../../../../store/kalender-diklat/kalender-diklat";
import { DeleteData } from "../../../../store/kalender-diklat/kalender-diklat.action";
import { PageBaseComponent } from '../../base/page-base.component'
import { KalenderDiklatService } from "../../services/kalender-diklat.service";

@Component({
    selector: 'diklat-card',
    templateUrl: './diklat-card.component.html'
})
export class DiklatCardComponent extends PageBaseComponent {
    @Input() data: BaseData<KalenderDiklats.KalenderDiklat>
    @Input() editing: boolean

    deleteSubscriber: Subscription;
    loading = false

    tag: string
    constructor(injector: Injector, 
        public kalenderService: KalenderDiklatService, 
        public http: HttpClient) {
        super(injector);
        this.tag = "schedule-diklat"
    }

    ngOnInit(): void {
        this.nbMenuService.onItemClick()
        .pipe(
            // tap(item => console.log(item)),
            filter(({ tag }) => tag === "schedule-diklat"),
            // map(({ item: { title, id } }) => title),
        )
        .subscribe(result => {
            if (result.item.title == 'Delete') this.OnDelete(result.item.data);
            if (result.item.title == 'Daftar') this.Register(result.item.data);
        });
    }

    public Register(data: any) {
        const {category, link} = data?.attributes
        if (category == 'internal') {
            const {name, group_name, explanation} = data?.relationships?.training?.attributes
            
            this.swalHandler.alert({
                title: name, 
                text: explanation,
                confirmButtonText: 'Daftar',
                showCancelButton: true,
                icon: 'question'
            }, {
                confirm: () => {
                    this.loading = true
                    this.doRequest(
                        this.kalenderService.register(data.id, {nip: [localStorage.getItem('nip')]}), {
                        next: (result) => this.successSubmit(result),
                        error: (error) => this.errorrSubmit(error),
                        complete: () => this.completeSubmit()
                    } as BehaviorSubject<any>)
                }
            })
        } else {
            window.open(link, '_blank')
        }
    }

    completeSubmit() {this.loading = false}
    successSubmit(result) {this.loading = false}
    errorrSubmit(error) {this.loading = false}

    public OnDelete(id: string): void {
        this.swalHandler.deletePopup.instance.fire()
        const subscribe = this.swalHandler.deletePopup.instance.confirm.asObservable()
        .subscribe(() => {
            this.doRequest(this.store.dispatch(new DeleteData(id)))
            subscribe.unsubscribe()
        })
    }
}