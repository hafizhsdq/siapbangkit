
import { Component, Injector, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserPermits } from '../../../../store/user-permit/user-permit';
import { AddDocumentDialog } from '../add-document-dialog.component';
import { UserPermitService } from '../../services/user-permit.service';
import { BaseResponse } from '../../../../store/base-model';
import { environment } from '../../../../environments/environment';
import { PageBaseComponent } from '../../base/page-base.component';
import { Settings } from '../../../../store/setting/setting';
import { Select } from '@ngxs/store';
import { Settingstate } from '../../../../store/setting/setting.state';
import { FormControl, NgForm, Validators } from '@angular/forms';
import { Settingservice } from '../../services/setting.service';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'ngx-index',
  templateUrl: './upload-berkas.component.html'
})
export class UploadBerkasComponent extends PageBaseComponent {

    @Select(Settingstate.getSettings)
    setting$: Observable<Settings.Setting>

    @ViewChild("f") f: NgForm

    userPermitInput: UserPermits.UserPermitsInput = {category: null, attachments: []}
    id: string
    loading = false
    additional = []
    title: string = ''
    isSetting = false
    config: string
    category: string

    now = moment().toDate()

    jenjang$: Observable<any>

    storageUrl = environment.storageUrl
    constructor(injector: Injector, private settingService: Settingservice, private userPermitService: UserPermitService) {
        super(injector)
        this.tag = "berkas-menu"
    }

    ngOnInit(): void {
        this.id = this.activateRoute.snapshot.paramMap.get('id')        
        this.activateRoute.data.subscribe(({category, title, isAdmin}) => {            
            this.title = title
            this.isSetting = isAdmin
            this.category = category

            if (category === 'study_permit')
                this.jenjang$ = this.simpegService.get('mJenjangPendidikan').pipe(map(({data}) => data))

            this.settingService.get(category).subscribe((result: any) => {
                this.config = result.data.config
                this.additional = this.config.split(',')
            })

            if (this.id) {
                this.userPermitService.getById(this.id, {params: `category=${this.category}`}).subscribe(
                    (result: any) => {                        
                        const data = result as BaseResponse<UserPermits.UserPermitAttachment>
                        const attachments: UserPermits.UserPermitAttachment[] = data.included.map(i => ({id: i.id, ...i.attributes, name: i.attributes.name}))
                        this.userPermitInput.attachments = attachments
                        this.userPermitInput.category = category
                        
                })
            }
        })
    }

    onChangeFile(e) {
        this.f.form.addControl(e.id, new FormControl(e.file, Validators.required))
    }

    trackByIndex(index: number, value: number) {
        return index;
    }

    addNewRequired() {
        this.additional.push('')
        
    }

    removeAdditional(i) {
        this.additional.splice(i, 1)
    }

    openAddNewDoc() { 
        this.dialogService.open(AddDocumentDialog).onClose.subscribe(
            (attachment: UserPermits.UserPermitAttachment) => {
                if (attachment) this.userPermitInput.attachments = [attachment, ...this.userPermitInput.attachments]
            });
    }

    remove(index: number) {
        const config = this.config.split(',')
        config.splice(index, 1)        
        this.config = config.join(',')
    }

    onSubmit(f: NgForm) {
        
        if (this.isSetting) {
            const configArr = [...this.config.split(','), ...this.additional].filter(f => f.length > 0)            
            this.settingService.update(this.category, {config: configArr.join(',')}).subscribe(() => {
                this.back()
            })
        } else {
            const attachments = Object.keys(f.value).map(key => {
                return {name: key, file: f.value[key], originalName: f.value[key].name} as UserPermits.UserPermitAttachment
            })
            this.doRequest(this.userPermitService.create({category: this.category, attachments: attachments}), {
                next: (result) => this.successSubmit(result),
                error: (error) => this.errorrSubmit(error),
                complete: () => this.completeSubmit()
            } as BehaviorSubject<any>)
        }
    }

    successSubmit(result) {
        this.back()
        this.loading = false
    }
    errorrSubmit(error) {
        this.loading = false
    }
    completeSubmit() {
        this.loading = false
    }

    preview(attachment: any) {
        const {path, mimeType} = attachment
        window.open(environment.storageUrl+path, '_blank')
    }
}
