import { Component, ViewChild } from "@angular/core";
import { FormControl, NgForm, Validators } from "@angular/forms";
import { NbDialogRef } from "@nebular/theme";
import { FileUpload } from "./upload-file.component";

@Component ({
    selector: 'add-attachment',
    template: `
    <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
        <nb-card>
            <nb-card-header>
                {{title}}
            </nb-card-header>
            <nb-card-body>
                <div class="form-group row" *ngIf="withFile">
                    <label for="title" class="label col col-form-label">File*
                        <ngx-upload id="file" (change)="onChangeFile($event)" [limit]='2'></ngx-upload>
                        <span class="caption">pdf max 2MB</span>
                    </label>
                </div>
                <div class="form-group row">
                    <label for="title" class="label col col-form-label">Keterangan (optional)
                        <textarea nbInput
                                fullWidth
                                name="info"
                                cols="50"
                                rows="5"
                                ngModel #info="ngModel"
                                fieldSize="medium"></textarea>
                    </label>
                </div>
            </nb-card-body>
            <nb-card-footer class="d-flex justify-content-end">
                <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                <button nbButton type="submit" status="primary" size="small">Simpan</button>
            </nb-card-footer>
        </nb-card>
    </form>
    `
})
export class ChangeStatusDialog {
    title: string
    data: any
    withFile: boolean = true

    constructor(public dialogRef: NbDialogRef<ChangeStatusDialog>) {}

    @ViewChild('f') f: NgForm

    onSubmit(f: NgForm) {
        if (this.withFile && !f.value.file) {
            alert('file harus diisi (surat rekomendasi/surat pengantar/surat tugas)')
            return
        }

        this.dialogRef.close({...this.data, info: f.value.info, file: f.value.file ?? null});
    }

    onChangeFile(e: FileUpload) {
        this.f.form.addControl(e.id, new FormControl(e.file, Validators.required))
    }

    cancel() {
        this.dialogRef.close()
    }
}