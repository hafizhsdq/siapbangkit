import { Component, ViewChild } from "@angular/core";
import { FormControl, NgForm, Validators } from "@angular/forms";
import { NbDialogRef } from "@nebular/theme";
import { UserPermits } from "../../../store/user-permit/user-permit";
import { FileUpload } from "./upload-file.component";

@Component ({
    selector: 'add-attachment',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    {{title}}
                </nb-card-header>
                <nb-card-body>
                    <div class="form-group row mb-3">
                        <label for="title" class="label col-form-label col-4">Judul Dokumen</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="name"
                                type="text"
                                id="title"
                                ngModel required #name="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="file" class="label col-form-label col-4">File</label>
                        <div class="col-sm-8">
                            <ngx-upload id="file" (change)="onChangeFile($event)" [limit]='2'></ngx-upload>
                            <span class="caption">pdf max 2MB</span>
                        </div>
                    </div>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
})
export class AddDocumentDialog {
    title: string = "Tambah Dokumen"
    constructor(public dialogRef: NbDialogRef<AddDocumentDialog>) {}

    @ViewChild('f') f: NgForm

    onSubmit(f: NgForm) {
        console.log(f.value);
        
        const {name, file} = f.value
        this.dialogRef.close({name, file});
    }

    onChangeFile(e: FileUpload) {
        this.f.form.addControl(e.id, new FormControl(e.file, Validators.required))
    }

    cancel() {
        this.dialogRef.close()
    }
}