import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { FileMateris } from '../../../store/file-materi/file-materi';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class FileMateriService extends BaseService<BaseResponse<FileMateris.FileMateri>, FileMateris.FileMateri, FileMateris.CreateUpdateFileMateriInput> {
  constructor(public restService: RestService) {
      super(restService);
      this.baseUrl = "/teaching-material"
  }

  create(payload: FileMateris.CreateUpdateFileMateriInput): Observable<FileMateris.FileMateri> {
    // change to form data
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        formData.append(key, payload[key])
    })

    return this.restService.request<FormData, FileMateris.FileMateri>({
      method: 'POST',
      url: this.baseUrl,
      body: formData
    }); 

  }

  view(id: string | number) {
    return this.restService.request<void, {url: string}>({
      method: 'GET',
      url: this.baseUrl+`/${id}/show`,
    });
  }
}