import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { UsulanDiklats } from '../../../store/usulan-diklat/usulan-diklat';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class UsulanDiklatService extends BaseService<BaseResponse<UsulanDiklats.UsulanDiklat>, UsulanDiklats.UsulanDiklat, UsulanDiklats.CreateUpdateUsulanDiklatInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/training-suggestion"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<UsulanDiklats.UsulanDiklat> {
    return this.restService.request<{[nip: string] : string[]}, UsulanDiklats.UsulanDiklat>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}