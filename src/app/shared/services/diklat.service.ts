import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Diklats } from '../../../store/diklat/diklat';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class DiklatService extends BaseService<BaseResponse<Diklats.Diklat>, Diklats.Diklat, Diklats.CreateUpdateDiklatInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/training"
  }

  addSubject(payload: {training_subject_id: (string | number)[]}, id: string | number): Observable<BaseResponse<Diklats.Diklat>> {
    return this.restService.request<{training_subject_id: (string | number)[]}, BaseResponse<Diklats.Diklat>>({
        method: 'PUT',
        url: this.baseUrl+`/${id}/subject`,
        body: payload
    }); 
  }
}