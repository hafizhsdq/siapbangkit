import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { PendidikanFormals } from '../../../store/pendidikan-formal/pendidikan-formal';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';
import * as moment from 'moment'

@Injectable({
  providedIn: 'root',
})
export class PendidikanFormalService extends BaseService<BaseResponse<PendidikanFormals.PendidikanFormal>, PendidikanFormals.PendidikanFormal, PendidikanFormals.CreateUpdatePendidikanFormalInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/user-education"
  }

  create(payload: PendidikanFormals.CreateUpdatePendidikanFormalInput): Observable<PendidikanFormals.PendidikanFormal> {
    // change to form data
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        const value = payload[key] instanceof Date ? moment(payload[key]).format() : payload[key]
        formData.append(key, value)
    })

    return this.restService.request<FormData, PendidikanFormals.PendidikanFormal>({
      method: 'POST',
      url: this.baseUrl,
      body: formData
    }); 

  }

  view(id: string | number) {
    return this.restService.request<void, {url: string}>({
      method: 'GET',
      url: this.baseUrl+`/${id}/show`,
    });
  }

  // register(id: string | number, payload: {[nip: string] : string[]}): Observable<PendidikanFormals.PendidikanFormal> {
  //   return this.restService.request<{[nip: string] : string[]}, PendidikanFormals.PendidikanFormal>({
  //       method: 'POST',
  //       url: this.baseUrl+`/${id}/register`,
  //       body: payload
  //   }); 
  // }
}