import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { UserCourses } from '../../../store/user-course/user-course';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class UserCourseService extends BaseService<BaseResponse<UserCourses.UserCourse>, UserCourses.UserCourse, UserCourses.UserCourseInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/user-course"
  }

  create(payload: UserCourses.UserCourseInput): Observable<UserCourses.UserCourse> {
    // change to form data
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        const value = payload[key] instanceof Date ? moment(payload[key]).format() : payload[key]
        formData.append(key, value)
    })

    return this.restService.request<FormData, UserCourses.UserCourse>({
      method: 'POST',
      url: this.baseUrl,
      body: formData
    }); 
  }

  changeStatus(id: string, payload: UserCourses.ChangeStatusInput): Observable<void> {
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        formData.append(key, payload[key])
    })
    formData.append('_method', 'PATCH');

    return this.restService.request<FormData, void>({
        method: 'POST',
        url: this.baseUrl+`/${id}`,
        body: formData
    }); 
  }
}