import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AktivitasPesertas } from '../../../store/aktivitas-peserta/aktivitas-peserta';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class NotificationService extends BaseService<any, any, any> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/notif"
  }

  delete(id: string): Observable<any> {
    return this.restService.request<void, void>({
        method: 'DELETE',
        url: this.baseUrl+`/${id}`
    }); 
  }

  read(id: string): Observable<any> {
    return this.restService.request<void, void>({
        method: 'PATCH',
        url: this.baseUrl+`/${id}`
    }); 
  }
}