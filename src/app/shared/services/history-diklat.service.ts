import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class HistoryDiklatService extends BaseService<any, any, any> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/diklat-history"
  }
}