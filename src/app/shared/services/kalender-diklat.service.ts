import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class KalenderDiklatService extends BaseService<BaseResponse<KalenderDiklats.KalenderDiklat>, KalenderDiklats.KalenderDiklat, KalenderDiklats.CreateUpdateKalenderDiklatInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/training-schedule"
  }

  register(id: string | number, {nip, skip = false}: {nip: string[], skip?: boolean}): Observable<void> {
    return this.restService.request<any, void>({
        method: 'PUT',
        url: this.baseUrl+`/${id}/register`,
        body: {nip, skip}
    }); 
  }

  changeStatus(id: string, payload: KalenderDiklats.ChangeStatusInput): Observable<void> {
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        formData.append(key, payload[key])
    })
    formData.append('_method', 'PATCH');

    return this.restService.request<FormData, void>({
        method: 'POST',
        url: this.baseUrl+`/${id}`,
        body: formData
    }); 
  }
}