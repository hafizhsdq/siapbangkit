import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { RestService } from "./rest.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class SimpegService {
    baseUrl = "/simpeg"

    jenisDiklat: any[]
    rumpun: any[]

    constructor(public restService: RestService) {}

    get(param: string): Observable<any> {
        return this.restService.request<void, any>({
            method: 'GET',
            url: this.baseUrl+'/'+param,
        }); 
    }

    getJenisDiklat(id?: string): Observable<any> {
        return this.restService.request<void, any>({
            method: 'GET',
            url: this.baseUrl+'/mJenisDiklat'+(id ? '&id='+id : ''),
        }).pipe(map(({data}) => data ?? [])); 
    }

    getRumpun(id?: string): Observable<any> {
        return this.restService.request<void, any>({
            method: 'GET',
            url: this.baseUrl+'/mRumpun'+(id ? '&id='+id : ''),
        }).pipe(map(({data}) => data ?? [])); 
    }

    storeDiklat(payload: any) {
        return this.restService.request<void, any>({
            method: 'POST',
            url: this.baseUrl+'/tDiklat',
            body: payload
        }).pipe(map(({data}) => data ?? [])); 
    }

    storeIzinBelajar(payload: any) {
        return this.restService.request<void, any>({
            method: 'POST',
            url: this.baseUrl+'/hIzinBelajar',
            body: payload
        }).pipe(map(({data}) => data ?? [])); 
    }
}