import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { UserPermits } from '../../../store/user-permit/user-permit';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class UserPermitService extends BaseService<BaseResponse<UserPermits.UserPermit>, UserPermits.UserPermit, UserPermits.UserPermitsInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/user-permit"
  }

  create(payload: UserPermits.UserPermitsInput): Observable<UserPermits.UserPermit> {
    // change to form data
    const formData: FormData = new FormData()
    formData.append('category', payload.category)
    payload.attachments.forEach(item => {
        formData.append(item.name, item.file)
    })

    return this.restService.request<FormData, UserPermits.UserPermit>({
      method: 'POST',
      url: this.baseUrl,
      body: formData
    }); 
  }

  changeStatus(id: string, payload: UserPermits.ChangeStatusInput): Observable<void> {
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        formData.append(key, payload[key])
    })
    formData.append('_method', 'PATCH');

    return this.restService.request<FormData, void>({
        method: 'POST',
        url: this.baseUrl+`/${id}`,
        body: formData
    }); 
  }
}