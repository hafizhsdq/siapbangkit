import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Diklats } from '../../../store/diklat/diklat';
import { Pengajars } from '../../../store/jadwal-pengajar/jadwal-pengajar';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class PengajarService extends BaseService<BaseResponse<Pengajars.Pengajar>, Pengajars.Pengajar, Pengajars.CreateUpdateJadwalPengajarInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/teacher-schedule"
  }
}