import { Injectable } from '@angular/core';
import { BaseResponse } from '../../../store/base-model';
import { Kbms } from '../../../store/kbm/kbm';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class KbmService extends BaseService<BaseResponse<Kbms.Kbm>, Kbms.Kbm, Kbms.CreateUpdateKbmInput> {
  constructor(public restService: RestService) {
      super(restService);
      this.baseUrl = "/teaching-phase"
  }
}