import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Filters } from '../../../store/filter/filter';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { Pengumumans } from '../../../store/pengumuman/pengumuman';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class FilterService extends BaseService<BaseResponse<Filters.Filter>, Filters.Filter, Filters.CreateUpdateFilterInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = ""
  }

//   register(id: string | number, payload: {[nip: string] : string[]}): Observable<Filters.Filter> {
//     return this.restService.request<{[nip: string] : string[]}, Filters.Filter>({
//         method: 'POST',
//         url: this.baseUrl+`/${id}/register`,
//         body: payload
//     }); 
//   }
}