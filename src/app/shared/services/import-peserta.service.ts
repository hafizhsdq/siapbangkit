import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { ImportPesertas } from '../../../store/import-peserta/import-peserta';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { Pengumumans } from '../../../store/pengumuman/pengumuman';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class ImportPesertaService extends BaseService<BaseResponse<ImportPesertas.ImportPeserta>, ImportPesertas.ImportPeserta, ImportPesertas.CreateUpdateImportPesertaInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/import-participant"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<ImportPesertas.ImportPeserta> {
    return this.restService.request<{[nip: string] : string[]}, ImportPesertas.ImportPeserta>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}