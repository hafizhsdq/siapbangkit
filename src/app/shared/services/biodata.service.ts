import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { BaseResponse, ModelHelper } from '../../../store/base-model';
import { Biodatas } from '../../../store/biodata/biodata';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';
import { map, tap } from 'rxjs/operators';
import { PDFGeneratorService } from './pdf-generator.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class BiodataService extends BaseService<BaseResponse<Biodatas.Biodata>, Biodatas.Biodata, Biodatas.CreateUpdateBiodataInput> {
  private biodata: any
  roles: any
  
  constructor(public restService: RestService, private pdfGenerator: PDFGeneratorService, private userService: UserService) {
      super(restService);
      this.baseUrl = "/user/profile"
  }

  get(payload: any): Observable<BaseResponse<Biodatas.Biodata>> {
    return this.biodata ? of(this.biodata) : super.get(payload).pipe(map((response: any) => {      
      const participants = ModelHelper.getOnly(response.included, 'participant')
      const user_permits = ModelHelper.getOnly(response.included, 'user_permit')
      
      const roles = ModelHelper.getOnly(response.included, 'roles')
      
      try {
          let participant_status = null
          participants.forEach(p => {
              const {relationships: {activity_log}} = p

              if (activity_log.length <= 0 && participant_status && participant_status['unverified'])  {
                  participant_status['unverified'] += 1
              } 
              else if (activity_log.length <= 0) {
                  participant_status = []
                  participant_status['unverified'] = 1
              }
              else if (activity_log.length >= 0 && participant_status && participant_status[activity_log[0].attributes.status]) {
                  participant_status[activity_log[0].attributes.status] += 1
              } 
              else if (activity_log.length >= 0) {
                  participant_status = []
                  participant_status[activity_log[0].attributes.status] = 1
              } 
              // else 
          })

          let user_permit_status = null
          user_permits.forEach(p => {
              const {attributes: {category}, relationships: {activity_log}} = p

              if (activity_log.length <= 0) return

              if (user_permit_status && user_permit_status[category]) {

                  if (user_permit_status[category][activity_log[0].attributes.status]) {
                      user_permit_status[category][activity_log[0].attributes.status] += 1
                  } else {
                      user_permit_status[category][activity_log[0].attributes.status] = 1
                  }
              } else {
                  user_permit_status = []
                  user_permit_status[category] = 1
              }
          })

          this.roles = roles
          
          this.biodata = {...ModelHelper.nestInclude(response).data[0], participant_status, user_permit_status, roles}          
          return this.biodata
      } catch(e) {
          console.log(e);
          
      }
    }))
  }

  update(payload: Biodatas.CreateUpdateBiodataInput, nip: string): Observable<Biodatas.Biodata> {
    return this.restService.request<Biodatas.CreateUpdateBiodataInput, Biodatas.Biodata>({
      method: 'PUT',
      url: this.baseUrl+'/'+nip,
      body: payload
    }); 
  }

  addDoc(payload: Biodatas.AddDocumentInput): Observable<void> {    
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        const value = payload[key] instanceof Date ? moment(payload[key]).format() : payload[key]        
        formData.append(key, value)
    })

    return this.restService.request<FormData, void>({
        method: 'POST',
        url: this.baseUrl,
        body: formData
    }); 
  }

  deleteDoc(id: string | number): Observable<void> {
    return this.restService.request<void, void>({
        method: 'DELETE',
        url: this.baseUrl+'/'+id
    }); 
  }

  // verify(): Observable<void> {
  //   return this.restService.request<void, void>({
  //       method: 'PATCH',
  //       url: this.baseUrl
  //   }); 
  // }

  changeAvatar(avatar: string | ArrayBuffer): Observable<void> {
    return this.restService.request<any, void>({
        method: 'POST',
        url: '/user/avatar',
        body: {avatar}
    }).pipe(tap((response: any) => {
      this.biodata = {...this.biodata, ...ModelHelper.nestInclude(response).data[0]}
    })); 
  }

  changePassword({new_password, old_password}): Observable<void> {
    return this.restService.request<any, void>({
        method: 'PATCH',
        url: '/user',
        body: {old_password, new_password}
    }); 
  }

  downloadBio() {
    this.pdfGenerator.downloadBiodata(
      this.biodata.attributes,
      this.userService.getTotalJP(this.biodata?.relationships?.user_course)
    )
  }

  deleteBio() {
    this.biodata = null
  }
}