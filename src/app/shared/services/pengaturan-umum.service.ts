import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { PengaturanUmums } from '../../../store/pengaturan-umum/pengaturan-umum';
import { Pengumumans } from '../../../store/pengumuman/pengumuman';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class PengaturanUmumService extends BaseService<BaseResponse<PengaturanUmums.PengaturanUmum>, PengaturanUmums.PengaturanUmum, PengaturanUmums.CreateUpdatePengaturanUmumInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/general-settings"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<PengaturanUmums.PengaturanUmum> {
    return this.restService.request<{[nip: string] : string[]}, PengaturanUmums.PengaturanUmum>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}