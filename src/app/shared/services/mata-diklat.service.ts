import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, GetPage } from '../../../store/base-model';
import { FileMateris } from '../../../store/file-materi/file-materi';
import { Kbms } from '../../../store/kbm/kbm';
import { MataDiklats } from '../../../store/mata-diklat/mata-diklat';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class MataDiklatService extends BaseService<BaseResponse<MataDiklats.MataDiklat>, MataDiklats.MataDiklat, MataDiklats.CreateUpdateMataDiklatInput> {
  constructor(public restService: RestService) {
      super(restService);
      this.baseUrl = "/training-subject"
  }

  getPhase(id: string | number, payload: GetPage | any): Observable<Kbms.Kbm[]> {
    let url = this.baseUrl+`/${id}/phase?`

    if (payload) {
      if (payload?.page) url += `page=${payload.page}&`
      if (payload?.per_page) url += `per_page=${payload.per_page}&`
      if (payload?.name) url += `name=${payload.name}&`
    }

    return this.restService.request<void, Kbms.Kbm[]>({
        method: 'GET',
        url: url,
    }); 
  }

  getFile(id: string | number, payload: GetPage | any): Observable<FileMateris.FileMateri[]> {
    let url = this.baseUrl+`/${id}/files?`
    if (payload) {
      if (payload?.page) url += `page=${payload.page}&`
      if (payload?.per_page) url += `per_page=${payload.per_page}&`
      if (payload?.name) url += `name=${payload.name}&`
    }

    return this.restService.request<void, FileMateris.FileMateri[]>({
        method: 'GET',
        url: url,
    }); 
  }
}