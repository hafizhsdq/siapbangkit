import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class ParticipantService extends BaseService<BaseResponse<Participants.Participant>, KalenderDiklats.KalenderDiklat, Participants.RegisterParticipant> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/participant"
  }

  // addParticipant(id: string, payload: {nip: string[], skip: boolean}): Observable<void> {
  //   return this.restService.request<{nip: string[]}, void>({
  //       method: 'PUT',
  //       url: this.baseUrl+`/${id}`,
  //       body: payload
  //   }); 
  // }

  changeStatus(id: string, payload: Participants.ChangeStatusInput): Observable<void> {
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        formData.append(key, payload[key])
    })
    formData.append('_method', 'PATCH');

    return this.restService.request<FormData, void>({
        method: 'POST',
        url: this.baseUrl+`/${id}`,
        body: formData
    }); 
  }

  clear_participant(): Observable<void> {
    return this.restService.request<void, void>({
        method: 'DELETE',
        url: this.baseUrl
    }); 
  }
}