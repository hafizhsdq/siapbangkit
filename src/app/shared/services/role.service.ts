import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { RolePermissions, Roles } from '../../../store/role-permission/role-permission';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class RoleService extends BaseService<BaseResponse<Roles.Role>, Roles.Role, Roles.CreateUpdateRoleInput> {
  constructor(public restService: RestService) {
      super(restService);
      this.baseUrl = "/role"
  }

  getPermissions(): Observable<RolePermissions.Permission[]> {
    return this.restService.request<void, RolePermissions.Permission[]>({
        method: 'GET',
        url: this.baseUrl+`/permission`,
    }); 
  }

  getRolePermissions(roleId: string): Observable<RolePermissions.RolePermission[]> {
    return this.restService.request<void, RolePermissions.RolePermission[]>({
        method: 'GET',
        url: this.baseUrl+`/${roleId}/permission`,
    }); 
  }
}