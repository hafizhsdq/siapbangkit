import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Users } from '../../../store/user/user';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';
import { JPPipe } from '../../@theme/pipes/jp.pipe';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService<BaseResponse<Users.User>, Users.User, Users.CreateUpdateUserInput> {

  private onCurrentUser = new Subject<any>()
  onCurrentUser$ = this.onCurrentUser.asObservable()

  private _currentUser: any

  private jpPipe = new JPPipe()

  getTotalJP (courses) {
      // console.log(this.courses);
      
      if (!courses) return 0

      // const totalJPFromSubject = (training_subject) => {
      //     if (training_subject?.length) {// training subject array
      //         return training_subject.map(x => {
      //             const phase = x.relationships?.teaching_phase
      //             if (phase.length) {
      //                 return phase.map(p => p.attributes.time_allocation).reduce((p,c) => p+c, 0)
      //             } else {
      //                 return phase?.attributes?.time_allocation ?? 0
      //             }
      //         }).reduce((p,c) => p+c, 0)
      //     } else {
      //         return training_subject?.relationships?.teaching_phase?.attributes?.time_allocation ?? 0
      //     }
      // }

      const getTrainingSubject = (course) => {
          return course.relationships?.training_schedule?.relationships?.training?.relationships?.training_subject
      }

      const isAccepted = (course) => {
          const {activity_log} = course.relationships
          return activity_log.length > 0 && activity_log[0].attributes.status == 'diterima'
      }

      if (courses.length) {
          return courses.map(course => {
              if (!isAccepted(course)) return 0

              const training_subject = getTrainingSubject(course)
              if (!training_subject) {
                  return course?.attributes?.jp ?? 0
              }
              return this.jpPipe.transform(training_subject)//totalJPFromSubject(training_subject)
          }).reduce((p,c) => p+c)
      } else {
          if (!isAccepted(courses)) return 0

          const training_subject = getTrainingSubject(courses)
          
          if (training_subject) return this.jpPipe.transform(training_subject)//totalJPFromSubject(training_subject)
          return courses?.attributes?.jp ?? 0
      }
  }

  get currentUser$ () { return this._currentUser ? of(this._currentUser) : this.onCurrentUser$ }

  constructor(public restService: RestService) {
      super(restService);
      this.baseUrl = "/user"
  }

  handleUser(user: any) {
    this._currentUser = user
    this.onCurrentUser.next(user)
  }
}