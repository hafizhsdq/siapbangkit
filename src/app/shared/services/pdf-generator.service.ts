import { Injectable } from '@angular/core';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { Biodatas } from '../../../store/biodata/biodata';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class PDFGeneratorService {

  constructor() { }

  async getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }

  downloadProfilePDF(profile: Biodatas.Biodata) {
    pdfMake.createPdf({
      content: [
        'First paragraph',
        'Another paragraph, this time a little bit longer to make sure, this line will be divided into at least two lines'
      ]
    }).open();
  }

  downloadAbsensi(name: string, participants: any) {    
    pdfMake.createPdf({
      pageOrientation: 'landscape',
      content: [
        {text: 'Absensi Daftar Peserta Diklat', style: 'subheader'},
        {text: name, style: 'header'},
        {
          style: 'tableExample',
          table: {
            widths: ['auto', 'auto', '*', 'auto', 'auto', 'auto', 'auto', 'auto', 50, 50],
            heights: function (row) {
              return row > 0 ? 40 : 25;
            },
            body: [
              [{text: 'No.', style: 'tableHeader'}, {text: 'NIP', style: 'tableHeader'}, {text: 'Nama', style: 'tableHeader'}, {text: 'Unit Kerja', style: 'tableHeader'}, {text: 'Jabatan', style: 'tableHeader'}, {text: 'Jenis Jabatan', style: 'tableHeader'}, {text: 'Tgl. Daftar', style: 'tableHeader'}, {text: 'Status', style: 'tableHeader'}, {text: 'TTD', colSpan: 2, style: 'tableHeader'}, ''],
              ...participants.data
              .map((p, i) => {
                const {
                  relationships: {
                    employee,
                    activity_log,
                    training_periode,
                  },
                  attributes: {
                    register_at
                  }
                } = p

                const {name, nip, department, position, position_type} = employee.attributes
                const status = activity_log.length > 0 ? activity_log[0].attributes.status : 'belum diverifikasi'
                return [(i+1)+'.', nip, name, department, position, position_type, register_at, status, i%2 == 0 ? (i+1)+'.' : '', i%2 != 0 ? (i+1)+'.' : '']
              })
            ]
          }
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
    }).open()
  }

  async downloadBiodata(profile: Biodatas.Biodata, totalJP) {
    if (!profile.avatar) return alert('Lengkapi foto anda terlebih dahulu')
    else {
      pdfMake.createPdf({
        pageOrientation: 'landscape',
        content: [
          {
            text: 'BIODATA '+profile.nama.toUpperCase(), style: {fontSize: 20, bold: true}, alignment: 'center', margin: [100, 10, 100, 20]
          },
          {
              layout: 'noBorders',
              table: {
                  widths: [145, '*'],
                  body: [
                      [
                        {
                            image: profile.avatar,
                            width: 145,
                            height: 200
                        },
                        {
                            layout: {
                              fillColor: function (rowIndex, node, columnIndex) {
                                return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
                              }
                            },
                            table: {
                                widths: [80, '*'],
                                heights: 35,
                                body: [
                                    [
                                        {text: 'NIP', style: {fontSize: 14, bold: true}},
                                        {text: `: ${profile.nip}`}
                                    ],
                                    [
                                        {text: 'Name', style: {fontSize: 14, bold: true}},
                                        {text: `: ${profile.nama}`}
                                    ],
                                    [
                                        {text: 'Jabatan', style: {fontSize: 14, bold: true}},
                                        {text: `: ${profile.jabatan}`}
                                    ],
                                    [
                                        {text: 'Unit Kerja', style: {fontSize: 14, bold: true}},
                                        {text: `: ${profile.unit_organisasi}`}
                                    ],
                                    [
                                        {text: 'JP', style: {fontSize: 14, bold: true}},
                                        {text: `: ${totalJP}`}
                                    ]
                                ]
                            }
                        }
                      ]
                    ]
              }
          }
        ]
      }).open()
    }
  }
}