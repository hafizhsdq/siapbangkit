import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Settings } from '../../../store/setting/setting';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class Settingservice {
    baseUrl = "/setting"
    constructor(public restService: RestService) {}

    get(slug: string): Observable<Settings.Setting> {
        return this.restService.request<void, Settings.Setting>({
            method: 'GET',
            url: this.baseUrl+'/'+slug
        }); 
    }

    update(slug: string, payload: Settings.UpdateSettingInput): Observable<Settings.Setting> {
        return this.restService.request<Settings.UpdateSettingInput, Settings.Setting>({
            method: 'POST',
            url: this.baseUrl+'/'+slug,
            body: payload
        }); 
    }
}