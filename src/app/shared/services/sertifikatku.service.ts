import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Sertifikats } from '../../../store/sertifikatku/sertifikatku';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class SertifikatService extends BaseService<BaseResponse<Sertifikats.Sertifikat>, Sertifikats.Sertifikat, Sertifikats.CreateUpdateSertifikatInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/certificate"
  }

  create(payload: Sertifikats.CreateUpdateSertifikatInput): Observable<Sertifikats.Sertifikat> {
    // change to form data
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        const value = payload[key] instanceof Date ? moment(payload[key]).format() : payload[key]
        formData.append(key, value)
    })

    return this.restService.request<FormData, Sertifikats.Sertifikat>({
      method: 'POST',
      url: this.baseUrl,
      body: formData
    }); 

  }

  view(id: string | number) {
    return this.restService.request<void, {url: string}>({
      method: 'GET',
      url: this.baseUrl+`/${id}/show`,
    });
  }

  // register(id: string | number, payload: {[nip: string] : string[]}): Observable<Sertifikats.Sertifikat> {
  //   return this.restService.request<{[nip: string] : string[]}, Sertifikats.Sertifikat>({
  //       method: 'POST',
  //       url: this.baseUrl+`/${id}/register`,
  //       body: payload
  //   }); 
  // }
}