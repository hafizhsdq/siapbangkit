import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Questionnaires } from '../../../store/questionnaire/questionnaire';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class QuestionnaireService extends BaseService<BaseResponse<Questionnaires.Questionnaire>, Questionnaires.Questionnaire, Questionnaires.CreateUpdateQuestionnaireInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/questionnaire"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<Questionnaires.Questionnaire> {
    return this.restService.request<{[nip: string] : string[]}, Questionnaires.Questionnaire>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}