import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { KaryaTuliss } from '../../../store/karya-tulis/karya-tulis';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class KaryaTulisService extends BaseService<BaseResponse<KaryaTuliss.KaryaTulis>, KaryaTuliss.KaryaTulis, KaryaTuliss.CreateUpdateKaryaTulisInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/user-paper"
  }

  create(payload: KaryaTuliss.CreateUpdateKaryaTulisInput): Observable<KaryaTuliss.KaryaTulis> {
    // change to form data
    const formData: FormData = new FormData()
    Object.keys(payload).forEach(key => {
        const value = payload[key] instanceof Date ? moment(payload[key]).format() : payload[key]
        formData.append(key, value)
    })

    return this.restService.request<FormData, KaryaTuliss.KaryaTulis>({
      method: 'POST',
      url: this.baseUrl,
      body: formData
    }); 

  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<KaryaTuliss.KaryaTulis> {
    return this.restService.request<{[nip: string] : string[]}, KaryaTuliss.KaryaTulis>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}