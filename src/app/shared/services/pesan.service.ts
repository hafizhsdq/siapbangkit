import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Pesans } from '../../../store/pesan/pesan';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class PesanService extends BaseService<BaseResponse<Pesans.Pesan>, Pesans.Pesan, Pesans.CreateUpdatePesanInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/message"
  }

  changeStatus(id: string | number): Observable<Pesans.Pesan> {
    return this.restService.request<void, Pesans.Pesan>({
        method: 'PATCH',
        url: this.baseUrl+`/${id}`
    }); 
  }

  read(id: string | number): Observable<Pesans.Pesan> {
    return this.restService.request<void, Pesans.Pesan>({
        method: 'PUT',
        url: this.baseUrl+`/${id}`
    }); 
  }
}