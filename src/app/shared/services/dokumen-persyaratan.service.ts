import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Dokumens } from '../../../store/dokumen-persyaratan/dokumen-persyaratan';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class DokumenService extends BaseService<BaseResponse<Dokumens.Dokumen>, Dokumens.Dokumen, Dokumens.CreateUpdateDokumenInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/document"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<Dokumens.Dokumen> {
    return this.restService.request<{[nip: string] : string[]}, Dokumens.Dokumen>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}