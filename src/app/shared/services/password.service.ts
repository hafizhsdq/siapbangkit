import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { Passwords } from '../../../store/password/password';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class PasswordService extends BaseService<BaseResponse<Passwords.Password>, Passwords.Password, Passwords.CreateUpdatePasswordInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/password"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<Passwords.Password> {
    return this.restService.request<{[nip: string] : string[]}, Passwords.Password>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}