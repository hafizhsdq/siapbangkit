import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { ManajemenUsers } from '../../../store/manajemen-user/manajemen-user';
import { Sertifikats } from '../../../store/sertifikatku/sertifikatku';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class ManajemenUserService extends BaseService<BaseResponse<ManajemenUsers.ManajemenUser>, ManajemenUsers.ManajemenUser, ManajemenUsers.CreateUpdateManajemenUserInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/user-management"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<ManajemenUsers.ManajemenUser> {
    return this.restService.request<{[nip: string] : string[]}, ManajemenUsers.ManajemenUser>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}