import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AktivitasPesertas } from '../../../store/aktivitas-peserta/aktivitas-peserta';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { Pengumumans } from '../../../store/pengumuman/pengumuman';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class AktivitasPesertaService extends BaseService<BaseResponse<AktivitasPesertas.AktivitasPeserta>, AktivitasPesertas.AktivitasPeserta, AktivitasPesertas.CreateUpdateAktivitasPesertaInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/participant-activity"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<AktivitasPesertas.AktivitasPeserta> {
    return this.restService.request<{[nip: string] : string[]}, AktivitasPesertas.AktivitasPeserta>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}