import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { Pengumumans } from '../../../store/pengumuman/pengumuman';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class PengumumanService extends BaseService<BaseResponse<Pengumumans.Pengumuman>, Pengumumans.Pengumuman, Pengumumans.CreateUpdatePengumumanInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/announcement"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<Pengumumans.Pengumuman> {
    return this.restService.request<{[nip: string] : string[]}, Pengumumans.Pengumuman>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}