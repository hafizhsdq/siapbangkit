import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../store/base-model';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { Participants } from '../../../store/participant/participant';
import { Pengumumans } from '../../../store/pengumuman/pengumuman';
import { TemplateAbsensis } from '../../../store/template-absensi/template-absensi';
import { BaseService } from '../base/base.service';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root',
})
export class TemplateAbsensiService extends BaseService<BaseResponse<TemplateAbsensis.TemplateAbsensi>, TemplateAbsensis.TemplateAbsensi, TemplateAbsensis.CreateUpdateTemplateAbsensiInput> {
  constructor(restService: RestService) {
    super(restService);
    this.baseUrl = "/attendance-template"
  }

  register(id: string | number, payload: {[nip: string] : string[]}): Observable<TemplateAbsensis.TemplateAbsensi> {
    return this.restService.request<{[nip: string] : string[]}, TemplateAbsensis.TemplateAbsensi>({
        method: 'POST',
        url: this.baseUrl+`/${id}/register`,
        body: payload
    }); 
  }
}