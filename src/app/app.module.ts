/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

 import { HashLocationStrategy, LocationStrategy, PathLocationStrategy, registerLocaleData } from '@angular/common';
 import localeId from '@angular/common/locales/id'; 
 registerLocaleData(localeId, 'id'); 

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LOCALE_ID, NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbTimepickerModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { BiodataState } from '../store/biodata/biodata.state';
import { DiklatState } from '../store/diklat/diklat.state';
import { KalenderDiklatState } from '../store/kalender-diklat/kalender-diklat.state';
import { MataDiklatState } from '../store/mata-diklat/mata-diklat.state';
import { RoleState, RolePermissionState } from '../store/role-permission/role.state';
import { AuthInterceptor } from './http-interceptors/auth-interceptor';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { KbmState } from '../store/kbm/kbm.state';
import { FileMateristate } from '../store/file-materi/file-materi.state';
import { ParticipantState } from '../store/participant/participant.state';
import { PengajaraState } from '../store/jadwal-pengajar/jadwal-pengajar.state';
import { UserState } from '../store/user/user.state';
import { QuestionnaireState } from '../store/questionnaire/questionnaire.state';
import { UsulanDiklatState } from '../store/usulan-diklat/usulan-diklat.state';
import { PesanState } from '../store/pesan/pesan.state';
import { PendidikanFormalState } from '../store/pendidikan-formal/pendidikan-formal.state';
import { KaryaTulisState } from '../store/karya-tulis/karya-tulis.state';
import { UserPermitState } from '../store/user-permit/user-permit.state';
import { MenuState } from '../store/menu/menu.state';
import { UserCourseState } from '../store/user-course/user-course.state';
import { Settingstate } from '../store/setting/setting.state';
import { SimpegState } from '../store/simpeg/simpeg.state';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    // NbChatModule.forRoot({
    //   messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    // }),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    NgxsModule.forRoot([
      DiklatState,
      RoleState,
      RolePermissionState,
      BiodataState,
      MataDiklatState,
      KbmState,
      FileMateristate,
      KalenderDiklatState,
      ParticipantState,
      PengajaraState,
      UserState,
      QuestionnaireState,
      UsulanDiklatState,
      PesanState,
      PendidikanFormalState,
      KaryaTulisState,
      UserPermitState,
      UserCourseState,
      MenuState,
      Settingstate,
      SimpegState
    ], {
      developmentMode: !environment.production
    }),
    NgxsLoggerPluginModule.forRoot({disabled: true}),
    SweetAlert2Module.forRoot(),
    NbDateFnsDateModule.forRoot({
      format: 'dd/MM/yyyy'
    }),
    NbTimepickerModule.forRoot()
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useExisting: AuthInterceptor, multi: true },
    // { provide: LocationStrategy, useClass: PathLocationStrategy },
    {
      provide: LOCALE_ID,
      useValue: "id-ID"
    }
  ]
})
export class AppModule {
}
