import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { map, startWith, take } from 'rxjs/operators';
import { data as dataDiklat } from '../shared/data/diklat'
// import Swiper core and required modules
import SwiperCore, { Autoplay, Navigation, Pagination, Scrollbar, A11y } from 'swiper';

// install Swiper modules
SwiperCore.use([Autoplay, Navigation, Pagination, Scrollbar, A11y]);

@Component({
  selector: 'ngx-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LandingComponent implements OnInit {

  images = [
    './assets/images/camera1.jpg',
    './assets/images/camera2.jpg',
    './assets/images/camera3.jpg',
    './assets/images/camera4.jpg'
  ];

  tempData: any[];

  public carouselTileItems$: Observable<number[]>;

  data = dataDiklat

  constructor() {
    this.tempData = [];

    this.carouselTileItems$ = interval(500).pipe(
      startWith(-1),
      take(30),
      map(val => {
        const data = (this.tempData = [
          ...this.tempData,
          this.images[Math.floor(Math.random() * this.images.length)]
        ]);
        return data;
      })
    );
  }

  ngOnInit(): void {
    
  }

  /* It will be triggered on every slide*/
  onmoveFn(data: any) {
    console.log(data);
  }

  trackCarousel(_, item) {
    return item;
  }

  onSwiper([swiper]) {
    console.log(swiper);
  }

  onSlideChange() {
    console.log('slide change');
  }

}
