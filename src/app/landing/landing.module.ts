import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { NbLayoutModule } from '@nebular/theme';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  declarations: [
    LandingComponent
  ],
  imports: [
    CommonModule,
    NbLayoutModule,
    SharedModule,
    SwiperModule,
    RouterModule.forChild([{path: '', component: LandingComponent}])
  ]
})
export class LandingModule { }
