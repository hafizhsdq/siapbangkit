import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment/moment';

@Pipe({ name: 'moment' })
export class MomentPipe implements PipeTransform {

  transform(input: any[], compare: string, option: moment.unitOfTime.Diff = 'minute') {
    if (compare == 'diff')
        return moment(input[0] ?? 0).diff(input[1] ?? 0, option)
    else if(compare == 'add') 
        return moment(input[0] ?? 0).add(input[1] ?? 0, option).format('DD/MM/yyyy')
    else if (compare == 'format')
        return moment(input[0] ?? 0).format('DD/MM/yyyy')
  }
}
