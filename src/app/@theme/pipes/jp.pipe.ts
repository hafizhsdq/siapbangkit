import { Pipe } from '@angular/core';
import { SetPipe } from './set.pipe';

@Pipe({ name: 'jp' })
export class JPPipe extends SetPipe {

    transform(input: any | any[], keys?: string) {        
        const result = super.transform(input, keys)        
        if (result?.length) {// training subject array
            return (result as any[]).map(x => {
                const phase = x.relationships?.teaching_phase

                if (phase?.length) {
                    return phase.map(p => p.attributes.time_allocation).reduce((p,c) => p+c, 0)
                } else {
                    return phase?.attributes?.time_allocation ?? 0
                }
            }).reduce((p,c) => p+c, 0)
        } else {
            return result?.attributes?.time_allocation ?? 0
        }
    }
}
