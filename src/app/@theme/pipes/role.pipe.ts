import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngxs/store';
import { BiodataState } from '../../../store/biodata/biodata.state';
import { MenuState } from '../../../store/menu/menu.state';
import { Roles } from '../../../store/role-permission/role-permission';
import { P } from '../../pages/roles';
import { BiodataService } from '../../shared/services/biodata.service';

interface RoleCheck {
    [key: string]: P[]
}

@Pipe({ name: 'role' })
export class RolePipe implements PipeTransform {

    constructor(public biodataService: BiodataService) {}

    transform(input: any | any[]) {        
        return input.length ? 
            input.filter(({roles}) => roles ? roles.find(r => this.hasRole(r) != null) : true) : 
            this.hasRole(input)
    }

    private hasRole(check) {
        const roles = this.biodataService.roles

        if (!roles) return false
        
        const {attributes} = roles[0] as any
        const dataPermission = (attributes as Roles.Role).permission        
        
        const {name, permission} = dataPermission.find(p => check[p.name])
        const hasRole = check[name].map(r => permission[r]).reduce((p,c) => p && c)
        
        return hasRole
    }
}
