import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngxs/store';
import { BiodataState } from '../../../store/biodata/biodata.state';

@Pipe({ name: 'status' })
export class StatusPipe implements PipeTransform {

  constructor(private store: Store) {}

  getStatus(activityLog) {
    const populateData = (data) => {            
        const {attributes, relationships: {users, attachment}} = data
        
        return {
            status: attributes.status, 
            info: attributes.info, 
            users, 
            created_at: attributes.created_at,
            filePath: (attachment && attachment?.attributes?.path)
        }
    }
    
    return activityLog.length ? activityLog.map(d => populateData(d)) : [populateData(activityLog)]
  }

  getParticipant(participant) {
    const biodata = this.store.selectSnapshot(BiodataState.getBiodatas)
    const check = (data) => {
      const {relationships: {employee}} = data
      return biodata.id == employee.id ? data : null
    }
    
    return participant.length ? participant.find(d => check(d) != null) : check(participant)
  }
  
  transform(participant: any, defaultStat: string = 'belum mendaftar'): string {
    if (!participant) return defaultStat
    
    const me = this.getParticipant(participant)

    if (me) {
      const {relationships: {activity_log}} = me
      if (activity_log) {
        return this.getStatus(activity_log)[0].status
      }
    }    
    return defaultStat
  }
}
