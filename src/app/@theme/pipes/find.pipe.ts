import { Pipe, PipeTransform } from '@angular/core';
import { SetPipe } from './set.pipe';

@Pipe({ name: 'find' })
export class FindPipe extends SetPipe {

  transform(input: any | any[], keys: string, same: string | number) {
    if (!input) return
    if (input?.length) {
      return input.find(i => this.getDataValue(i, keys) == same)
    }
    console.log(this.getDataValue(input, keys));
    
    return this.getDataValue(input, keys) == same && input
  }

  getDataValue(i: any, str: string) {
    const keys = str.split('.')
    let obj = i
    return keys.reduce((p, c) => {
      // console.log(c);
      // console.log(Object.keys(obj).find(key => key === c));
      
      p = obj && Object.keys(obj).find(key => key === c) ? obj[c] : null
      obj = p
      return obj
    }, obj)
  }
}
