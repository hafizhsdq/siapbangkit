import { Pipe, PipeTransform } from '@angular/core';
import { parseISO } from 'date-fns';

@Pipe({ name: 'dns' })
export class DnsDatePipe implements PipeTransform {

  transform(input: string): Date | string {
    const date = parseISO(input).toString() === 'Invalid Date' ? input : parseISO(input)
    return date
  }
}
