import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'o2value' })
export class Object2ValuePipe implements PipeTransform {
  transform(object: any, index: number): any {    
    let key = Object.keys(object)[index]
    return object[key]
  }
}
