import { Pipe, PipeTransform } from '@angular/core';
import { parseISO } from 'date-fns';

@Pipe({ name: 'set' })
export class SetPipe implements PipeTransform {

  transform(input: any | any[], keys: string, option?: string): any {
    if(input.length) {
      return input.map(i => this.getDataValue(i, keys)).filter((value, index, self) => {        
        if (option == 'unique') return this.onlyUnique(value, index, self)
        if (!keys) return value ?? value != null
        return value
      })
    } else {
      return this.getDataValue(input, keys)
    }
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  getDataValue(i: any, str: string) {
    const keys = str?.split('.') ?? []    
    let obj = i
    return keys.reduce((p, c) => {
      // console.log(c);
      // console.log(Object.keys(obj).find(key => key === c));
      
      p = obj && Object.keys(obj).find(key => key === c) ? obj[c] : null
      obj = p
      return obj
    }, obj)
  }
}
