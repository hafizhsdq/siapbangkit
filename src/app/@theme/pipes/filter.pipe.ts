import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filter' })
export class FilterPipe implements PipeTransform {

  transform(input: any, column: string[], compare: boolean = true, operand: string = 'AND') {    
    const value = input?.filter(i => {
        return column.map(c => {
            const filter = c.split(':')
            if (filter[1].length <= 0) return true
            
            if (filter.length > 1) {
              const key = filter[0]
              const compareValue = filter[1].split('.')
              
              return compare ? compareValue.indexOf(this.getDataValue(i, key)) > -1 : compareValue.indexOf(this.getDataValue(i, key)) < 0
            }
            return i == filter[0]
        }).reduce((p, c) => {
          return operand == 'AND' ? p && c : p || c
        }, !compare)
    })    

    // console.log(value);
    
    return value
  }

  getDataValue(i: any, str: string) {
    const keys = str.split('.')
    let obj = i
    return keys.reduce((p, c) => {
      // console.log(c);
      // console.log(Object.keys(obj).find(key => key === c));
      
      p = obj && Object.keys(obj).find(key => key === c) ? obj[c] : null
      obj = p
      return obj
    }, obj)
  }
}
