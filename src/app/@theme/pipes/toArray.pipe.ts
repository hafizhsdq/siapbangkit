import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'toArray' })
export class ToArrayPipe implements PipeTransform {

    transform(input: any, split: string) {        
        return input?.length > 0 ? input?.split(split) : []
    }
}
