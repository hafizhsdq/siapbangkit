import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'simply' })
export class SimpleDataPipe implements PipeTransform {

  transform(input: any, column: string[]): number {
    if (!input?.length) return
    return input?.map(i => {
        return column.map(pattern => {
          const found = pattern.indexOf(':')

          let key = pattern,
              addition = '',
              subtitute = null

          if (found > -1) {
            const template = pattern.split(':')            
            key = template[0]
            addition = template[1]
          }

          const foundSubtitute = key.indexOf('|')
          if (foundSubtitute) {
            const template = key.split('|')
            key = template[0]
            subtitute = template[1]
          }
          
          // console.log(this.getData(i, key));
          
          return this.getData(i, key, addition, subtitute) //{[key]: (i[key]+" "+addition).trim()}
        })
        .reduce((p, c) => ({...p, ...c}), [])
    })
  }

  getData(i: any, str: string, addition: string, subtituteKey: string) {
    const keys = str.split('.')
    let obj = i
    return keys.reduce((p, c) => {
      // console.log(c);
      // console.log(Object.keys(obj).find(key => key === c));
      
      p = obj && Object.keys(obj).find(key => key === c) ? obj[c] : null
      obj = p
      // console.log(p);
      
      return addition.length > 0 ? {[subtituteKey ?? c]: (obj+' '+addition).trim()} : {[subtituteKey ?? c]: obj}//(obj+' '+addition).trim()}
    }, obj)
  }
}
