import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil, tap, filter } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { getDeepFromObject, NbAuthResult, NbAuthService, NB_AUTH_OPTIONS } from '@nebular/auth';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { GetBiodatas } from '../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../store/biodata/biodata.state';
import { Biodatas } from '../../../../store/biodata/biodata';
import { Roles } from '../../../../store/role-permission/role-permission';
import { PDFGeneratorService } from '../../../shared/services/pdf-generator.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { BiodataService } from '../../../shared/services/biodata.service';
import { PageBaseComponent } from '../../../shared/base/page-base.component';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent extends PageBaseComponent {

  // @Select(BiodataState.getBiodatas)
  // biodata$: Observable<Biodatas.Biodata>

  @Select(BiodataState.getRoles)
  role$: Observable<Roles.Role>

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    // {
    //   value: 'cosmic',
    //   name: 'Cosmic',
    // },
    // {
    //   value: 'corporate',
    //   name: 'Corporate',
    // },
  ];

  currentTheme = 'default';

  messageMenu = [ { title: 'pesan', link: 'pages/pesan'}];
  userMenu = [ 
    { title: 'Profile', link: '/pages/profile', icon: 'person-outline' }, 
    { title: 'Download Biodata', icon: 'download-outline' },
    { title: 'Log Out', icon: 'log-out-outline' },
  ];

  strategy: string = ''
  redirectDelay: number = 0

  constructor(injector: Injector, 
              private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private layoutService: LayoutService,
              private notificationService: NotificationService,
              private breakpointService: NbMediaBreakpointsService,
              @Inject(NB_AUTH_OPTIONS) protected options = {},
              protected authService: AuthService) {
                super(injector);
                this.strategy = this.getConfigValue('forms.logout.strategy')

                // this.store.dispatch(new GetBiodatas())
  }

  ngOnInit() {
    super.ngOnInit()

    this.currentTheme = this.themeService.currentTheme;

    // this.userService.getUsers()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((users: any) => this.user = users.nick);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

    this.menuService.onItemClick()
      .pipe(
          // tap(item => console.log(item)),
          filter(({ tag }) => tag === "user-menu"),
          map(({ item: { title } }) => title),
      )
      .subscribe(title => {            
        if (title == 'Log Out') this.logout();
        if (title == 'Download Biodata') {
            this.biodataService.downloadBio()
          }
      });
  }

  logout() {
    this.authService.logout('email').subscribe((result: NbAuthResult) => {      
      const redirect = result.getRedirect();
      if (redirect) {
        this.biodataService.deleteBio()
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  delete(notifId: string) {
    this.notificationService.delete(notifId).subscribe(() => {

    })
  }

  read(notifId: string) {
    this.notificationService.read(notifId).subscribe(() => {
      
    })
  }
}
