import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { NbAuthResult, NbAuthService, NbTokenService, NB_AUTH_STRATEGIES } from "@nebular/auth";
import { map } from "leaflet";
import { forkJoin, Observable, of } from "rxjs";
import { switchMap } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { Users } from "../../store/user/user";

@Injectable({
    providedIn: 'root'
})
export class AuthService extends NbAuthService {
    starategy: string

    constructor(public http: HttpClient, protected tokenService: NbTokenService,
        @Inject(NB_AUTH_STRATEGIES) protected strategies) {
        super(tokenService, strategies)
      }
    
    private getCookie() {
        return this.http.get(environment.baseUrl+'/sanctum/csrf-cookie')
    }
    
    public getAuthUser(): Observable<Users.User> {
        return this.http.get<Users.User>(environment.baseUrl+'/api/user')
    }
    
    authenticate(strategyName: string, data?: any): Observable<NbAuthResult> {
        this.starategy = strategyName
        return this.getCookie().pipe(
          switchMap(() => {
            return super.authenticate(strategyName, {...data, strategy: this.starategy})
          })
        )
    }

    logout(strategyName: string): Observable<NbAuthResult> {
        return this.http.post(environment.baseUrl+'/logout', {}).pipe(
          switchMap(() => {
            return super.logout(strategyName)
          })
        )
    }
}