import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NbAuthComponent } from '@nebular/auth';
import { AuthComponent } from './auth.component';
import { NgxLoginComponent } from './login/login.component';
import { CallbackSSOComponent } from '../callback-sso/callback-sso.component';

export const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
        {
          path: 'login',
          component: NgxLoginComponent
        },
        {
          path: 'callback',
          component: CallbackSSOComponent
        },
        { path: '**', redirectTo: 'login' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgxAuthRoutingModule {
}