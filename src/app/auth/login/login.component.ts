import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NbLoginComponent, NB_AUTH_OPTIONS,} from '@nebular/auth';
import { AuthInterceptor } from '../../http-interceptors/auth-interceptor';
import { AuthService } from '../auth.service';
import { Store } from '@ngxs/store';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks'
import { authCodeFlowConfig } from '../../callback-sso/sso.config';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService, AuthInterceptor]
})
export class NgxLoginComponent extends NbLoginComponent {
  showPassword = false;
  submittedSSO = false;

  constructor(
    service: AuthService, 
    @Inject(NB_AUTH_OPTIONS) protected options = {}, 
    cd: ChangeDetectorRef,
    private oauthService: OAuthService,
    router: Router) {
    super(service, options, cd, router)    
  }

  configureSingleSignOn(){
    this.oauthService.configure(authCodeFlowConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }


  getInputType() {
    if (this.showPassword) {
      return 'text';
    }
    return 'password';
  }

  toggleShowPass() {
    this.showPassword = !this.showPassword
  }
  
  login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;

    this.doLogin('email')
  } 

  loginSSO() {
    this.doLogin('sso')
  }

  private doLogin(strategy: string) {
    this.service.authenticate(strategy, {nip: this.user.email, password: this.user.password})
      .subscribe({
        next: (result) => {          
          const redirect = strategy == 'sso' ? result.getResponse().body.redirect : result.getRedirect();
          if (strategy == 'sso') {
            this.router.navigate(['../auth/callback'])
            return
          }

          if (result.isFailure()) this.errors = ['Kombinasi NIP/Password salah, mohon coba lagi.']
          this.messages = result.getMessages();

          this.submitted = false;
          
          if (redirect) {
            setTimeout(() => {
              this.router.navigateByUrl(redirect);
            }, this.redirectDelay);
          }
          this.cd.detectChanges();
        },
        error: (error) => {
          console.log(error);
          
          this.errors = error
        },
        complete: () => {
          this.submitted = false;
        }
      });
  }
}