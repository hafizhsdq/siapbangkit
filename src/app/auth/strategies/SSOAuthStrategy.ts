import { Injectable } from "@angular/core";
import { NbAuthStrategyClass, NbPasswordAuthStrategy, NbPasswordAuthStrategyOptions } from "@nebular/auth";

@Injectable()
export class SSOStrategy extends NbPasswordAuthStrategy {
    static setup(options: NbPasswordAuthStrategyOptions): [NbAuthStrategyClass, NbPasswordAuthStrategyOptions] {
        return [SSOStrategy, options]; // HERE we make sure our strategy reutrn correct class reference
    }
}

@Injectable()
export class SSOCallbackStrategy extends NbPasswordAuthStrategy {
    static setup(options: NbPasswordAuthStrategyOptions): [NbAuthStrategyClass, NbPasswordAuthStrategyOptions] {
        return [SSOCallbackStrategy, options]; // HERE we make sure our strategy reutrn correct class reference
    }
}