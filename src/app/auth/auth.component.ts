import { Component } from '@angular/core';
import { NbAuthComponent } from '@nebular/auth';

@Component({
    selector: 'auth',
    styleUrls: ['./auth.component.scss'],
    template: `
    <nb-layout>
      <nb-layout-column class="p-0">
      <router-outlet></router-outlet>
      <!--<ngx-login></ngx-login>
        <nb-card>
          <nb-card-body>
            <nb-auth-block>
            </nb-auth-block>
          </nb-card-body>
        </nb-card>-->
      </nb-layout-column>
    </nb-layout>
  `,
})
export class AuthComponent extends NbAuthComponent {
}