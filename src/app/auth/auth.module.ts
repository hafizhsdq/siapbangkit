import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxAuthRoutingModule } from './auth-routing.module';
import { NbAuthJWTToken, NbAuthModule, NbAuthSimpleToken, NbOAuth2AuthStrategy, NbPasswordAuthStrategy } from '@nebular/auth';
import { 
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarModule,
  NbSpinnerModule,
  NbTreeGridModule
} from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';
import { NgxLoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { environment } from '../../environments/environment';
import { SSOCallbackStrategy, SSOStrategy } from './strategies/SSOAuthStrategy';
import { CallbackSSOComponent } from '../callback-sso/callback-sso.component';
import { OAuthModule } from 'angular-oauth2-oidc';

@NgModule({
  providers: [SSOStrategy, SSOCallbackStrategy],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NgxAuthRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbAuthModule.forRoot({
      strategies: [
        SSOStrategy.setup({
          name: 'sso',
          token: {
            class: NbAuthSimpleToken,
            key: 'redirect'
          },
         
          baseEndpoint: environment.baseUrl,
          login: {
            // ...
            endpoint: '/login',
            redirect: {
            success: '/pages',
            failure: null,
            }
          },
          logout: {
            // ...
            endpoint: '/logout',
            redirect: {
            success: '/'
            }
          }
        }),
        SSOCallbackStrategy.setup({
          name: 'callback',
          token: {
            class: NbAuthSimpleToken,
            key: 'access_token'
          },
         
          baseEndpoint: environment.baseUrl,
           login: {
             // ...
             endpoint: '/auth/callback?oauth=sso',
             redirect: {
              success: '/pages',
              failure: null,
             }
           },
           logout: {
             // ...
             endpoint: '/logout',
             redirect: {
              success: '/'
             }
           }
        }),
        NbPasswordAuthStrategy.setup({
          name: 'email',

          token: {
            class: NbAuthSimpleToken,

            key: 'access_token'
          },
         
          baseEndpoint: environment.baseUrl,
           login: {
             // ...
             endpoint: '/login',
             redirect: {
              success: '/pages',
              failure: null,
             }
           },
           logout: {
             // ...
             endpoint: '/logout',
             redirect: {
              success: '/'
             }
           }
        }),
      ],
      forms: {
        login: {
          redirectDelay: 0,
          showMessages: {
            success: true,
          },
        }
      }
    }),
    NbLayoutModule,
    NbSidebarModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NgxDatatableModule,
    NbFormFieldModule,
    NbSpinnerModule,
    OAuthModule.forRoot()
  ],
  declarations: [
    NgxLoginComponent,
    AuthComponent,
    CallbackSSOComponent
  ],
})
export class NgxAuthModule {}