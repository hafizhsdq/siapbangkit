export enum P {
    CREATE = 'create',
    READ = 'read',
    UPDATE = 'update',
    DELETE = 'delete',
    PRINT = 'print',
    VERIFIER = 'verifier'
}

export const getRoles = (key: string, permissions: P[]) => { return {roles: {[key]: permissions}}}