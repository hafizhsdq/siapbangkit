import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-pengaturan',
  templateUrl: './pengaturan.component.html',
  styleUrls: ['./pengaturan.component.scss']
})
export class PengaturanComponent implements OnInit {

  data = [
    {
      "diklat": "Diklat Penyelesaian Sengketa Hukum",
      "jadwal": "April",
      "lokasi": "Gedung STIKES Jayakarta",
      "id": 0
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
