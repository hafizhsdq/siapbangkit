import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailDiklatComponent } from '../../shared/components/detail-diklat/detail-diklat.component';
import { AuthGuard } from '../../shared/guard/auth.guard';
import { getRoles, P } from '../roles';
import { DiklatComponent } from './diklat.component';
import { PencarianDataPesertaComponent } from './pencarian-data-peserta/pencarian-data-peserta.component';
import { PencarianKaryaTulisComponent } from './pencarian-karya-tulis/pencarian-karya-tulis.component';
import { CertificateStatusComponent } from './sertifikat-status/sertifikat-status.component';
import { StatusComponent } from './status/status.component';

const routes: Routes = [
  { path: '',
    component: DiklatComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'kalender',
        loadChildren: () => import('./kalender/kalender.module').then(m => m.KalenderModule)
      },
      {
        path: 'diklat-internal',
        loadChildren: () => import('./diklat-internal/diklat-internal.module').then(m => m.DiklatInternalModule)
      },
      {
        path: 'permohonan',
        component: StatusComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: 'detail/:id',
        component: DetailDiklatComponent,
        data: {...getRoles('training_schedule', [P.READ]), isAdmin: true},
      },
      {
        path: 'sertifikat',
        component: CertificateStatusComponent,
        data: getRoles('user_course', [P.READ]),
      },
      {
        path: 'search-peserta',
        component: PencarianDataPesertaComponent,
        data: getRoles('participant', [P.READ]),
      },
      {
        path: 'search-paper',
        component: PencarianKaryaTulisComponent,
        data: getRoles('user_paper', [P.READ]),
      },
      { path: 'pengaturan', loadChildren: () => import('./pengaturan/pengaturan.module').then(m => m.PengaturanModule) },
      { path: 'pengaturan-mata', loadChildren: () => import('./pengaturan-mata/pengaturan-mata.module').then(m => m.PengaturanMataModule) }
    ] 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiklatRoutingModule { }
