import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiklatRoutingModule } from './diklat-routing.module';
import { DiklatComponent } from './diklat.component';
import { KalenderComponent } from './kalender/kalender.component';
import { NbDatepickerModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { StatusComponent } from './status/status.component';

import { DiklatInternalComponent } from './diklat-internal/diklat-internal.component'; // a plugin!
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PencarianDataPesertaComponent } from './pencarian-data-peserta/pencarian-data-peserta.component';
import { KalenderModule } from './kalender/kalender.module';
import { CertificateStatusComponent } from './sertifikat-status/sertifikat-status.component';
import { PencarianKaryaTulisComponent } from './pencarian-karya-tulis/pencarian-karya-tulis.component';

@NgModule({
  declarations: [
    DiklatComponent,
    KalenderComponent,
    StatusComponent,
    DiklatInternalComponent,
    PencarianDataPesertaComponent,
    CertificateStatusComponent,
    PencarianKaryaTulisComponent
  ],
  imports: [
    ThemeModule,
    CommonModule,
    SharedModule,
    DiklatRoutingModule,
    FormsModule,
    NbDatepickerModule,
    KalenderModule,
  ]
})
export class DiklatModule { }
