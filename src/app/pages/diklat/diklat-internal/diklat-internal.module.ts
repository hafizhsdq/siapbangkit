import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DiklatInternalComponent } from './diklat-internal.component';
import { IndexComponent } from './index/index.component';
import { FormComponent } from './form/form.component';
import { NbCardModule, NbContextMenuModule, NbDatepickerModule } from '@nebular/theme';
import { PesertaComponent } from './peserta/peserta.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from '../../../shared/shared.module';

const routes: Routes = [
  { path: '',
    component: DiklatInternalComponent,
    children: [
      {
        path: 'index',
        component: IndexComponent
      },
      {
        path: 'form',
        component: FormComponent
      },
      {
        path: 'peserta/:id',
        component: PesertaComponent
      },
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full'
      }
    ] 
  },
];

@NgModule({
  declarations: [
    IndexComponent,
    FormComponent,
    PesertaComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    NbDatepickerModule
  ],
  exports: [RouterModule]
})
export class DiklatInternalModule { }
