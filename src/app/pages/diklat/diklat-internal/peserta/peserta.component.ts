import { Component, Injector } from '@angular/core';
import { PageTableComponent } from '../../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-peserta',
  templateUrl: './peserta.component.html',
  styleUrls: ['./peserta.component.scss']
})
export class PesertaComponent extends PageTableComponent {

  dataTable = [
    {
      "name": "Tono Budi Aprianto",
      "time": "2022-07-29",
      "nip": "1305265669656",
      "selection" : "2022-07-31",
      "status": "checkmark-circle-2"
    },
    {
      "name": "Hercules Andreas",
      "time": "2022-07-29",
      "nip": "1305265669656",
      "selection" : "-",
      "status": "checkmark-circle-2-outline"
    },
  ]

  title = ""
  year = new Date().getFullYear();
  constructor(injector: Injector) { 
    super(injector)
  }

  ngOnInit(): void {
  }

}
