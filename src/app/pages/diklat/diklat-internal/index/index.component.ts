import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { NbMenuService } from '@nebular/theme';
import { PageTableComponent } from '../../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableComponent {

  items = [
    { title: 'Detail'}
  ]

  @ViewChild('filter') filter: ElementRef

  isHidden = true
  coba() {
    // this.filter.ElementRef.hidden = this.isHidden
    console.log(this.filter)
  }

  dataDiklat = [
    {
      "id" : 123,
      "jenis" : "Diklat Teknis Pertanahan",
      "angkatan" : "5",
      "peserta" : "150",
      "durasi" : "3",
      "waktu" : "30 Juni 2020 - 2 Agustus 2020",
      "tempat" : "Gedung STIKES Jayakarta PKP Ciracas",
    },
    {
      "id" : 123,
      "jenis" : "Diklat Teknis Pertanahan",
      "angkatan" : "5",
      "peserta" : "150",
      "durasi" : "3",
      "waktu" : "30 Juni 2020 - 2 Agustus 2020",
      "tempat" : "Gedung STIKES Jayakarta PKP Ciracas",
    },
    {
      "id" : 123,
      "jenis" : "Diklat Teknis Pertanahan",
      "angkatan" : "5",
      "peserta" : "150",
      "durasi" : "3",
      "waktu" : "30 Juni 2020 - 2 Agustus 2020",
      "tempat" : "Gedung STIKES Jayakarta PKP Ciracas",
    },
    {
      "id" : 123,
      "jenis" : "Diklat Teknis Pertanahan",
      "angkatan" : "5",
      "peserta" : "150",
      "durasi" : "3",
      "waktu" : "30 Juni 2020 - 2 Agustus 2020",
      "tempat" : "Gedung STIKES Jayakarta PKP Ciracas",
    }
  ]

  constructor(injector: Injector) {
    super(injector)
    this.tag = "diklat-internal-menu"
  }

  ngOnInit(): void {
    super.ngOnInit()
  }

}
