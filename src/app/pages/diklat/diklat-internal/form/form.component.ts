import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ngx-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  
  currentYear = new Date().getFullYear();

  constructor() { }

  ngOnInit(): void {
  }

  OnSubmit(f: NgForm) {
    console.log(f.value);
    
  }

}
