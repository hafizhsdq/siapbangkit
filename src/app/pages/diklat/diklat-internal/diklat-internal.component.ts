import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-diklat-internal',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class DiklatInternalComponent {}
