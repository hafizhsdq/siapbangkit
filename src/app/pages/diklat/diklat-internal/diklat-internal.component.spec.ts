import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiklatInternalComponent } from './diklat-internal.component';

describe('DiklatInternalComponent', () => {
  let component: DiklatInternalComponent;
  let fixture: ComponentFixture<DiklatInternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiklatInternalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiklatInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
