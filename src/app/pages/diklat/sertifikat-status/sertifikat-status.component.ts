import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';
import { KalenderDiklats } from '../../../../store/kalender-diklat/kalender-diklat';
import { GetData, DeleteData } from '../../../../store/user-course/user-course.action';
import { KalenderDiklatState } from '../../../../store/kalender-diklat/kalender-diklat.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { ChangeStatusDialog } from '../../../shared/components/change-status-dialog.component';
import { KalenderDiklatService } from '../../../shared/services/kalender-diklat.service';
import { UserCourseState } from '../../../../store/user-course/user-course.state';
import { UserCourses } from '../../../../store/user-course/user-course';
import { UserCourseService } from '../../../shared/services/user-course.service';
import { environment } from '../../../../environments/environment';
import * as moment from 'moment';

@Component({
  selector: 'ngx-index',
  templateUrl: './sertifikat-status.component.html',
  styleUrls: ['./sertifikat-status.component.scss']
})
export class CertificateStatusComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(UserCourseState.getUserCourses)
  userCourses$: Observable<UserCourses.UserCourse[]>

  storageUrl = environment.storageUrl

  constructor(injector: Injector, private userCourseService: UserCourseService) {
    super(injector, GetData, DeleteData, {
      include: 'employee.users,training_schedule.training.training_subject.teaching_phase'
    })
    this.tag = "permohonan-sertifikat-menu"
  }

  ngOnInit(): void {
      this.nbMenuService.onItemClick()
      .pipe(
          // tap(item => console.log(item)),
          filter(({ tag }) => tag === this.tag),
          // map(({ item: { title, id } }) => title),
      )
      .subscribe(result => {
        if (result.item.title == 'Diterima') this.accepting(result.item.data)
        if (result.item.title == 'Ditolak') this.rejecting(result.item.data)
        if (result.item.title == 'Lakukan Verifikasi') this.verifying(result.item.data)
      });
  
      this.getData()
  }

  public accepting({users, training, id, training_schedule}: any) {

    const {attributes: {first_name, email}} = users 
    // const {attributes: {name, group_name} = null} = training

    const title = `Diterima sertifikat - ${first_name}`
    

    this.dialogService?.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        withFile: false,
        title, data: {id, users, training}} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({users, training, training_schedule, ...result}, 'diterima')
        })
  }

  public rejecting({users, training, id, training_schedule}: any) {
    const {attributes: {first_name, email}} = users 

    const title = `Diterima sertifikat - ${first_name}`

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        withFile: false,
        title, data: {id, users, training}} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({users, training, training_schedule, ...result}, 'ditolak')

        })
  }

  public verifying({users, training, id, training_schedule}: any) {
    const {attributes: {first_name, email}} = users 
    const title = `Diterima sertifikat - ${first_name}`

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title, data: {id, users, training}} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({users, training, training_schedule, ...result}, 'diproses')
        })
  }

  private showConfirmation(data: any, status: string) {
      const {users, training, training_schedule, info, file, id} = data;

      this.swalHandler.alert({
          title: "Ubah Status", 
          text: `Anda merubah sertifikat ${users.attributes.first_name} - ${training?.attributes?.name ?? ''}, dengan status ${status}`,
          confirmButtonText: 'Ubah',
          showCancelButton: true,
          icon: 'question'
      }, {confirm: () => {
          this.loading = true
          
          // handle post simpeg
          const data = {
            id: training_schedule.id, // Jika ID kosong, maka akan menambahkan sebagai data baru. Jika ID diisi, maka akan mengubah data sesuai ID.
            jenis_diklat_id: training_schedule.attributes.jenis_id, // Required.
            // nomor_sk: "TRIAL", // Required.
            // tanggal_sk: "2023-06-01", // Required.
            // pejabat_id: 1, // Required.
            // nama_pejabat_id: 1, // Required.
            diklat_id: training_schedule.id,
            nama_diklat: training_schedule.attributes.name, // Required.
            tanggal_mulai: moment(training_schedule.attributes.startDate).format('YYYY-MM-DD'), // Required.
            tanggal_selesai: moment(training_schedule.attributes.startDate).add(training_schedule.attributes.duration, 'day').format('YYYY-MM-DD'), // Required.
            total_jam: training_schedule.attributes.duration * 24, // Required.
            angkatan: training_schedule.attributes.periode, // Required.
            tempat: training_schedule.attributes.location, // Required.
            // penyelenggara: "TRIAL", // Required.
            rumpun_id: training_schedule.attributes.rumpun_id, // Required.
            keterangan: training_schedule.attributes.info,
            pegawai: [ // Required. Berupa array yang berisikan daftar NIP.
                users.attributes.nip,
            ]
          }

          const request$ = status === 'diterima' ? 
          this.simpegService.storeDiklat(data).pipe(
            mergeMap(respons => this.userCourseService.changeStatus(id, file ? {status, info, file} : {status, info}))
          ) 
          : this.userCourseService.changeStatus(id, file ? {status, info, file} : {status, info})

          this.doRequest(
            request$, {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
          } as BehaviorSubject<any>)
      }})
  }

  completeSubmit() {this.loading = false}
  successSubmit(result) {
    this.loading = false
    this.getData()
  }
  errorrSubmit(error) {this.loading = false}
}
