import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { getRoles, P } from '../../roles';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { IndexComponent } from './index/index.component';
import { PengaturanComponent } from './pengaturan.component';
import { PilihMataComponent } from './pilih-mata/pilih-mata.component';

const routes: Routes = [
  { 
    path: '', 
    component: PengaturanComponent,
    children: [
      { 
        path: '',
        component: IndexComponent,
        data: getRoles('training', [P.READ]),
      },
      { 
        path: 'create-update',
        component: CreateUpdateComponent,
        data: getRoles('training', [P.CREATE]),
      },
      { 
        path: 'update/:id',
        component: CreateUpdateComponent,
        data: getRoles('training', [P.UPDATE]),
      },
      { 
        path: 'pilih-mata/:id',
        component: PilihMataComponent,
        data: getRoles('training_subject', [P.UPDATE]),
      },
      { 
        path: 'pengaturan-angkatan/:id',
        loadChildren: () => import('./pengaturan-angkatan/pengaturan-angkatan.module').then(m => m.PengaturanAngkatanModule)
      },
      { 
        path: 'mata-diklat/:diklatId',
        loadChildren: () => import('../pengaturan-mata/pengaturan-mata.module').then(m => m.PengaturanMataModule)
      },
      { 
        path: 'registrasi-diklat/:id',
        loadChildren: () => import('./registrasi-diklat/registrasi-diklat.module').then(m => m.RegistrasiDiklatModule)
      },
    ]
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PengaturanRoutingModule { }
