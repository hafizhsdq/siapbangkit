import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { IndexComponent } from './index/index.component';
import { PengaturanAngkatanRoutingModule } from './pengaturan-angkatan-routing.module';
import { PengaturanAngkatanComponent } from './pengaturan-angkatan.component';
import { NbRadioModule } from '@nebular/theme';
import { PengaturanPesertaComponent } from './pengaturan-peserta/pengaturan-peserta.component';
import { UnduhAbsensiComponent } from './unduh-absensi/unduh-absensi.component';


@NgModule({
  declarations: [
    PengaturanAngkatanComponent,
    IndexComponent,
    PengaturanPesertaComponent,
    UnduhAbsensiComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NbRadioModule,
    PengaturanAngkatanRoutingModule
  ]
})
export class PengaturanAngkatanModule { }
