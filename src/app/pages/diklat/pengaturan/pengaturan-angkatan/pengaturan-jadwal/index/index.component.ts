import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Pengajars } from '../../../../../../../store/jadwal-pengajar/jadwal-pengajar';
import { DeleteData, GetData } from '../../../../../../../store/jadwal-pengajar/jadwal-pengajar.action';
import { PengajaraState } from '../../../../../../../store/jadwal-pengajar/jadwal-pengajar.state';
import { PageTableStoreComponent } from '../../../../../../shared/base/page-table.component';
import { Biodatas } from '../../../../../../../store/biodata/biodata';
import { PDFGeneratorService } from '../../../../../../shared/services/pdf-generator.service';
import { KalenderDiklatService } from '../../../../../../shared/services/kalender-diklat.service';
import { ModelHelper } from '../../../../../../../store/base-model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(PengajaraState.getLecture)
  tschedule$: Observable<Pengajars.Pengajar>

  schedule$: Observable<any>
  
  constructor(injector: Injector, 
    private scheduleService: KalenderDiklatService,
    private pdfService: PDFGeneratorService) {
    super(injector, GetData, DeleteData, {include: 'training,training_schedule', useId:'training_schedule_id'});
    this.tag = "pengajar-diklat" 
  }

  public getData(): void {
    super.getData()
    
    this.schedule$ = this.scheduleService.getById(this.id, {include: 'training.training_subject.teaching_material'}).pipe(map((result: any) => ModelHelper.nestInclude(result).data[0]))
  }

  downloadPdf() {
    this.pdfService.downloadProfilePDF({} as Biodatas.Biodata)
  }
}
