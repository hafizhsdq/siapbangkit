import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PengaturanJadwalRoutingModule } from './pengaturan-jadwal-routing.module';
import { SharedModule } from '../../../../../shared/shared.module';
import { NbRadioModule, NbTimepickerModule } from '@nebular/theme';
import { IndexComponent } from './index/index.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { PengaturanJadwalComponent } from './pengaturan-jadwal.component';


@NgModule({
  declarations: [
    PengaturanJadwalComponent,
    IndexComponent,
    CreateUpdateComponent
  ],
  imports: [
    CommonModule,
    PengaturanJadwalRoutingModule,
    NbRadioModule,
    NbTimepickerModule,
    SharedModule
  ]
})
export class PengaturanJadwalModule { }
