import { Component } from '@angular/core';

@Component({
  selector: 'ngx-pengaturan-jadwal',
  template: `<router-outlet></router-outlet>`
})
export class PengaturanJadwalComponent {}
