import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Pengajars } from '../../../../../../../store/jadwal-pengajar/jadwal-pengajar';
import { CreateUpdateData, GetData } from '../../../../../../../store/jadwal-pengajar/jadwal-pengajar.action';
import { PengajaraState } from '../../../../../../../store/jadwal-pengajar/jadwal-pengajar.state';
import { CreateEditStoreBaseComponent } from '../../../../../../shared/base/create-edit-base.component';
import * as moment from 'moment/moment';
import { map } from 'rxjs/operators';
import { FileMateriService } from '../../../../../../shared/services/file-materi.service';
@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends CreateEditStoreBaseComponent<CreateUpdateData, Pengajars.CreateUpdateJadwalPengajarInput, GetData, Pengajars.Pengajar> {

  @Select(PengajaraState.getLecture)
  tschedule$: Observable<Pengajars.Pengajar>

  fileMateri$: Observable<any>

  total_minutes$: Observable<any>

  trainingId: any
  scheduleId: string
  constructor(injector: Injector, private fileMaterialService: FileMateriService) {    
    super(injector, 'pengajar', CreateUpdateData, GetData, {include: 'teaching_material'});
  }

  ngOnInit(): void {
    this.scheduleId = this.activateRoute.snapshot.paramMap.get('angkatanId')
    this.trainingId = this.activateRoute.parent.parent.parent.snapshot.paramMap.get('id')
    this.fileMateri$ = this.fileMaterialService.get({params: 'filter[training_id]='+this.trainingId}).pipe(map(({data}) => data))
  }

  ngAfterViewInit(): void {
    // calculation start time and end time
    setTimeout(() => {
      this.total_minutes$ = this.f.valueChanges.pipe(map((data: Pengajars.CreateUpdateJadwalPengajarInput) => {        
        const {start_time, end_time} = data
        // console.log(moment(end_time ?? start_time).diff(start_time, 'minutes'));
        
        return moment(end_time ?? start_time).diff(start_time, 'minutes') ?? 0
      }))
    }, 1000);

    super.ngAfterViewInit()
  }

  successSubmit = (result) => {
    this.back()
  }
}
