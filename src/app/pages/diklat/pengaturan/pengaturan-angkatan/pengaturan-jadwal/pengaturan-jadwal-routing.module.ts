import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { getRoles, P } from '../../../../roles';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { IndexComponent } from './index/index.component';
import { PengaturanJadwalComponent } from './pengaturan-jadwal.component';

const routes: Routes = [{ 
  path: '', 
  component: PengaturanJadwalComponent,
  children: [
    {
      path: ':id',
      component: IndexComponent,
      data: getRoles('teacher_schedule', [P.READ]),
    },
    { 
      path: ':angkatanId/create',
      component: CreateUpdateComponent,
      data: getRoles('teacher_schedule', [P.CREATE]),
    },
    { 
      path: ':angkatanId/update/:id',
      component: CreateUpdateComponent,
      data: getRoles('teacher_schedule', [P.UPDATE]),
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PengaturanJadwalRoutingModule { }
