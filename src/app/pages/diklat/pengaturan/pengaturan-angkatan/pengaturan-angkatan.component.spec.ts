import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengaturanAngkatanComponent } from './pengaturan-angkatan.component';

describe('PengaturanAngkatanComponent', () => {
  let component: PengaturanAngkatanComponent;
  let fixture: ComponentFixture<PengaturanAngkatanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PengaturanAngkatanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PengaturanAngkatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
