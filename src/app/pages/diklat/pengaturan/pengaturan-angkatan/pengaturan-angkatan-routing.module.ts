import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { getRoles, P } from '../../../roles';
import { CreateUpdateComponent } from '../../kalender/create-update/create-update.component';
import { InternalComponent } from '../../kalender/create-update/internal-form.component';
import { IndexComponent } from './index/index.component';
import { PengaturanAngkatanComponent } from './pengaturan-angkatan.component';
import { PengaturanPesertaComponent } from './pengaturan-peserta/pengaturan-peserta.component';
import { UnduhAbsensiComponent } from './unduh-absensi/unduh-absensi.component';

const routes: Routes = [{ 
  path: '', 
  component: PengaturanAngkatanComponent,
  children: [
    {
      path: '',
      component: IndexComponent,
      data: getRoles('training_periode', [P.READ]),
    },
    { 
      path: 'pengaturan-peserta/:id',
      component: PengaturanPesertaComponent,
      data: {...getRoles('training_periode', [P.UPDATE]), storeKey: 'angkatanId'},
    },
    { 
      path: 'update/:id/type/:type',
      component: CreateUpdateComponent,
      data: {...getRoles('training_periode', [P.UPDATE]), storeKey: 'angkatanId'},
    },
    { 
      path: 'create/:type',
      component: CreateUpdateComponent,
      data: {...getRoles('training_periode', [P.UPDATE]), storeKey: 'diklat'},
      // children: [
      //   {
      //     path: 'internal/:diklatId',
      //     component: InternalComponent,
      //   }
      // ]
    },
    { 
      path: 'jadwal',
      loadChildren: () => import('./pengaturan-jadwal/pengaturan-jadwal.module').then(m => m.PengaturanJadwalModule),
    },
    { 
      path: 'unduh-absensi/:id',
      component: UnduhAbsensiComponent,
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PengaturanAngkatanRoutingModule { }
