import { Component } from '@angular/core';

@Component({
  selector: 'ngx-pengaturan-angkatan',
  template: `<router-outlet></router-outlet>`
})
export class PengaturanAngkatanComponent {}
