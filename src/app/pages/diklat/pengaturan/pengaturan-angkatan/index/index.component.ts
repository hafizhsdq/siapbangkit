import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { PageTableStoreComponent } from '../../../../../shared/base/page-table.component';
import { DeleteData, GetData } from '../../../../../../store/kalender-diklat/kalender-diklat.action';
import { DiklatState } from '../../../../../../store/diklat/diklat.state';
import { Diklats } from '../../../../../../store/diklat/diklat';
import { GetData as GetDataDiklat } from '../../../../../../store/diklat/diklat.action';
import { take } from 'rxjs/operators';
import { ParticipantService } from '../../../../../shared/services/participant.service';
import { PDFGeneratorService } from '../../../../../shared/services/pdf-generator.service';
import { BaseResponse, ModelHelper } from '../../../../../../store/base-model';
import { Participants } from '../../../../../../store/participant/participant';
import { KalenderDiklatState } from '../../../../../../store/kalender-diklat/kalender-diklat.state';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedule$: Observable<any>

  @Select(DiklatState.getDiklat)
  diklat$: Observable<Diklats.Diklat>
  
  constructor(injector: Injector, private participantService: ParticipantService, private pdfGenerator: PDFGeneratorService) {
    super(injector, GetData, DeleteData, {include: 'training.training_subject', useId: 'training_id'});
    this.tag = "angkatan-diklat"

    this.id = this.activateRoute.snapshot.paramMap.get('id')    
    this.store.dispatch(new GetDataDiklat({}, this.id))
  }

  assignTableAction({item}) {
    if (item.title == 'Delete') this.OnDelete(item.data);
    if (item.title == 'Edit') this.OnEdit(item.data);
    if (item.title == 'Unduh Template Absensi') this.downloadAbsentTemplate(item.data);
  }

  downloadAbsentTemplate(name: string) {
    this.participantService
    .get({include: 'employee,training', params: 'training='+this.id})
    .pipe(take(1))
    .subscribe((result: BaseResponse<Participants.Participant>) => {      
      if (result.data.length > 0) {
        // console.log(ModelHelper.nestInclude(result));
        this.pdfGenerator.downloadAbsensi(name, ModelHelper.nestInclude(result))
      }
    })
  }
}
