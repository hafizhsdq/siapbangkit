import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnduhAbsensiComponent } from './unduh-absensi.component';

describe('UnduhAbsensiComponent', () => {
  let component: UnduhAbsensiComponent;
  let fixture: ComponentFixture<UnduhAbsensiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnduhAbsensiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnduhAbsensiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
