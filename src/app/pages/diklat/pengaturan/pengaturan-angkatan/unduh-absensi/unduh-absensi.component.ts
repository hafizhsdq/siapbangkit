import { Component, Injector, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Diklats } from '../../../../../../store/diklat/diklat';
import { DiklatState } from '../../../../../../store/diklat/diklat.state';
import { PageBaseComponent } from '../../../../../shared/base/page-base.component';
import { KalenderDiklatService } from '../../../../../shared/services/kalender-diklat.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ngx-unduh-absensi',
  templateUrl: './unduh-absensi.component.html',
  styleUrls: ['./unduh-absensi.component.scss']
})
export class UnduhAbsensiComponent extends PageBaseComponent {

  options = [
    { value: '1', label: 'Absensi Kegiatan' },
    { value: '2', label: 'Absensi Harian'},
    { value: '3', label: 'Absensi Apel' },
    { value: '4', label: 'Tanda Terima ATK'},
    { value: '5', label: 'Lainnya' },
  ];

  schedulePeriode$: Observable<any>

  @Select(DiklatState.getADiklat)
  diklat$: Observable<Diklats.Diklat>

  id: string
  constructor(injector: Injector, 
    private scheduleService: KalenderDiklatService) {
    super(injector);

    this.id = this.activateRoute.snapshot.paramMap.get('id')
    this.schedulePeriode$ = this.scheduleService.getById(this.id).pipe(map(({data}: any) => data[0].attributes.periode))
  }

  onSubmit(f: NgForm) {
    console.log(f.value);
  }
}
