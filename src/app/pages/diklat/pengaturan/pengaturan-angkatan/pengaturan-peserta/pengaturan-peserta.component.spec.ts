import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengaturanPesertaComponent } from './pengaturan-peserta.component';

describe('PengaturanPesertaComponent', () => {
  let component: PengaturanPesertaComponent;
  let fixture: ComponentFixture<PengaturanPesertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PengaturanPesertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PengaturanPesertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
