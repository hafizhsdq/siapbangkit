import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { Participants } from '../../../../../../store/participant/participant';
import { DeleteData, GetData } from '../../../../../../store/participant/participant.action';
import { GetData as GetAngkatanData } from '../../../../../../store/kalender-diklat/kalender-diklat.action';
import { ParticipantState } from '../../../../../../store/participant/participant.state';
import { PageTableStoreComponent } from '../../../../../shared/base/page-table.component';
import { BaseData } from '../../../../../../store/base-model';
import { RegisterDialogComponent } from '../../registrasi-diklat/tambah-peserta/register-dialog.component';
import { UploadRegistarDialogComponent } from '../../registrasi-diklat/tambah-peserta/upload-registar-dialog.component';
import { UploadSertifikat } from '../../../../administrasi-peserta/semua-diklat/upload-sertifikat.component';
import { ChangeStatusDialog } from '../../../../../shared/components/change-status-dialog.component';
import { ParticipantService } from '../../../../../shared/services/participant.service';
import { KalenderDiklatState } from '../../../../../../store/kalender-diklat/kalender-diklat.state';

@Component({
  selector: 'ngx-pengaturan-peserta',
  templateUrl: './pengaturan-peserta.component.html',
  styleUrls: ['./pengaturan-peserta.component.scss']
})
export class PengaturanPesertaComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(KalenderDiklatState.getAKalenderDiklats)
  schedule$: Observable<any>

  @Select(ParticipantState.getParticipant)
  participants$: Observable<Participants.Participant[]>

  constructor(injector: Injector, private participantService: ParticipantService) {
    super(injector, GetData, DeleteData, {include: 'employee,training_schedule', useId: 'training_schedule_id'});

    this.id = this.activateRoute.snapshot.paramMap.get('id')
    this.store.dispatch(new GetAngkatanData({include: 'training'}, this.id))
  }

  register = (schedule) => {
    const dialog = this.dialogService.open(RegisterDialogComponent, { hasScroll: true })
    dialog.componentRef.instance.scheduleId = schedule.id
    dialog.onClose.subscribe(() => {this.getData()})
  }

  upload = (schedule) => {
    const dialog = this.dialogService.open(UploadRegistarDialogComponent, { hasScroll: true })
    dialog.componentRef.instance.scheduleId = schedule.id
    dialog.onClose.subscribe(() => {this.getData()})
  }

  uploadSertifikat = (scheduleId: string, startDate: Date, category: string) => {         
    this.dialogService.open(UploadSertifikat, { hasScroll: true, context: { scheduleId, startDate, category } }).onClose.subscribe((result) => {
      if (result == 'success') {
        this.router.navigate(['../sertifikatku'], {relativeTo: this.activateRoute})
      }
    }) 
  }

  downloadDaftarPeserta = () => {
    
  }

  downloadBiodataPeserta = () => {

  }

  clearPeserta = () => {
    this.swalHandler.alert({
        title: "Kosongkan Peserta", 
        text: `Anda yakin kosongkan seluruh peserta?`,
        confirmButtonText: 'Hapus',
        confirmButtonColor: '#ff3d71',
        showCancelButton: true,
        icon: 'question'
    }, {
      confirm: () => {
        this.loading = true
        this.doRequest(this.participantService.clear_participant(), {
          next: (result) => this.successSubmit(result),
          error: (error) => this.errorrSubmit(error),
          complete: () => this.completeSubmit()
        } as BehaviorSubject<any>)
      }
    })
  }

  assignTableAction({item}) {
    if (item.title == 'Delete') this.OnDelete(item.data)
    if (item.title == 'Diterima') this.accepting(item.data)
    if (item.title == 'Ditolak') this.rejecting(item.data)
    if (item.title == 'Lakukan Verifikasi') this.verifying(item.data)
    if (item.title == 'Upload Sertifikat') {
      // console.log(item.data);
      const {scheduleId, category, startDate} = item.data
      this.uploadSertifikat(scheduleId, category, startDate)
    }
  }

  public accepting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Diterima ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diterima')
        })
  }

  public rejecting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Ditolak ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'ditolak')
        })
  }

  public verifying(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Proses ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diproses')
        })
  }

  private showConfirmation(data: any, status: string) {
      const {name, nip, info, file, id} = data;
      
      this.swalHandler.alert({
          title: "Ubah Status", 
          text: `Anda merubah ${name} - ${nip}, dengan status ${status}`,
          confirmButtonText: 'Ubah',
          showCancelButton: true,
          icon: 'question'
      }, {
        confirm: () => {
          this.doRequest(this.participantService.changeStatus(id, {status, info, file}), {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
          } as BehaviorSubject<any>)
        }
      })
  }

  completeSubmit() {this.loading = false}
  successSubmit(result) {
    this.loading = false
    this.getData()
  }
  errorrSubmit(error) {this.loading = false}
}
