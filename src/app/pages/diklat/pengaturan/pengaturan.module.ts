import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PengaturanRoutingModule } from './pengaturan-routing.module';
import { PengaturanComponent } from './pengaturan.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { IndexComponent } from './index/index.component';
import { SharedModule } from '../../../shared/shared.module';
import { NbAutocompleteModule, NbRadioModule, NbTagModule, NbTimepickerModule } from '@nebular/theme';
import { PilihMataComponent } from './pilih-mata/pilih-mata.component';


@NgModule({
  declarations: [    
    PengaturanComponent,
    CreateUpdateComponent,
    IndexComponent,
    PilihMataComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    NbRadioModule,
    NbTimepickerModule,
    PengaturanRoutingModule,
    NbTagModule,
    NbAutocompleteModule
  ]
})
export class PengaturanModule { }
