import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store'
import { Observable, forkJoin, of } from 'rxjs';
import { DiklatState } from '../../../../../store/diklat/diklat.state';
import { Diklats } from '../../../../../store/diklat/diklat';
import { PageTableComponent, PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { DeleteData, GetData } from '../../../../../store/diklat/diklat.action';
import { SimpegService } from '../../../../shared/services/simpeg.service';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import { DiklatService } from '../../../../shared/services/diklat.service';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {
  
  // @Select(DiklatState.getDiklat)
  diklat$: Observable<any> = of({})

  constructor(injector: Injector, 
    private diklatService: DiklatService) {
    super(injector, GetData, DeleteData);
    this.tag = "pengaturan-diklat"
  }

  getData() {
    this.diklat$ = this.diklatService.get({}).pipe(
      mergeMap(({data, links, meta}: {data: any, links, meta}) => {

        return forkJoin([
          this.simpegService.getJenisDiklat(),
          this.simpegService.getRumpun()
        ]).pipe(
          map(([jenis, rumpun]) => {
            return {links, meta, data: data.map(d => {
                d.attributes.jenis_name = jenis.find(j => j.id == d.attributes.jenis_id)?.nama ?? 'unknown'
                d.attributes.rumpun_name = rumpun.find(r => r.id == d.attributes.rumpun_id)?.nama ?? 'unknown'
                return d
              })}
          }))
      })
    )
  }

  ngOnInit() {
    super.ngOnInit()
  }
}
