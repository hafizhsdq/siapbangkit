import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Biodatas } from '../../../../../../store/biodata/biodata';
import { CreateUpdateBiodata } from '../../../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../../../store/biodata/biodata.state';
import { OPD } from '../../../../../@core/data/opd';
import { PageBaseComponent } from '../../../../../shared/base/page-base.component';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends PageBaseComponent implements AfterViewInit {

  @Select(BiodataState.getBiodatas)
  biodata$: Observable<Biodatas.Biodata>

  // sameAsKTP = false
  listOPD = OPD

  constructor(injector: Injector, public store: Store) {
    super(injector)

    // this.store.dispatch(new GetBiodatas())
  }

  ngAfterViewInit(): void {
    // const { biodatas } = this.store.selectSnapshot(BiodataState)
    // console.log(biodatas);
        
    
    // timeout used to recognize f as NgForm, cause null if directly set value
    setTimeout(() => {
      // Object.keys(biodatas).forEach(
      //   key => {      
      //     // const value = parseISO(biodatas[key]).toString() === 'Invalid Date' ? biodatas[key] : parseISO(biodatas[key])          
      //     if (this.f.controls[key]) this.f.controls[key].setValue(biodatas[key])}
      // )

      // console.log(this.f.value);
      
    }, 100)
  }

  OnSubmit(f: NgForm) {

    // if (this.sameAsKTP) {
    //   f.value.domicile_address = f.value.home_address
    //   f.value.domicile_address_ward = f.value.home_address_ward
    //   f.value.domicile_address_district = f.value.home_address_district
    //   f.value.domicile_address_city = f.value.home_address_city
    //   f.value.domicile_address_province = f.value.home_address_province
    //   f.value.domicile_zip_code = f.value.home_zip_code
    //   f.value.domicile_phone = f.value.home_phone
    //   f.value.domicile_mobile = f.value.home_mobile
    // }

    const doSave = this.store.dispatch(new CreateUpdateBiodata(f.value as Biodatas.Biodata, f.value.nip))
    this.doRequest(doSave)
  }

}
