import { Component, Injector } from "@angular/core";
import { NgForm } from "@angular/forms";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { BehaviorSubject, Observable } from "rxjs";
import { Diklats } from "../../../../../../store/diklat/diklat";
import { DiklatState } from "../../../../../../store/diklat/diklat.state";
import { CreateEditDialogStoreBaseComponent } from "../../../../../shared/base/create-edit-dialog-base.component";
import { KalenderDiklatService } from "../../../../../shared/services/kalender-diklat.service";

@Component ({
    selector: 'form-formal',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
            <!-- {{periode | json}} -->
                <nb-card-header>
                    <h6>{{title}}</h6>
                    <span class="caption"><strong>Note*</strong> Masukan NIP untuk mendaftarkan peserta diklat</span>
                </nb-card-header>
                <nb-card-body>
                    <div class="form-group row mb-3">
                        <label for="info" class="label col-form-label col-4">NIP</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="nip"
                                type="text"
                                id="nip"
                                ngModel required #nip="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
})
export class RegisterDialogComponent extends CreateEditDialogStoreBaseComponent<RegisterDialogComponent> {
    title: string = "Registrasi Peserta Diklat"

    @Select(DiklatState.getADiklat)
    diklat$: Observable<Diklats.Diklat>;

    scheduleId: string | number
    constructor(injector: Injector, 
        private scheduleService: KalenderDiklatService,
        public dialogRef: NbDialogRef<RegisterDialogComponent>) {
        super(injector, dialogRef)
    }

    onSubmit(f: NgForm): void {
        const value = f.value        
        const doSave = this.scheduleService.register(this.scheduleId, {nip: [value.nip]})
        this.doRequest(doSave, {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
        } as BehaviorSubject<any>
        )
    }
    
    successSubmit(result: any): void {
        this.dialogRef.close()
    }
}