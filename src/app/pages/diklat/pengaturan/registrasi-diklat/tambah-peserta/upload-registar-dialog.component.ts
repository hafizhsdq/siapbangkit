import { Component, Injector } from "@angular/core";
import { FormControl, NgForm, Validators } from "@angular/forms";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { BehaviorSubject, Observable } from "rxjs";
import { Diklats } from "../../../../../../store/diklat/diklat";
import { DiklatState } from "../../../../../../store/diklat/diklat.state";
import { CreateEditDialogStoreBaseComponent } from "../../../../../shared/base/create-edit-dialog-base.component";
import { FileUpload } from "../../../../../shared/components/upload-file.component";
import { ExcelReaderService } from "../../../../../shared/services/excel-reader.service";
import { KalenderDiklatService } from "../../../../../shared/services/kalender-diklat.service";

@Component ({
    selector: 'form-formal',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
            <!-- {{periode | json}} -->
                <nb-card-header>
                    <h6>{{title}}</h6>
                    <span class="caption"><strong>Note*</strong> Upload File .Xls</span>
                </nb-card-header>
                <nb-card-body>
                    <div class="form-group row mb-3">
                        <label for="info" class="label col-form-label col-4">Upload File</label>
                        <div class="col-sm-8">
                            <ngx-upload accept=".xlsx,.xls,application/vnd.ms-excel" id="nip" (change)="onChangeFile($event)"></ngx-upload>
                        </div>
                    </div>
                    <div class="form-control-group accept-group mb-3">
                        <nb-checkbox name="skip" ngModel #skip=ngModel>Skip nip terdaftar</nb-checkbox>
                    </div>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
    providers: [ExcelReaderService]
})
export class UploadRegistarDialogComponent extends CreateEditDialogStoreBaseComponent<UploadRegistarDialogComponent> {
    title: string = "Upload Daftar Peserta Diklat"

    @Select(DiklatState.getADiklat)
    diklat$: Observable<Diklats.Diklat>;

    scheduleId: string | number
    constructor(injector: Injector, 
        public dialogRef: NbDialogRef<UploadRegistarDialogComponent>, 
        private xlreaderService: ExcelReaderService,
        private scheduleService: KalenderDiklatService) {
        super(injector, dialogRef)
    }

    onSubmit(f: NgForm): void {
        const value = f.value
        const doSave = this.scheduleService.register(this.scheduleId, {nip: value.nip, skip: value.skip})
        this.doRequest(doSave, {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
        } as BehaviorSubject<any>)
    }
    
    successSubmit(result: any): void {
        this.dialogRef.close()
    }

    onChangeFile(e: FileUpload): void {
        this.xlreaderService.importFromExcel(e.file).subscribe((result) => {
            const NIPs = result.map(r => r.NIP)
            
            this.f.form.addControl(e.id, new FormControl(NIPs, Validators.required))
        })
    }
}