import { Component } from '@angular/core';

@Component({
  selector: 'ngx-registrasi-diklat',
  template: `<router-outlet></router-outlet>`
})
export class RegistrasiDiklatComponent {}