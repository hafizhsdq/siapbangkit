import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { Diklats } from '../../../../../../store/diklat/diklat';
import { DiklatState } from '../../../../../../store/diklat/diklat.state';
import { Participants } from '../../../../../../store/participant/participant';
import { DeleteData, GetData } from '../../../../../../store/participant/participant.action';
import { GetData as GetDataDiklat } from '../../../../../../store/diklat/diklat.action';
import { ParticipantState } from '../../../../../../store/participant/participant.state';
import { PageTableStoreComponent } from '../../../../../shared/base/page-table.component';
import { ParticipantService } from '../../../../../shared/services/participant.service';
import { filter } from 'rxjs/operators';
import { RegisterDialogComponent } from '../tambah-peserta/register-dialog.component';
import { UploadRegistarDialogComponent } from '../tambah-peserta/upload-registar-dialog.component';
import { ChangeStatusDialog } from '../../../../../shared/components/change-status-dialog.component';
import { KalenderDiklats } from '../../../../../../store/kalender-diklat/kalender-diklat';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData,DeleteData> {

  @Select(ParticipantState.getParticipant)
  participants$: Observable<Participants.Participant[]>

  @Select(DiklatState.getADiklat)
  diklat$: Observable<Diklats.Diklat>

  diklat: any
  latestPeriode: any
  angkatan = 0

  selectSchedule: KalenderDiklats.KalenderDiklat

  constructor(injector: Injector, private participantService: ParticipantService) {
    super(injector, GetData, DeleteData, {include: 'employee,training_schedule', useId: `training_id`});
    this.tag = "pengaturan-registrasi"
  }

  ngOnInit(): void {
    this.nbMenuService.onItemClick()
    .pipe(
        filter(({ tag }) => tag === this.tag),
    )
    .subscribe(result => {        
        if (result.item.title == 'Delete') this.OnDelete(result.item.data)
        if (result.item.title == 'Diterima') this.accepting(result.item.data)
        if (result.item.title == 'Ditolak') this.rejecting(result.item.data)
        if (result.item.title == 'Lakukan Verifikasi') this.verifying(result.item.data)
    });

    this.getData()
  }

  public getData(): void {
    this.id = this.activateRoute.snapshot.paramMap.get("id")
    this.store.dispatch(new GetDataDiklat({include: 'training_schedule'}, this.id))

    super.getData()
    // this.store.dispatch(new GetDataDiklat({include: 'training_schedule'}, this.id))
    // .subscribe(() => {
    //   const {response} = this.store.selectSnapshot(DiklatState)
    //   if (response?.data?.length) {
    //     this.diklat = ModelHelper.nestInclude(this.store.selectSnapshot(DiklatState).response)[0]
    //     const {training_periode} = this.diklat.relationships        
    //     if (training_periode) {
    //       this.latestPeriode = training_periode?.length ? 
    //         sort((a, b) => b.attributes.periode - a.attributes.periode)[0] : 
    //         training_periode            
    //       this.store.dispatch(new GetData({include: 'employee,training_schedule', params: `training_periode_id=${this.latestPeriode.id}`}))
    //     } else {
    //       this.swalHandler.alert({
    //         title: 'Kesalahan Data', 
    //         text: 'Angkatan tidak ditemukan, buat minimal satu angkatan'}, 
    //         () => this.back())
    //     }
        
    //   }
    // })
  }

  changeSchedule(schedule: KalenderDiklats.KalenderDiklat) {
    this.store.dispatch(new GetData({include: 'employee,training_schedule', params: `filter[training_schedule_id]=${schedule.id}`}))
  }

  register = (schedule: any) => {
    const dialog = this.dialogService.open(RegisterDialogComponent, { hasScroll: true })
    dialog.componentRef.instance.scheduleId = schedule.id
  }

  upload = (schedule: any) => {
    const dialog = this.dialogService.open(UploadRegistarDialogComponent, { hasScroll: true })
    dialog.componentRef.instance.scheduleId = schedule.id
  }

  public accepting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Diterima ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diterima')
        })
  }

  public rejecting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Ditolak ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'ditolak')
        })
  }

  public verifying(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Proses ${name} - ${nip}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diproses')
        })
  }

  private showConfirmation(data: any, status: string) {
      const {name, nip, info, file, id} = data;
      
      this.swalHandler.alert({
          title: "Ubah Status", 
          text: `Anda merubah ${name} - ${nip}, dengan status ${status}`,
          confirmButtonText: 'Ubah',
          showCancelButton: true,
          icon: 'question'
      }, {
        confirm: () => {
          this.loading = true
          this.doRequest(this.participantService.changeStatus(id, {status, info, file}), {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
          } as BehaviorSubject<any>)
        }
      })
  }

  completeSubmit() {this.loading = false}
  successSubmit(result) {
    this.loading = false
    this.getData()
  }
  errorrSubmit(error) {this.loading = false}

}
