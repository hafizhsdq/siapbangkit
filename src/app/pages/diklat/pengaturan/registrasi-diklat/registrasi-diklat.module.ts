import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { RegistrasiDiklatComponent } from './registrasi-diklat.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { getRoles, P } from '../../../roles';
import { RegisterDialogComponent } from './tambah-peserta/register-dialog.component';
import { UploadRegistarDialogComponent } from './tambah-peserta/upload-registar-dialog.component';

const routes: Routes = [
  { 
    path: '', 
    component: RegistrasiDiklatComponent,
    children: [
      { 
        path: '',
        component: IndexComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      }
    ]
  }    
];

@NgModule({
  declarations: [
    RegistrasiDiklatComponent,
    IndexComponent,
    ProfileComponent,
    RegisterDialogComponent,
    UploadRegistarDialogComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RegistrasiDiklatModule { }
