import { Component, Injector } from '@angular/core';
import { GetData } from '../../../../../store/diklat/diklat.action';
import { Diklats } from '../../../../../store/diklat/diklat';
import { CreateUpdateData } from '../../../../../store/diklat/diklat.action';
import { CreateEditStoreBaseComponent } from '../../../../shared/base/create-edit-base.component';
import { Select } from '@ngxs/store';
import { DiklatState } from '../../../../../store/diklat/diklat.state';
import { Observable } from 'rxjs';
import { SimpegService } from '../../../../shared/services/simpeg.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends CreateEditStoreBaseComponent<CreateUpdateData, Diklats.CreateUpdateDiklatInput, GetData, Diklats.Diklat> {

  @Select(DiklatState.getADiklat)
  diklat$: Observable<Diklats.Diklat>

  // listRumpun: string[] = []
  
  jenisDiklat$: Observable<any>
  jenis2Diklat$: Observable<any>
  rumpun$: Observable<any>

  constructor(injector: Injector) {
    super(injector, 'diklat.attributes', CreateUpdateData, GetData)
  }

  ngOnInit(): void {
    super.ngOnInit()
    this.jenisDiklat$ = this.simpegService.get('mJenisDiklat').pipe(map(({data}) => data))
    this.jenis2Diklat$ = this.simpegService.get('mDiklat').pipe(map(({data}) => data.filter(d => d.jenis_diklat.id == 2)))
    this.rumpun$ = this.simpegService.get('mRumpun').pipe(map(({data}) => data))
  }

  successSubmit(result: any): void {
    this.back()
  }
}
