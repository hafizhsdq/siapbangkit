import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PilihMataComponent } from './pilih-mata.component';

describe('PilihMataComponent', () => {
  let component: PilihMataComponent;
  let fixture: ComponentFixture<PilihMataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PilihMataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PilihMataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
