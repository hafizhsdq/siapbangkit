import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Diklats } from '../../../../../store/diklat/diklat';
import { DiklatState } from '../../../../../store/diklat/diklat.state';
import { MataDiklats } from '../../../../../store/mata-diklat/mata-diklat';
import { GetData } from '../../../../../store/mata-diklat/mata-diklat.action';
import { GetData as GetDataDiklat } from '../../../../../store/diklat/diklat.action';
import { MataDiklatState } from '../../../../../store/mata-diklat/mata-diklat.state';
import { PageBaseComponent } from '../../../../shared/base/page-base.component';
import { NbTagComponent, NbTagInputDirective } from '@nebular/theme';
import { DiklatService } from '../../../../shared/services/diklat.service';
import { switchMap } from 'rxjs-compat/operator/switchMap';
import { BaseData, ModelHelper } from '../../../../../store/base-model';

@Component({
  selector: 'ngx-pilih-mata',
  templateUrl: './pilih-mata.component.html',
  styleUrls: ['./pilih-mata.component.scss']
})
export class PilihMataComponent extends PageBaseComponent {

  @Select(MataDiklatState.getMataDiklats)
  subjects$: Observable<MataDiklats.MataDiklat[]>

  @Select(DiklatState.getDiklat)
  diklat$: Observable<Diklats.Diklat>;

  @ViewChild(NbTagInputDirective, { read: ElementRef }) tagInput: ElementRef<HTMLInputElement>;

  selectedSubjects: Set<BaseData<MataDiklats.MataDiklat>> = new Set<BaseData<MataDiklats.MataDiklat>>()
  get selectedSubjectName() : string[] {return Array.from(this.selectedSubjects).map(s => s.attributes.name)}
  get selectedSubjectIds() : (string | number)[] {return Array.from(this.selectedSubjects).map(s => s.id)}

  id: string // diklat id
  constructor(injector: Injector, public store: Store, public diklatService: DiklatService) {
    super(injector);

    this.store.dispatch(new GetData)

    this.id = this.activateRoute.snapshot.paramMap.get('id')
    this.store.dispatch(new GetDataDiklat({include: 'training_subject'}, this.id)).subscribe(() => {
      const snapshot = ModelHelper.nestInclude(this.store.selectSnapshot(DiklatState).response).data[0]
      // console.log(snapshot);
      
      this.selectedSubjects = new Set<BaseData<MataDiklats.MataDiklat>>(snapshot.relationships?.training_subject?.id ? 
        [snapshot.relationships?.training_subject] : 
        snapshot.relationships?.training_subject)
    })    
  }

  onSubmit() {
    const request = this.diklatService.addSubject({training_subject_id: this.selectedSubjectIds}, this.id)
    this.doRequest(request)
  }

  onTagAdd(value) {
    if (value) {
      this.selectedSubjects.add(value);
    }
    this.tagInput.nativeElement.value = '';
  }
  
  onTagRemove(tagToRemove: NbTagComponent) {
    const deleteSubject = Array.from(this.selectedSubjects).find(s => s.attributes.name == tagToRemove.text)
    this.selectedSubjects.delete(deleteSubject)
  }
}
