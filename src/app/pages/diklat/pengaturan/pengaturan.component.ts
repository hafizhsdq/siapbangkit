import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-pengaturan',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./pengaturan.component.scss']
})
export class PengaturanComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
