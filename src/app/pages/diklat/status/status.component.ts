import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { KalenderDiklats } from '../../../../store/kalender-diklat/kalender-diklat';
import { GetData, DeleteData } from '../../../../store/kalender-diklat/kalender-diklat.action';
import { KalenderDiklatState } from '../../../../store/kalender-diklat/kalender-diklat.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { ChangeStatusDialog } from '../../../shared/components/change-status-dialog.component';
import { KalenderDiklatService } from '../../../shared/services/kalender-diklat.service';

@Component({
  selector: 'ngx-index',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedules$: Observable<KalenderDiklats.KalenderDiklat[]>

  constructor(injector: Injector, private klaenderDiklatService: KalenderDiklatService) {
    super(injector, GetData, DeleteData, {include: 'employee.users,training'})
    this.tag = "permohonan-diklat-menu"
  }

  assignTableAction({item}) {
    if (item.title == 'Diterima') this.accepting(item.data)
    if (item.title == 'Ditolak') this.rejecting(item.data)
    if (item.title == 'Lakukan Verifikasi') this.verifying(item.data)
  }

  public accepting(data: any) {    
    const {name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Diterima diklat individu - ${name}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diterima')
        })
  }

  public rejecting(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Ditolak diklat individu - ${name}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'ditolak')
        })
  }

  public verifying(data: any) {
    const {nip, name} = data

    this.dialogService.open(ChangeStatusDialog, { 
      hasScroll: true, 
      context: {
        title: `Proses diklat individu - ${name}`, data} }).onClose.subscribe((result) => {
          if (result) this.showConfirmation({...data, ...result}, 'diproses')
        })
  }

  private showConfirmation(data: any, status: string) {
      const {name, nip, info, file, id} = data;
      
      this.swalHandler.alert({
          title: "Ubah Status", 
          text: `Anda merubah ${name}, dengan status ${status}`,
          confirmButtonText: 'Ubah',
          showCancelButton: true,
          icon: 'question'
      }, {
        confirm: () => {
          this.loading = true
          this.doRequest(this.klaenderDiklatService.changeStatus(id, file ? {status, info, file} : {status, info}), {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
          } as BehaviorSubject<any>)
        }
      })
  }

  completeSubmit() {this.loading = false}
  successSubmit(result) {    
    this.loading = false
    this.getData()
  }
  errorrSubmit(error) {this.loading = false}
}
