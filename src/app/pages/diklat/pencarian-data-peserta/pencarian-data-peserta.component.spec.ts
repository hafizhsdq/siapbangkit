import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PencarianDataPesertaComponent } from './pencarian-data-peserta.component';

describe('PencarianDataPesertaComponent', () => {
  let component: PencarianDataPesertaComponent;
  let fixture: ComponentFixture<PencarianDataPesertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PencarianDataPesertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PencarianDataPesertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
