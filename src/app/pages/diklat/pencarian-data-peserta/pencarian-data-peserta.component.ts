import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Participants } from '../../../../store/participant/participant';
import { GetData } from '../../../../store/participant/participant.action';
import { ParticipantState } from '../../../../store/participant/participant.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-pencarian-data-peserta',
  templateUrl: './pencarian-data-peserta.component.html',
  styleUrls: ['./pencarian-data-peserta.component.scss']
})
export class PencarianDataPesertaComponent extends PageTableStoreComponent<GetData> {

  @Select(ParticipantState.getParticipant)
  participants$: Observable<Participants.Participant[]>

  constructor(injector: Injector) {
    super(injector, GetData, null, {include: 'employee,training_schedule.training_periode,training_schedule.training'});
    this.tag = "search-peserta"
  }
}
