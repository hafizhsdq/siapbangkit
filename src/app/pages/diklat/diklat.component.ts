import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-diklat',
  template: `<router-outlet></router-outlet>`
})
export class DiklatComponent {}
