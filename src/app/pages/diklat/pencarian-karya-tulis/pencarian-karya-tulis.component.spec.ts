import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PencarianKaryaTulisComponent } from './pencarian-karya-tulis.component';

describe('PencarianKaryaTulisComponent', () => {
  let component: PencarianKaryaTulisComponent;
  let fixture: ComponentFixture<PencarianKaryaTulisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PencarianKaryaTulisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PencarianKaryaTulisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
