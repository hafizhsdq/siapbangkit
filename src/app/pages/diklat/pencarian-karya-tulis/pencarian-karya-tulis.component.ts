import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { KaryaTuliss } from '../../../../store/karya-tulis/karya-tulis';
import { GetData } from '../../../../store/karya-tulis/karya-tulis.action';
import { KaryaTulisState } from '../../../../store/karya-tulis/karya-tulis.state';
import { RolePipe } from '../../../@theme/pipes/role.pipe';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { P } from '../../roles';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-pencarian-karya-tulis',
  templateUrl: './pencarian-karya-tulis.component.html',
  styleUrls: ['./pencarian-karya-tulis.component.scss']
})
export class PencarianKaryaTulisComponent extends PageTableStoreComponent<GetData> {

  @Select(KaryaTulisState.getKaryaTuliss)
  userPaper$: Observable<KaryaTuliss.KaryaTulis[]>

  storageUrl = environment.storageUrl

  constructor(injector: Injector) {
    super(injector, GetData, null, {include: 'employee', useCurrentDate: true})
    this.tag = "search-paper"
  }

  getData() {
    const permission = this.rolePipe.transform({'user_paper': [P.VERIFIER]})
    // console.log(permission);
    
    if (!permission) {
      this.pageObject.params = 'filter[activity_log]=diterima'
    }

    super.getData()
  }
}
