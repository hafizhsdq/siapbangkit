import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { map, take, tap } from 'rxjs/operators';
import { GetData as GetDataMataDiklat } from '../../../../../store/mata-diklat/mata-diklat.action';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { UploadFileMateri } from './tambah-data/upload-file-materi.component';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { MataDiklats } from '../../../../../store/mata-diklat/mata-diklat';
import { MataDiklatState } from '../../../../../store/mata-diklat/mata-diklat.state';
import { FileMateriService } from '../../../../shared/services/file-materi.service';
import { environment } from '../../../../../environments/environment';
import { DeleteData, GetData } from '../../../../../store/file-materi/file-materi.action';
import { FileMateristate } from '../../../../../store/file-materi/file-materi.state';
import { FileMateris } from '../../../../../store/file-materi/file-materi';

@Component({
  selector: 'ngx-file-materi',
  templateUrl: './file-materi.component.html',
  styleUrls: ['./file-materi.component.scss']
})
export class FileMateriComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(MataDiklatState.getMataDiklats)
  subject$: Observable<MataDiklats.MataDiklat>;

  @Select(FileMateristate.getFiles)
  files$: Observable<FileMateris.FileMateri[]>;

  @ViewChild('viewFile') viewAnchor: ElementRef<any>
  
  constructor(injector: Injector, public fileMateriService: FileMateriService) {
    super(injector, GetData, DeleteData, {include: 'training_subject', useId: 'training_subject_id'})
    this.tag = "file-materi"
  }

  public getData(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id')
    // console.log(this.id);
    
    this.store.dispatch(new GetDataMataDiklat({}, this.id))

    super.getData()
  }

  openFileMateri = () => { 
    const dialog = this.dialogService.open(UploadFileMateri, { hasScroll: true })
    dialog.onClose.subscribe(() => {this.getData()})
  }

  OnView(id: string | number) {
    this.fileMateriService.view(id).pipe(
      take(1),
      map(result => environment.storageUrl+result.url),
      tap((url) => {
        this.viewAnchor.nativeElement.href = url
        this.viewAnchor.nativeElement.click()
      })
    ).subscribe()
    
    // .subscribe(result => {
    
    //   this.viewAnchor.nativeElement.click()      
    // })
  }
}
