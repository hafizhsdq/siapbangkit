import { Component, Injector } from "@angular/core";
import { NgForm } from "@angular/forms";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { Observable } from "rxjs";
import { FileMateris } from "../../../../../../store/file-materi/file-materi";
import { CreateUpdateData, GetData } from "../../../../../../store/file-materi/file-materi.action";
import { MataDiklats } from "../../../../../../store/mata-diklat/mata-diklat";
import { MataDiklatState } from "../../../../../../store/mata-diklat/mata-diklat.state";
import { CreateEditDialogStoreBaseComponent } from "../../../../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'form-formal',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    <h6>{{title}}</h6>
                    <span class="caption"><strong>Note*</strong> FileMateri Diklat yang perlu diupload antara lain Silabus, Paparan Materi, Referensi, dan dokumen terkait lainnya.</span>
                </nb-card-header>
                <nb-card-body *ngIf="((subject$ | async).response.data && (subject$ | async).response.data[0]) as subject">
                 <!-- {{subject | json}} -->
                    <input name="training_subject_id" type="hidden" [ngModel]="subject?.id" #training_subject_id="ngModel">
                    <div class="form-group row mb-3">
                        <label for="info" class="label col-form-label col-4">Keterangan</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="info"
                                type="text"
                                id="info"
                                [ngModel]="subject?.info" required #info="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="file" class="label col-form-label col-4">File Materi</label>
                        <div class="col-sm-8">
                            <ngx-upload id="file" (change)="onChangeFile($event)"></ngx-upload>
                        </div>
                    </div>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
})
export class UploadFileMateri extends CreateEditDialogStoreBaseComponent<UploadFileMateri, CreateUpdateData, FileMateris.CreateUpdateFileMateriInput, GetData, FileMateris.FileMateri> {
    title: string = "Upload File Materi Diklat"

    @Select(MataDiklatState.getMataDiklats)
    subject$: Observable<MataDiklats.MataDiklat>;

    constructor(injector: Injector, public dialogRef: NbDialogRef<UploadFileMateri>) {
        super(injector, dialogRef, 'filemateri', CreateUpdateData)
    }
    
    successSubmit(result: any): void {
        this.dialogRef.close()
    }
}