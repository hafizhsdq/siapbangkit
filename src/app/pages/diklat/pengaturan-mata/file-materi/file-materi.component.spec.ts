import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileMateriComponent } from './file-materi.component';

describe('FileMateriComponent', () => {
  let component: FileMateriComponent;
  let fixture: ComponentFixture<FileMateriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileMateriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileMateriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
