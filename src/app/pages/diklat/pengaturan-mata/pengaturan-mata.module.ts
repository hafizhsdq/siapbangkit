import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PengaturanMataRoutingModule } from './pengaturan-mata-routing.module';
import { PengaturanMataComponent } from './pengaturan-mata.component';
import { IndexComponent } from './index/index.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { SharedModule } from '../../../shared/shared.module';
import { FileMateriComponent } from './file-materi/file-materi.component';
import { UploadFileMateri } from './file-materi/tambah-data/upload-file-materi.component';
import { SimpleDataPipe } from '../../../@theme/pipes/simple-data.pipe';


@NgModule({
  declarations: [
    PengaturanMataComponent,
    IndexComponent,
    CreateUpdateComponent,
    FileMateriComponent,

    UploadFileMateri
  ],
  imports: [
    CommonModule,
    SharedModule,
    PengaturanMataRoutingModule
  ]
})
export class PengaturanMataModule { }
