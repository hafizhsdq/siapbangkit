import { Component, Injector } from '@angular/core';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { MataDiklats } from '../../../../../store/mata-diklat/mata-diklat'
import { DeleteData, GetData } from '../../../../../store/mata-diklat/mata-diklat.action';
import { MataDiklatState } from '../../../../../store/mata-diklat/mata-diklat.state';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { DiklatState } from '../../../../../store/diklat/diklat.state';
import { GetData as GetDataDiklat } from '../../../../../store/diklat/diklat.action'
import { DiklatService } from '../../../../shared/services/diklat.service';
import { switchMap } from 'rxjs/operators';
import { BaseResponse } from '../../../../../store/base-model';
import { Diklats } from '../../../../../store/diklat/diklat';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(MataDiklatState.getMataDiklats)
  subjects$: Observable<MataDiklats.MataDiklat[]>;

  @Select(MataDiklatState.getTotalTimeAllocation)
  total$: Observable<number>;

  @Select(DiklatState.getDiklat)
  diklat$: Observable<BaseResponse<Diklats.Diklat>>;

  diklatId: string
  constructor(injector: Injector, public diklatService: DiklatService) {
    super(injector, GetData, DeleteData, {include: 'training'});
    this.tag = "pengaturan-mata"
  }

  public getData(): void {
    this.diklatId = this.activateRoute.snapshot.paramMap.get('diklatId')    
    if (this.diklatId) {
      // get subjec by diklat id
      this.store.dispatch(new GetData({include: 'training', params: `training_id=${this.diklatId}`}))
      this.store.dispatch(new GetDataDiklat({}, this.diklatId))
    } else {
      super.getData()
    }
  }

  // public OnDelete(id: string | number): void {
  //   if (this.diklatId) {
  //     const subjects = this.store.selectSnapshot(DiklatState).response.included
  //     // remove subject id
  //     const updateId = subjects.filter(s => s.id != id).map(s => parseInt(s.id))
  //     // console.log(id);
  //     // console.log(updateId);
      
  //     const request = this.diklatService.addSubject({training_subject_id: updateId}, this.diklatId).pipe(
  //       switchMap(() => this.store.dispatch(new GetMataDiklatByDiklatId({}, this.diklatId)))
  //     )
  //     this.doRequest(request)
  //   } else {
  //     super.OnDelete(id)
  //   }
  // }

}
