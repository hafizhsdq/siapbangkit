import { Component, Injector, OnInit } from '@angular/core';
import { PageTableComponent } from '../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-pengaturan-mata',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./pengaturan-mata.component.scss']
})
export class PengaturanMataComponent extends PageTableComponent {

  constructor(injector: Injector) {
    super(injector);
    this.tag = "pengaturan-mata"
  }
}
