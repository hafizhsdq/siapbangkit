import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Kbms } from '../../../../../../store/kbm/kbm';
import { CreateUpdateData, GetData } from '../../../../../../store/kbm/kbm.action';
import { GetData as GetDataSubject } from '../../../../../../store/mata-diklat/mata-diklat.action';
import { KbmState } from '../../../../../../store/kbm/kbm.state';
import { MataDiklats } from '../../../../../../store/mata-diklat/mata-diklat';
import { MataDiklatState } from '../../../../../../store/mata-diklat/mata-diklat.state';
import { CreateEditStoreBaseComponent } from '../../../../../shared/base/create-edit-base.component';

@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends CreateEditStoreBaseComponent<CreateUpdateData, Kbms.CreateUpdateKbmInput, GetData, Kbms.Kbm> {

  @Select(KbmState.getKbm)
  phase$: Observable<Kbms.Kbm>;

  @Select(MataDiklatState.getMataDiklats)
  subject$: Observable<MataDiklats.MataDiklat>;

  constructor(injector: Injector) {
    super(injector, 'kbm.attributes', CreateUpdateData, GetData, {include: 'training_subject'});
    const subjectId = this.activateRoute.parent.snapshot.paramMap.get('id')

    this.store.dispatch(new GetDataSubject({}, subjectId))
  }

  successSubmit(result: any): void {
    this.back()
  }
}
