import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-kbm',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./kbm.component.scss']
})
export class KbmComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
