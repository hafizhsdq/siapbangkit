import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { KbmComponent } from './kbm.component';
import { getRoles, P } from '../../../roles';

const routes: Routes = [{ 
  path: '', 
  component: KbmComponent,
  children: [
    {
      path: '',
      component: IndexComponent,
      data: getRoles('teaching_phase', [P.READ]),
    },
    {
      path: 'create',
      component: CreateUpdateComponent,
      data: getRoles('teaching_phase', [P.CREATE]),
    },
    {
      path: 'update/:id',
      component: CreateUpdateComponent,
      data: getRoles('teaching_phase', [P.UPDATE]),
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KbmRoutingModule { }
