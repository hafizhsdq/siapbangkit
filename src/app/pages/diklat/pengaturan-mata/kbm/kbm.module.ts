import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KbmRoutingModule } from './kbm-routing.module';
import { KbmComponent } from './kbm.component';
import { IndexComponent } from './index/index.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { SharedModule } from '../../../../shared/shared.module';


@NgModule({
  declarations: [
    KbmComponent,
    IndexComponent,
    CreateUpdateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    KbmRoutingModule
  ]
})
export class KbmModule { }
