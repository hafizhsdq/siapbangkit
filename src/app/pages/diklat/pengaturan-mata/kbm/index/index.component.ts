import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Kbms } from '../../../../../../store/kbm/kbm';
import { DeleteData, GetData } from '../../../../../../store/kbm/kbm.action';
import { KbmState } from '../../../../../../store/kbm/kbm.state';
import { MataDiklats } from '../../../../../../store/mata-diklat/mata-diklat';
import { GetData as GetDataMataDiklat } from '../../../../../../store/mata-diklat/mata-diklat.action';
import { MataDiklatState } from '../../../../../../store/mata-diklat/mata-diklat.state';
import { PageTableStoreComponent } from '../../../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(MataDiklatState.getMataDiklats)
  subject$: Observable<MataDiklats.MataDiklat>;

  @Select(KbmState.getKbm)
  phase$: Observable<Kbms.Kbm[]>;

  constructor(injector: Injector) {
    super(injector, GetData, DeleteData, {include: 'training_subject', useId: 'training_subject_id'});
    this.tag = "tahapan-kbm"
  }

  public getData(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id')
    this.store.dispatch(new GetDataMataDiklat({}, this.id))

    super.getData()
  }
}
