import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KbmComponent } from './kbm.component';

describe('KbmComponent', () => {
  let component: KbmComponent;
  let fixture: ComponentFixture<KbmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KbmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KbmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
