import { Component, Injector } from '@angular/core';
import { CreateEditStoreBaseComponent } from '../../../../shared/base/create-edit-base.component';
import { CreateUpdateData, GetData } from '../../../../../store/mata-diklat/mata-diklat.action';
import { GetData as GetDataDiklat } from '../../../../../store/diklat/diklat.action';
import { MataDiklats } from '../../../../../store/mata-diklat/mata-diklat';
import { NgForm } from '@angular/forms';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Diklats } from '../../../../../store/diklat/diklat';
import { DiklatState } from '../../../../../store/diklat/diklat.state';
import { MataDiklatState } from '../../../../../store/mata-diklat/mata-diklat.state';
import { MataDiklatService } from '../../../../shared/services/mata-diklat.service';

@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends CreateEditStoreBaseComponent<CreateUpdateData, MataDiklats.CreateUpdateMataDiklatInput, GetData, MataDiklats.MataDiklat> {
  @Select(DiklatState.getDiklat)
  diklat$: Observable<Diklats.Diklat>;

  @Select(MataDiklatState.getMataDiklats)
  subject$: Observable<MataDiklats.MataDiklat>
  
  diklatId: string
  constructor(injector: Injector, private mataService: MataDiklatService) {
    super(injector, 'matadiklat', CreateUpdateData, GetData);

    this.diklatId = this.activateRoute.snapshot.paramMap.get('diklatId')
    this.store.dispatch(new GetDataDiklat({}, this.diklatId))
  }

  ngAfterViewInit(): void {
    if (this.id) {
      this.mataService.getById(this.id).subscribe((result: any) => {
        const {data} = result
        const {attributes = {}} = data[0]

        this.f.setValue(attributes)
      })
    }
  }

  successSubmit(result: any): void {
    this.back()
  }

  // custom to handle directly add to subject
  onSubmit(f: NgForm): void {
    const training_id = this.activateRoute.snapshot.paramMap.get("diklatId")
    if (training_id) {
      f.value.training_id = training_id
    }
    
    super.onSubmit(f)
  }
}
