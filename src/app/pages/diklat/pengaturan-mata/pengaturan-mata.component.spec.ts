import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengaturanMataComponent } from './pengaturan-mata.component';

describe('PengaturanMataComponent', () => {
  let component: PengaturanMataComponent;
  let fixture: ComponentFixture<PengaturanMataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PengaturanMataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PengaturanMataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
