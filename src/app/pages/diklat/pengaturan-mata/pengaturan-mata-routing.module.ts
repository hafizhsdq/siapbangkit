import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { getRoles, P } from '../../roles';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { FileMateriComponent } from './file-materi/file-materi.component';
import { IndexComponent } from './index/index.component';
import { PengaturanMataComponent } from './pengaturan-mata.component';

const routes: Routes = [{ 
  path: '', 
  component: PengaturanMataComponent,
  children: [
    {
      path: '',
      component: IndexComponent,
      data: getRoles('training_subject', [P.READ]),
    },
    {
      path: 'create-update',
      component: CreateUpdateComponent,
      data: getRoles('training_subject', [P.CREATE]),
    },
    {
      path: 'add/:diklatId',
      component: CreateUpdateComponent,
      data: getRoles('training_subject', [P.UPDATE]),
    },
    {
      path: 'update/:id',
      component: CreateUpdateComponent,
      data: getRoles('training_subject', [P.UPDATE]),
    },
    { path: 'kbm/:id', loadChildren: () => import('./kbm/kbm.module').then(m => m.KbmModule) },
    { path: 'file-materi/:id', component: FileMateriComponent, data: getRoles('teaching_material', [P.READ]), },
    {
      path: "**",
      redirectTo: ''
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PengaturanMataRoutingModule { }
