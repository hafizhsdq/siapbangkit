import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KalenderRoutingModule } from './kalender-routing.module';
import { IndexComponent } from './index/index.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { SharedModule } from '../../../shared/shared.module';
import { RekapPesertaComponent } from './rekap-peserta/rekap-peserta.component';
import { PengaturanRegistrasiComponent } from './pengaturan-registrasi/pengaturan-registrasi.component';
// import { FilterPipe } from '../../../@theme/pipes/filter.pipe';
import { NbMomentDateModule } from '@nebular/moment';
import { NbRouteTabsetModule, NbTabsetModule } from '@nebular/theme';
import { ExternalComponent } from './create-update/external.component';
import { InternalComponent } from './create-update/internal-form.component';

@NgModule({
  declarations: [
    IndexComponent,
    CreateUpdateComponent,
    RekapPesertaComponent,
    PengaturanRegistrasiComponent,
    InternalComponent,
    ExternalComponent
    // FilterPipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    KalenderRoutingModule,
    NbMomentDateModule
  ]
})
export class KalenderModule { }
