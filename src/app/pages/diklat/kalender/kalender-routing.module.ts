import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { getRoles, P } from '../../roles';
import { PengaturanPesertaComponent } from '../pengaturan/pengaturan-angkatan/pengaturan-peserta/pengaturan-peserta.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { ExternalComponent } from './create-update/external.component';
import { InternalComponent } from './create-update/internal-form.component';
import { IndexComponent } from './index/index.component';
import { KalenderComponent } from './kalender.component';
import { PengaturanRegistrasiComponent } from './pengaturan-registrasi/pengaturan-registrasi.component';
import { RekapPesertaComponent } from './rekap-peserta/rekap-peserta.component';

const routes: Routes = [
  {
    path: '',
    component: KalenderComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: 'create/:type',
        component: CreateUpdateComponent,
        data: getRoles('training_schedule', [P.CREATE]),
        // children: [
        //   {
        //     path: 'internal',
        //     component: InternalComponent,
        //   },
        //   {
        //     path: 'eksternal',
        //     component: ExternalComponent,
        //   },
        //   {
        //     path: '',
        //     pathMatch: 'full',
        //     redirectTo: 'internal'
        //   }
        // ]
      },
      {
        path: 'update/:id/type/:type',
        component: CreateUpdateComponent,
        data: getRoles('training_schedule', [P.UPDATE]),
        // children: [
        //   {
        //     path: 'internal/:id',
        //     component: InternalComponent,
        //   },
        //   {
        //     path: 'eksternal/:id',
        //     component: ExternalComponent,
        //   },          
        // ]
      },
      {
        path: 'rekap-peserta/:id',
        component: PengaturanPesertaComponent,
        data: getRoles('training_schedule', [P.UPDATE]),
      },
      {
        path: 'pengaturan-registrasi',
        component: PengaturanRegistrasiComponent,
        data: getRoles('training_schedule', [P.UPDATE]),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KalenderRoutingModule { }
