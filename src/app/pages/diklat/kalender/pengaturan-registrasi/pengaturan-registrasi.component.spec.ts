import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengaturanRegistrasiComponent } from './pengaturan-registrasi.component';

describe('PengaturanRegistrasiComponent', () => {
  let component: PengaturanRegistrasiComponent;
  let fixture: ComponentFixture<PengaturanRegistrasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PengaturanRegistrasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PengaturanRegistrasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
