import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RekapPesertaComponent } from './rekap-peserta.component';

describe('RekapPesertaComponent', () => {
  let component: RekapPesertaComponent;
  let fixture: ComponentFixture<RekapPesertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RekapPesertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RekapPesertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
