import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-kalender',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./kalender.component.scss']
})
export class KalenderComponent implements OnInit {

  year = new Date().getFullYear()
  constructor() { }

  ngOnInit(): void {
  }

}
