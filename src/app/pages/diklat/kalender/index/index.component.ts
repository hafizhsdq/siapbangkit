import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import * as moment from 'moment/moment'
import { Observable } from 'rxjs';
import { KalenderDiklats } from '../../../../../store/kalender-diklat/kalender-diklat';
import { DeleteData, GetData } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { KalenderDiklatState } from '../../../../../store/kalender-diklat/kalender-diklat.state';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedule$: Observable<KalenderDiklats.KalenderDiklat[]>;
  // year = new Date().getFullYear()
  search: string = '';

  year = new Date().getFullYear()

  constructor(injector: Injector) {
    super(injector, GetData, DeleteData, {include: 'employee,training', params: `filter[category]=internal,eksternal&filter[date]=${moment().startOf('year').format('YYYY/MM/DD')},${moment().endOf('year').format('YYYY/MM/DD')}`});
  }
}
