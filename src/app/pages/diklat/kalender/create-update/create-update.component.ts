import { Component, Injector } from '@angular/core';
import { GetData as GetDataKalender } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { PageBaseComponent } from '../../../../shared/base/page-base.component';

@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends PageBaseComponent { //extends CreateEditStoreBaseComponent<CreateUpdateData, KalenderDiklats.CreateUpdateKalenderDiklatInput, GetDataKalender, KalenderDiklats.KalenderDiklat> {

  id: string
  diklatId: string
  angkatanId: string

  type: string = 'internal'
  // tabs: NbRouteTab[] 

  // @Select(KalenderDiklatState.getAKalenderDiklats)
  // schedule$: Observable<KalenderDiklats.KalenderDiklat>

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id')    
    this.diklatId = this.activateRoute.snapshot.paramMap.get('diklatId')

    this.type = this.activateRoute.snapshot.paramMap.get('type')
    
    if (this.id) {
      this.store.dispatch(new GetDataKalender({}, this.id))
    }
    super.ngOnInit()
  }
  // @Select(KalenderDiklatState.getAKalenderDiklats)
  // schedule$: Observable<KalenderDiklats.KalenderDiklat>;

  // @Select(DiklatState.getDiklat)
  // diklats$: Observable<Diklats.Diklat[]>;

  // @Select(AngkatanDiklatState.getAngkatanDiklats)
  // periode$: Observable<AngkatanDiklats.AngkatanDiklat[]>;

  // // @Select(DiklatState.getDiklat)
  // // diklats$: Observable<Diklats.Diklat[]>;

  // selectGroup: string
  // angkatanId: string
  // diklatId: string
  // constructor(injector: Injector) {
  //   super(injector, 'kalenderdiklat', CreateUpdateData, GetDataKalender, {include: 'training'});

  //   this.angkatanId = this.activateRoute.snapshot.paramMap.get('angkatanId')
  //   this.diklatId = this.activateRoute.snapshot.paramMap.get('diklatId')
  //   // console.log(this.diklatId);
  //   // ?? diklatId ? diklatId : id ? (calendar && calendar[0].training.id) : null
  //   // if (this.diklatId)
  //   // this.store.dispatch(new GetDataDiklat({include: 'periode'}, this.angkatanId))
  // }

  // ngAfterViewInit(): void {
  //   super.ngAfterViewInit()

  //   setTimeout(() => {
  //     if (this.f.controls) {        
  //       // from pengaturan angkatan
  //       if (this.angkatanId) {
  //         this.store.dispatch(new GetDataAngkatan({include: 'training'}, this.angkatanId))
  //       } else if (this.diklatId) {
  //         this.store.dispatch(new GetDataDiklat({include: 'training_periode'}, this.diklatId))
  //         this.f.controls['training_id'].setValue(this.diklatId)
  //       } else {
  //         this.store.dispatch(new GetDataDiklat({include: 'training_periode'}))
  //       }

  //       this.f.controls['training_id'].valueChanges
  //         .pipe(map(training_id => {            
  //           if (training_id) this.store.dispatch(new GetDataAngkatan({params: 'training_id='+training_id}))
  //         })).subscribe(() => {
  
  //         })
  //     }
  //   }, 100)
  // }

  // successSubmit(result: any): void {
  //   this.back()
  // }

  // changeGroup(e) {this.f.value.training_id = null}

  // onChangeImage(e: FileImage) {
  //   // this.f.addControl({[e.id]: new FormControl(e.image)})
  //   this.f.controls[e.id].setValue(e.image) //image is base64    
  //   console.log(e.id);
    
  //   // this.f.form.addControl(e.id, new FormControl(e.image, Validators.required))
  //   console.log(this.f.value);
  // }
}
