import { Component, Injector, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable, forkJoin, of } from 'rxjs';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { FileImage } from '../../../../shared/components/upload-image.component';
import { NgForm } from '@angular/forms';
import { SimpegService } from '../../../../shared/services/simpeg.service';
import { KalenderDiklatService } from '../../../../shared/services/kalender-diklat.service';
import { PageBaseComponent } from '../../../../shared/base/page-base.component';
import { DiklatService } from '../../../../shared/services/diklat.service';

@Component({
  selector: 'ngx-internal-diklat',
  template: `
    <div *ngIf="(data$ | async) as data" [nbSpinner]="!data" class="pt-3">
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>

            <input type="hidden" name="category" [ngModel]="'internal'" #category="ngModel">
            <input type="hidden" name="banner" [ngModel]="f.value.banner" #banner="ngModel">

            <div class="form-group row mb-3">
                <label for="banner" class="label col-form-label col-3">Banner</label>
                <div class="col-sm-9">
                    <ngx-upload-image id="banner" [base64]="data?.attributes?.banner" (change)="onChangeImage($event)"></ngx-upload-image>
                </div>
            </div>

            <ng-container
                *ngIf="(training$ | async) as training"
                [ngTemplateOutlet]="training_id ? useDiklat : selectDiklat"
                [ngTemplateOutletContext]="{$implicit: training, data: data, form: f}">
            </ng-container>

            <input type="hidden" name="training_id" [ngModel]="training_id">

            <div class="form-group row mb-3">
                <label for="periode" class="label col-form-label col-3">Angkatan Ke</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="periode"
                        type="number"
                        id="periode"
                        [ngModel]="(totalSchedule$ | async) + 1"
                        placeholder="1"
                        readonly
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="nomor_sk" class="label col-form-label col-3">Nomor SK</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="nomor_sk"
                        type="text"
                        id="nomor_sk"
                        [ngModel]="data?.attributes?.nomor_sk"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="nomor_sk" class="label col-form-label col-3">Tanggal SK</label>
                <div class="col-sm-9">
                    <input autocomplete="off" id="tanggal_sk" name="tanggal_sk" nbInput fullWidth [nbDatepicker]="tanggalSK" [ngModel]="data?.attributes?.tanggal_sk | dns" required>
                    <nb-datepicker #tanggalSK></nb-datepicker>
                </div>
            </div>

            <!-- <div class="form-group row mb-3">
                <label for="nama_pejabat_id" class="label col-form-label col-3">Nama Pejabat</label>
                <div class="col-sm-9">
                    <input nbInput
                        fullWidth
                        name="nama_pejabat_id"
                        ngModel
                        type="text"
                        (input)="onChange($event, 'nama_pejabat')"
                        [nbAutocomplete]="autoNama" />

                    <nb-autocomplete #autoNama [handleDisplayFn]="viewHandle">
                        <nb-option *ngFor="let option of filterNamaPejabat$ | async" [value]="option">
                            {{ option.nama }}
                        </nb-option>
                    </nb-autocomplete>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="pejabat_id" class="label col-form-label col-3">Pejabat</label>
                <div class="col-sm-9">
                    <input nbInput
                        fullWidth
                        name="pejabat_id"
                        ngModel
                        type="text"
                        (input)="onChange($event, 'nama_pejabat')"
                        [nbAutocomplete]="autoPejabat" />

                    <nb-autocomplete #autoPejabat [handleDisplayFn]="viewHandle">
                        <nb-option *ngFor="let option of filterPejabat$ | async" [value]="option">
                            {{ option.nama }}
                        </nb-option>
                    </nb-autocomplete>
                </div>
            </div> -->

            <div class="form-group row mb-3">
                <label for="penyelenggara" class="label col-form-label col-3">Penyelenggara</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="penyelenggara"
                        type="text"
                        id="penyelenggara"
                        [ngModel]="id ? schedule?.attributes?.penyelenggara : f.value.penyelenggara" required #penyelenggara="ngModel"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="quota" class="label col-form-label col-3">Jumlah Peserta</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="quota"
                        type="number"
                        id="quota"
                        [ngModel]="data?.attributes?.quota" placeholder="10"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="duration" class="label col-form-label col-3">Durasi Hari</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="duration"
                        type="number"
                        id="duration"
                        [ngModel]="data?.attributes?.duration"
                        min="1"
                        placeholder="1"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="start_date" class="label col-form-label col-3">Waktu Pelaksanaan</label>
                <div class="col-sm-9">
                    <input autocomplete="off" 
                        id="start_date" name="start_date" nbInput fullWidth 
                        placeholder="Waktu Pelaksanaan" 
                        [nbDatepicker]="startDatepicker" 
                        [ngModel]="data?.attributes?.start_date | dns" required>
                    <nb-datepicker #startDatepicker></nb-datepicker>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="location" class="label col-form-label col-3">Lokasi Pelaksanaan</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="location"
                        type="text"
                        id="location"
                        [ngModel]="data?.attributes?.location" required
                        placeholder="Lokasi diklat"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="purpose" class="label col-form-label col-3">Tujuan</label>
                <div class="col-sm-9">
                    <textarea rows="5" name="purpose" nbInput fullWidth placeholder="Tujuan dilaksanakannya diklat" [ngModel]="data?.attributes?.purpose" required></textarea>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="requirement" class="label col-form-label col-3">Persyaratan</label>
                <div class="col-sm-9">
                    <textarea rows="5" name="requirement" nbInput fullWidth placeholder="Syarat mengikuti diklat" [ngModel]="data?.attributes?.requirement" required></textarea>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="start_registration" class="label col-form-label col-3">Mulai Registrasi</label>
                <div class="col-sm-9">
                    <input autocomplete="off" id="start_registration" name="start_registration" nbInput fullWidth placeholder="Mulai Registrasi" [nbDatepicker]="tanggalMulai" [ngModel]="data?.attributes?.start_registration | dns" required>
                    <nb-datepicker #tanggalMulai></nb-datepicker>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="end_registration" class="label col-form-label col-3">Selesai Registrasi</label>
                <div class="col-sm-9">
                    <input autocomplete="off" id="end_registration" name="end_registration" nbInput fullWidth placeholder="Selesai Registrasi" [nbDatepicker]="tanggalSelesai" [ngModel]="data?.attributes?.end_registration | dns" required>
                    <nb-datepicker #tanggalSelesai></nb-datepicker>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="registration_status" class="label col-form-label col-3">Status Registrasi</label>
                <div class="col-sm-9">
                    <nb-select name="registration_status" fullWidth [ngModel]="data?.attributes?.registration_status" required>
                        <nb-option value="open">Buka</nb-option>
                        <nb-option value="close">Tutup</nb-option>
                    </nb-select>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="info" class="label col-form-label col-3">Keterangan</label>
                <div class="col-sm-9">
                    <textarea rows="5" name="info" nbInput fullWidth placeholder="Penjelasan tentang diklat lainnya..." [ngModel]="data?.attributes?.info" required></textarea>
                </div>
            </div>
            
            <div class="row justify-content-end">
                <div class="col-auto">
                    <button nbButton ghost type="reset" (click)="back()" size="small" class="mr-2">Batalkan</button>
                    <button nbButton type="submit" size="small" status="primary">Submit</button>
                </div>
            </div>

        </form>
    </div>

    <ng-template #useDiklat let-diklat>
        <div class="form-group row mb-3">
            <label for="jenis_name" class="label col-form-label col-3">Kelompok Diklat</label>
            <div class="col-sm-9">
                <input type="hidden" name="jenis_id" [value]="diklat[0]?.attributes?.jenis_id" ngModel />
                <input class="col" nbInput
                    fullWidth
                    name="jenis_name"
                    type="text"
                    id="jenis_name"
                    [value]="diklat[0].attributes.jenis_nama ?? 'unknown'"
                    readonly
                    fieldSize="medium">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="rumpun_name" class="label col-form-label col-3">Rumpun</label>
            <div class="col-sm-9">
                <input type="hidden" name="rumpun_id" [value]="diklat[0]?.attributes?.rumpun_id" ngModel />
                <input class="col" nbInput
                    fullWidth
                    name="rumpun_name"
                    type="text"
                    id="rumpun_name"
                    [value]="diklat[0].attributes.rumpun_nama ?? 'unknown'"
                    readonly
                    fieldSize="medium">
            </div>
        </div>
        
        <div class="form-group row mb-3">
            <label for="name" class="label col-form-label col-3">Nama Diklat</label>
            <div class="col-sm-9">
                <input class="col" nbInput
                    fullWidth
                    name="name"
                    type="text"
                    id="name"
                    [value]="diklat[0]?.attributes?.name"
                    readonly
                    fieldSize="medium">
            </div>
        </div>
    </ng-template>
    
    <ng-template #selectDiklat let-form="form">
        <ng-container *ngIf="(training$ | async) as diklats">
            <div class="form-group row mb-3">
                <label for="training_id" class="label col-form-label col-3">Nama Diklat</label>
                <div class="col-sm-9">
                    <nb-select name="training_id" fullWidth [(selected)]="training_id" (selectedChange)="getTotalAngkatan()" required>
                        <nb-option *ngFor="let diklat of diklats" value="{{diklat.id}}">{{diklat.attributes.name}}</nb-option>
                    </nb-select>
                </div>
            </div>
        </ng-container>
    </ng-template>  
  `
})
export class InternalComponent extends PageBaseComponent {
  jenisDiklat$: Observable<any>
  rumpun$: Observable<any>

  data$: Observable<any> = of({})
  training$: Observable<any[]>
  totalSchedule$: Observable<number>
  
  loading = false

  @ViewChild('f') f: NgForm
  id: string
  training_id: string

//   pejabat$: Observable<any>
//   namaPejabat$: Observable<any>
//   filterNamaPejabat$: Observable<any>
//   filterPejabat$: Observable<any>

//   pejabat: any
//   namaPejabat: any
  
  constructor(injector: Injector,
    private scheduleService: KalenderDiklatService,
    private diklatService: DiklatService) {
    super(injector);
  }

  ngOnInit() {
    this.id = this.activateRoute.snapshot.paramMap.get('id')
    this.training_id = this.activateRoute.parent.snapshot.paramMap.get('id')
        
    if (this.training_id) {// there is parent training id in query url
        this.totalSchedule$ = this.scheduleService.get({include: 'training', params: 'filter[training_id]='+this.training_id})
                                                .pipe(map(data => data.data.length))
    }

    // this.filterNamaPejabat$ = this.pejabat$ = this.simpegService.get("mPejabat").pipe(map(({data}) => data))
    // this.filterPejabat$ = this.namaPejabat$ = this.simpegService.get("mNamaPejabat").pipe(map(({data}) => data))

    if (this.id) {// this must be edit
        this.data$ = this.scheduleService.getById(this.id, {include: 'training'}).pipe(
            mergeMap(({data, included}: any) => {
                return forkJoin([
                    // this.filterNamaPejabat$,
                    // this.filterPejabat$,
                    this.simpegService.getRumpun(data[0].attributes.rumpun_id).pipe(map(({data}) => data?.nama)),
                    this.simpegService.getJenisDiklat(data[0].attributes.jenis_id).pipe(map(({data}) => data?.nama)),
                    of({...data[0], trainig_id: included[0].id})
                ])
            }),
            tap(([/*namaPejabat, pejabat,*/ rumpun_name, jenis_name, data]) => {
                this.training_id = data.trainig_id
            }), 
            map(([/*namaPejabat, pejabat,*/ rumpun_name, jenis_name, data]) => {
                // this.namaPejabat = namaPejabat.find(n => n.id === data.attributes.nama_pejabat_id)
                // this.pejabat = pejabat.find(n => n.id === data.attributes.pejabat_id)
                data.attributes = {...data.attributes, jenis_name, rumpun_name}
                return data
            })
        )
    }

    this.training$ = this.diklatService.get({}).pipe(
        map((response: any) => (response.data as any[]).filter(d => this.training_id ? d.id == this.training_id : true)))
    this.rumpun$ = this.simpegService.getRumpun(),
    this.jenisDiklat$ = this.simpegService.getJenisDiklat()

  }

//   onChange(event: any, eName: 'nama_pejabat' | 'pejabat') {
//     const text = event.target.value
//     if (eName === 'nama_pejabat') {
//       this.filterNamaPejabat$ = this.namaPejabat$.pipe(map((data: any[]) => data?.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
//     } else if (eName === 'pejabat') {
//       this.filterPejabat$ = this.pejabat$.pipe(map((data: any[]) => data?.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
//     }
//   }

  viewHandle(value: any) {
    return typeof value === 'string' ? value : value.nama
  }

  ngAfterViewInit(): void {

    setTimeout(() => {
    //   if (this.f.controls) {

    // this.f.controls['training_id'].valueChanges
    //     .pipe(map(training_id => {                        
    //     if (training_id) {
    //         // this.diklatId = training_id
    //         // console.log(this.diklatId);
            
    //         // this.store.dispatch(new GetDataDiklat({include: 'training_periode'}, training_id))
    //         // this.store.dispatch(new GetDataAngkatan({params: 'training_id='+training_id, include: 'training,training_schedule'}))

    //         this.totalSchedule$ = this.scheduleService.get({params: 'filter[training_id]='+training_id}).pipe(map(data => data.data.length))
    //     }
    //     })).subscribe()
    //   }
    }, 300)
  }

  successSubmit(result: any): void {
    this.back()
  }

//   changeGroup(e) {this.f.value.training_id = null}

  onChangeImage(e: FileImage) {
    // this.f.addControl({[e.id]: new FormControl(e.image)})
    this.f.controls[e.id].setValue(e.image) //image is base64    
    console.log(e.id);
    
    // this.f.form.addControl(e.id, new FormControl(e.image, Validators.required))
    console.log(this.f.value);
  }

  getTotalAngkatan() {
    console.log('change diklat');
    
    this.totalSchedule$ = this.scheduleService.get({include: 'training', params: 'filter[training_id]='+this.training_id})
    .pipe(map(data => data.data.length))
  }

  onSubmit(f: NgForm) {
    const id = this.storeKey == 'angkatanId' ? this.activateRoute.snapshot.paramMap.get('id') : this.id

    const value = {...f.value}//, nama_pejabat_id: this.namaPejabat.id, pejabat_id: this.pejabat.id}
    const doSave = id ? this.scheduleService.update(value, id) : this.scheduleService.create(value)
    this.doRequest(doSave, {
        next: (result) => this.successSubmit(result),
        error: (error) => {},
        complete: () => {}
      } as BehaviorSubject<any>
    )
  } 
}
