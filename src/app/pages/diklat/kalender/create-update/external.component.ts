import { Component, Injector } from '@angular/core';
import { FormControl, NgForm, Validators } from '@angular/forms';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable, forkJoin } from 'rxjs';
import { KalenderDiklats } from '../../../../../store/kalender-diklat/kalender-diklat';
import { CreateUpdateData, GetData as GetDataKalender } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { KalenderDiklatState } from '../../../../../store/kalender-diklat/kalender-diklat.state';
import { CreateEditStoreBaseComponent } from '../../../../shared/base/create-edit-base.component';
import { FileImage } from '../../../../shared/components/upload-image.component';
import { SimpegService } from '../../../../shared/services/simpeg.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'ngx-eksternal-diklat',
    template: `
    <div class="pt-3" *ngIf="(schedule$ | async) as schedules">
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate *ngIf="!id || schedules.response.data && schedules.response.data[0] as schedule">

            <input type="hidden" name="category" [ngModel]="'eksternal'" #category="ngModel">

            <div class="form-group row mb-3">
                <label for="link" class="label col-form-label col-3">Banner</label>
                <div class="col-sm-9">
                    <ngx-upload-image id="banner" [base64]="schedule?.attributes?.banner" (change)="onChangeImage($event)"></ngx-upload-image>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="name" class="label col-form-label col-3">Nama Diklat</label>
                <!-- {{(nextPeriode$ | async)}} -->
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="name"
                        type="text"
                        id="name"
                        [ngModel]="id ? schedule?.attributes?.name : f.value.name" required #periode="ngModel"
                        placeholder=""
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="jenis_id" class="label col-form-label col-3">Kelompok Diklat</label>
                <div class="col-sm-9">
                    <nb-select name="jenis_id" fullWidth [ngModel]="id ? schedule?.attributes?.jenis_id : f?.value?.jenis_id" required #jenis_id="ngModel">
                        <nb-option *ngFor="let jenis of (jenisDiklat$ | async)" [value]="jenis.id">{{jenis.nama}}</nb-option>
                    </nb-select>
                </div>
            </div>

            <div *ngIf="jenis_id.value == 2" class="form-group row mb-3">
                <label for="jenis_diklat_id" class="label col-form-label col-3">Jenis Diklat</label>
                <div class="col-sm-9">
                    <nb-select name="jenis_diklat_id" fullWidth [ngModel]="id ? schedule?.attributes?.jenis_diklat_id : f?.value?.jenis_diklat_id" required>
                        <nb-option *ngFor="let jenis_diklat of (jenis2Diklat$ | async)" [value]="jenis_diklat.id">{{jenis_diklat.nama}}</nb-option>
                    </nb-select>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="rumpun_id" class="label col-form-label col-3">Rumpun</label>
                <div class="col-sm-9">  
                    <nb-select name="rumpun_id" fullWidth [ngModel]="id ? schedule?.attributes?.rumpun_id : f?.value?.rumpun_id" required #rumpun_id="ngModel">
                        <nb-option *ngFor="let rumpun of (rumpun$ | async)" [value]="rumpun.id">{{rumpun.nama}}</nb-option>
                    </nb-select>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="nomor_sk" class="label col-form-label col-3">Nomor SK</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="nomor_sk"
                        type="text"
                        id="nomor_sk"
                        [ngModel]="data?.attributes?.nomor_sk"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="nomor_sk" class="label col-form-label col-3">Tanggal SK</label>
                <div class="col-sm-9">
                    <input autocomplete="off" id="tanggal_sk" name="tanggal_sk" nbInput fullWidth [nbDatepicker]="tanggalSK" [ngModel]="data?.attributes?.tanggal_sk | dns" required>
                    <nb-datepicker #tanggalSK></nb-datepicker>
                </div>
            </div>

            <!-- <div class="form-group row mb-3">
                <label for="nama_pejabat_id" class="label col-form-label col-3">Nama Pejabat</label>
                <div class="col-sm-9">
                    <input nbInput
                        fullWidth
                        name="nama_pejabat_id"
                        [(ngModel)]="namaPejabat"
                        type="text"
                        (input)="onChange($event, 'nama_pejabat')"
                        [nbAutocomplete]="autoNama" />

                    <nb-autocomplete #autoNama [handleDisplayFn]="viewHandle">
                        <nb-option *ngFor="let option of filterNamaPejabat$ | async" [value]="option">
                            {{ option.nama }}
                        </nb-option>
                    </nb-autocomplete>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="pejabat_id" class="label col-form-label col-3">Pejabat</label>
                <div class="col-sm-9">
                    <input nbInput
                        fullWidth
                        name="pejabat_id"
                        [(ngModel)]="pejabat"
                        type="text"
                        (input)="onChange($event, 'nama_pejabat')"
                        [nbAutocomplete]="autoPejabat" />

                    <nb-autocomplete #autoPejabat [handleDisplayFn]="viewHandle">
                        <nb-option *ngFor="let option of filterPejabat$ | async" [value]="option">
                            {{ option.nama }}
                        </nb-option>
                    </nb-autocomplete>
                </div>
            </div> -->

            <div class="form-group row mb-3">
                <label for="penyelenggara" class="label col-form-label col-3">Penyelenggara</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="penyelenggara"
                        type="text"
                        id="penyelenggara"
                        [ngModel]="id ? schedule?.attributes?.penyelenggara : f.value.penyelenggara" required #penyelenggara="ngModel"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="duration" class="label col-form-label col-3">Durasi Hari</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                            fullWidth
                            name="duration"
                            type="number"
                            id="duration"
                            [ngModel]="id ? schedule?.attributes?.duration : f.value.duration" required #duration="ngModel"
                            min="1"
                            placeholder="1"
                            fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="start_date" class="label col-form-label col-3">Waktu Pelaksanaan</label>
                <div class="col-sm-9">
                    <input autocomplete="off" id="start_date" name="start_date" nbInput fullWidth placeholder="Waktu Pelaksanaan" [nbDatepicker]="startDatepicker" [ngModel]="(id ? schedule?.attributes?.start_date : f.value.start_date) | dns" required #start_date="ngModel">
                    <nb-datepicker #startDatepicker></nb-datepicker>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="location" class="label col-form-label col-3">Lokasi Pelaksanaan</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                        fullWidth
                        name="location"
                        type="text"
                        id="location"
                        [ngModel]="id ? schedule?.attributes?.location : f.value.location" required #location="ngModel"
                        placeholder="Lokasi diklat"
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="purpose" class="label col-form-label col-3">Tujuan</label>
                <div class="col-sm-9">
                    <textarea rows="5" name="purpose" nbInput fullWidth placeholder="Tujuan dilaksanakannya diklat" [ngModel]="id ? schedule?.attributes?.purpose : f.value.purpose" required #purpose="ngModel"></textarea>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="info" class="label col-form-label col-3">Keterangan</label>
                <div class="col-sm-9">
                    <textarea rows="5" name="info" nbInput fullWidth placeholder="Penjelasan tentang diklat" [ngModel]="id ? schedule?.attributes?.info : f.value.info" required #info="ngModel"></textarea>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="link" class="label col-form-label col-3">Link Pendaftaran</label>
                <div class="col-sm-9">
                    <input class="col" nbInput
                            fullWidth
                            name="link"
                            type="text"
                            id="link"
                            [ngModel]="id ? schedule?.attributes?.link : f.value.link" required #link="ngModel"
                            placeholder=""
                            fieldSize="medium">
                </div>
            </div>
            <div class="row justify-content-end">
                <div class="col-auto">
                    <button nbButton type="button" ghost size="small" class="mr-2" (click)="back()">Batalkan</button>
                    <button nbButton type="submit" size="small" status="primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
  `,
  styleUrls: ['./create-update.component.scss']
})
export class ExternalComponent extends CreateEditStoreBaseComponent<CreateUpdateData, KalenderDiklats.CreateUpdateKalenderDiklatInput, GetDataKalender, KalenderDiklats.KalenderDiklat> {

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedule$: Observable<KalenderDiklats.KalenderDiklat>;

  jenisDiklat$: Observable<any>
  jenis2Diklat$: Observable<any>
  rumpun$: Observable<any>
//   pejabat$: Observable<any>
//   namaPejabat$: Observable<any>
//   filterNamaPejabat$: Observable<any>
//   filterPejabat$: Observable<any>

  pejabat: any
  namaPejabat: any

  constructor(injector: Injector) {
    super(injector, 'kalenderdiklat', CreateUpdateData, GetDataKalender, {include: 'training'});

    this.jenisDiklat$ = this.simpegService.get('mJenisDiklat').pipe(map(({data}) => data))
    this.jenis2Diklat$ = this.simpegService.get('mDiklat').pipe(map(({data}) => data.filter(d => d.jenis_diklat.id == 2)))
    // this.filterPejabat$ = this.pejabat$ = this.simpegService.get("mPejabat").pipe(map(({data}) => data))
    // this.filterNamaPejabat$ = this.namaPejabat$ = this.simpegService.get("mNamaPejabat").pipe(map(({data}) => data))
    this.rumpun$ = this.simpegService.get('mRumpun').pipe(map(({data}) => data))
  }

  ngAfterViewInit() {
    // if (this.id) {
    //     forkJoin([
    //         this.filterNamaPejabat$,
    //         this.filterPejabat$,
    //         this.store.dispatch(new GetDataKalender({include: 'training'}, this.id))])
    //       .subscribe(([namaPejabat, pejabat, item]) => {
    //         this.namaPejabat = namaPejabat.find(n => n.id === item.attributes.nama_pejabat_id)
    //         this.pejabat = pejabat.find(n => n.id === item.attributes.pejabat_id)
    //       })
    // }
  }

  onSubmit(f: NgForm) {
    const value = {...f.value, /*nama_pejabat_id: this.namaPejabat.id, pejabat_id: this.pejabat.id*/}
    const doSave = this.store.dispatch(new this.createUpdateCreator(value, this.id))
    this.doRequest(doSave, {
        next: (result) => this.successSubmit(result),
        error: (error) => this.errorrSubmit(error),
        complete: () => this.completeSubmit()
      } as BehaviorSubject<any>
    )
  } 

  successSubmit(result: any): void {
    this.back()
  }

  onChangeImage(e: FileImage) {    
    this.f.form.addControl(e.id, new FormControl(e.image, Validators.required))
    console.log(this.f.value);
  }

//   onChange(event: any, eName: 'nama_pejabat' | 'pejabat') {
//     const text = event.target.value
//     if (eName === 'nama_pejabat') {
//       this.filterNamaPejabat$ = this.namaPejabat$.pipe(map((data: any[]) => data?.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
//     } else if (eName === 'pejabat') {
//       this.filterPejabat$ = this.pejabat$.pipe(map((data: any[]) => data?.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
//     }
//   }

  viewHandle(value: any) {
    return typeof value === 'string' ? value : value.nama
  }
}
