import { Component, Injector, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { routes } from '@nebular/auth';
import { Select } from '@ngxs/store';
import { clear } from 'console';
import { Observable } from 'rxjs';
import { Pesans } from '../../../../store/pesan/pesan';
import { CreateUpdateData, GetData } from '../../../../store/pesan/pesan.action';
import { PesanState } from '../../../../store/pesan/pesan.state';
import { CreateEditStoreBaseComponent } from '../../../shared/base/create-edit-base.component';

@Component({
  selector: 'ngx-buat-pesan',
  templateUrl: './buat-pesan.component.html',
  styleUrls: ['./buat-pesan.component.scss']
})
export class BuatPesanComponent extends CreateEditStoreBaseComponent<CreateUpdateData, Pesans.CreateUpdatePesanInput, GetData, Pesans.Pesan> {

  @Select(PesanState.getAPesans)
  pesan$: Observable<Pesans.Pesan>;
  constructor(injector: Injector) {
    super(injector, 'pesan', CreateUpdateData, GetData)
   }

   successSubmit(result: any): void {
    this.back()
  }

  // OnSubmit(f: NgForm) {
  //   console.log(f.value);
    
  // }

}
