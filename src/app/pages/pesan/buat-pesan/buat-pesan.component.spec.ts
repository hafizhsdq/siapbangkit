import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuatPesanComponent } from './buat-pesan.component';

describe('BuatPesanComponent', () => {
  let component: BuatPesanComponent;
  let fixture: ComponentFixture<BuatPesanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuatPesanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuatPesanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
