import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-pesan',
  templateUrl: './pesan.component.html',
  styleUrls: ['./pesan.component.scss']
})
export class PesanComponent implements OnInit {

  tabs: any[] = [
    {
      title: 'Buat Pesan',
      route: '/pages/pesan/buat-pesan',
    },
    {
      title: 'Kotak Masuk',
      route: '/pages/pesan/kotak-masuk',
    },
    {
      title: 'Kotak Keluar',
      route: '/pages/pesan/kotak-keluar',
    },
    {
      title: 'Arsip',
      route: '/pages/pesan/arsip-pesan',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
