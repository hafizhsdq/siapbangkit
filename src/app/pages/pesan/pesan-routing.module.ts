import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PesanComponent } from './pesan.component';
import { BuatPesanComponent } from './buat-pesan/buat-pesan.component';
import { KotakPesanComponent } from './kotak-pesan/kotak-pesan.component';

const routes: Routes = [{
  path: '',
  component: PesanComponent,
  children: [
    {
        path: "buat-pesan",
        component: BuatPesanComponent
    },
    {
        path: "kotak-masuk",
        component: KotakPesanComponent,
        data: { params: 'message=in&status=unarchive'}
    },
    {
      path: "kotak-keluar",
      component: KotakPesanComponent,
      data: { params: 'message=out&status=unarchive'}
    },
    {
      path: "arsip-pesan",
      component: KotakPesanComponent,
      data: { params: 'status=archive'}
    },
    {
        path: "",
        redirectTo: "buat-pesan",
        pathMatch: "full"
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PesanRoutingModule {
}
