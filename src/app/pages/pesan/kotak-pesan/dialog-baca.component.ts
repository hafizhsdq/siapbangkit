import { ChangeDetectionStrategy, Component, Injector, OnChanges, SimpleChanges } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs-compat/operator/filter';
import { BaseData } from '../../../../store/base-model';
import { Pesans } from '../../../../store/pesan/pesan';
import { CreateUpdateData, DeleteData, GetData } from '../../../../store/pesan/pesan.action';
import { PesanState } from '../../../../store/pesan/pesan.state';
import { CreateEditDialogStoreBaseComponent } from "../../../shared/base/create-edit-dialog-base.component";


@Component ({
    selector: 'dialog-baca',
    template: `
      <nb-card>
        <!-- {{ message | json }} -->
        <nb-card-header class="text-primary">{{ message?.subject }}</nb-card-header>
        <nb-card-body>
          <div class="row" style="min-width: 300px">
            <div class="col-12">
              <label class="label">Dari</label>
              <span>: {{ message?.from }}</span>
              <p>{{ message?.content }}</p>
            </div>
            <div class="col-12 text-right">
              <span class="caption">{{message.created_at | date:'fullDate'}}</span>
            </div>
          </div>
        </nb-card-body>
        <nb-card-footer class="d-flex justify-content-end">
          <button nbButton status="primary" (click)="cancel()">Tutup</button>
        </nb-card-footer>
      </nb-card>
    `
})
  
export class DialogBacaComponent {

  message: Pesans.Pesan
  
  constructor(public dialogRef: NbDialogRef<DialogBacaComponent>) {}

  cancel() {
    this.dialogRef.close(0)
  }
}