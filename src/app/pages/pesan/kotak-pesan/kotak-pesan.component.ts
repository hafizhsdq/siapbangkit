import { Component, Injector, TemplateRef, ViewChild } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { BaseData } from '../../../../store/base-model';
import { Pesans } from '../../../../store/pesan/pesan';
import { DeleteData, GetData } from '../../../../store/pesan/pesan.action';
import { PesanState } from '../../../../store/pesan/pesan.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { PesanService } from '../../../shared/services/pesan.service';
import { DialogBacaComponent } from './dialog-baca.component';

@Component({
  selector: 'ngx-kotak-masuk',
  templateUrl: './kotak-pesan.component.html'
})
export class KotakPesanComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(PesanState.getPesans)
  pesan$: Observable<Pesans.Pesan[]>;

  @ViewChild('dialog') dialog: TemplateRef<any>;

  constructor(injector: Injector, public messageService: PesanService) {
    super(injector, GetData, DeleteData, {params: 'message=in&status=unarchive'})
    this.tag = 'pesan-menu'
  }

  public read(message: Pesans.Pesan): void {    
    this.dialogService.open(DialogBacaComponent, { hasScroll: true, context: {message} })    
  }

  public archive(id: string): void {
    this.loading = true
    console.log(id);
    
    this.messageService.changeStatus(id).subscribe(() => {
      this.loading = false
      this.getData()
    })
  }

  ngOnInit(): void {

    this.nbMenuService.onItemClick()
    .pipe(
        filter(({ tag }) => tag === this.tag),
    )
    .subscribe(result => {        
        if (result.item.title == 'Baca') this.read(result.item.data);
        if (result.item.title == 'Unarchive' || result.item.title == 'Archive') this.archive(result.item.data);
        if (result.item.title == 'Delete') this.OnDelete(result.item.data);
    });

    this.getData()
  }

  public getData(): void {
    this.activateRoute.data.subscribe(({params}) => {
      this.store.dispatch(new GetData({params}))
    })
  }

  

}


