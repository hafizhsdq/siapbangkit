import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PesanComponent } from './pesan.component';
import { PesanRoutingModule } from './pesan-routing.module';
import { NbDatepickerModule, NbLayoutModule, NbRouteTabsetModule, NbTabsetModule } from '@nebular/theme';
import { SharedModule } from '../../shared/shared.module';
import { BuatPesanComponent } from './buat-pesan/buat-pesan.component';
import { DialogBacaComponent } from './kotak-pesan/dialog-baca.component';
import { KotakPesanComponent } from './kotak-pesan/kotak-pesan.component';



@NgModule({
  declarations: [
    PesanComponent,
    BuatPesanComponent,
    DialogBacaComponent,
    KotakPesanComponent
  ],
  imports: [
    CommonModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbLayoutModule,
    SharedModule,
    NbDatepickerModule,
    PesanRoutingModule
  ]
})
export class PesanModule { }
