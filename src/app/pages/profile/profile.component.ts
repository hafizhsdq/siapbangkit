import { Component, Injector } from '@angular/core';
import { Store } from '@ngxs/store';
import { BiodataState } from '../../../store/biodata/biodata.state';
import { PageBaseComponent } from '../../shared/base/page-base.component';
import { FileUpload } from '../../shared/components/upload-file.component';
import { SimpegService } from '../../shared/services/simpeg.service';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends PageBaseComponent {

  tabs: any[] = [
    {
      title: 'Biodata Diri',
      route: '/pages/profile/biodata',
    },
    // {
    //   title: 'Data Kompetensi',
    //   route: '/pages/profile/data-kompetensi',
    // },
    {
      title: 'Ganti Password',
      route: '/pages/profile/ganti-password',
    },
    // {
    //   title: 'Isian Khusus Bagi Peserta Seleksi',
    //   route: '/pages/profile/data-seleksi',
    // },
  ];

  constructor(injector: Injector, public store: Store) {
    super(injector);
  }

  changeAvatar(fileUpload: FileUpload) {
    this.biodataService.changeAvatar(fileUpload.filePath)
      .subscribe(() => {
      })
  }

  matchingSimpeg() {
    const biodata = this.store.selectSnapshot(BiodataState).response.data[0]
    
    this.simpegService.get(biodata.nip).subscribe((result) => {
      if(result?.response.success) {
        const {
          nama,
          nip,
          gelar_depan,
          gelar_belakang,
          jenis_kelamin,
          status_kawin,
          jenis_pegawai,
          status_pegawai,
          kedudukan_pegawai,
          jabatan,
          pangkat,
          nip_lama,
          foto,
          tempat_lahir,
          tanggal_lahir,
          telepon,
          handphone_1,
          handphone_2,
          whatsapp,
          email,
          email_sidoarjokab,
          is_sesuai_ktp,
          ktp_kelurahan,
          ktp_kodepos,
          ktp_rw,
          ktp_rt,
          ktp_alamat,
          domisili_kelurahan,
          domisili_kodepos,
          domisili_rw,
          domisili_rt,
          domisili_alamat,
          nik,
          nomor_kk,
          npwp,
          nuptk,
          duk,
          nomor_karpeg,
          nomor_korpri,
        } = result.data

        const {unit_organisasi, subunit_organisasi, seksi} = jabatan.struktur_organisasi

        this.doRequest(this.biodataService.update({
          avatar: foto,
          nama,
          nip,
          gelar_depan,
          gelar_belakang,
          jenis_kelamin: jenis_kelamin.nama,
          status_kawin: status_kawin.nama,
          jenis_pegawai: jenis_pegawai.nama,
          status_pegawai: status_pegawai.nama,
          kedudukan_pegawai: kedudukan_pegawai.nama,
          jabatan: jabatan.struktur_organisasi.jabatan.nama,
          pangkat,
          nip_lama,
          tempat_lahir,
          tanggal_lahir,
          telepon,
          handphone_1,
          handphone_2,
          whatsapp,
          email,
          email_sidoarjokab,
          is_sesuai_ktp: is_sesuai_ktp == 'Ya' ? true : false,
          ktp_kelurahan: ktp_kelurahan?.nama,
          ktp_kodepos,
          ktp_rw,
          ktp_rt,
          ktp_alamat,
          domisili_kelurahan: domisili_kelurahan?.nama,
          domisili_kodepos,
          domisili_rw,
          domisili_rt,
          domisili_alamat,
          nik,
          nomor_kk,
          npwp,
          nuptk,
          duk,
          nomor_karpeg,
          nomor_korpri,
          unit_organisasi: unit_organisasi.nama,
          sub_unit_organisasi: subunit_organisasi.nama,
          seksi: seksi.nama
        }, nip))
      } else {
        this.swalHandler.alert({
          title: 'Informasi', 
          text: 'Data NIP pada database SIMPEG tidak ditemukan',
          icon: 'error'
        })
      }
    })
  }
}
