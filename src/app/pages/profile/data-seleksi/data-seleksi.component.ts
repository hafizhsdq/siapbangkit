import { Component, Injector, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { LampiranDokumenForm } from './tambah-data/lampiran-component';

@Component({
  selector: 'ngx-data-seleksi',
  templateUrl: './data-seleksi.component.html',
  styleUrls: ['./data-seleksi.component.scss']
})
export class DataSeleksiComponent implements OnInit {

  constructor(private dialogService: NbDialogService) {}

  openLampiran = () => { this.dialogService.open(LampiranDokumenForm, { hasScroll: true }) }

  ngOnInit(): void {
  }

  OnSubmit(f: NgForm) {
    console.log(f.value);
    
  }
}
