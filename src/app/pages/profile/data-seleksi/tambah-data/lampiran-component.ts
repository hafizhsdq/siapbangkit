import { Component, Injector } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { CreateEditBaseComponent } from "../../../../shared/base/create-edit-base.component";
import { CreateEditDialogBaseComponent } from "../../../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'form-formal',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    {{title}}
                </nb-card-header>
                <nb-card-body>
                    <ng-container 
                        [ngTemplateOutlet]="formGroup"></ng-container>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>

        <ng-template #formGroup>
            <div class="form-group row mb-3">
                <label for="jenis" class="label col-form-label col-4">Jenis</label>
                <div class="col-sm-8">
                    <nb-select name="jenis" selected="1" fullWidth ngModel required #jenis="ngModel">
                        <nb-option value="1">Option 1</nb-option>
                        <nb-option value="2">Option 2</nb-option>
                    </nb-select>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="judul" class="label col-form-label col-4">Judul</label>
                <div class="col-sm-8">
                    <ngx-upload id="judul" (change)="onChange($event)"></ngx-upload>
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="keterangan" class="label col-form-label col-4">Keterangan</label>
                <div class="col-sm-8">
                    <input class="col" nbInput
                        fullWidth
                        name="keterangan"
                        type="text"
                        id="keterangan"
                        ngModel required #keterangan="ngModel"
                        placeholder=""
                        fieldSize="medium">
                </div>
            </div>
        </ng-template>
    `,
})
export class LampiranDokumenForm extends CreateEditDialogBaseComponent<LampiranDokumenForm> {
    title: string = "Tambah Data Lampiran Dokumen"
    constructor(injector: Injector, public dialogRef: NbDialogRef<LampiranDokumenForm>) {
        super(injector, dialogRef)
    }
}