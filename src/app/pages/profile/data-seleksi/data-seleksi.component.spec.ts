import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataSeleksiComponent } from './data-seleksi.component';

describe('DataSeleksiComponent', () => {
  let component: DataSeleksiComponent;
  let fixture: ComponentFixture<DataSeleksiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataSeleksiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataSeleksiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
