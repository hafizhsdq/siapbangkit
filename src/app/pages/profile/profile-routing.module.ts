import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProfileComponent } from './profile.component';
import { BiodataComponent } from './biodata/biodata.component';
import { DataKompetensiComponent } from './data-kompetensi/data-kompetensi.component';
import { DataSeleksiComponent } from './data-seleksi/data-seleksi.component';
import { GantiPasswordComponent } from './ganti-password/ganti-password.component';
import { AuthGuard } from '../../shared/guard/auth.guard';


const routes: Routes = [{
  path: '',
  component: ProfileComponent,
  canActivateChild: [AuthGuard],
  children: [
    {
        path: "biodata",
        component: BiodataComponent
    },
    {
      path: "biodata/:id",
      component: BiodataComponent
    },
    {
        path: "data-kompetensi",
        component: DataKompetensiComponent
    },
    // {
    //     path: "data-seleksi",
    //     component: DataSeleksiComponent
    // },
    {
      path: "ganti-password",
      component: GantiPasswordComponent
    },
    {
        path: "",
        redirectTo: "biodata",
        pathMatch: "full"
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {
}
