import { Component, Injector } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { CreateEditStoreBaseComponent } from '../../../shared/base/create-edit-base.component';
import { BiodataService } from '../../../shared/services/biodata.service';

@Component({
  selector: 'ngx-ganti-password',
  templateUrl: './ganti-password.component.html',
  styleUrls: ['./ganti-password.component.scss']
})
export class GantiPasswordComponent extends CreateEditStoreBaseComponent<null, null, null, null> {

  constructor(injector: Injector) {
    super(injector, 'biodata');
  }

  onSubmit(f: NgForm): void {
    const{old_password, new_password} = f.value
    this.doRequest(this.biodataService.changePassword({old_password, new_password}), {
        next: (result) => this.successSubmit(result),
        error: (error) => this.errorrSubmit(error),
        complete: () => this.completeSubmit()
      } as BehaviorSubject<any>
    )
  }

  successSubmit(result) {
    this.f.reset()
  }
}
