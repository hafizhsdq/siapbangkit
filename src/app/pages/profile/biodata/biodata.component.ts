import { AfterViewInit, Component, Injector } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Biodatas } from '../../../../store/biodata/biodata';
import { CreateUpdateBiodata } from '../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../store/biodata/biodata.state';
import { PageBaseComponent } from '../../../shared/base/page-base.component';
import { Observable } from 'rxjs';
import { OPD } from '../../../@core/data/opd';
import { SimpegService } from '../../../shared/services/simpeg.service';
import { GetDataSimpeg } from '../../../../store/simpeg/simpeg';
import { map, mergeMap } from 'rxjs/operators';
import { switchMap } from 'rxjs-compat/operator/switchMap';

@Component({
  selector: 'ngx-biodata',
  templateUrl: './biodata.component.html',
  styleUrls: ['./biodata.component.scss']
})
export class BiodataComponent extends PageBaseComponent {

  results: any

  ngOnInit(): void {
    super.ngOnInit()
    
    this.biodata$.pipe(
        map(data => data.attributes.nip),
        mergeMap((nip) => this.simpegService.get(`tPegawai&nip=${nip}`))
    ).subscribe(result => {
        this.results = result
    })
    // this.results = {
    //     "response": {
    //         "success": true,
    //         "message": "OK"
    //     },
    //     "data": {
    //         "id": 78,
    //         "nip": "196903301989031004",
    //         "nama": "KUSDIANTO",
    //         "nip_lama": "510109776",
    //         "gelar_depan": null,
    //         "gelar_belakang": "SH., MH",
    //         "nama_panggilan": null,
    //         "foto": null,
    //         "agama": {
    //             "id": 1,
    //             "nama": "Islam"
    //         },
    //         "jenis_kelamin": {
    //             "id": 1,
    //             "nama": "Laki-Laki"
    //         },
    //         "tempat_lahir": "Sidoarjo",
    //         "tanggal_lahir": "1969-03-30",
    //         "status_kawin": {
    //             "id": 2,
    //             "nama": "Kawin"
    //         },
    //         "track_record": null,
    //         "jenis_pegawai": {
    //             "id": 1,
    //             "nama": "PNS Daerah Kabupaten / Kota yang Bekerja Pada Kabupaten / Kota"
    //         },
    //         "status_pegawai": {
    //             "id": 2,
    //             "nama": "Pegawai Negeri Sipil"
    //         },
    //         "kedudukan_pegawai": {
    //             "id": 1,
    //             "nama": "Pegawai Aktif"
    //         },
    //         "status_pelimpahan": [],
    //         "asal_pelimpahan": null,
    //         "tanggal_pelimpahan": null,
    //         "tmt_masuk": "1989-03-01",
    //         "tmt_pensiun": "2027-04-01",
    //         "mk_tahun": null,
    //         "mk_bulan": null,
    //         "tmt_berhenti": null,
    //         "jenis_pemberhentian": [],
    //         "is_hak_pensiun": null,
    //         "jenis_pensiun": [],
    //         "is_aktif": "Ya",
    //         "handphone_1": "08121772009",
    //         "handphone_2": null,
    //         "whatsapp": null,
    //         "email": "kusdianto69@gmail.com",
    //         "email_sidoarjokab": "kusdianto@sidoarjokab.go.id",
    //         "ktp_provinsi": [],
    //         "ktp_kota": [],
    //         "ktp_kecamatan": [],
    //         "ktp_kelurahan": [],
    //         "ktp_kodepos": null,
    //         "ktp_rw": null,
    //         "ktp_rt": null,
    //         "ktp_alamat": "Perum Bumi Suko Indah Blok A5 No. 5 Sidoarjo",
    //         "is_sesuai_ktp": "Ya",
    //         "domisili_provinsi": [],
    //         "domisili_kota": [],
    //         "domisili_kecamatan": [],
    //         "domisili_kelurahan": [],
    //         "domisili_kodepos": null,
    //         "domisili_rw": null,
    //         "domisili_rt": null,
    //         "domisili_alamat": "Perum Bumi Suko Indah Blok A5 No. 5 Sidoarjo",
    //         "nik": null,
    //         "nomor_kk": null,
    //         "npwp": null,
    //         "nuptk": null,
    //         "duk": "2427",
    //         "nomor_karpeg": "E.736124",
    //         "nomor_korpri": null,
    //         "nomor_karis_karsu": "010771 I",
    //         "nomor_askes": null,
    //         "nomor_bpjs": null,
    //         "nomor_taspen": "510109776",
    //         "is_general_checkup": "Tidak",
    //         "hasil_general_checkup_id": null,
    //         "tanggal_general_checkup": null,
    //         "keterangan_general_checkup": null,
    //         "is_asuransi": "Tidak",
    //         "tanggal_asuransi": null,
    //         "jenis_asuransi": null,
    //         "tanggal_jenis_asuransi": null,
    //         "is_ambil_asuransi": null,
    //         "tanggal_ambil_asuransi": null,
    //         "nominal_asuransi": null,
    //         "is_bapetarum": "Tidak",
    //         "jenis_bapetarum": [],
    //         "nomor_bapetarum": null,
    //         "tanggal_bapetarum": null,
    //         "jabatan": {
    //             "id": 25,
    //             "struktur_organisasi": {
    //                 "id": 1561,
    //                 "unit_organisasi": {
    //                     "id": 351,
    //                     "nama": "BADAN KEPEGAWAIAN DAERAH"
    //                 },
    //                 "subunit_organisasi": [],
    //                 "seksi": {
    //                     "id": 2417,
    //                     "nama": "BIDANG PENDIDIKAN DAN PELATIHAN"
    //                 },
    //                 "jabatan": {
    //                     "id": 208,
    //                     "nama": "KEPALA"
    //                 },
    //                 "is_aktif": "Ya"
    //             },
    //             "atasan": [],
    //             "tmt_jabatan": "2021-10-21",
    //             "pejabat": {
    //                 "id": 14,
    //                 "nama": "BUPATI SIDOARJO"
    //             },
    //             "nomor_sk": "821.2/9165/438.6.4/2021",
    //             "tanggal_sk": "2021-10-21",
    //             "golongan": {
    //                 "id": 13,
    //                 "kode": "IV/a",
    //                 "nama": "Pembina"
    //             },
    //             "golongan_pppk": [],
    //             "tmt_golongan": "2020-04-01",
    //             "eselon": {
    //                 "id": 6,
    //                 "nama": "IIIb"
    //             },
    //             "tmt_eselon": "2020-01-07",
    //             "unit_organisasi_text": null,
    //             "subunit_organisasi_text": null,
    //             "seksi_text": null,
    //             "jabatan_text": null,
    //             "rumpun": []
    //         },
    //         "jabatan_plt": []
    //     }
    // }
  }
}
