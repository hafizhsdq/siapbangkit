import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataKompetensiComponent } from './data-kompetensi.component';

describe('DataKompetensiComponent', () => {
  let component: DataKompetensiComponent;
  let fixture: ComponentFixture<DataKompetensiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataKompetensiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataKompetensiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
