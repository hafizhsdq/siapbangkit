import { Component, Injector } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DeleteData, GetData } from '../../../../store/pendidikan-formal/pendidikan-formal.action';
import { DeleteData as DeleteDataPapers } from '../../../../store/karya-tulis/karya-tulis.action';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { DialogFormUploadComponent, DialogPengantarComponent } from '../../pengajuan/izin-belajar/izin-belajar.component';
import { KaryaTulisForm } from './tambah-data/karya-tulis.component';
import { PendidikanFormalForm } from './tambah-data/pendidikan-formal.component';
import { PendidikanPelatihanForm } from './tambah-data/pendidikan-pelatihan.component';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { PendidikanFormals } from '../../../../store/pendidikan-formal/pendidikan-formal';
import { KaryaTuliss } from '../../../../store/karya-tulis/karya-tulis';
import { BiodataState } from '../../../../store/biodata/biodata.state';
import { DeleteDocument, GetBiodatas } from '../../../../store/biodata/biodata.action';
import { BaseData } from '../../../../store/base-model';
import { Dokumens } from '../../../../store/dokumen-persyaratan/dokumen-persyaratan';
import { DocumentForm } from './tambah-data/document.component';
import { UserCourses } from '../../../../store/user-course/user-course';

@Component({
  selector: 'ngx-data-kompetensi',
  templateUrl: './data-kompetensi.component.html',
  styleUrls: ['./data-kompetensi.component.scss']
})
export class DataKompetensiComponent extends PageTableStoreComponent<GetData, DeleteData> {

  documenType = [
    {
      "name": "SK CPNS",
      "icon": "edit-outline",
      "button": "Edit"
    },
    {
      "name": "SK PNS",
      "icon": "edit-outline",
      "button": "Edit"
    },
    {
      "name": "SK Pangkat Terakhir",
      "icon": "edit-outline",
      "button": "Edit"
    },
    {
      "name": "SKP 2 Tahun Terakhir",
      "icon": "edit-outline",
      "button": "Edit"
    }
  ]

  @Select(BiodataState.getUserDoc)
  userAttachments$: Observable<BaseData<Dokumens.Dokumen[]>>

  @Select(BiodataState.getUserCourse)
  userCourse$: Observable<BaseData<UserCourses.UserCourse[]>>

  @Select(BiodataState.getUserPaper)
  userPaper$: Observable<BaseData<KaryaTuliss.KaryaTulis[]>>

  @Select(BiodataState.getUserEducation)
  userEdu$: Observable<BaseData<PendidikanFormals.PendidikanFormal[]>>

  constructor(injector: Injector) {
    super(injector, GetBiodatas, null, {include: 'user_education,user_course,users,attachment,user_paper'})
  }

  // public getData(): void {
  //   this.loading = true
  //   this.store.dispatch([new GetData(), new GetDataPelatihan, new GetDataPapers])
  //   .subscribe(() => {
  //     this.loading = false
  //   });
  // }

  public OnEditData(id: string | number, tag: string): void {
    let dialog
    if (tag === 'formal') dialog = this.openPendidikanFormal()
    else if (tag === 'pelatihan') dialog = this.openPendidikanPelatihan()
    else dialog = this.openKaryaTulis()
    
    dialog.componentRef.instance.edit(id)
  }

  public OnDeleteData(id: string, tag: string): void {
    this.swalHandler.deletePopup.instance.fire()
        this.deleteSubscriber = this.swalHandler.deletePopup.instance.confirm.asObservable()
        .subscribe(() => {
            this.doRequest(
            tag === "formal"    ? this.store.dispatch(new DeleteData(id)) :
            // tag === "pelatihan" ? this.store.dispatch(new DeleteDataPelatihan(id)) :
            tag === "paper"     ? this.store.dispatch(new DeleteDataPapers(id)) :
                                  this.store.dispatch(new DeleteDocument(id))
            , {
              next: (result) => this.successSubmit(result),
              error: (error) => this.errorrSubmit(error),
              complete: () => this.completeSubmit()
            } as BehaviorSubject<any>)
            this.deleteSubscriber.unsubscribe()
        })
  }

  successSubmit(result) {this.getData()}
  errorrSubmit(error) {}
  completeSubmit() {}

  openPendidikanFormal = () => { 
    const dialog = this.dialogService.open(PendidikanFormalForm, { hasScroll: true })
    dialog.onClose.subscribe(() => {this.getData()})
    return dialog
  }
  openPendidikanPelatihan = () => { 
    const dialog = this.dialogService.open(PendidikanPelatihanForm, { hasScroll: true }) 
    dialog.onClose.subscribe(() => {this.getData()})
    return dialog
  }
  openAddNewDoc = () => { 
    const dialog = this.dialogService.open(DocumentForm, { hasScroll: true }) 
    dialog.onClose.subscribe(() => {this.getData()})
    return dialog
  }
  openKaryaTulis = () => { this.dialogService.open(KaryaTulisForm, { hasScroll: true }) }

  uploadRekomendasi = () => this.dialogService.open(DialogFormUploadComponent)
  uploadPengantar = () => this.dialogService.open(DialogPengantarComponent)


  OnSubmit(f: NgForm) {
    // this.biodataService.verify().subscribe(() => {

    // })
  }
}
