import { Component, Injector } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { Observable } from "rxjs";
import { GetBiodatas } from "../../../../../store/biodata/biodata.action";
import { KaryaTuliss } from "../../../../../store/karya-tulis/karya-tulis";
import { CreateUpdateData, GetData } from "../../../../../store/karya-tulis/karya-tulis.action";
import { KaryaTulisState } from "../../../../../store/karya-tulis/karya-tulis.state";
import { CreateEditDialogStoreBaseComponent } from "../../../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'form-karya-tulis',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    {{title}}
                </nb-card-header>
                <nb-card-body *ngIf="(papers$ | async ) as papers">
                    <!-- <ng-container [ngTemplateOutlet]="formGroup"></ng-container> -->

                    <div class="form-group row mb-3">
                        <label for="title" class="label col-form-label col-4">Judul Tulisan</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="title"
                                type="text"
                                id="title"
                                [ngModel]="id ? papers?.attributes?.title : f.value.title" required #title="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="year" class="label col-form-label col-4">Tahun/Tanggal</label>
                        <div class="col-sm-8">
                            <nb-form-field>
                                    <input id="year" autocomplete="off" name="year" nbInput fullWidth placeholder="" [nbDatepicker]="yearPicker" [ngModel]="(id ? pelatihan?.attributes?.year : f.value.year) | dns" required #year="ngModel">
                                    <nb-datepicker #yearPicker></nb-datepicker>
                                    <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                            </nb-form-field>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="publish_at" class="label col-form-label col-4">Sarana Publikasi</label>
                        <div class="col-sm-8">
                            <nb-select name="publish_at" selected="1" fullWidth [ngModel]="id ? papers?.attributes?.publish_at : f.value.publish_at" required #publish_at="ngModel">
                                <nb-option value="Seminar">Seminar</nb-option>
                                <nb-option value="Majalah">Majalah</nb-option>
                                <nb-option value="Koran">Koran</nb-option>
                                <nb-option value="Website">Website</nb-option>
                            </nb-select>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="other_info" class="label col-form-label col-4">Keterangan</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="other_info"
                                type="text"
                                id="other_info"
                                [ngModel]="id ? papers?.attributes?.other_info : f.value.other_info" required #other_info="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="file" class="label col-form-label col-4">Karya Ilmiah</label>
                        <div class="col-sm-8">
                            <ngx-upload id="file" (change)="onChangeFile($event)" [limit]='5'></ngx-upload>
                            <span class="caption">pdf max 5MB</span>
                        </div>
                    </div>

                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>

        <ng-template #formGroup>
            
        </ng-template>
    `,
})
export class KaryaTulisForm extends CreateEditDialogStoreBaseComponent<KaryaTulisForm, CreateUpdateData, KaryaTuliss.CreateUpdateKaryaTulisInput, GetData, KaryaTuliss.KaryaTulis> {
    title: string = "Tambah Data Karya Tulis"

    @Select(KaryaTulisState.getKaryaTuliss)
    papers$: Observable<KaryaTuliss.KaryaTulis>

    constructor(injector: Injector, public dialogRef: NbDialogRef<KaryaTulisForm>) {
        super(injector, dialogRef, 'papers', CreateUpdateData, GetData)
    }

    successSubmit(result: any): void {
        // this.store.dispatch(new GetBiodatas({include: 'user_education,user_course,users.attachment,user_paper'}))
        this.dialogRef.close()
    }
}