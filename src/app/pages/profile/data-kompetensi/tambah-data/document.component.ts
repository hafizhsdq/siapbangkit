import { Component, Injector } from "@angular/core";
import { CreateEditDialogStoreBaseComponent } from "../../../../shared/base/create-edit-dialog-base.component";
import { Users } from "../../../../../store/user/user";
import { NgForm } from "@angular/forms";
import { AddDocument, GetBiodatas } from "../../../../../store/biodata/biodata.action";
import { Biodatas } from "../../../../../store/biodata/biodata";
import { NbDialogRef } from "@nebular/theme";

@Component ({
    selector: 'form-karya-tulis',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    {{title}}
                </nb-card-header>
                <nb-card-body>
                    <div class="form-group row mb-3">
                        <label for="title" class="label col-form-label col-4">Judul Dokumen</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="name"
                                type="text"
                                id="title"
                                ngModel required #name="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="file" class="label col-form-label col-4">File</label>
                        <div class="col-sm-8">
                            <ngx-upload id="file" (change)="onChangeFile($event)" [limit]='2'></ngx-upload>
                            <span class="caption">pdf max 2MB</span>
                        </div>
                    </div>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
})
export class DocumentForm extends CreateEditDialogStoreBaseComponent<DocumentForm, AddDocument, Biodatas.AddDocumentInput, GetBiodatas, Biodatas.Biodata> {
    title: string = "Tambah Dokumen"

    constructor(injector: Injector, public dialogRef: NbDialogRef<DocumentForm>) {
        super(injector, dialogRef, 'biodatas', AddDocument, GetBiodatas)
    }

    successSubmit(result: any): void {
        this.dialogRef.close()
    }
}