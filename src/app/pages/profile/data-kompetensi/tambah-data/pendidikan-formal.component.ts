import { Component, Injector } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { Observable } from "rxjs";
import { BaseData } from "../../../../../store/base-model";
import { GetBiodatas } from "../../../../../store/biodata/biodata.action";
import { BiodataState } from "../../../../../store/biodata/biodata.state";
import { PendidikanFormals } from "../../../../../store/pendidikan-formal/pendidikan-formal";
import { CreateUpdateData, GetData } from "../../../../../store/pendidikan-formal/pendidikan-formal.action";
import { CreateEditDialogStoreBaseComponent } from "../../../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'form-formal',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    {{title}}
                </nb-card-header>
                <nb-card-body style="max-height: calc(100vh - 200px);" *ngIf="(userEdu$ | async) as userEdu">
                    <!-- <ng-container [ngTemplateOutlet]="formGroup"></ng-container> -->
                        <div class="form-group row mb-3">
                            <label for="grade" class="label col-form-label col-4">Jenjang Pendidikan</label>
                            <div class="col-sm-8">
                                <nb-select name="grade" selected="1" fullWidth [ngModel]="id ? formal?.attributes?.grade : f.value.grade" required #grade="ngModel">
                                    <nb-option value="sd">SD</nb-option>
                                    <nb-option value="smp">SMP</nb-option>
                                    <nb-option value="sma">SMA</nb-option>
                                    <nb-option value="sarjana">Sarjana</nb-option>
                                    <nb-option value="magister">Magister</nb-option>
                                    <nb-option value="doktor">Doktor</nb-option>
                                    <nb-option value="profesor">Profesor</nb-option>
                                </nb-select>
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="school_name" class="label col-form-label col-4">Nama Sekolah/Universitas</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="school_name"
                                    type="text"
                                    id="school_name"
                                    [ngModel]="id ? formal?.attributes?.school_name : f.value.school_name" required #school_name="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="department" class="label col-form-label col-4">Jurusan/Program Studi</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="department"
                                    type="text"
                                    id="department"
                                    [ngModel]="id ? formal?.attributes?.department : f.value.department" required #department="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="location" class="label col-form-label col-4">Tempat Pendidikan</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="location"
                                    type="text"
                                    id="location"
                                    [ngModel]="id ? formal?.attributes?.location : f.value.location" required #location="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="graduate_at" class="label col-form-label col-4">Tahun Lulus</label>
                            <div class="col-sm-8">
                                <nb-form-field>
                                        <input id="graduate_at" autocomplete="off" name="graduate_at" nbInput fullWidth placeholder="" [nbDatepicker]="graduate_atPicker" [ngModel]="(id ? formal?.attributes?.graduate_at : f.value.graduate_at) | dns" required #graduate_at="ngModel">
                                        <nb-datepicker #graduate_atPicker></nb-datepicker>
                                        <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                                </nb-form-field>
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="izinBelajar" class="label col-form-label col-4">Izin Belajar</label>
                            <div class="col-sm-8">
                                <nb-select name="izinBelajar" selected="0" fullWidth ngModel #izinBelajar="ngModel">
                                    <nb-option value="1">Ya</nb-option>
                                    <nb-option value="0">Tidak</nb-option>
                                </nb-select>
                            </div>
                        </div>

                        <div class="form-group row mb-3" *ngIf="izinBelajar.value == 1">
                            <label for="permit_study_no" class="label col-form-label col-4">No. Izin Belajar</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="permit_study_no"
                                    type="text"
                                    id="permit_study_no"
                                    [ngModel]="id ? formal?.attributes?.permit_study_no : f.value.permit_study_no" required #permit_study_no="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>

                        <div class="form-group row mb-3" *ngIf="izinBelajar.value == 1">
                            <label for="permit_study_at" class="label col-form-label col-4">Tanggal Izin Belajar</label>
                            <div class="col-sm-8">
                                <nb-form-field>
                                        <input id="permit_study_at" name="permit_study_at" autocomplete="off" nbInput fullWidth placeholder="" [nbDatepicker]="permit_study_atPicker" [ngModel]="(id ? formal?.attributes?.permit_study_at : f.value.permit_study_at) | dns" required #permit_study_at="ngModel">
                                        <nb-datepicker #permit_study_atPicker></nb-datepicker>
                                        <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                                </nb-form-field>
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="other_info" class="label col-form-label col-4">Keterangan</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="other_info"
                                    type="text"
                                    id="other_info"
                                    [ngModel]="id ? formal?.attributes?.other_info : f.value.other_info" required #other_info="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>

                        <div class="form-group row mb-3" *ngIf="izinBelajar.value == 1">
                            <label for="permit_study_letter" class="label col-form-label col-4">Scan Surat Izin Belajar Nilai</label>
                            <div class="col-sm-8">
                                <ngx-upload id="permit_study_letter" (change)="onChangeFile($event)" [limit]='2'></ngx-upload>
                                <span class="caption">pdf max 2MB</span>
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="ijazah" class="label col-form-label col-4">Scan Ijazah</label>
                            <div class="col-sm-8">
                                <ngx-upload id="ijazah" (change)="onChangeFile($event)" [limit]='2'></ngx-upload>
                                <span class="caption">pdf max 2MB</span>
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="transcript" class="label col-form-label col-4">Scan Transkrip Nilai</label>
                            <div class="col-sm-8">
                                <ngx-upload id="transcript" (change)="onChangeFile($event)" [limit]='2'></ngx-upload>
                                <span class="caption">pdf max 2MB</span>
                            </div>
                        </div>
                        
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
})
export class PendidikanFormalForm extends CreateEditDialogStoreBaseComponent<PendidikanFormalForm, CreateUpdateData, PendidikanFormals.CreateUpdatePendidikanFormalInput, GetData, PendidikanFormals.PendidikanFormal > {
    title: string = "Tambah Data Pendidikan Formal"

    @Select(BiodataState.getBiodatas)
    formal$: Observable<PendidikanFormals.PendidikanFormal>;

    @Select(BiodataState.getUserEducation)
    userEdu$: Observable<BaseData<PendidikanFormals.PendidikanFormal[]>>

    constructor(injector: Injector, public dialogRef: NbDialogRef<PendidikanFormalForm>) {
        super(injector, dialogRef, 'formal', CreateUpdateData)
    }

    edit(id: string | number) {
        console.log(id)
        this.id = id
        this.store.dispatch(new GetData({}, id))
    }

    successSubmit(result: any): void {
        // this.store.dispatch(new GetBiodatas({include: 'user_education,user_course,users.attachment,user_paper'}))
        this.dialogRef.close()
    }
}