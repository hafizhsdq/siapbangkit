import { Component, Injector } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { Observable } from "rxjs";
import { Diklats } from "../../../../../store/diklat/diklat";
import { DiklatState } from "../../../../../store/diklat/diklat.state";
import { GetData as GetDataDiklat } from "../../../../../store/diklat/diklat.action"
import { CreateEditDialogStoreBaseComponent } from "../../../../shared/base/create-edit-dialog-base.component";
import { UserCourseState } from "../../../../../store/user-course/user-course.state";
import { UserCourses } from "../../../../../store/user-course/user-course";
import { CreateUpdateData, GetData } from "../../../../../store/user-course/user-course.action";

@Component ({
    selector: 'form-pelatihan',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    {{title}}
                </nb-card-header>
                <nb-card-body *ngIf="(pelatihan$ | async ) as pelatihan">
                <!-- <ng-container ngTemplateOutlet="selectDiklat"></ng-container> -->

                    <!-- <ng-template #selectDiklat let-form="form">
                    
                    </ng-template> -->
                    
                    <ng-container *ngIf="(diklats$ | async) as diklats" >
                        <div class="form-group row mb-3" >
                            <label for="type" class="label col-form-label col-4">Kelompok Diklat</label>
                            <div class="col-sm-8">
                                <nb-select name="type" selected="1" fullWidth [(selected)]="selectGroup" (selectedChange)="changeGroup($event)">
                                    <nb-option *ngFor="let diklat of diklats ?? [] | set:'attributes.group_name':'unique'" value="{{diklat}}">{{diklat}}</nb-option>
                                </nb-select>
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="training_id" class="label col-form-label col-4">Nama Diklat</label>
                            <div class="col-sm-8">
                                <nb-select name="training_id" fullWidth [selected]="f.value.training_id" ngModel required #training_id="ngModel">
                                    <nb-option *ngFor="let diklat of diklats ?? [] | filter:['attributes.group_name:'+selectGroup]" value="{{diklat.id}}">{{diklat.attributes.name}}</nb-option>
                                </nb-select>
                            </div>
                        </div>
                    </ng-container>
                    

                    <div class="form-group row mb-3">
                        <label for="organized_by" class="label col-form-label col-4">Penyelenggara</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="organized_by"
                                type="text"
                                id="organized_by"
                                [ngModel]="id ? pelatihan?.attributes?.organized_by : f.value.organized_by" required #organized_by="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="year" class="label col-form-label col-4">Tahun/Tanggal</label>
                        <div class="col-sm-8">
                            <nb-form-field>
                                    <input id="year" autocomplete="off" name="year" nbInput fullWidth placeholder="" [nbDatepicker]="yearPicker" [ngModel]="(id ? pelatihan?.attributes?.year : f.value.year) | dns" required #year="ngModel">
                                    <nb-datepicker #yearPicker></nb-datepicker>
                                    <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                            </nb-form-field>
                        </div>
                    </div>

                    <!-- <div class="form-group row mb-3">
                        <label for="year" class="label col-form-label col-4">Tahun</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="year"
                                type="number"
                                id="year"
                                [ngModel]="id ? pelatihan?.attributes?.year : f.value.year" required #year="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div> -->

                    <div class="form-group row mb-3">
                        <label for="other_info" class="label col-form-label col-4">Keterangan</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="other_info"
                                type="text"
                                id="other_info"
                                [ngModel]="id ? pelatihan?.attributes?.other_info : f.value.other_info" required #other_info="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="certificate_number" class="label col-form-label col-4">Nomor Sertifikat</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="certificate_number"
                                type="number"
                                id="certificate_number"
                                [ngModel]="id ? pelatihan?.attributes?.certificate_number : f.value.certificate_number" required #certificate_number="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="certificate_date" class="label col-form-label col-4">Tanggal Sertifikat</label>
                        <div class="col-sm-8">
                            <nb-form-field>
                                    <input id="certificate_date" autocomplete="off" name="certificate_date" nbInput fullWidth placeholder="" [nbDatepicker]="certificate_datePicker" [ngModel]="(id ? pelatihan?.attributes?.certificate_date : f.value.certificate_date) | dns" required #certificate_date="ngModel">
                                    <nb-datepicker #certificate_datePicker></nb-datepicker>
                                    <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                            </nb-form-field>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="certificate" class="label col-form-label col-4">Upload Sertifikat / Dokumen</label>
                        <div class="col-sm-8">
                            <ngx-upload id="certificate" (change)="onChangeFile($event)" [linit]='2'></ngx-upload>
                            <span class="caption">pdf max 2MB</span>
                        </div>
                    </div>

                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>

        <!-- <ng-template #formGroup>
            
        </ng-template> -->
    `,
})
export class PendidikanPelatihanForm extends CreateEditDialogStoreBaseComponent<PendidikanPelatihanForm, CreateUpdateData, UserCourses.UserCourseInput, GetData, UserCourses.UserCourse> {
    title: string = "Tambah Data Pendidikan dan Pelatihan"

    jenisDiklat = [
        'pelatihan struktural kepemimpinan dan pelatihan manajerial',
        'pelatihan teknis',
        'pelatihan fungsional',
        'pelatihan sosial kultural'
    ]

    kategoriDiklat = [
        'Pelatihan Dasar CPNS',
        'Pelatihan Kepemimpinan Nasional Tingkat I',
        'Pelatihan Kepemimpinan Nasional Tingkat II',
        'Pelatihan Kepemimpinan Administator',
        'Pelatihan Kepemimpinan Pengawas',
        'LEMHANAS',
        'Adum',
        'Adumla',
        'Sepada',
        'Spama',
        'spadaya',
        'Spamen',
        'Spati'
    ]

    optionalJenis = this.jenisDiklat[0]

    selectGroup: string;

    @Select(DiklatState.getDiklat)
    diklats$: Observable<Diklats.Diklat[]>;

    @Select(UserCourseState.getUserCourses)
    pelatihan$: Observable<UserCourses.UserCourse>

    constructor(injector: Injector, public dialogRef: NbDialogRef<PendidikanPelatihanForm>) {
        super(injector, dialogRef, 'pelatihan', CreateUpdateData)

        this.store.dispatch(new GetDataDiklat())
    }

    successSubmit(result: any): void {
        // this.store.dispatch(new GetBiodatas({include: 'user_education,user_course,users.attachment,user_paper'}))
        this.dialogRef.close()
    }

    changeGroup(e) {this.f.value.training_id = null}

    changeJenisDiklat(e: any) {        
    }
}