import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { NbDatepickerModule, NbLayoutModule, NbRouteTabsetModule, NbTabsetModule } from '@nebular/theme';
import { SharedModule } from '../../shared/shared.module';
import { BiodataComponent } from './biodata/biodata.component';
import { DataKompetensiComponent } from './data-kompetensi/data-kompetensi.component';
import { DataSeleksiComponent } from './data-seleksi/data-seleksi.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { PendidikanFormalForm } from './data-kompetensi/tambah-data/pendidikan-formal.component';
import { PendidikanPelatihanForm } from './data-kompetensi/tambah-data/pendidikan-pelatihan.component';
import { KaryaTulisForm } from './data-kompetensi/tambah-data/karya-tulis.component';
import { LampiranDokumenForm } from './data-seleksi/tambah-data/lampiran-component';
import { GantiPasswordComponent } from './ganti-password/ganti-password.component';
import { DocumentForm } from './data-kompetensi/tambah-data/document.component';



@NgModule({
  declarations: [
    ProfileComponent,
    BiodataComponent,
    DataKompetensiComponent,
    DataSeleksiComponent,

    PendidikanFormalForm,
    PendidikanPelatihanForm,
    KaryaTulisForm,
    DocumentForm,

    LampiranDokumenForm,
    GantiPasswordComponent
  ],
  imports: [
    CommonModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbLayoutModule,
    SharedModule,
    NbDatepickerModule,
    ProfileRoutingModule
  ]
})
export class ProfileModule { }
