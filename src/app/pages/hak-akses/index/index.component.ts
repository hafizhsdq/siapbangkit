import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Roles } from '../../../../store/role-permission/role-permission';
import { DeleteRole, GetRoles } from '../../../../store/role-permission/role.action';
import { RoleState } from '../../../../store/role-permission/role.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetRoles, DeleteRole> {

  @Select(RoleState.getRoles)
  roles$: Observable<any>;

  constructor(injector: Injector) {
    super(injector, GetRoles, DeleteRole)
    this.tag = 'hak-akses-menu'
  }
}
