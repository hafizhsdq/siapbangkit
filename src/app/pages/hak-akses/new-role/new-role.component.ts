import { Component, Injector } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { RolePermissions, Roles } from '../../../../store/role-permission/role-permission';
import { CreateUpdateRole, GetPermissions, GetRoles } from '../../../../store/role-permission/role.action';
import { RolePermissionState, RoleState } from '../../../../store/role-permission/role.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { RoleService } from '../../../shared/services/role.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ngx-new-role',
  templateUrl: './new-role.component.html',
  styleUrls: ['./new-role.component.scss']
})
export class NewRoleComponent extends PageTableStoreComponent<GetPermissions>  {

  @Select(RolePermissionState.getPermissions)
  permissions$: Observable<RolePermissions.Permission[]>;

  role$: Observable<any>;

  roleName = ''
  roleDescription = ''
  rolePermissions: RolePermissions.RolePermission[] = []
  roleId = null
  permission = []

  constructor(injector: Injector, private roleService: RoleService) {
    super(injector, GetPermissions)
    this.tag = 'role-permission-menu'

    this.roleId = this.activateRoute.snapshot.paramMap.get('id')
        
    if (this.roleId) {
      this.role$ = this.roleService.getById(this.roleId).pipe(map(({data}: any) => {
        //assign to permission array where is true
        this.permission = data[0].attributes.permission.filter(p => {
          const permissions = Object.keys(p.permission).filter(key => p.permission[key])//.map(key => ({[key]: true})).reduce((p,c) => ({...p, ...c}), {})
          return permissions?.length > 0
        }).map(p => {
          const permissions = Object.keys(p.permission).filter(key => p.permission[key]).map(key => ({[key]: true})).reduce((p,c) => ({...p, ...c}), {})
          return {[p.name]: permissions}
        }).reduce((p,c) => ({...p, ...c}), {})
        

        return data[0]
      }))
    }
  }

  get permissionJson () {
    return this.rolePermissions.map(p => ({[p.name]: {...p.permission}}))
          .reduce((p,c) => ({...p, ...c}), {})
  }

  get permissionArray (): RolePermissions.UpdateRolePermission[] {
    return Object.keys(this.permission).map(key => {
      return ({
      name: key,
      permission: {
        create: this.permission[key]['create'] ?? false,
        read: this.permission[key]['read'] ?? false,
        update: this.permission[key]['update'] ?? false,
        delete: this.permission[key]['delete'] ?? false,
        print: this.permission[key]['print'] ?? false
      }
    })})
  }
  
  submit(f: NgForm, role) {
    const {name, description} = f.value

    const payload: Roles.CreateUpdateRoleInput =  {
      name,
      description,
      permission: this.permissionArray
    }
    
    const doSave = this.store.dispatch(new CreateUpdateRole(payload, this.roleId))
    this.doRequest(doSave)
  }

  updatePermission(e: any, authorizeFor: string, permissionName: string) {    
    this.permission[permissionName] = this.permission[permissionName] ?? {}
    this.permission[permissionName][authorizeFor] = e.target.checked
  }
}
