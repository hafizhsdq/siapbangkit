import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HakAksesComponent } from './hak-akses.component';
import { RouterModule, Routes } from '@angular/router';
import { NewRoleComponent } from './new-role/new-role.component';
import { IndexComponent } from './index/index.component';
import { SharedModule } from '../../shared/shared.module';
import { getRoles, P } from '../roles';

const routes: Routes = [
  { path: '',
    component: HakAksesComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('role', [P.READ]),
      },
      {
        path: 'new-role/:id',
        component: NewRoleComponent,
        data: getRoles('role', [P.UPDATE]),
      },
      {
        path: 'new-role',
        component: NewRoleComponent,
        data: getRoles('role', [P.CREATE]),
      }
    ] 
  }
];

@NgModule({
  declarations: [
    HakAksesComponent,
    NewRoleComponent,
    IndexComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class HakAksesModule { }
