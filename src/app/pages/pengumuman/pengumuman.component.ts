import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-pengumuman',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./pengumuman.component.scss']
})
export class PengumumanComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
