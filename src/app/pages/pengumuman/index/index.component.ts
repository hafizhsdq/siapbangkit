import { Component, OnInit } from '@angular/core';
import { data as dataDiklat } from '../../../shared/data/diklat'

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  data = dataDiklat

  year = new Date().getFullYear()

  constructor() { }

  ngOnInit(): void {
  }

}
