import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PengumumanComponent } from './pengumuman.component';
import { IndexComponent } from './index/index.component';
import { SharedModule } from '../../shared/shared.module';
import { PengumumanRoutingModule } from './pengumuman-routing.module';
import { CreateUpdateComponent } from './create-update/create-update.component';



@NgModule({
  declarations: [
    PengumumanComponent,
    IndexComponent,
    CreateUpdateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PengumumanRoutingModule
  ]
})
export class PengumumanModule { }
