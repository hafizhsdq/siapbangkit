import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { IndexComponent } from './index/index.component';
import { PengumumanComponent } from './pengumuman.component';

const routes: Routes = [
  {
    path: '',
    component: PengumumanComponent,
    children: [
      {
        path: '',
        component: IndexComponent
      },
      {
        path: 'create-update',
        component: CreateUpdateComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PengumumanRoutingModule { }
