import { Component, Injector } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { Observable, Subject } from "rxjs";
import { Questionnaires } from "../../../../store/questionnaire/questionnaire";
import { CreateUpdateData, GetData } from "../../../../store/questionnaire/questionnaire.action";
import { GetData as GetSchedule } from "../../../../store/kalender-diklat/kalender-diklat.action";
import { QuestionnaireState } from "../../../../store/questionnaire/questionnaire.state";
import { CreateEditDialogStoreBaseComponent } from "../../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'add-questionnaire',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    <h6>{{title}}</h6>
                </nb-card-header>
                <nb-card-body *ngIf="(questionnaire$ | async ) as qe">
                <!-- {{(questionnaire$ | async) | json}} -->

                <nb-radio-group class="d-flex mb-3" name="type" [ngModel]="id ? qe?.attributes?.type : f.value.type" required #type="ngModel">
                    <nb-radio value="teacher">Pengajar</nb-radio>
                    <nb-radio value="organizer">Penyelenggara</nb-radio>
                </nb-radio-group>

                <div class="form-group row mb-3">
                    <label for="training_schedule_id" class="label col-form-label col-4">Angkatan</label>
                    <div class="col-sm-8" *ngIf="(schedule$ | async) as schedule">
                        <!-- {{schedule | json}} -->
                        <nb-select name="training_schedule_id" fullWidth [ngModel]="id ? qe?.relationships?.training_schedule?.id : f.value.training_schedule_id" required #training_schedule_id="ngModel">
                            <nb-option *ngFor="let p of schedule" [value]="p?.id" >{{ p?.relationships?.training.attributes.name }} - {{ p?.attributes?.schedule }}</nb-option>
                        </nb-select>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="platform" class="label col-form-label col-4">Platform</label>
                    <div class="col-sm-8">
                        <input class="col" nbInput
                            fullWidth
                            name="platform"
                            type="text"
                            id="platform"
                            [ngModel]="id ? qe?.attributes?.platform : f.value.platform" required #platform="ngModel"
                            placeholder=""
                            fieldSize="medium">
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="url" class="label col-form-label col-4">Link URL</label>
                    <div class="col-sm-8">
                        <nb-form-field>
                            <span style="width: 40%" nbPrefix>https://</span>
                            <input class="col" nbInput
                                fullWidth
                                name="url"
                                type="text"
                                id="url"
                                style="padding-left: 70px"
                                [ngModel]="id ? qe?.attributes?.url : f.value.url" required #url="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </nb-form-field>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="email" class="label col-form-label col-4">Email</label>
                    <div class="col-sm-8">
                        <input class="col" nbInput
                            fullWidth
                            name="email"
                            type="text"
                            id="email"
                            [ngModel]="id ? qe?.attributes?.email : f.value.email" required #email="ngModel"
                            placeholder=""
                            fieldSize="medium">
                    </div>
                </div>

                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button nbButton ghost size="small" type="button" class="mr-2" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>

    `,
})
export class AddQuestionnaire extends CreateEditDialogStoreBaseComponent<AddQuestionnaire, CreateUpdateData, Questionnaires.CreateUpdateQuestionnaireInput, GetData, Questionnaires.Questionnaire>{
    title: string = "Tambah Kuisioner"
    id: string | number

    OnClose: Subject<any> = new Subject<any>()

    @Select(QuestionnaireState.getAQuestionnaire)
    questionnaire$: Observable<Questionnaires.Questionnaire>;

    constructor(injector: Injector, 
        public dialogRef: NbDialogRef<AddQuestionnaire>) {
        super(injector, dialogRef, 'questionnaire', CreateUpdateData)

        this.store.dispatch(new GetSchedule({params: 'filter[participate]=true', include: 'training'}))
    }

    successSubmit(result: any): void {        
        this.dialogRef.close()
        this.OnClose.next()
    }
    
    edit(id: string | number) {
        this.id = id
        this.store.dispatch(new GetData({include: 'training_schedule.traininig'}, this.id))
    }
}