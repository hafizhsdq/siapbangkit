import { Component, Injector } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { DeleteData, GetData } from '../../../../store/questionnaire/questionnaire.action';
import { GetData as GetSchedule } from '../../../../store/kalender-diklat/kalender-diklat.action';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { AddQuestionnaire } from '../tambah-data/add-questionnaire.component';
import { QuestionnaireState } from '../../../../store/questionnaire/questionnaire.state';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(QuestionnaireState.getQuestionnaires)
  questionnaire$: Observable<any>;

  constructor(public dialogService: NbDialogService, injector: Injector) {
    super(injector, GetData, DeleteData, {
      include: 'users,training_schedule.training,training_schedule.participant:status(diterima)',
    })
    this.tag = "questionnaire"

    this.store.dispatch(new GetSchedule({params: 'filter[participate]=true', include: 'training'}))
   }

  addQuestionnaire = () => { 
    const dialog = this.dialogService.open(AddQuestionnaire, { hasScroll: true }) 
    dialog.onClose.subscribe(() => {this.getData()})

    return dialog
  }

  public OnEdit(id: string | number): void {
    const dialog = this.addQuestionnaire()
    dialog.componentRef.instance.edit(id)
  }
}
