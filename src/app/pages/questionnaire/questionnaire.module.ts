import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionnaireRoutingModule } from './questionnaire-routing.module';
import { QuestionnaireComponent } from './questionnaire.component';
import { IndexComponent } from './index/index.component';
import { SharedModule } from '../../shared/shared.module';
import { AddQuestionnaire } from './tambah-data/add-questionnaire.component';
import { NbRadioModule } from '@nebular/theme';


@NgModule({
  declarations: [
    QuestionnaireComponent,
    IndexComponent,
    AddQuestionnaire
  ],
  imports: [
    CommonModule,
    QuestionnaireRoutingModule,
    SharedModule,
    NbRadioModule
  ]
})
export class QuestionnaireModule { }
