import { Component } from '@angular/core';

@Component({
  selector: 'ngx-questionnaire',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent {}
