import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../shared/guard/auth.guard';
import { getRoles, P } from '../roles';
import { IndexComponent } from './index/index.component';
import { QuestionnaireComponent } from './questionnaire.component';

const routes: Routes = [
  { 
    path: '', 
    component: QuestionnaireComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('training_schedule', [P.READ]),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionnaireRoutingModule { }
