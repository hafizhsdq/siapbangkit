import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrasiPesertaComponent } from './administrasi-peserta.component';

describe('AdministrasiPesertaComponent', () => {
  let component: AdministrasiPesertaComponent;
  let fixture: ComponentFixture<AdministrasiPesertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministrasiPesertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrasiPesertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
