import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable, forkJoin } from 'rxjs';
import { KalenderDiklats } from '../../../../../store/kalender-diklat/kalender-diklat';
import { CreateUpdateData, GetData } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { KalenderDiklatState } from '../../../../../store/kalender-diklat/kalender-diklat.state';
import { CreateEditStoreBaseComponent } from '../../../../shared/base/create-edit-base.component';
import { map } from 'rxjs/operators';
import { KalenderDiklatService } from '../../../../shared/services/kalender-diklat.service';
import { parseISO } from 'date-fns';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends CreateEditStoreBaseComponent<CreateUpdateData, KalenderDiklats.CreateUpdateKalenderDiklatInput, GetData, KalenderDiklats.KalenderDiklat> {

  @Select(KalenderDiklatState.getAKalenderDiklats)
  schedule$: Observable<KalenderDiklats.KalenderDiklat>

  jenisDiklat$: Observable<any>
  jenis2Diklat$: Observable<any>
  rumpun$: Observable<any>
  // pejabat$: Observable<any>
  // namaPejabat$: Observable<any>
  // filterNamaPejabat$: Observable<any>
  // filterPejabat$: Observable<any>
  
  constructor(injector: Injector, 
    private scheduleService: KalenderDiklatService) {
    super(injector, 'kalenderdiklat', CreateUpdateData, GetData)

    this.jenisDiklat$ = this.simpegService.get('mJenisDiklat').pipe(map(({data}) => data))
    this.jenis2Diklat$ = this.simpegService.get('mDiklat').pipe(map(({data}) => data.filter(d => d.jenis_diklat.id == 2)))
    this.rumpun$ = this.simpegService.get('mRumpun').pipe(map(({data}) => data))
  }

  ngOnInit(): void {
    
    if (this.id) {
      forkJoin([ 
        // this.filterNamaPejabat$,
        // this.filterPejabat$,
        this.scheduleService.getById(this.id)
      ])
      .subscribe(([/*namaPejabat, pejabat,*/ res]) => {
        const {
          category,
          name,
          jenis_id = null,
          rumpun_id = null,
          duration,
          start_date,
          location,
          purpose,
          nama_pejabat_id = null,
          pejabat_id = null,
          nomor_sk,
          tanggal_sk,
          penyelenggara,
          info} = (res as any).data[0].attributes
        this.f.setValue({
          category,
          name,
          jenis_id,
          rumpun_id,
          duration,
          start_date: parseISO(start_date),
          location,
          purpose,
          info,
          // nama_pejabat_id: namaPejabat.find(n => n.id === nama_pejabat_id),
          // pejabat_id: pejabat.find(n => n.id === pejabat_id),
          nomor_sk,
          tanggal_sk,
          penyelenggara
        })
      })
    }
  }

  // onChange(event: any, eName: 'nama_pejabat' | 'pejabat') {
  //   const text = event.target.value
  //   if (eName === 'nama_pejabat') {
  //     this.filterNamaPejabat$ = this.namaPejabat$.pipe(map((data: any[]) => data?.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
  //   } else if (eName === 'pejabat') {
  //     this.filterPejabat$ = this.pejabat$.pipe(map((data: any[]) => data?.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
  //   }
  // }

  viewHandle(value: any) {
    return typeof value === 'string' ? value : value.nama
  }

  successSubmit(result: any): void {
    this.back()
  }

  onSubmit(f: NgForm) {
  const value = {...f.value/*, nama_pejabat_id: f.value.nama_pejabat_id.id, pejabat_id: f.value.pejabat_id.*/}
    const doSave = this.store.dispatch(new this.createUpdateCreator(value, this.id))
    this.doRequest(doSave, {
        next: (result) => this.successSubmit(result),
        error: (error) => this.errorrSubmit(error),
        complete: () => this.completeSubmit()
      } as BehaviorSubject<any>
    )
  } 
}
