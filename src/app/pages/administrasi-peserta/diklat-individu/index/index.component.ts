import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Biodatas } from '../../../../../store/biodata/biodata';
import { GetBiodatas } from '../../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../../store/biodata/biodata.state';
import { DeleteData } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { ChangeStatusDialog } from '../../../../shared/components/change-status-dialog.component';
import { KalenderDiklatService } from '../../../../shared/services/kalender-diklat.service';
import { UploadSertifikat } from '../../semua-diklat/upload-sertifikat.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetBiodatas, DeleteData> {

  @Select(BiodataState.getDiklat)
  schedules$: Observable<Biodatas.Biodata>

  constructor(injector: Injector, private klaenderDiklatService: KalenderDiklatService) {
    super(injector, GetBiodatas, DeleteData, {include: 'training_schedule:where(category|individu)'})
    this.tag = "dikla-individu-menu"
  }

  uploadFile = (scheduleId: string, startDate: Date, category: string) => {  
    console.log(category);
         
    this.dialogService.open(UploadSertifikat, { hasScroll: true, context: { scheduleId, startDate, category } }).onClose.subscribe((result) => {
      if (result == 'success') {
        this.router.navigate(['../sertifikatku'], {relativeTo: this.activateRoute})
      }
    }) 
  }

  ngOnInit(): void {
      this.nbMenuService.onItemClick()
      .pipe(
          // tap(item => console.log(item)),
          filter(({ tag }) => tag === this.tag),
          // map(({ item: { title, id } }) => title),
      )
      .subscribe(result => {        
        // if (result.item.title == 'Upload Sertifikat') this.uploadFile(result.item.data);
        if (result.item.title == 'Delete') this.OnDelete(result.item.data);
        if (result.item.title == 'Edit') this.OnEdit(result.item.data);
      });
  
      this.getData()
  }
}
