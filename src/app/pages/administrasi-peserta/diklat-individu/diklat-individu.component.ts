import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-diklat-individu',
  template: `<router-outlet></router-outlet>`
})
export class DiklatIndividuComponent {}
