import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiklatIndividuComponent } from './diklat-individu.component';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { SharedModule } from '../../../shared/shared.module';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { ThemeModule } from '../../../@theme/theme.module';
import { NbTooltipModule } from '@nebular/theme';
import { getRoles, P } from '../../roles';
import { AuthGuard } from '../../../shared/guard/auth.guard';
import { DetailDiklatComponent } from '../../../shared/components/detail-diklat/detail-diklat.component';

const routes: Routes = [
  {
    path: '',
    component: DiklatIndividuComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: 'create-update/:id',
        component: CreateUpdateComponent,
        data: getRoles('training_schedule', [P.UPDATE]),
      },
      {
        path: 'create-update',
        component: CreateUpdateComponent,
        data: getRoles('training_schedule', [P.CREATE]),
      },
      {
        path: 'detail/:id',
        component: DetailDiklatComponent,
        data: getRoles('training_schedule', [P.READ]),
      }
    ]
  }
]

@NgModule({
  declarations: [
    IndexComponent,
    CreateUpdateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NbTooltipModule,
    ThemeModule,
    RouterModule.forChild(routes)
  ]
})
export class DiklatIndividuModule { }
