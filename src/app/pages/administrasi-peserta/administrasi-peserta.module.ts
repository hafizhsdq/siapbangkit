import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrasiPesertaComponent } from './administrasi-peserta.component';
import { AdministrasiPesertaRoutingModule } from './administrasi-peserta-routing.module';
import { KalenderDiklatComponent } from './kalender-diklat/kalender-diklat.component';
import { SemuaDiklatComponent } from './semua-diklat/semua-diklat.component';
import { DiklatIndividuComponent } from './diklat-individu/diklat-individu.component';
import { SertifikatkuComponent } from './sertifikatku/sertifikatku.component';
import { KaryaTuliskuComponent } from './karya-tulisku/karya-tulisku.component';



@NgModule({
  declarations: [
    AdministrasiPesertaComponent,
    KalenderDiklatComponent,
    SemuaDiklatComponent,
    DiklatIndividuComponent,
    SertifikatkuComponent,
    KaryaTuliskuComponent
  ],
  imports: [
    CommonModule,
    AdministrasiPesertaRoutingModule
  ]
})

export class AdministrasiPesertaModule { }
