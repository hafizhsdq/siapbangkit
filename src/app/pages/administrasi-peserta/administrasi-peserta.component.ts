import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-administrasi-peserta',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./administrasi-peserta.component.scss']
})
export class AdministrasiPesertaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
