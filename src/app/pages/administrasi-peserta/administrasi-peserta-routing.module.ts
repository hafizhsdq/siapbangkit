import { NgModule } from '@angular/core';
import { AdministrasiPesertaComponent } from './administrasi-peserta.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: AdministrasiPesertaComponent,
        children: [
          {
            path: 'diklat-individu',
            loadChildren: () => import('./diklat-individu/diklat-individu.module').then(m => m.DiklatIndividuModule)
          },
          {
            path: 'kalender-diklat',
            loadChildren: () => import('./kalender-diklat/kalender-diklat.module').then(m => m.KalenderDiklatModule)
          },
          {
            path: 'semua-diklat',
            loadChildren: () => import('./semua-diklat/semua-diklat.module').then(m => m.SemuaDiklatModule)
          },
          {
            path: 'sertifikatku',
            loadChildren: () => import('./sertifikatku/sertifikatku.module').then(m => m.SertifikatkuModule)
          },
          {
            path: 'karya-tulisku',
            loadChildren: () => import('./karya-tulisku/karya-tulisku.module').then(m => m.KaryaTuliskuModule)
          },
          
          // {
          //   path: 'buka-elearning',
          //   loadChildren: () => import('./buka-elearning/buka-elearning.module').then(m => m.BukaElearningModule)
          // },
        ]
    }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrasiPesertaRoutingModule { }
