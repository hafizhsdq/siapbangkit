import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SertifikatkuComponent } from './sertifikatku.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { IndexComponent } from './index/index.component';
import { getRoles, P } from '../../roles';

const routes: Routes = [
  {
    path: '',
    component: SertifikatkuComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('training_schedule', [P.READ]),
      }
    ]
  }
]

@NgModule({
  declarations: [
    IndexComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class SertifikatkuModule { }
