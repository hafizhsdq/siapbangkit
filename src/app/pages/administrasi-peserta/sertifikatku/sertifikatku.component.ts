import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-sertifikatku',
  template: `<router-outlet></router-outlet>`
})
export class SertifikatkuComponent {}
