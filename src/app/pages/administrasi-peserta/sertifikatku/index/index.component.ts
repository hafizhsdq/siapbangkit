import { Component, Injector } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Biodatas } from '../../../../../store/biodata/biodata';
import { DeleteBiodata, GetBiodatas } from '../../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../../store/biodata/biodata.state';
import { GetData } from '../../../../../store/user-course/user-course.action';
import { UserCourseState } from '../../../../../store/user-course/user-course.state';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetBiodatas, DeleteBiodata> {

  @Select(UserCourseState.getUserCourses)
  userCourse$: Observable<any>

  storageUrl = environment.storageUrl

  constructor(injector: Injector, public dialogService: NbDialogService) {
    super(injector, GetData, null, {
      include: 'training_schedule.training.training_subject.teaching_phase,training_schedule.participant:me(true)'
    })
    this.tag = 'diklatselesai'
  }
}
