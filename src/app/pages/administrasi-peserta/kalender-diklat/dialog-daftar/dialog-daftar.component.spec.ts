import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDaftarComponent } from './dialog-daftar.component';

describe('DialogDaftarComponent', () => {
  let component: DialogDaftarComponent;
  let fixture: ComponentFixture<DialogDaftarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDaftarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDaftarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
