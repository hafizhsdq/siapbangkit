import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-dialog-daftar',
  template: `
    <nb-card>
        <nb-card-header>
          <div class="text-center">
            <nb-icon icon="alert-triangle"></nb-icon> &nbsp;
            <strong>PERHATIAN!</strong>
          </div>
        </nb-card-header>
        <nb-card-body class="d-inline-block">
          <div>
            <p>Anda yakin ingin mendaftar?</p>
          </div>
        </nb-card-body>
        <nb-card-footer class="d-flex justify-content-end">
          <button class="mr-2" nbButton status="primary" size="small" (click)="cancel()">Ok</button>
          <button nbButton ghost size="small" (click)="cancel()">Batalkan</button>
        </nb-card-footer>
      </nb-card>
  `,
  styleUrls: ['./dialog-daftar.component.scss']
})
export class DialogDaftarComponent {

  constructor(protected ref: NbDialogRef<DialogDaftarComponent>) { }

  cancel() {
    this.ref.close();
  }

}
