import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KalenderDiklatComponent } from './kalender-diklat.component';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { NbButtonModule, NbIconModule, NbInputModule, NbLayoutModule } from '@nebular/theme';
import { SharedModule } from '../../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DialogDaftarComponent } from './dialog-daftar/dialog-daftar.component';
import { DetailDiklatComponent } from '../../../shared/components/detail-diklat/detail-diklat.component';
import { getRoles, P } from '../../roles';

const routes: Routes = [
  { path: '',
    component: KalenderDiklatComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: 'detail/:id',
        component: DetailDiklatComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full'
      }
    ] 
  }
];

@NgModule({
  declarations: [
    IndexComponent,
    DialogDaftarComponent
  ],
  imports: [
    CommonModule,
    NbLayoutModule,
    SharedModule,
    NbIconModule,
    NgxDatatableModule,
    NbButtonModule,
    NbInputModule,
    RouterModule.forChild(routes)
  ]
})
export class KalenderDiklatModule { }
