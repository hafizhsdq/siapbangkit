import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { BiodataState } from '../../../../../store/biodata/biodata.state';
import { KalenderDiklats } from '../../../../../store/kalender-diklat/kalender-diklat';
import { GetData } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { KalenderDiklatState } from '../../../../../store/kalender-diklat/kalender-diklat.state';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { KalenderDiklatService } from '../../../../shared/services/kalender-diklat.service';
import { DialogDaftarComponent } from '../dialog-daftar/dialog-daftar.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData> {

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedules$: Observable<KalenderDiklats.KalenderDiklat[]>

  @Select(BiodataState.getBiodatas)
  biodata$: Observable<any>

  constructor(injector: Injector, private kalenderService: KalenderDiklatService) {
    super(injector, GetData, null, 
      {
        include: 'training,training_schedule,participant:me(true)', 
        params: `filter[category]=internal,eksternal&filter[activity_log]=diterima`,
        useCurrentDate: true
      });
    this.tag = "kalender-diklat"
  }

  ngOnInit(): void {        
    this.nbMenuService.onItemClick()
    .pipe(
        // tap(item => console.log(item)),
        filter(({ tag }) => tag === this.tag),
    )
    .subscribe(result => {        
        if (result.item.title == 'Daftar') this.Daftar(result.item.data);
    });

    this.getData()
  }

  public Daftar(data: any) {    
    const {category, link} = data
    if (category == 'eksternal') {
      window.open(link, '_blank')
    } else {
      const {name, group_name, explanation} = data.training
          
      this.swalHandler.alert({
          title: name, 
          text: explanation,
          confirmButtonText: 'Daftar',
          showCancelButton: true,
          icon: 'question'
      }, {
        confirm: () => {
          this.loading = true
          this.doRequest(this.kalenderService.register(data.id, {nip: [localStorage.getItem('nip')]}), {
              next: (result) => this.successSubmit(result),
              error: (error) => this.errorrSubmit(error),
              complete: () => this.completeSubmit()
          } as BehaviorSubject<any>)
        }
      })
    }

  }

  completeSubmit() {this.loading = false}
  successSubmit(result) {
    this.loading = false
    this.getData()
  }
  errorrSubmit(error) {this.loading = false}
}
