import { Component, Injector, OnInit } from '@angular/core';
import { PageTableComponent } from '../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-kalender-diklat',
  template: `<router-outlet></router-outlet>`
})
export class KalenderDiklatComponent extends PageTableComponent {

  constructor(injector: Injector) {
    super(injector);
    this.tag = "kalender-diklat"
  }
}
