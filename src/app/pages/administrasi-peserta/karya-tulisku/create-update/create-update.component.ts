import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { KaryaTuliss } from '../../../../../store/karya-tulis/karya-tulis';
import { CreateUpdateData, GetData } from '../../../../../store/karya-tulis/karya-tulis.action';
import { KaryaTulisState } from '../../../../../store/karya-tulis/karya-tulis.state';
import { CreateEditStoreBaseComponent } from '../../../../shared/base/create-edit-base.component';

@Component({
  selector: 'ngx-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent extends CreateEditStoreBaseComponent<CreateUpdateData, KaryaTuliss.CreateUpdateKaryaTulisInput, GetData, KaryaTuliss.KaryaTulis> {

  title: string = "Tambah Data Karya Tulis"

    @Select(KaryaTulisState.getKaryaTuliss)
    papers$: Observable<KaryaTuliss.KaryaTulis>

    constructor(injector: Injector) {
        super(injector, 'papers', CreateUpdateData, GetData)
    }

    successSubmit(result: any): void {
        // this.store.dispatch(new GetBiodatas({include: 'user_education,user_course,users.attachment,user_paper'}))
        this.back()
    }


}
