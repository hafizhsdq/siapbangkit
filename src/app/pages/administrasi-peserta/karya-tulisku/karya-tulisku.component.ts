import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-karya-tulisku',
  template: '<router-outlet></router-outlet>'
})
export class KaryaTuliskuComponent {}
