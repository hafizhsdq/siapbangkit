import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KaryaTuliskuComponent } from './karya-tulisku.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { IndexComponent } from './index/index.component';
import { getRoles, P } from '../../roles';
import { CreateUpdateComponent } from './create-update/create-update.component';

const routes: Routes = [
  { path: '',
    component: KaryaTuliskuComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('user_paper', [P.READ]),
      },
      {
        path: 'update/:id',
        component: CreateUpdateComponent,
        data: getRoles('user_paper', [P.UPDATE]),
      },
      {
        path: 'create',
        component: CreateUpdateComponent,
        data: getRoles('user_paper', [P.CREATE]),
      },
      // {
      //   path: 'detail/:id',
      //   component: DetailDiklatComponent,
      //   data: getRoles('training_schedule', [P.READ]),
      // },
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full'
      }
    ] 
  }
];

@NgModule({
  declarations: [
    IndexComponent,
    CreateUpdateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class KaryaTuliskuModule { }
