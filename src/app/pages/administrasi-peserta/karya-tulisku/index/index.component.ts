import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { DeleteData, GetData } from '../../../../../store/karya-tulis/karya-tulis.action';
import { KaryaTulisState } from '../../../../../store/karya-tulis/karya-tulis.state';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(KaryaTulisState.getKaryaTuliss)
  userPaper$: Observable<any>

  storageUrl = environment.storageUrl

  constructor(injector: Injector) {
    super(injector, GetData, DeleteData, {include: 'users', params: 'filter[self]=true', useCurrentDate: true})
   }

  //  openKaryaTulis = () => { this.dialogService.open(KaryaTulisForm, { hasScroll: true }) }

}
