import { Component } from '@angular/core';

@Component({
  selector: 'ngx-semua-diklat',
  template: `<router-outlet></router-outlet>`
})
export class SemuaDiklatComponent {}
