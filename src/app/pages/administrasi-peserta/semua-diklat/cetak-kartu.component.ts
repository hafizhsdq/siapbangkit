import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import * as moment from "moment";
import * as html2canvas from "html2canvas"
import * as FileSaver from 'file-saver'

@Component({
    selector: 'ngx-dialog-print',
    template: `
      <nb-card>
        <nb-card-header>
          <h6>{{title}}</h6>
        </nb-card-header>
        <nb-card-body>
        <!--{{data | json}}-->
          <div #body>
            <nb-card style="min-width: 300px" class="mb-0">
              <nb-card-body class="d-flex flex-column justify-content-center align-items-center" >
                <img src="./assets/images/logo.png" width="50" alt="logo-bkd">
                <h6>PESERTA</h6>
                <nb-card class="text-center mb-0">
                  <nb-card-body>
                    <span class="text-basic">{{data.name}}</span><br>
                    <span class="text-basic">(Angkatan: {{data.periode}})</span>
                  </nb-card-body>
                </nb-card>
                <span class="caption">{{ data.startDate | date:'fullDate' }} - {{ end_date | date:'fullDate' }}</span>
                <img [src]="data.biodata?.attributes?.avatar ?? './assets/images/undefined-photo.jpeg'" width="150" alt="photo-profile" class="my-2"/>
                <span class="text-basic">{{data.biodata?.attributes?.name}}</span>
                <span class="caption">Badan Diklat Sidoarjo, Jawa Timur</span>
              </nb-card-body>
            </nb-card>
          </div>
        </nb-card-body>
        <nb-card-footer class="d-flex justify-content-end">
          <button nbButton (click)="cancel()" ghost class="mr-2">Batalkan</button>
          <a nbButton status="primary" (click)="download($event)">Download</a>
        </nb-card-footer>
      </nb-card>      
    `,
  })
  
  export class CetakKartuComponent implements AfterViewInit {
    title: string = "Cetak Kartu Peserta"
    data: any
    @ViewChild('body') body: ElementRef

    h2c: any = html2canvas;

    end_date: Date = moment().toDate()
    constructor(protected ref: NbDialogRef<CetakKartuComponent>) { }

    ngAfterViewInit(): void {      
      this.end_date = moment(this.data.startDate).add(this.data.duration, 'day').toDate()
    }
  
    cancel() {
      this.ref.close();
    }

    download(e) {
      this.h2c(this.body.nativeElement).then((canvas) => {
        FileSaver.saveAs(canvas.toDataURL('image/png'), `IDCARD-${this.data.name}-${this.data.biodata?.attributes?.name}.png`)
      })
    }
  }