import { Component, Injector } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { GetData as GetKalendarDiklat, DeleteData } from '../../../../../store/kalender-diklat/kalender-diklat.action';
import { PageTableStoreComponent } from '../../../../shared/base/page-table.component';
import { CetakKartuComponent } from '../cetak-kartu.component';
import { UploadSertifikat } from '../upload-sertifikat.component';
import { KalenderDiklatState } from '../../../../../store/kalender-diklat/kalender-diklat.state';
import { HistoryDiklatService } from '../../../../shared/services/history-diklat.service';
import { SimpegService } from '../../../../shared/services/simpeg.service';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetKalendarDiklat, DeleteData> {

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedules$: Observable<any>

  history$: Observable<any>

  constructor(injector: Injector,
    public dialogService: NbDialogService) {
    super(injector, GetKalendarDiklat, DeleteData, {
      include: 'training,participant:me(true).training_schedule.user_course',
      params: 'filter[participate]=true&filter[has_activity_log]=true'
    })
    this.tag = "semuadiklat"
  }

  uploadFile = (scheduleId: string, startDate: Date, category: string) => {
    this.dialogService.open(UploadSertifikat, { hasScroll: true, context: { scheduleId, startDate, category } }).onClose.subscribe((result) => {
      if (result == 'success') {
        this.router.navigate(['../sertifikatku'], {relativeTo: this.activateRoute})
      }
    })
  }

  printCard(data) {
    this.dialogService.open(CetakKartuComponent, { hasScroll: true, context: {data} })
  }

  ngOnInit(): void {
    
    super.ngOnInit()
    
    this.history$ = this.biodata$.pipe(
      mergeMap(data => {
        return this.simpegService.get(`hDiklat&nip=${data.attributes.nip}`)
      }),
      map(({data = []} : {data: any}) => {
        return data.map(d => {
          const {pegawai: {nip, nama}, diklat: {jenis_diklat, nomor_sk, pejabat, nama_pejabat, diklat, nama_diklat, tanggal_mulai, tanggal_selesai, total_jam, angkatan, tempat, penyelenggara, rumpun, keterangan}} = d
          return {nip, 
            nama, 
            jenis_diklat: jenis_diklat.nama, 
            nomor_sk, 
            pejabat: pejabat["nama"], 
            nama_pejabat: nama_pejabat["nama"], 
            nama_diklat: diklat.nama, 
            tanggal_mulai,
            tanggal_selesai,
            total_jam,
            angkatan,
            tempat,
            penyelenggara,
            rumpun,
            keterangan
          }
        })
      }))
  }

  assignTableAction({item}) {
    if (item.title == 'Cetak Kartu Peserta') this.printCard(item.data);
    if (item.title == 'Upload Sertifikat') this.uploadFile(item.data.scheduleId, item.data.startDate, item.data.category);
    if (item.title == 'Delete') this.OnDelete(item.data);
    if (item.title == 'Edit') this.OnEdit(item.data);
  }

  onBiodataReady(data: any): void {
    console.log(data);
  }
}
