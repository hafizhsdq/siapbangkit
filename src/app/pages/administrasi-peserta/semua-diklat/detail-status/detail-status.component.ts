import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-detail-status',
  templateUrl: './detail-status.component.html',
  styleUrls: ['./detail-status.component.scss']
})
export class DetailStatusComponent implements OnInit {

  detail = [
    {
      "title": "Diklat : Diklat Prajabatan Golongan III (Reguler) (Angkatan ke-197)",
      "kelompok": "Pengembangan Kompetensi Dasar dan Kader",
      "lokasi": "Jakarta Islamic Center",
      "mulai": "23 Agustus 2021",
      "selesai": "18 Oktober 2021",
      "statusFooter": "Accept"
    }
  ]

  hasUpload = true;

  constructor() { }

  ngOnInit(): void {
  }

}
