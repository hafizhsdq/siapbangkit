import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailStatusComponent } from './detail-status.component';

describe('DetailStatusComponent', () => {
  let component: DetailStatusComponent;
  let fixture: ComponentFixture<DetailStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
