import { Component, Injector, ViewChild } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { UserCourses } from "../../../../store/user-course/user-course";
import { CreateUpdateData, GetData } from "../../../../store/user-course/user-course.action";
import { CreateEditDialogStoreBaseComponent } from "../../../shared/base/create-edit-dialog-base.component";
import { NgModel } from "@angular/forms";
import * as moment from "moment";

@Component ({
    selector: 'upload-sertifikat',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    <h6>{{title}}</h6>
                    <span class="caption"><strong>Note*</strong> Sertifikat dari diklat yang telah diselesaikan</span>
                </nb-card-header>
                <nb-card-body>
                    <input type="hidden" name="training_schedule_id" [ngModel]="scheduleId" #training_schedule_id="ngModel"/>
                    <input type="hidden" name="start_date" [ngModel]="startDate" #start_date="ngModel"/>
                    <!--{{startDate}}-->

                    <ng-container *ngIf="category != 'internal'" hidden>
                        <div class="form-group row mb-3">
                            <label for="jp" class="label col-form-label col-4">JP</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="jp"
                                    type="number"
                                    id="jp"
                                    ngModel required #jp="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="organized_by" class="label col-form-label col-4">Penyelenggara</label>
                            <div class="col-sm-8">
                                <input class="col" nbInput
                                    fullWidth
                                    name="organized_by"
                                    type="text"
                                    id="organized_by"
                                    ngModel required #organized_by="ngModel"
                                    placeholder=""
                                    fieldSize="medium">
                            </div>
                        </div>
            
                        <!--
                        <div class="form-group row mb-3">
                            <label for="start_date" class="label col-form-label col-4">Tanggal Mulai</label>
                            <div class="col-sm-8">
                                <nb-form-field class="p-0">
                                        <input autocomplete="off" id="start_date" name="start_date" nbInput fullWidth placeholder="" [nbDatepicker]="start_datePicker" ngModel required #start_date="ngModel">
                                        <nb-datepicker #start_datePicker></nb-datepicker>
                                        <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                                </nb-form-field>
                            </div>
                        </div>-->
                            
                        <div class="form-group row mb-3">
                            <label for="end_date" class="label col-form-label col-4">Tanggal Selesai</label>
                            <div class="col-sm-8">
                                <nb-form-field class="p-0">
                                        <input autocomplete="off" id="end_date" name="end_date" nbInput fullWidth placeholder="" [nbDatepicker]="tglSelesaiPicker" ngModel required #end_date="ngModel">
                                        <nb-datepicker #tglSelesaiPicker></nb-datepicker>
                                        <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                                </nb-form-field>
                            </div>
                        </div>
                    </ng-container>

                    <div class="form-group row mb-3">
                        <label for="certificate_date" class="label col-form-label col-4">Tanggal Sertifikat</label>
                        <div class="col-sm-8">
                            <nb-form-field class="p-0">
                                    <input autocomplete="off" id="certificate_date" name="certificate_date" nbInput fullWidth placeholder="" [nbDatepicker]="tglSertifikat" ngModel required #certificate_date="ngModel">
                                    <nb-datepicker #tglSertifikat [filter]="filterFn"></nb-datepicker>
                                    <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                            </nb-form-field>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="certificate_no" class="label col-form-label col-4">Nomor Sertifikat</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="certificate_no"
                                type="text"
                                id="certificate_no"
                                ngModel required #certificate_no="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>
                        
                    <div class="form-group row mb-3">
                        <label for="certificate" class="label col-form-label col-4">File Sertifikat</label>
                        <div class="col-sm-8">
                            <ngx-upload id="certificate" (change)="onChangeFile($event)"></ngx-upload>
                        </div>
                    </div>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>
    `,
})
export class UploadSertifikat extends CreateEditDialogStoreBaseComponent<UploadSertifikat, CreateUpdateData, UserCourses.UserCourseInput, GetData, UserCourses.UserCourse> {
    title: string = "Upload Sertifikat"

    category: string= 'internal';
    startDate: Date;
    scheduleId: string

    @ViewChild('end_date') end_date: NgModel

    filterFn = (date) => {
        return moment(date).diff(this.end_date?.value) <= 0
    }

    constructor(injector: Injector, public dialogRef: NbDialogRef<UploadSertifikat>) {
        super(injector, dialogRef, 'sertifikat', CreateUpdateData)
        // console.log(this.tag)
    }

    successSubmit(result: any): void {
        this.dialogRef.close('success')
    }
}