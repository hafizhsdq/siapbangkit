import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemuaDiklatComponent } from './semua-diklat.component';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { NbButtonModule, NbIconModule, NbInputModule, NbLayoutModule, NbTabsetModule, NbTooltipModule } from '@nebular/theme';
import { SharedModule } from '../../../shared/shared.module'; 
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DetailStatusComponent } from './detail-status/detail-status.component';
import { DetailDiklatComponent } from '../../../shared/components/detail-diklat/detail-diklat.component';
import { UploadSertifikat } from './upload-sertifikat.component';
import { getRoles, P } from '../../roles';
import { CetakKartuComponent } from './cetak-kartu.component';
import { CreateUpdateComponent } from '../diklat-individu/create-update/create-update.component';



const routes: Routes = [
  { path: '',
    component: SemuaDiklatComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: 'create-update/:id',
        component: CreateUpdateComponent,
        data: getRoles('training_schedule', [P.UPDATE]),
      },
      {
        path: 'create-update',
        component: CreateUpdateComponent,
        data: getRoles('training_schedule', [P.CREATE]),
      },
      {
        path: 'detail/:id',
        component: DetailDiklatComponent,
        data: getRoles('training_schedule', [P.READ]),
      },
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full'
      }
    ] 
  }
];

@NgModule({
  declarations: [
    IndexComponent,
    DetailStatusComponent,
    UploadSertifikat,
    CetakKartuComponent
  ],
  imports: [
    CommonModule,
    NbTooltipModule,
    NbTabsetModule,
    NbLayoutModule,
    SharedModule,
    NbIconModule,
    NgxDatatableModule,
    NbButtonModule,
    NbInputModule,
    RouterModule.forChild(routes)
  ]
})
export class SemuaDiklatModule { }
