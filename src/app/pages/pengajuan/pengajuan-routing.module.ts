import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { getRoles, P } from '../roles';
import { PengajuanComponent } from './pengajuan.component';
import { UsulanDiklatComponent } from './usulan-diklat/usulan-diklat.component';

const routes: Routes = [
  { path: '',
    component: PengajuanComponent,
    children: [
      {
        path: 'izin-belajar',
        loadChildren: () => import('./izin-belajar/izin-belajar.module').then(m => m.IzinBelajarModule)
      },
      {
        path: 'keterangan-pendidikan',
        loadChildren: () => import('./keterangan-pendidikan/keterangan-pendidikan.module').then(m => m.KeteranganPendidikanModule)
      },
      {
        path: 'keterangan-belajar',
        loadChildren: () => import('./keterangan-belajar/keterangan-belajar.module').then(m => m.KeteranganBelajarModule)
      },
      {
        path: 'usulan-diklat',
        component: UsulanDiklatComponent,
        data: getRoles('training_suggestion', [P.READ]),
      }
    ] 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PengajuanRoutingModule { }
