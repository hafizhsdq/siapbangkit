import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-keterangan-pendidikan',
  template: `<router-outlet></router-outlet>`
})
export class KeteranganPendidikanComponent {}