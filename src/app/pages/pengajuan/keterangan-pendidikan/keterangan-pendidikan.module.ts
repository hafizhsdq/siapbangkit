import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeteranganPendidikanComponent } from './keterangan-pendidikan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { UploadBerkasComponent } from '../../../shared/components/upload-berkas/upload-berkas.component';
import { PermitTableComponent } from '../../../shared/components/permit-table/permit-table.component';
import { getRoles, P } from '../../roles';
import { AuthGuard } from '../../../shared/guard/auth.guard';

const routes: Routes = [
  { path: '',
    component: KeteranganPendidikanComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: PermitTableComponent,
        data: { category: 'education_permit', title: 'Daftar Pengajuan Surat Keterangan Pendidikan', ...getRoles(('user_permit'), [P.READ]) }
      },
      {
        path: 'add',
        component: UploadBerkasComponent,
        data: { category: 'education_permit', title: 'Upload Berkas Keterangan Pendidikan', ...getRoles(('user_permit'), [P.CREATE]) }
      },
      {
        path: 'setting',
        component: UploadBerkasComponent,
        data: { category: 'study_permit', title: 'Pengaturan Upload Berkas Keterangan Pendidikan', ...getRoles(('setting'), [P.UPDATE]), isAdmin: true }
      },
      {
        path: 'add/:id',
        component: UploadBerkasComponent,
        data: { category: 'education_permit', title: 'Pengaturan Upload Berkas Keterangan Pendidikan', ...getRoles(('user_permit'), [P.UPDATE]) }
      }
    ] 
  }
];

@NgModule({
  declarations: [
    KeteranganPendidikanComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class KeteranganPendidikanModule { }
