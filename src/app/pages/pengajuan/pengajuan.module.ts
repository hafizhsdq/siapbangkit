import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PengajuanComponent } from './pengajuan.component';
import { SharedModule } from '../../shared/shared.module';
import { PengajuanRoutingModule } from './pengajuan-routing.module';
import { ThemeModule } from '../../@theme/theme.module';
import { UsulanDiklatComponent } from './usulan-diklat/usulan-diklat.component';
import { AddUsulan } from './usulan-diklat/add-usulan.component';

@NgModule({
  declarations: [
    PengajuanComponent,
    UsulanDiklatComponent,
    AddUsulan
  ],
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    PengajuanRoutingModule
  ]
})
export class PengajuanModule { }
