import { Component, Injector, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetBiodatas } from '../../../../store/biodata/biodata.action';
import { BiodataState } from '../../../../store/biodata/biodata.state';
import { UsulanDiklats } from '../../../../store/usulan-diklat/usulan-diklat';
import { DeleteData, GetData } from '../../../../store/usulan-diklat/usulan-diklat.action';
import { UsulanDiklatState } from '../../../../store/usulan-diklat/usulan-diklat.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';
import { AddUsulan } from './add-usulan.component';

@Component({
  selector: 'ngx-usulan-diklat',
  templateUrl: './usulan-diklat.component.html',
  styleUrls: ['./usulan-diklat.component.scss']
})
export class UsulanDiklatComponent extends PageTableStoreComponent<GetData, DeleteData> {

  data = []
  @Select(UsulanDiklatState.getUsulanDiklats)
  usulandiklat$: Observable<UsulanDiklats.UsulanDiklat[]>;

  isAdmin = false
  constructor(public dialogService: NbDialogService, injector: Injector) { 
    super(injector, GetData, DeleteData)
    this.tag = 'usulandiklat'
  }

  public getData(): void {
    this.activateRoute.data.subscribe(({category, title}) => {
      const roleNames = this.biodataService.roles?.map((role: any) => role.attributes.name) ?? []
      if (roleNames.indexOf('system') > -1 || roleNames.indexOf('admin') > -1) {
        this.isAdmin = true
        this.store.dispatch(new GetData({include: 'users.employee'}))
      } else {
        this.store.dispatch(new GetData({include: 'users.employee', params: 'filter[self]=true'}))
      }

    })
  }

  public OnEdit(id: string | number): void {
    const dialog = this.addUsulan(id)

    dialog.componentRef.instance.edit(id)

    dialog.onClose.subscribe(() => {
      this.getData()
    })

  }

  addUsulan = (id?: any) => {return this.dialogService.open(AddUsulan, { hasScroll: true, context:{id} }) }


}
