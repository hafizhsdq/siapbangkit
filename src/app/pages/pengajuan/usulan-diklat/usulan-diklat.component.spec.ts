import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsulanDiklatComponent } from './usulan-diklat.component';

describe('UsulanDiklatComponent', () => {
  let component: UsulanDiklatComponent;
  let fixture: ComponentFixture<UsulanDiklatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsulanDiklatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsulanDiklatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
