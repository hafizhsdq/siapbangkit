import { Component, Injector } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { BaseData } from "../../../../store/base-model";
import { UsulanDiklats } from "../../../../store/usulan-diklat/usulan-diklat";
import { CreateUpdateData, GetData } from "../../../../store/usulan-diklat/usulan-diklat.action";
import { UsulanDiklatState } from "../../../../store/usulan-diklat/usulan-diklat.state";
import { CreateEditDialogStoreBaseComponent } from "../../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'add-usulan',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    <h6>Usulan Diklat</h6>
                </nb-card-header>
                <nb-card-body>

                    <div class="form-group row mb-3">
                        <label for="title" class="label col-form-label col-4">Judul Diklat</label>
                        <div class="col-sm-8">
                            <input class="col" nbInput
                                fullWidth
                                name="title"
                                type="text"
                                id="title"
                                [ngModel]="id ? data?.attributes?.title : f.value.title " required #title="ngModel"
                                placeholder=""
                                fieldSize="medium">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="explanation" class="label col-form-label col-4">Keterangan</label>
                        <div class="col-sm-8">
                        <textarea rows="5" name="explanation" nbInput fullWidth placeholder="" [ngModel]="id ? data?.attributes?.explanation : f.value.explanation" #explanation="ngModel"></textarea>
                        </div>
                    </div>

                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button type="button" class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>

        <!-- <ng-template #formGroup>
        </ng-template> -->
    `,
})
export class AddUsulan extends CreateEditDialogStoreBaseComponent<AddUsulan, CreateUpdateData, UsulanDiklats.CreateUpdateUsulanDiklatInput, GetData, UsulanDiklats.UsulanDiklat> {
    title: string = "Usulan Diklat"
    data: BaseData<UsulanDiklats.UsulanDiklat>

    constructor(injector: Injector, public dialogRef: NbDialogRef<AddUsulan>) {
        super(injector, dialogRef, 'usulandiklat', CreateUpdateData)
    }

    edit(id: string | number) {
        console.log(this.title);
        
        this.id = id
        this.data = this.store.selectSnapshot(UsulanDiklatState).response.data[0]
        // this.store.dispatch(new GetData({}, id))
    }

    successSubmit(result: any): void {
        this.dialogRef.close()
    }
}