import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-keterangan-belajar',
  template: `<router-outlet></router-outlet>`
})
export class KeteranganBelajarComponent {}