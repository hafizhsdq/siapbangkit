import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeteranganBelajarComponent } from './keterangan-belajar.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { UploadBerkasComponent } from '../../../shared/components/upload-berkas/upload-berkas.component';
import { PermitTableComponent } from '../../../shared/components/permit-table/permit-table.component';
import { getRoles, P } from '../../roles';
import { AuthGuard } from '../../../shared/guard/auth.guard';


const routes: Routes = [
  { path: '',
    component: KeteranganBelajarComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: PermitTableComponent,
        data: { category: 'statement_permit', title: 'Daftar Pengajuan Surat Keterangan Belajar', ...getRoles(('user_permit'), [P.READ]) }
      },
      {
        path: 'add',
        component: UploadBerkasComponent,
        data: { category: 'statement_permit', title: 'Upload Berkas Keterangan Belajar', ...getRoles(('user_permit'), [P.CREATE]) }
      },
      {
        path: 'setting',
        component: UploadBerkasComponent,
        data: { category: 'study_permit', title: 'Pengaturan Upload Berkas Keterangan Belajar', ...getRoles(('setting'), [P.UPDATE]), isAdmin: true }
      },
      {
        path: 'add/:id',
        component: UploadBerkasComponent,
        data: { category: 'statement_permit', title: 'Pengaturan Upload Berkas Keterangan Belajar', ...getRoles(('user_permit'), [P.UPDATE]) }
      }
    ] 
  }
];

@NgModule({
  declarations: [
    KeteranganBelajarComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class KeteranganBelajarModule { }
