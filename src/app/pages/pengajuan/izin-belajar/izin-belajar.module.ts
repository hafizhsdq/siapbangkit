import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogFormUploadComponent, DialogPengantarComponent, IzinBelajarComponent } from './izin-belajar.component';
import { ThemeModule } from '../../../@theme/theme.module'; 
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { UploadBerkasComponent } from '../../../shared/components/upload-berkas/upload-berkas.component';
import { PermitTableComponent } from '../../../shared/components/permit-table/permit-table.component';
import { getRoles, P } from '../../roles';
import { AuthGuard } from '../../../shared/guard/auth.guard';

const routes: Routes = [
  { path: '',
    component: IzinBelajarComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: PermitTableComponent,
        data: { category: 'study_permit', title: 'Daftar Pengajuan Izin Belajar', ...getRoles(('user_permit'), [P.READ]) }
      },
      {
        path: 'add',
        component: UploadBerkasComponent,
        data: { category: 'study_permit', title: 'Upload Berkas Izin Belajar', ...getRoles(('user_permit'), [P.CREATE]) }
      },
      {
        path: 'setting',
        component: UploadBerkasComponent,
        data: { category: 'study_permit', title: 'Pengaturan Upload Berkas Izin Belajar', ...getRoles(('setting'), [P.UPDATE]), isAdmin: true }
      },
      {
        path: 'add/:id',
        component: UploadBerkasComponent,
        data: { category: 'study_permit', title: 'Pengaturan Upload Berkas Izin Belajar', ...getRoles(('user_permit'), [P.UPDATE]) }
      }
    ] 
  }
];

@NgModule({
  declarations: [
    IzinBelajarComponent,
    DialogFormUploadComponent,
    DialogPengantarComponent
  ],
  imports: [
    ThemeModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class IzinBelajarModule { }
