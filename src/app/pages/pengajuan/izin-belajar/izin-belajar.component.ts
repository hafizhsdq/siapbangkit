import { Component, ElementRef, ViewChild } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-izin-belajar',
  template: `<router-outlet></router-outlet>`
})
export class IzinBelajarComponent {}

@Component ({
  selector: 'ngx-dialog-form-upload',
  template: `
    <nb-card>
      <nb-card-header class="text-primary">Upload Surat Rekomendasi</nb-card-header>
      <nb-card-body class="d-inline-block">
        <div>
          <p class="label">Attach Document</p>
          <div class="drag-file" style="width: 30vw; height: 40vh; background-color: #E5E5E5;">
            <p class="label text-center" style="line-height: 40vh;">Drop file here</p>  
          </div>
          <p class="label" style="font-size: .8rem;">Format file harus berbentuk .pdf</p>
        </div>
      </nb-card-body>
      <nb-card-footer class="d-flex justify-content-end">
        <button nbButton status="primary" (click)="submit(name.nativeElement.files[0].name)">Upload</button>
      </nb-card-footer>
    </nb-card>
  `
})

export class DialogFormUploadComponent {
  @ViewChild('name') name: ElementRef
  constructor(protected ref: NbDialogRef<DialogFormUploadComponent>) {}

  cancel() {
    this.ref.close();
  }

  submit(name) {
    this.ref.close(name);
    console.log(name)
  }

}

@Component ({
  selector: 'ngx-dialog-pengantar',
  template: `
    <nb-card>
      <nb-card-header class="text-primary">Upload Surat Pengantar</nb-card-header>
      <nb-card-body class="d-inline-block">
        <div>
          <p class="label">Attach Document</p>
          <div class="drag-file" style="width: 30vw; height: 40vh; background-color: #E5E5E5;">
            <p class="label text-center" style="line-height: 40vh;">Drop file here</p>  
          </div>
          <p class="label" style="font-size: .8rem;">Format file harus berbentuk .pdf</p>
        </div>
      </nb-card-body>
      <nb-card-footer class="d-flex justify-content-end">
        <button nbButton status="primary" (click)="submit(name.nativeElement.files[0].name)">Upload</button>
      </nb-card-footer>
    </nb-card>
  `
})

export class DialogPengantarComponent {
  @ViewChild('name') name: ElementRef
  constructor(protected ref: NbDialogRef<DialogPengantarComponent>) {}

  cancel() {
    this.ref.close();
  }

  submit(name) {
    this.ref.close(name);
    console.log(name)
  }

}
