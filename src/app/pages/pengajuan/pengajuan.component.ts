import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-pengajuan',
  template: `<router-outlet></router-outlet>`
})
export class PengajuanComponent {}
