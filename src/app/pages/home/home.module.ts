import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbCardModule, NbIconModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { JPProgressBar } from './jp-progress-bar';
import { DialogSertifikat } from './dialog-sertifikat.component';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  declarations: [
    HomeComponent,
    JPProgressBar,
    DialogSertifikat
  ],
  imports: [
    SwiperModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    ThemeModule,
    CommonModule,
    SharedModule,
    RouterModule,
    NgApexchartsModule
  ]
})
export class HomeModule { }
