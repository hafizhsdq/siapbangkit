import { Component, Injector, Input } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { Select } from "@ngxs/store";
import { Observable } from "rxjs";
import { CreateUpdateData, GetData } from "../../../store/sertifikatku/sertifikatku.action";
import { Sertifikats } from "../../../store/sertifikatku/sertifikatku";
import { BiodataState } from "../../../store/biodata/biodata.state";
import { Biodatas } from "../../../store/biodata/biodata";
import { CreateEditDialogStoreBaseComponent } from "../../shared/base/create-edit-dialog-base.component";

@Component ({
    selector: 'dialog-sertifikat',
    template: `
        <form #f="ngForm" (ngSubmit)="onSubmit(f)" ngNativeValidate>
            <nb-card>
                <nb-card-header>
                    <h6>{{title}}</h6>
                    <span class="caption"><strong>Note*</strong> Sertifikat dari diklat yang telah diselesaikan</span>
                </nb-card-header>
                <nb-card-body *ngIf="(biodata$ | async) as biodata">
                <!-- {{ biodata.relationships.participant | json }} -->
                    <ng-container 
                        [ngTemplateOutlet]="formGroup"></ng-container>
                </nb-card-body>
                <nb-card-footer class="d-flex justify-content-end">
                    <button class="mr-2" nbButton ghost size="small" (click)="cancel()">Batalkan</button>
                    <button nbButton type="submit" status="primary" size="small">Simpan</button>
                </nb-card-footer>
            </nb-card>
        </form>

        <ng-template #formGroup>
            <div class="form-group row mb-3">
                <label for="name" class="label col-form-label col-4">Nama Diklat</label>
                <div class="col-sm-8">
                    <nb-select name="name" selected="0" fullWidth ngModel #name="ngModel">
                        <nb-option value="individu">Diklat Individu</nb-option>
                        <nb-option value="internal">Diklat Internal</nb-option>
                    </nb-select>
                </div>
            </div>

        <ng-container *ngIf="name.value == 'individu'" hidden>
            <div class="form-group row mb-3">
                <label for="jp" class="label col-form-label col-4">JP</label>
                <div class="col-sm-8">
                    <input class="col" nbInput
                        fullWidth
                        name="jp"
                        type="number"
                        id="jp"
                        ngModel required #jp="ngModel"
                        placeholder=""
                        fieldSize="medium">
                </div>
            </div>

            <div class="form-group row mb-3">
                <label for="start_date" class="label col-form-label col-4">Tanggal Mulai</label>
                <div class="col-sm-8">
                    <nb-form-field class="p-0">
                            <input id="start_date" name="start_date" nbInput fullWidth placeholder="" [nbDatepicker]="start_datePicker" ngModel required #start_date="ngModel">
                            <nb-datepicker #start_datePicker></nb-datepicker>
                            <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                    </nb-form-field>
                </div>
            </div>
                
            <div class="form-group row mb-3">
                <label for="tglSelesai" class="label col-form-label col-4">Tanggal Selesai</label>
                <div class="col-sm-8">
                    <nb-form-field class="p-0">
                            <input id="tglSelesai" name="tglSelesai" nbInput fullWidth placeholder="" [nbDatepicker]="tglSelesaiPicker" ngModel required #tglSelesai="ngModel">
                            <nb-datepicker #tglSelesaiPicker></nb-datepicker>
                            <nb-icon nbSuffix icon="calendar-outline"></nb-icon> 
                    </nb-form-field>
                </div>
            </div>
        </ng-container>

            <div class="form-group row mb-3">
                <label for="ceertificate_number" class="label col-form-label col-4">Nomor Sertifikat</label>
                <div class="col-sm-8">
                    <input class="col" nbInput
                        fullWidth
                        name="certificate_number"
                        type="number"
                        id="certificate_number"
                        ngModel required #certificate_number="ngModel"
                        placeholder=""
                        fieldSize="medium">
                </div>
            </div>
                
            <div class="form-group row mb-3">
                <label for="ijazah" class="label col-form-label col-4">File Sertifikat</label>
                <div class="col-sm-8">
                    <ngx-upload id="ijazah" (change)="onChange($event)"></ngx-upload>
                </div>
            </div>
        </ng-template>
    `,
})
export class DialogSertifikat extends CreateEditDialogStoreBaseComponent<DialogSertifikat, CreateUpdateData, Sertifikats.CreateUpdateSertifikatInput, GetData, Sertifikats.Sertifikat> {
    title: string = "Upload Sertifikat"

    @Input() category: string= '';
    constructor(injector: Injector, public dialogRef: NbDialogRef<DialogSertifikat>) {
        super(injector, dialogRef, 'sertifikat', CreateUpdateData)
        // console.log(this.tag)
    }

    

    @Select(BiodataState.getBiodatas)
    biodata$: Observable<Biodatas.Biodata>

    successSubmit(result: any): void {
        this.dialogRef.close()
    }

    dataTable = [
        {
            "title": "Nama Kelompok",
            "name": "nama_kelompok"
        },
        {
            "title": "Nama Diklat",
            "name": "nama_diklat"
        },
        {
            "title": "Lokasi Diklat",
            "name": "lokasi_diklat"
        },
    ]
}