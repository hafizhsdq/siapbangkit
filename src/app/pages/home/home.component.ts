import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { KalenderDiklats } from '../../../store/kalender-diklat/kalender-diklat';
import { GetData } from '../../../store/kalender-diklat/kalender-diklat.action';
import { KalenderDiklatState } from '../../../store/kalender-diklat/kalender-diklat.state';
import { PageBaseComponent } from '../../shared/base/page-base.component';

// import Swiper core and required modules
import SwiperCore, { Autoplay, Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { DialogSertifikat } from './dialog-sertifikat.component';
import { KalenderDiklatService } from '../../shared/services/kalender-diklat.service';
import { map } from 'rxjs/operators';
import { ModelHelper } from '../../../store/base-model';
import { SimpegService } from '../../shared/services/simpeg.service';

// install Swiper modules
SwiperCore.use([Autoplay, Navigation, Pagination, Scrollbar, A11y]);

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends PageBaseComponent {

  uploadSertifikat = () => { this.dialogService.open(DialogSertifikat, { hasScroll: true }) }

  @Select(KalenderDiklatState.getKalenderDiklats)
  schedule$: Observable<KalenderDiklats.KalenderDiklat[]>

  participant$: Observable<any>  

  constructor(injector: Injector, private schedulService: KalenderDiklatService) {
    super(injector);

    this.store.dispatch(new GetData({include: 'training', params: 'filter[category]=internal,eksternal&filter[activity_log]=diterima'}))

    this.participant$ = this.schedulService.get({
      include: 'training_schedule.training', 
      params: 'filter[participate]=true'
    }).pipe(map(response => ({...response, response: ModelHelper.nestInclude(response)})))
  }

  downloadBio() {
    this.biodataService.downloadBio()
  }
}
