import { Component, Input, OnInit } from "@angular/core";
import {
    ApexNonAxisChartSeries,
    ApexPlotOptions,
    ApexChart,
    ApexFill,
    ChartComponent,
    ApexStroke
} from "ng-apexcharts";
import { JPPipe } from "../../@theme/pipes/jp.pipe";
import { UserService } from "../../shared/services/user.service";

export type ChartOptions = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    plotOptions: ApexPlotOptions;
    fill: ApexFill;
    stroke: ApexStroke;
};

@Component({
    selector: 'ngx-jp-bar',
    template: `
    <div class="position-relative w-100">
      <apx-chart
        [series]="chartOptions.series"
        [chart]="chartOptions.chart"
        [plotOptions]="chartOptions.plotOptions"
        [labels]="chartOptions.labels"
        [stroke]="chartOptions.stroke"
        [fill]="chartOptions.fill"
      ></apx-chart>

      <div class="d-block w-100 position-absolute d-flex justify-content-center" style="bottom: 0;">
        <button nbButton status="primary" size="medium">
            {{TotalJP}} JP
        </button>
      </div>
    </div>
    `
})
export class JPProgressBar implements OnInit {

    @Input() courses: any

    public chartOptions: Partial<ChartOptions>;
    jp = 0
    maximumJp = 20
    jpPipe = new JPPipe()

    TotalJP = 0

    constructor(private userService: UserService) {}

    ngOnInit(): void {
        this.TotalJP = this.userService.getTotalJP(this.courses)

        const percentage = this.TotalJP/this.maximumJp * 100

        this.chartOptions = {
            series: [percentage],
            chart: {
            height: 255,
            type: "radialBar",
            toolbar: {
                show: false
            }
            },
            plotOptions: {
            radialBar: {
                startAngle: -150,
                endAngle: 150,
                hollow: {
                margin: 0,
                size: "70%",
                background: "",
                position: "front",
                dropShadow: {
                    enabled: true,
                    top: 3,
                    left: 0,
                    blur: 4,
                    opacity: 0.24
                }
                },
                track: {
                background: "#fff",
                strokeWidth: "67%",
                margin: 0, // margin is in pixels
                dropShadow: {
                    enabled: true,
                    top: -3,
                    left: 0,
                    blur: 4,
                    opacity: 0.35
                }
                },
    
                dataLabels: {
                show: true,
                name: {
                    offsetY: -2,
                    show: false,
                    color: "#888",
                    fontSize: "17px"
                },
                value: {
                    // formatter: function(val) {
                    //     console.log(this.jp.toString());
                    //     return this.jp.toString()// parseInt(val.toString(), 10).toString()+'JP';
                    // },
                    color: "#111",
                    fontSize: "25px",
                    offsetY: 60,
                    show: false
                }
                }
            }
            },
            fill: {
            // type: "gradient",
            // gradient: {
            //     shade: "dark",
            //     type: "horizontal",
            //     shadeIntensity: 0.5,
            //     gradientToColors: ["#ABE5A1"],
            //     inverseColors: true,
            //     opacityFrom: 1,
            //     opacityTo: 1,
            //     stops: [0, 100]
            // }
            },
            stroke: {
            lineCap: "round"
            },
            labels: ["Percent"],
        };
    }
}