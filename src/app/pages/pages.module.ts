import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { HomeModule } from './home/home.module';
import { HakAksesModule } from './hak-akses/hak-akses.module';
import { ManajemenUserModule } from './manajemen-user/manajemen-user.module';
import { ProfileModule } from './profile/profile.module';
import { AdministrasiPesertaModule } from './administrasi-peserta/administrasi-peserta.module';
import { PengumumanModule } from './pengumuman/pengumuman.module';
import { PengaturanComponent } from './pengaturan/pengaturan.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    MiscellaneousModule,
    HomeModule,
    HakAksesModule,
    ManajemenUserModule,
    ProfileModule,
    AdministrasiPesertaModule,
    // ActionLearningModule,
    PengumumanModule,
    SharedModule
  ],
  declarations: [
    PagesComponent,
    PengaturanComponent
  ],
})
export class PagesModule {
}
