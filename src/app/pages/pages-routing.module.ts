import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'home',
      component: HomeComponent
    },
    {
      path: 'pesan',
      loadChildren: () => import('./pesan/pesan.module').then(m => m.PesanModule),
    },
    {
      path: 'profile',
      loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
    },
    {
      path: 'administrasi-peserta',
      loadChildren: () => import('./administrasi-peserta/administrasi-peserta.module').then(m => m.AdministrasiPesertaModule),
    },
    { path: 'diklat',
      loadChildren: () => import('./diklat/diklat.module').then(m => m.DiklatModule)
    },
    {
      path: 'pengajuan',
      loadChildren: () => import('./pengajuan/pengajuan.module').then(m => m.PengajuanModule)
    },
    {
      path: 'manajemen-user',
      loadChildren: () => import('./manajemen-user/manajemen-user.module').then(m => m.ManajemenUserModule)
    },
    {
      path: 'hak-akses',
      loadChildren: () => import('./hak-akses/hak-akses.module').then(m => m.HakAksesModule)
    },
    {
      path: 'pengumuman',
      loadChildren: () => import('./pengumuman/pengumuman.module').then(m => m.PengumumanModule)
    },
    { 
      path: 'questionnaire', 
      loadChildren: () => import('./questionnaire/questionnaire.module').then(m => m.QuestionnaireModule) 
    },
    {
      path: 'berkas',
      loadChildren: () => import('./berkas/berkas.module').then(m => m.BerkasModule)
    },
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
