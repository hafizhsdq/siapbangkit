import { NbMenuItem } from '@nebular/theme';
import { P } from './roles';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Home',
    icon: 'home-outline',
    link: '/pages/home',
    home: true,
  },
  {
    title: 'ADMIN CONTROL',
    group: true,
    data: {
      roles: {
        participant: [P.READ],
        training_schedule: [P.READ],
        training: [P.READ],
        training_subject: [P.READ]
      }
    },
  },
  {
    title: 'Administrasi Diklat',
    icon: 'book-open-outline',
    data: {
      roles: {
        participant: [P.READ],
        training_schedule: [P.READ],
        training: [P.READ],
        training_subject: [P.READ]
      }
    },
    children: [
      {
        title: 'PENGATURAN DIKLAT',
        group: true,
        data: {
          roles: {
            participant: [P.READ],
            training_schedule: [P.READ],
            training: [P.READ],
            training_subject: [P.READ]
          }
        },
      },
      {
        title: 'Pengaturan Kalender Diklat',
        link: '/pages/diklat/kalender',
        data: {
          roles: {
            training_schedule: [P.READ]
          }
        },
      },
      {
        title: 'Pengaturan Diklat',
        link: '/pages/diklat/pengaturan',
        data: {
          roles: {
            training: [P.READ]
          }
        },
      },
      {
        title: 'Pengaturan Mata Diklat',
        link: '/pages/diklat/pengaturan-mata',
        data: {
          roles: {
            training_subject: [P.READ]
          }
        },
      },
      {
        title: 'PERMOHONAN',
        group: true,
        data: {
          roles: {
            training_schedule: [P.READ],
            user_course: [P.READ]
          }
        },
      },
      {
        title: 'Permohonan Diklat',
        link: '/pages/diklat/permohonan',
        data: {
          roles: {
            training_schedule: [P.READ]
          }
        },
      },
      {
        title: 'Permohonan Sertifikat',
        link: '/pages/diklat/sertifikat',
        data: {
          roles: {
            user_course: [P.READ]
          }
        },
      },
    ]
  },
  {
    title: 'PENCARIAN DATA',
    group: true,
  },
  {
    title: 'Pencarian Peserta Diklat',
    link: '/pages/diklat/search-peserta',
    data: {
      roles: {
        participant: [P.READ]
      }
    },
  },
  {
    title: 'Pencarian Karya Tulis',
    link: '/pages/diklat/search-paper',
    data: {
      roles: {
        user_paper: [P.READ]
      }
    },
  },
  {
    title: 'PELATIHAN',
    group: true,
    data: {
      roles: {
        training_schedule: [P.READ]
      }
    },
  },
  {
    title: 'Administrasi Peserta',
    icon: 'file-text-outline',
    data: {
      roles: {
        training_schedule: [P.READ]
      }
    },
    children: [
      // {
      //   title: 'Diklat Individu',
      //   link: '/pages/administrasi-peserta/diklat-individu'
      // },
      {
        title: 'Kalender Diklat',
        link: '/pages/administrasi-peserta/kalender-diklat'
      },
      {
        title: 'Diklatku',
        link: '/pages/administrasi-peserta/semua-diklat'
      },
      {
        title: 'Sertifikatku',
        link: '/pages/administrasi-peserta/sertifikatku'
      },
      {
        title: 'Karya Tulisku',
        link: '/pages/administrasi-peserta/karya-tulisku'
      }
    ]
  },
  {
    title: 'KUISIONER & PENGAJUAN',
    group: true,
    data: {
      roles: {
        questionnaire: [P.READ],
        training_suggestion: [P.READ],
        user_permit: [P.READ]
      }
    },
  },
  {
    title: 'Kuisioner Diklat',
    icon: 'checkmark-square-outline',
    link: '/pages/questionnaire',
    data: {
      roles: {
        questionnaire: [P.READ]
      }
    },
  },
  {
    title: 'Pengajuan',
    icon: 'edit-2-outline',
    link: '/pages/izin-belajar',
    data: {
      roles: {
        training_suggestion: [P.READ],
        user_permit: [P.READ]
      }
    },
    children: [
      {
        title: 'Izin Belajar',
        link: '/pages/pengajuan/izin-belajar',
        data: {
          roles: {
            user_permit: [P.READ]
          }
        },
      },
      {
        title: 'Surat Keterangan Pendidikan',
        link: '/pages/pengajuan/keterangan-pendidikan',
        data: {
          roles: {
            user_permit: [P.READ]
          }
        },        
      },
      {
        title: 'Surat Keterangan Belajar',
        link: '/pages/pengajuan/keterangan-belajar',
        data: {
          roles: {
            user_permit: [P.READ]
          }
        },
      },
      {
        title: 'Usulan Diklat',
        link: '/pages/pengajuan/usulan-diklat',
        data: {
          roles: {
            training_suggestion: [P.READ]
          }
        },
      }
    ]
  },
  {
    title: 'PENGATURAN',
    group: true,
    data: {
      roles: {
        user: [P.READ],
        role: [P.READ],
        permissions: [P.READ]
      }
    },
  },
  // {
  //   title: 'Manajemen File',
  //   icon: 'archive-outline',
  //   link: '/pages/manajemen-file'
  // },
  {
    title: 'Manajemen User',
    icon: 'person-outline',
    link: '/pages/manajemen-user',
    data: {
      roles: {
        user: [P.READ]
      }
    },
  },
  {
    title: 'Hak Akses',
    icon: 'lock-outline',
    link: '/pages/hak-akses',
    data: {
      roles: {
        role: [P.READ]
      }
    },
  },
  // {
  //   title: 'Pengaturan Pengumuman',
  //   icon: 'volume-up-outline',
  //   link: '/pages/pengumuman'
  // },
  // {
  //   title: 'Pengaturan Umum',
  //   icon: 'settings-outline',
  //   link: '/pages/pengaturan'
  // },
  // {
  //   title: 'Berkas',
  //   icon: 'file-text-outline',
  //   children: [
  //     {
  //       title: 'Berkas Saya',
  //       link: '/pages/berkas/berkas-saya'
  //     }
  //   ]
  // },
  
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'E-commerce',
  //   icon: 'shopping-cart-outline',
  //   link: '/pages/dashboard'
  // },
  // {
  //   title: 'IoT Dashboard',
  //   icon: 'home-outline',
  //   link: '/pages/iot-dashboard',
  // },
  // {
  //   title: 'Layout',
  //   icon: 'layout-outline',
  //   children: [
  //     {
  //       title: 'Stepper',
  //       link: '/pages/layout/stepper',
  //     },
  //     {
  //       title: 'List',
  //       link: '/pages/layout/list',
  //     },
  //     {
  //       title: 'Infinite List',
  //       link: '/pages/layout/infinite-list',
  //     },
  //     {
  //       title: 'Accordion',
  //       link: '/pages/layout/accordion',
  //     },
  //     {
  //       title: 'Tabs',
  //       pathMatch: 'prefix',
  //       link: '/pages/layout/tabs',
  //     },
  //   ],
  // },
  // {
  //   title: 'Forms',
  //   icon: 'edit-2-outline',
  //   children: [
  //     {
  //       title: 'Form Inputs',
  //       link: '/pages/forms/inputs',
  //     },
  //     {
  //       title: 'Form Layouts',
  //       link: '/pages/forms/layouts',
  //     },
  //     {
  //       title: 'Buttons',
  //       link: '/pages/forms/buttons',
  //     },
  //     {
  //       title: 'Datepicker',
  //       link: '/pages/forms/datepicker',
  //     },
  //   ],
  // },
  // {
  //   title: 'UI Features',
  //   icon: 'keypad-outline',
  //   link: '/pages/ui-features',
  //   children: [
  //     {
  //       title: 'Grid',
  //       link: '/pages/ui-features/grid',
  //     },
  //     {
  //       title: 'Icons',
  //       link: '/pages/ui-features/icons',
  //     },
  //     {
  //       title: 'Typography',
  //       link: '/pages/ui-features/typography',
  //     },
  //     {
  //       title: 'Animated Searches',
  //       link: '/pages/ui-features/search-fields',
  //     },
  //   ],
  // },
  // {
  //   title: 'Modal & Overlays',
  //   icon: 'browser-outline',
  //   children: [
  //     {
  //       title: 'Dialog',
  //       link: '/pages/modal-overlays/dialog',
  //     },
  //     {
  //       title: 'Window',
  //       link: '/pages/modal-overlays/window',
  //     },
  //     {
  //       title: 'Popover',
  //       link: '/pages/modal-overlays/popover',
  //     },
  //     {
  //       title: 'Toastr',
  //       link: '/pages/modal-overlays/toastr',
  //     },
  //     {
  //       title: 'Tooltip',
  //       link: '/pages/modal-overlays/tooltip',
  //     },
  //   ],
  // },
  // {
  //   title: 'Extra Components',
  //   icon: 'message-circle-outline',
  //   children: [
  //     {
  //       title: 'Calendar',
  //       link: '/pages/extra-components/calendar',
  //     },
  //     {
  //       title: 'Progress Bar',
  //       link: '/pages/extra-components/progress-bar',
  //     },
  //     {
  //       title: 'Spinner',
  //       link: '/pages/extra-components/spinner',
  //     },
  //     {
  //       title: 'Alert',
  //       link: '/pages/extra-components/alert',
  //     },
  //     {
  //       title: 'Calendar Kit',
  //       link: '/pages/extra-components/calendar-kit',
  //     },
  //     {
  //       title: 'Chat',
  //       link: '/pages/extra-components/chat',
  //     },
  //   ],
  // },
  // {
  //   title: 'Maps',
  //   icon: 'map-outline',
  //   children: [
  //     {
  //       title: 'Google Maps',
  //       link: '/pages/maps/gmaps',
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       link: '/pages/maps/leaflet',
  //     },
  //     {
  //       title: 'Bubble Maps',
  //       link: '/pages/maps/bubble',
  //     },
  //     {
  //       title: 'Search Maps',
  //       link: '/pages/maps/searchmap',
  //     },
  //   ],
  // },
  // {
  //   title: 'Charts',
  //   icon: 'pie-chart-outline',
  //   children: [
  //     {
  //       title: 'Echarts',
  //       link: '/pages/charts/echarts',
  //     },
  //     {
  //       title: 'Charts.js',
  //       link: '/pages/charts/chartjs',
  //     },
  //     {
  //       title: 'D3',
  //       link: '/pages/charts/d3',
  //     },
  //   ],
  // },
  // {
  //   title: 'Editors',
  //   icon: 'text-outline',
  //   children: [
  //     {
  //       title: 'TinyMCE',
  //       link: '/pages/editors/tinymce',
  //     },
  //     {
  //       title: 'CKEditor',
  //       link: '/pages/editors/ckeditor',
  //     },
  //   ],
  // },
  // {
  //   title: 'Tables & Data',
  //   icon: 'grid-outline',
  //   children: [
  //     {
  //       title: 'Smart Table',
  //       link: '/pages/tables/smart-table',
  //     },
  //     {
  //       title: 'Tree Grid',
  //       link: '/pages/tables/tree-grid',
  //     },
  //     {
  //       title: 'Swimlane Datatable',
  //       link: '/pages/tables/swimlane-datatable',
  //     },
  //   ],
  // },
  // {
  //   title: 'Miscellaneous',
  //   icon: 'shuffle-2-outline',
  //   children: [
  //     {
  //       title: '404',
  //       link: '/pages/miscellaneous/404',
  //     },
  //   ],
  // },
  // {
  //   title: 'Auth',
  //   icon: 'lock-outline',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
