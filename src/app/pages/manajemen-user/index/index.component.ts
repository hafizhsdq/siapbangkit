import { Component, Injector } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { DeleteData, GetData } from '../../../../store/user/user.action';
import { UserState } from '../../../../store/user/user.state';
import { PageTableStoreComponent } from '../../../shared/base/page-table.component';

@Component({
  selector: 'ngx-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends PageTableStoreComponent<GetData, DeleteData> {

  @Select(UserState.getUsers)
  users$: Observable<any>

  constructor(public injector: Injector) {
    super(injector, GetData, DeleteData, {include: 'roles'})
    this.tag = 'manajemen-user-menu'
  }
}
