import { Component } from '@angular/core';

@Component({
  selector: 'ngx-manajemen-user',
  template: `<router-outlet></router-outlet>`
})
export class ManajemenUserComponent {}
