import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManajemenUserComponent } from './manajemen-user.component';
import { NbAutocompleteModule, NbButtonModule, NbCardModule, NbContextMenuModule, NbFormFieldModule, NbIconModule, NbInputModule, NbSelectModule, NbTagModule, NbTooltipModule } from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { IndexComponent } from './index/index.component';
import { AddDataComponent } from './add-data/add-data.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { getRoles, P } from '../roles';

const routes: Routes = [
  { path: '',
    component: ManajemenUserComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: getRoles('user', [P.READ]),
      },
      {
        path: 'update/:id',
        component: AddDataComponent,
        data: getRoles('user', [P.UPDATE]),
      },
      {
        path: 'create',
        component: AddDataComponent,
        data: getRoles('user', [P.CREATE]),
      }
    ] 
  }
];

@NgModule({
  declarations: [
    ManajemenUserComponent,
    IndexComponent,
    AddDataComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NbTooltipModule,
    NbTagModule,
    NbAutocompleteModule,
    RouterModule.forChild(routes)
  ]
})
export class ManajemenUserModule { }
