import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NbTagInputDirective, NbTagComponent } from '@nebular/theme';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable, forkJoin, of } from 'rxjs';
import { BaseData } from '../../../../store/base-model';
import { Diklats } from '../../../../store/diklat/diklat';
import { Roles } from '../../../../store/role-permission/role-permission';
import { GetRoles } from '../../../../store/role-permission/role.action';
import { RoleState } from '../../../../store/role-permission/role.state';
import { Users } from '../../../../store/user/user';
import { CreateUpdateData, GetData } from '../../../../store/user/user.action';
import { UserState } from '../../../../store/user/user.state';
import { OPD } from '../../../@core/data/opd';
import { CreateEditStoreBaseComponent } from '../../../shared/base/create-edit-base.component';
import { SimpegService } from '../../../shared/services/simpeg.service';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
// import test from './test.json'

@Component({
  selector: 'ngx-add-data',
  templateUrl: './add-data.component.html',
  styleUrls: ['./add-data.component.scss']
})
export class AddDataComponent extends CreateEditStoreBaseComponent<CreateUpdateData, Users.CreateUpdateUserInput, GetData, Users.User> {

  // unitOrg = {data: [
  //   {
  //     "id": 138,
  //     "kode": "0000",
  //     "unit_organisasi": [],
  //     "kode_organisasi": null,
  //     "nama": "PEMERINTAH KABUPATEN SIDOARJO A",
  //     "singkatan": "PEMKAB SIDOARJO",
  //     "alamat": null,
  //     "kodepos": null,
  //     "telepon": "8921945, 894114",
  //     "struktur": "0901-BUP",
  //     "keterangan": null
  //   },
  //   {
  //     "id": 20,
  //     "kode": "0000",
  //     "unit_organisasi": [],
  //     "kode_organisasi": null,
  //     "nama": "PEMERINTAH KABUPATEN SIDOARJO B",
  //     "singkatan": "PEMKAB SIDOARJO",
  //     "alamat": null,
  //     "kodepos": null,
  //     "telepon": "8921945, 894114",
  //     "struktur": "0901-BUP",
  //     "keterangan": null
  //   }
  // ]}

  // subUnit = {data: [
  //   {
  //     "id": 1,
  //     "kode": "01",
  //     "unit_organisasi": {
  //       "id": 20,
  //       "nama": "DINAS KESEHATAN A"
  //     },
  //     "nama": "PUSKESMAS BALONG BENDO A",
  //     "alamat": null,
  //     "kodepos": null,
  //     "telepon": null,
  //     "keterangan": null
  //   },
  //   {
  //     "id": 2,
  //     "kode": "01",
  //     "unit_organisasi": {
  //       "id": 20,
  //       "nama": "DINAS KESEHATAN B"
  //     },
  //     "nama": "PUSKESMAS BALONG BENDO B",
  //     "alamat": null,
  //     "kodepos": null,
  //     "telepon": null,
  //     "keterangan": null
  //   }
  // ]}

  @Select(RoleState.getRoles)
  roles$: Observable<Roles.Role[]>

  @Select(UserState.getUsers)
  user$: Observable<Diklats.Diklat>;

  @ViewChild(NbTagInputDirective, { read: ElementRef }) tagInput: ElementRef<HTMLInputElement>;
  @ViewChild("tagOPDInput", { read: ElementRef }) tagOPDInput: ElementRef<HTMLInputElement>;

  selectedRoles: Set<BaseData<Roles.Role>> = new Set<BaseData<Roles.Role>>()
  get selectedRoleName() : string[] {return Array.from(this.selectedRoles).map(s => s?.attributes?.name)}
  get selectedRoleIds() : (string | number)[] {return Array.from(this.selectedRoles).map(s => s.id)}

  listOPD = OPD
  selectedOPD: Set<string> = new Set<string>()

  showPass = false
  showConfirmPass = false

  unitOrganisasi$: Observable<any> = of([])
  subUnitOrganisasi$: Observable<any> = of([])
  filterUnitOrganisasi$: Observable<any> = of([])
  filterSubUnitOrganisasi$: Observable<any> = of([])

  userData: any
  loading = false

  state = 'none'

  constructor(injector: Injector) {
    super(injector, 'users', CreateUpdateData, GetData);

    this.store.dispatch(new GetRoles())

    // get all unit organisasi
    this.filterUnitOrganisasi$ = this.unitOrganisasi$ = this.simpegService.get('mUnitOrganisasi').pipe(map(({data}) => data))
    this.filterSubUnitOrganisasi$ = this.subUnitOrganisasi$ = this.simpegService.get('mSubunitOrganisasi').pipe(map(({data}) => data))
  }

  syncSimpeg() {
    this.sync().subscribe((result) => {
      if(result?.response?.success && result?.data?.nip) {
        const {
          nama,
          nip,
          jabatan,
        } = result.data

        const {unit_organisasi, subunit_organisasi} = jabatan.struktur_organisasi

        this.f.setValue({first_name: nama, nip, unit_organisasi: unit_organisasi ?? null, sub_unit_organisasi: subunit_organisasi ?? null, status: 'active', password: '', confirm: ''})        
      }
    })
  }

  sync() {
    const {nip} = this.f.value    
    return this.simpegService.get(`tPegawai&nip=${nip}`)
  }

  ngAfterViewInit(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id')
    if (this.id) {
      forkJoin([
        this.filterUnitOrganisasi$,
        this.filterSubUnitOrganisasi$,
        this.userService.getById(this.id, {include: 'roles'})
      ])
      .subscribe(([unit, subunit, result]) => {
        const {data, included} = (result as any)
        const roles = included.filter(i => i.type == 'roles')
        const {attributes: {nip, first_name, last_name, status, unit_organisasi = '', sub_unit_organisasi = ''}} = data[0]

        this.userData = {nip, first_name, last_name, status, unit_organisasi, sub_unit_organisasi, roles}

        this.selectedRoles = roles
        this.f.setValue({
          nip, first_name, status, 
          unit_organisasi: unit?.find(d => d.id === unit_organisasi) ?? null,
          sub_unit_organisasi: subunit?.find(d => d.id === sub_unit_organisasi) ?? null,
          password: '', 
          confirm: ''
        })
      })
    }
  }

  onTagAdd(value) {
    if (value) {
      this.selectedRoles.add(value);      
    }
    this.tagInput.nativeElement.value = '';
  }
  
  onTagRemove(tagToRemove: NbTagComponent) {
    const deleteSubject = Array.from(this.selectedRoles).find(s => s.attributes.name == tagToRemove.text)
    this.selectedRoles.delete(deleteSubject)
  }

  onTagOPDAdd(value) {
    if (value) {
      this.selectedOPD.add(value);      
    }
    this.tagOPDInput.nativeElement.value = '';
  }
  
  onTagOPDRemove(tagToRemove: NbTagComponent) {
    const deleteSubject = Array.from(this.selectedOPD).find(s => s == tagToRemove.text)
    this.selectedOPD.delete(deleteSubject)
  }

  onChange(event: any, eName: 'subunit' | 'unit') {
    const text = event.target.value
    if (eName === 'subunit') {
      const unit_id = this.f.value.unit_organisasi
      this.filterSubUnitOrganisasi$ = this.subUnitOrganisasi$.pipe(map((data: any[]) => data.filter((d: any) =>  (!unit_id || d.unit_organisasi.id == unit_id) && text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
    } else if (eName === 'unit') {
      this.filterUnitOrganisasi$ = this.unitOrganisasi$.pipe(map((data: any[]) => data.filter((d: any) =>  text === '' || d.nama.toLowerCase().indexOf(text.toLowerCase()) > -1)))
    }
  }

  viewHandle(value: any) {
    return typeof value === 'string' ? value : value.nama
  }

  onSubmit(f: NgForm) {
    this.loading = true
    this.state = 'mencoba mengambil data simpeg'
    this.f.value['role_ids'] = this.selectedRoleIds
    const {nip} = this.f.value
    
    this.sync().subscribe((result) => {
      if(result?.response?.success && result?.data?.nama) {
        const {
          nama,
          nip,
          gelar_depan,
          gelar_belakang,
          jenis_kelamin,
          status_kawin,
          jenis_pegawai,
          status_pegawai,
          kedudukan_pegawai,
          jabatan,
          pangkat,
          nip_lama,
          foto,
          tempat_lahir,
          tanggal_lahir,
          telepon,
          handphone_1,
          handphone_2,
          whatsapp,
          email,
          email_sidoarjokab,
          is_sesuai_ktp,
          ktp_kelurahan,
          ktp_kodepos,
          ktp_rw,
          ktp_rt,
          ktp_alamat,
          domisili_kelurahan,
          domisili_kodepos,
          domisili_rw,
          domisili_rt,
          domisili_alamat,
          nik,
          nomor_kk,
          npwp,
          nuptk,
          duk,
          nomor_karpeg,
          nomor_korpri,
        } = result.data

        const {unit_organisasi = { nama: '' }, subunit_organisasi = { nama: '' }, seksi = { nama: '' }} = jabatan?.struktur_organisasi
        // this.f.setValue({...this.f.value, first_name: nama, nip})
        this.state = 'mengambil data simpeg berhasil'

        this.doRequest(
          this.userService.create({
            ...this.f.value, 
            unit_organisasi: this.f.value.unit_organisasi?.id, 
            sub_unit_organisasi: this.f.value.sub_unit_organisasi?.id
          }).pipe(mergeMap(() => {
            return this.biodataService.update({
              avatar: foto,
              nama,
              nip,
              gelar_depan,
              gelar_belakang,
              jenis_kelamin: jenis_kelamin?.nama,
              status_kawin: status_kawin?.nama,
              jenis_pegawai: jenis_pegawai?.nama,
              status_pegawai: status_pegawai?.nama,
              kedudukan_pegawai: kedudukan_pegawai?.nama,
              jabatan: jabatan?.struktur_organisasi?.jabatan?.nama,
              pangkat,
              nip_lama,
              tempat_lahir,
              tanggal_lahir,
              telepon,
              handphone_1,
              handphone_2,
              whatsapp,
              email,
              email_sidoarjokab,
              is_sesuai_ktp: is_sesuai_ktp == 'Ya' ? true : false,
              ktp_kelurahan: ktp_kelurahan?.nama,
              ktp_kodepos,
              ktp_rw,
              ktp_rt,
              ktp_alamat,
              domisili_kelurahan: domisili_kelurahan?.nama,
              domisili_kodepos,
              domisili_rw,
              domisili_rt,
              domisili_alamat,
              nik,
              nomor_kk,
              npwp,
              nuptk,
              duk,
              nomor_karpeg,
              nomor_korpri,
              unit_organisasi: unit_organisasi.nama,
              sub_unit_organisasi: subunit_organisasi.nama,
              seksi: seksi.nama,
              // totalJP: 0,
            }, nip)
          })),
          {
            next: (result) => this.successSubmit(result),
            error: (error) => this.errorrSubmit(error),
            complete: () => this.completeSubmit()
          } as BehaviorSubject<any>
        )
      } else {
        this.swalHandler.alert({
          title: 'Konfirmasi Pendaftaran', 
          text: 'data nip pada database SIMPEG tidak ditemukan, tekan daftar untuk tetap mendaftar',
          confirmButtonText: 'Daftar',
          showCancelButton: true,
          icon: 'error'
        }, {
          confirm: () => {
            const doSave = this.store.dispatch(new CreateUpdateData({...this.f.value, 
              unit_organisasi: this.f.value.unit_organisasi.id, 
              sub_unit_organisasi: this.f.value.sub_unit_organisasi.id}, this.id))
            this.doRequest(doSave, {
                next: (result) => this.successSubmit(result),
                error: (error) => this.errorrSubmit(error),
                complete: () => this.completeSubmit()
              } as BehaviorSubject<any>
            )
          },
          callback: () => {
            this.loading = false
          }
        })
      }
    })
  }

  successSubmit(result: any): void {
    this.back()
  }

  completeSubmit(): void {
    this.loading = false
  }

  toggleShowConfirmPass = () => this.showConfirmPass = !this.showConfirmPass
  toggleShowPass = () => this.showPass = !this.showPass

  onChangeUnit() {
    const unit_id = this.f.value.unit_organisasi?.id
    this.filterSubUnitOrganisasi$ = this.subUnitOrganisasi$.pipe(
    map((data) => {
      return data?.filter(d => d.unit_organisasi.id == unit_id) ?? []
    }))
  }
}
