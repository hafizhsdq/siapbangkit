import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BaseData } from '../../store/base-model';
import { GetMenu, MenuState } from '../../store/menu/menu.state';
import { Roles } from '../../store/role-permission/role-permission';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="(menu$ | async) ?? []"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {

  @Select(MenuState.getMenu)
  menu$: Observable<BaseData<Roles.Role>[]>

  // menu = MENU_ITEMS;
  constructor(private store: Store) {    
    this.store.dispatch(new GetMenu({include: 'users'}))
  }

  
}
