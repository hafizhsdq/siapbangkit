import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { PageBaseComponent } from '../shared/base/page-base.component';
import { NbAuthService } from '@nebular/auth';
import { error } from 'console';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks'
import { authCodeFlowConfig } from './sso.config';


@Component({
  selector: 'ngx-callback-sso',
  templateUrl: './callback-sso.component.html',
  styleUrls: ['./callback-sso.component.scss']
})
export class CallbackSSOComponent extends PageBaseComponent {

  url = environment.baseUrl

  hasLoad = false
  simpegData = null

  get givenName() {
    const claims = this.oauthService.getIdentityClaims();
    if (!claims) {
      return null;
    }
    return claims['name'];
  }

  get token(){
    let claims:any = this.oauthService.getIdentityClaims();
    return claims ? claims : null;
  }

  constructor(injector: Injector, private http: HttpClient, private auth: NbAuthService, private oauthService: OAuthService,) {
    super(injector);
    this.configureSingleSignOn()
  }

  configureSingleSignOn(){
    this.oauthService.configure(authCodeFlowConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  ngOnInit(): void {
    console.log(this.token);
    
    // has token to be login
    if (this.token) {
      this.simpegService.get(`tPegawai&nip=${this.token.sub}`).subscribe((result) => {
        if(result?.response?.success && result?.data?.nama) {
          const {
            nama,
            nip,
            gelar_depan,
            gelar_belakang,
            jenis_kelamin,
            status_kawin,
            jenis_pegawai,
            status_pegawai,
            kedudukan_pegawai,
            jabatan,
            pangkat,
            nip_lama,
            foto,
            tempat_lahir,
            tanggal_lahir,
            telepon,
            handphone_1,
            handphone_2,
            whatsapp,
            email,
            email_sidoarjokab,
            is_sesuai_ktp,
            ktp_kelurahan,
            ktp_kodepos,
            ktp_rw,
            ktp_rt,
            ktp_alamat,
            domisili_kelurahan,
            domisili_kodepos,
            domisili_rw,
            domisili_rt,
            domisili_alamat,
            nik,
            nomor_kk,
            npwp,
            nuptk,
            duk,
            nomor_karpeg,
            nomor_korpri,
          } = result.data
    
          const {unit_organisasi = { nama: '' }, subunit_organisasi = { nama: '' }, seksi = { nama: '' }} = jabatan?.struktur_organisasi
          this.simpegData = {role_ids: [3], unit_organisasi: unit_organisasi.id, sub_unit_organisasi: subunit_organisasi, name: nama}
          this.redirect()

          this.hasLoad = true
        } else {
          this.simpegData = {role_ids: [3], unit_organisasi: -1, sub_unit_organisasi: -1, name: 'unknown'}
          this.redirect()
        }
      })
    } else {
      // this.router.navigate(['auth'])
      this.oauthService.initImplicitFlow()
    }
  }

  redirect() {

    this.auth.authenticate('callback', {nip: this.token.sub, ...this.simpegData}).subscribe((result) => {
      const success = result.isSuccess()
      if (success) {
        this.router.navigate(['pages'])
      } else {
        const { error } = result.getResponse()
        if (error) window.location = error.redirect
      }
    })
  }

}
