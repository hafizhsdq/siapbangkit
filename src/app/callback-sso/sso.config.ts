import { AuthConfig } from 'angular-oauth2-oidc';

export const authCodeFlowConfig: AuthConfig = {
//  issuer: 'https://idsvr4.azurewebsites.net',
  issuer: 'https://sso.sidoarjokab.go.id',

  redirectUri: window.location.origin + '/auth/callback',

//  clientId: 'spa',
  clientId: 'appsiapbangkit',

  dummyClientSecret: 'MgAaUMRC0hWcurOc2j8AakkxGMnQmFgs',

  responseType: 'code',

//  scope: 'openid profile email offline_access api',
  scope: 'openid profile email email_alias phone_number uid name',
  
  showDebugInformation: true,
};