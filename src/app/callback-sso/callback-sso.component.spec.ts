import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallbackSSOComponentComponent } from './callback-sso.component';

describe('CallbackSSOComponentComponent', () => {
  let component: CallbackSSOComponentComponent;
  let fixture: ComponentFixture<CallbackSSOComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallbackSSOComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallbackSSOComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
