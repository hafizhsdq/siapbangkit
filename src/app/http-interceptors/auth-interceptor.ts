import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpXsrfTokenExtractor } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { getDeepFromObject, NB_AUTH_OPTIONS } from '@nebular/auth';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  headerName = 'X-XSRF-TOKEN';
  strategy: string = ''

  constructor(
    private tokenService: HttpXsrfTokenExtractor, 
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private auth: AuthService
  ) {
    this.strategy = this.auth.starategy
    //getConfigValue('forms.logout.strategy')    
  }

  handleError(error): Observable<HttpEvent<any>> {
    switch(error.status) {
      case 419: {// csrf token missmatch|token expired
        window.location.href = '/'
      }
      case 401: {// unauthorized
        window.location.href = '/'
      }
      default: return throwError(error)
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    
    // if (this.auth.starategy == 'sso') {  
      if (req.url.indexOf("sso.sidoarjokab.go.id") == -1) {
        req = req.clone({withCredentials: true})
      }

      // if (req.method === 'GET' || req.method === 'HEAD') {
      //   return next.handle(req).pipe(
      //     catchError(this.handleError)
      //   );
      // }

      const token = this.tokenService.getToken();
      // console.log(token);

      // Be careful not to overwrite an existing header of the same name.
      if (token !== null && !req.headers.has(this.headerName)) {
        req = req.clone({headers: req.headers.set(this.headerName, token)});
      }
      
      return next.handle(req).pipe(
        catchError(this.handleError)
      );
    // }
    // Get the auth token from the service.
    // return this.auth.getToken().pipe(
    //         switchMap(token => {
    //             const authReq = req.clone({
    //                 setHeaders: {
    //                   Authorization: `Bearer ${token}`
    //                 }
    //             });
          
    //             return next.handle(authReq)
    //         }),
    //         catchError(error => {              
    //           if (error.status !== 401) {
    //               return throwError(error)
    //           } else {
    //               return this.auth.logout(this.strategy).pipe(
    //                 switchMap((result) => {
    //                   window.location.href = "/"
    //                   return throwError(error)
    //                 })
    //               )
    //           }
    //         })
    //     )
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}